import React, { Component } from "react";
import { Icon, IconButton, withStyles } from "@material-ui/core";
import { useSelector } from "react-redux";
import { Link } from "react-router-dom";

const styles = theme => ({
  root: {
    backgroundColor: theme.palette.primary.main,
    color: theme.palette.primary.contrastText
  }
});

const EgretSearchBox = (props) => {

  const [search, setSearch] = React.useState(null);
  const [open, setOpen] = React.useState(false);

  const openPanel = () => {
    if (!open) {
      setSearch('');
      setSearchResults([]);
    }
    setOpen(!open);
  };

  const [searchResults, setSearchResults] = React.useState([
    { result: 'Kamal Hassan', type: 'Client', path: '/clients/view/:id' }
  ])

  const { list: itineraries } = useSelector(state => state.builder);
  const { list: clients } = useSelector(state => state.clients);
  const { list: attractions } = useSelector(state => state.attractions);
  const { list: excursions } = useSelector(state => state.excursions);
  const { list: drivers } = useSelector(state => state.drivers);
  const { list: guides } = useSelector(state => state.guides);
  const { countries, cities } = useSelector(state => state.places);


  const onSearch = ({ target: { value } }) => {

    const results = [];

    setSearch(value);

    if (value) {

      itineraries.forEach(document => {
        if (
          (document.name && document.name.toLowerCase().indexOf(value.toLowerCase()) !== -1) ||
          !!countries.filter(c => c.country_id === document.country_id).
            filter(c => c.name && c.name.toLowerCase().indexOf(value.toLowerCase()) !== -1).length ||
          (document.itinerary_id.toString().indexOf(value.toLowerCase()) !== -1)
        ) {
          results.push({
            result: `<span style="font-weight: bold">${document.name}<span>, ${document.pax} Pax [Itinerary ID: ${document.itinerary_id}]`,
            type: 'Itinerary', path: `/itinerary/builder/view/${document.itinerary_id}`
          })
        }
      })

      
      countries.forEach(country => {
        if (
          (country.name && country.name.toLowerCase().indexOf(value.toLowerCase()) !== -1)
        ) {
          results.push({
            result: `<span style="font-weight: bold">${country.name}`,
            type: 'Country', path: `/library/places/country/view/${country.country_id}`
          })
        }
      })


      cities.forEach(city => {
        if (
          (city.name && city.name.toLowerCase().indexOf(value.toLowerCase()) !== -1)
        ) {
          results.push({
            result: `<span style="font-weight: bold">${city.name}`,
            type: 'City', path: `/library/places/city/view/${city.city_id}`
          })
        }
      })

      clients.forEach(client => {
        if (
          (client.name && client.name.toLowerCase().indexOf(value.toLowerCase()) !== -1) ||
          (client.email && client.email.toLowerCase().indexOf(value.toLowerCase()) !== -1) ||
          (client.phone_number && client.phone_number.toLowerCase().indexOf(value.toLowerCase()) !== -1) ||
          (client.country && client.country.toLowerCase().indexOf(value.toLowerCase()) !== -1)
        ) {
          results.push({
            result: `<span style="font-weight: bold">${client.name}<span>, ${client.email} from ${client.country} [${client.phone_number}] `,
            type: 'Client', path: `/clients/view/${client.client_id}`
          })
        }
      })


      attractions.forEach(attraction => {
        if (
          (attraction.name && attraction.name.toLowerCase().indexOf(value.toLowerCase()) !== -1) ||
          ((countries.find(c => c.country_id === attraction.country_id) &&
            countries.find(c => c.country_id === attraction.country_id).name && countries.find(c => c.country_id === attraction.country_id).name.toLowerCase().indexOf(value.toLowerCase()) !== -1)) ||
          ((cities.find(c => c.city_id === attraction.city_id) && cities.find(c => c.city_id === attraction.city_id).name && cities.find(c => c.city_id === attraction.city_id).name.
            toLowerCase().indexOf(value.toLowerCase()) !== -1))
        ) {
          results.push({
            result: `<span style="font-weight: bold">${attraction.name}<span>, ${cities.find(c => c.city_id === attraction.city_id) ? cities.find(c => c.city_id === attraction.city_id).name : ''},
           ${countries.find(c => c.country_id === attraction.country_id) ? countries.find(c => c.country_id === attraction.country_id).name : ''}`,
            type: 'Attraction', path: `/library/attractions/view/${attraction.attraction_id}`
          })
        }
      })

      excursions.forEach(excursion => {
        if (
          (excursion.name && excursion.name.toLowerCase().indexOf(value.toLowerCase()) !== -1)
        ) {
          results.push({
            result: `<span style="font-weight: bold">${excursion.name}<span>, From ${excursion.from} To ${excursion.to}`,
            type: 'Excursion', path: `/library/excursions/view/${excursion.excursion_id}`
          })
        }
      })


      drivers.forEach(driver => {
        if (
          (driver.name && driver.name.toLowerCase().indexOf(value.toLowerCase()) !== -1)
        ) {
          results.push({
            result: `<span style="font-weight: bold">${driver.name}<span>, ${driver.email} [${driver.phone_number}]`,
            type: 'Driver', path: `/transport/drivers/view/${driver.driver_id}`
          })
        }
      })


      guides.forEach(guide => {
        if (
          (guide.name && guide.name.toLowerCase().indexOf(value.toLowerCase()) !== -1)
        ) {
          results.push({
            result: `<span style="font-weight: bold">${guide.name}<span>, ${guide.email} [${guide.phone_number}]`,
            type: 'Guide', path: `/transport/guides/view/${guide.guide_id}`
          })
        }
      })

    }

    setSearchResults([...results])
  }

  return (
    <React.Fragment>
      {!open && (
        <IconButton onClick={openPanel}>
          <Icon>search</Icon>
        </IconButton>
      )}

      {open && (
        <React.Fragment>
          <div
            className={`flex flex-middle h-100 egret-search-box ${props.classes.root}`}>
            <input
              className={`px-16 search-box w-100 ${props.classes.root}`}
              type="text"
              placeholder="Search for Itineraries,Libraries"
              autoFocus
              onChange={onSearch}
            />
            <IconButton onClick={openPanel} className="text-middle mx-4">
              <Icon>close</Icon>
            </IconButton>
          </div>
          {!!(search && searchResults.length) &&
            <div className={`h-100 egret-search-box ${props.classes.root} search-result`}>
              {
                searchResults.map((item, key) => (
                  <div key={key} className="search-item">
                    <Link to={item.path} className='result-link' onClick={openPanel}>
                      <div dangerouslySetInnerHTML={{ __html: item.result }}></div>
                      <div className='label'>{item.type}</div>
                    </Link>
                  </div>
                ))
              }

            </div>
          }
        </React.Fragment>
      )}
    </React.Fragment>
  );
}

export default withStyles(styles, { withTheme: true })(EgretSearchBox);
