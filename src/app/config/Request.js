import Axios from "axios";

export const Request = Axios.create({
    baseURL: `${process.env.REACT_APP_API}`,
    headers: {
        Accept: `application/json`,
        'Content-Type': `application/json`,
        'Access-Control-Allow-Origin' : '*',
        'Access-Control-Allow-Methods' : 'GET,PUT,POST,DELETE,PATCH,OPTIONS'
    }
});
