import React from 'react';
import { Loader } from 'semantic-ui-react';

const LoaderComponent = ({ content = null , styles = {}}) => {

    return (
        <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center', ...styles }}>
            <Loader size='tiny' inline active />
            <span style={{ margin: "auto 10px" }}>{content ? content : 'Please wait'}</span>
        </div>
    )
}

export default LoaderComponent;
