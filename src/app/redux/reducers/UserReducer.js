import {
  SET_USER_DATA,
  REMOVE_USER_DATA,
  USER_LOGGED_OUT
} from "../actions/UserActions";

import { UPDATE_PROFILE_USER } from "../actions/modules/Users.actions";

const initialState = {
  authenticated: false,
  user: null
};

const userReducer = function (state = initialState, action) {
  switch (action.type) {
    case SET_USER_DATA: {
      return {
        ...state,
        ...action.data
      };
    }
    case UPDATE_PROFILE_USER: {
      return {
        ...state, user: { ...state.user, data: action.payload }
      };
    }
    case REMOVE_USER_DATA: {
      return {
        ...initialState
      };
    }
    case USER_LOGGED_OUT: {
      return { ...initialState };
    }
    default: {
      return state;
    }
  }
};

export default userReducer;
