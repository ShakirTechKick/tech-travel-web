import * as AttractionsActions from "../../actions/modules/Attractions.action";

const data = {
    list: []
};


const attractionsReducer = function (state = data, action) {

    switch (action.type) {
        case AttractionsActions.ALL_ATTRACTIONS: {
            return {
                ...state,
                list: action.payload
            };
        }
        case AttractionsActions.ADD_ATTRACTION: {
            return {
                ...state,
                list: [...state.list, action.payload]
            };
        }
        case AttractionsActions.UPDATE_ATTRACTION: {
            return {
                ...state,
                list: state.list.map(o => {
                    if (o.attraction_id == action.payload.attraction_id) {
                        o = action.payload;
                    }

                    return o;
                })
            };
        }
        case AttractionsActions.REMOVE_ATTRACTION: {
            return {
                ...state,
                list: state.list.filter(o => o.attraction_id !== action.payload)
            };
        }
        case AttractionsActions.STATUS_ATTRACTION: {
            return {
                ...state,
                list: state.list.map(o => {
                    if (o.attraction_id == action.payload) {
                        o.status = o.status == 'A' ? 'B' : 'A';
                    }
                    return o;
                })
            };
        }
        default: {
            return state;
        }

    }
};

export default attractionsReducer;
