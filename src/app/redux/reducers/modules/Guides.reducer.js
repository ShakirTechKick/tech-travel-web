import { ALL_GUIDES, ADD_GUIDES, UPDATE_GUIDES, REMOVE_GUIDES, STATUS_GUIDES } from "../../actions/modules/Guides.action";

const initialState = {
    list: []
};

const guidesReducer = function (state = initialState, action) {
    switch (action.type) {
        case ALL_GUIDES: {
            return {
                ...state,
                list: action.payload
            };
        }
        case ADD_GUIDES: {
            return {
                ...state,
                list: [...state.list, action.payload]
            };
        }
        case UPDATE_GUIDES: {
            return {
                ...state,
                list: state.list.map(o => {
                    if (o.guide_id == action.payload.guide_id) {
                        o = action.payload;
                    }

                    return o;
                })
            };
        }
        case REMOVE_GUIDES: {
            return {
                ...state,
                list: state.list.filter(o => o.guide_id !== action.payload)
            };
        }
        case STATUS_GUIDES: {
            return {
                ...state,
                list: state.list.map(o => {
                    if (o.guide_id == action.payload) {
                        o.status = o.status == 'A' ? 'B' : 'A';
                    }
                    return o;
                })
            };
        }
        default: {
            return state;
        }
    }
};

export default guidesReducer;
