import { ALL_TRIPS, ADD_TRIP, UPDATE_TRIP, REMOVE_TRIP, STATUS_TRIP } from "../../actions/modules/Trips.action";

const initialState = {
    list: []
};

const tripsReducer = function (state = initialState, action) {
    switch (action.type) {
        case ALL_TRIPS: {
            return {
                ...state,
                list: action.payload
            };
        }
        case ADD_TRIP: {
            return {
                ...state,
                list: [...state.list, action.payload]
            };
        }
        case UPDATE_TRIP: {
            return {
                ...state,
                list: state.list.map(o => {
                    if (o.trip_id == action.payload.trip_id) {
                        o = action.payload;
                    }

                    return o;
                })
            };
        }
        case REMOVE_TRIP: {
            return {
                ...state,
                list: state.list.filter(o => o.trip_id !== action.payload)
            };
        }
        case STATUS_TRIP: {
            return {
                ...state,
                list: state.list.map(o => {
                    if (o.trip_id == action.payload.trip_id) {
                        o.status = action.payload.status
                    }
                    return o;
                })
            };
        }
        default: {
            return state;
        }
    }
};

export default tripsReducer;
