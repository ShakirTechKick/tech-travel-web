import { ALL_COMPANIES, UPDATE_COMPANY } from "../../actions/modules/Company.actions";

const initialState = {
    companies: [],
    company: {}
};

const companyReducer = function (state = initialState, action) {
    switch (action.type) {
        case ALL_COMPANIES: {
            return {
                ...state,
                companies: action.payload
            };
        }
        case UPDATE_COMPANY: {
            return {
                ...state,
                companies: state.companies.map(c => {
                    if (c.company_id == action.payload.company_id) {
                        c = action.payload;
                    }

                    return c;
                })
            };
        }
        default: {
            return state;
        }
    }
};

export default companyReducer;
