import { ALL_ITINERARIES, ADD_ITINERARY, AUTO_SAVE_ITINERARY, UPDATE_ITINERARY, REMOVE_ITINERARY, STATUS_ITINERARY } from "../../actions/modules/Builder.actions";

const initialState = {
    list: []
};

const builderReducer = function (state = initialState, action) {
    switch (action.type) {
        case ALL_ITINERARIES: {
            return {
                ...state,
                list: action.payload
            };
        }
        case ADD_ITINERARY: {
            return {
                ...state,
                list: [...state.list, action.payload]
            };
        }
        case AUTO_SAVE_ITINERARY: {

            if (state.list.find(i => i.itinerary_id === action.payload.itinerary_id)) {
                return {
                    ...state,
                    list: [...state.list.filter(i => i.itinerary_id !== action.payload.itinerary_id),
                    action.payload]
                };
            }

            return {
                ...state,
                list: [...state.list, action.payload]
            };

        }
        case UPDATE_ITINERARY: {
            return {
                ...state,
                list: state.list.map(b => {
                    if (b.itinerary_id == action.payload.itinerary_id) {
                        b = action.payload;
                    }

                    return b;
                })
            };
        }
        case REMOVE_ITINERARY: {

            if(typeof action.payload === 'string'){
                action.payload = (parseInt(action.payload));
            }

            return {
                ...state,
                list: state.list.filter(b => b.itinerary_id !== action.payload)
            };

        }
        case STATUS_ITINERARY: {
            return {
                ...state,
                list: state.list.map(b => {
                    if (b.itinerary_id == action.payload.itinerary_id) {
                        b.status = action.payload.status
                    }
                    return b;
                })
            };
        }
        default: {
            return state;
        }
    }
};

export default builderReducer;
