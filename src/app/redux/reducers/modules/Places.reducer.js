import * as PlacesActions from "../../actions/modules/Places.action";

const data = {
    countries: [],
    cities: []
};

const placesReducer = function (state = data, action) {

    switch (action.type) {
        case PlacesActions.ALL_COUNTRIES: {
            return {
                ...state,
                countries: action.payload
            };
        }
        case PlacesActions.ADD_COUNTRY: {
            return {
                ...state,
                countries: [...state.countries, action.payload]
            };
        }
        case PlacesActions.UPDATE_COUNTRY: {
            return {
                ...state,
                countries: state.countries.map(c => {
                    if (c.country_id == action.payload.country_id) {
                        c = action.payload;
                    }

                    return c;
                })
            };
        }
        case PlacesActions.REMOVE_COUNTRY: {
            return {
                ...state,
                countries: state.countries.filter(c => c.country_id !== action.payload)
            };
        }
        case PlacesActions.STATUS_COUNTRY: {
            return {
                ...state,
                countries: state.countries.map(c => {
                    if (c.country_id == action.payload) {
                        c.status = c.status == 'A' ? 'B' : 'A';
                    }
                    return c;
                })
            };
        }
        case PlacesActions.ALL_CITIES: {
            return {
                ...state,
                cities: action.payload
            };
        }
        case PlacesActions.ADD_CITY: {
            return {
                ...state,
                cities: [...state.cities, action.payload]
            };
        }
        case PlacesActions.UPDATE_CITY: {
            return {
                ...state,
                cities: state.cities.map(c => {
                    if (c.city_id == action.payload.city_id) {
                        c = action.payload;
                    }

                    return c;
                })
            };
        }
        case PlacesActions.REMOVE_CITY: {
            return {
                ...state,
                cities: state.cities.filter(c => c.city_id !== action.payload)
            };
        }
        case PlacesActions.STATUS_CITY: {
            return {
                ...state,
                cities: state.cities.map(c => {
                    if (c.city_id == action.payload) {
                        c.status = c.status == 'A' ? 'B' : 'A';
                    }
                    return c;
                })
            };
        }
        default: {
            return state;
        }
        
    }
};

export default placesReducer;
