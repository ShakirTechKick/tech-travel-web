import * as ActivitiesActions from "../../actions/modules/Activities.action";

const data = {
    list: []
};


const activitiesReducer = function (state = data, action) {

    switch (action.type) {
        case ActivitiesActions.ALL_ACTIVITIES: {
            return {
                ...state,
                list: action.payload
            };
        }
        case ActivitiesActions.ADD_ACTIVITY: {
            return {
                ...state,
                list: [...state.list, action.payload]
            };
        }
        case ActivitiesActions.UPDATE_ACTIVITY: {
            return {
                ...state,
                list: state.list.map(o => {
                    if (o.activity_id == action.payload.activity_id) {
                        o = action.payload;
                    }

                    return o;
                })
            };
        }
        case ActivitiesActions.REMOVE_ACTIVITY: {
            return {
                ...state,
                list: state.list.filter(o => o.activity_id !== action.payload)
            };
        }
        case ActivitiesActions.STATUS_ACTIVITY: {
            return {
                ...state,
                list: state.list.map(o => {
                    if (o.activity_id == action.payload) {
                        o.status = o.status == 'A' ? 'B' : 'A';
                    }
                    return o;
                })
            };
        }
        default: {
            return state;
        }

    }
};

export default activitiesReducer;
