import { ALL_ROLES, ADD_ROLES, UPDATE_ROLES, REMOVE_ROLES, STATUS_ROLES, AVAILABLE_MODULES, PERMITTED_MODULES } from "../../actions/modules/Roles.actions";

const initialState = {
    list: [],
    availableModules: [],
    permittedModules: []
};

const rolesReducer = function (state = initialState, action) {
    switch (action.type) {
        case ALL_ROLES: {
            return {
                ...state,
                list: action.payload
            };
        }
        case ADD_ROLES: {
            return {
                ...state,
                list: [...state.list, action.payload]
            };
        }
        case UPDATE_ROLES: {
            return {
                ...state,
                list: state.list.map(r => {
                    if (r.role_id == action.payload.role_id) {
                        r = action.payload;
                    }

                    return r;
                })
            };
        }
        case REMOVE_ROLES: {
            return {
                ...state,
                list: state.list.filter(r => r.role_id !== action.payload)
            };
        }
        case STATUS_ROLES: {
            return {
                ...state,
                list: state.list.map(r => {
                    if (r.role_id == action.payload) {
                        r.status = r.status == 'A' ? 'B' : 'A';
                    }
                    return r;
                })
            };
        }
        case AVAILABLE_MODULES: {
            return {
                ...state,
                availableModules: action.payload
            };
        }
        case PERMITTED_MODULES: {
            return {
                ...state,
                permittedModules: action.payload
            };
        }
        default: {
            return state;
        }
    }
};

export default rolesReducer;
