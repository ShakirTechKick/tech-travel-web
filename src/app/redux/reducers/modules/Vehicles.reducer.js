import { ALL_VEHICLES, ADD_VEHICLES, UPDATE_VEHICLES, REMOVE_VEHICLES, STATUS_VEHICLES } from "../../actions/modules/Vehicles.action";

const initialState = {
    list: []
};

const vehiclesReducer = function (state = initialState, action) {
    switch (action.type) {
        case ALL_VEHICLES: {
            return {
                ...state,
                list: action.payload
            };
        }
        case ADD_VEHICLES: {
            return {
                ...state,
                list: [...state.list, action.payload]
            };
        }
        case UPDATE_VEHICLES: {
            return {
                ...state,
                list: state.list.map(o => {
                    if (o.vehicle_id == action.payload.vehicle_id) {
                        o = action.payload;
                    }

                    return o;
                })
            };
        }
        case REMOVE_VEHICLES: {
            return {
                ...state,
                list: state.list.filter(o => o.vehicle_id !== action.payload)
            };
        }
        case STATUS_VEHICLES: {
            return {
                ...state,
                list: state.list.map(o => {
                    if (o.vehicle_id == action.payload) {
                        o.status = o.status == 'A' ? 'B' : 'A';
                    }
                    return o;
                })
            };
        }
        default: {
            return state;
        }
    }
};

export default vehiclesReducer;
