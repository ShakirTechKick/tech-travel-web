import * as HotelsActions from "../../actions/modules/Hotels.action";

const data = {
    list: [],
    standards: [],
    categories: [],
    rooms: [],
    meals: []
};


const hotelsReducer = function (state = data, action) {

    switch (action.type) {
        case HotelsActions.ALL_HOTELS: {
            return {
                ...state,
                list: action.payload
            };
        }
        case HotelsActions.ADD_HOTEL: {
            return {
                ...state,
                list: [...state.list, action.payload]
            };
        }
        case HotelsActions.UPDATE_HOTEL: {
            return {
                ...state,
                list: state.list.map(o => {
                    if (o.hotel_id == action.payload.hotel_id) {
                        o = action.payload;
                    }

                    return o;
                })
            };
        }
        case HotelsActions.REMOVE_HOTEL: {
            return {
                ...state,
                list: state.list.filter(h => h.hotel_id !== action.payload)
            };
        }
        case HotelsActions.STATUS_HOTEL: {
            return {
                ...state,
                list: state.list.map(o => {
                    if (o.hotel_id == action.payload) {
                        o.status = o.status == 'A' ? 'B' : 'A';
                    }
                    return o;
                })
            };
        }

        case HotelsActions.ALL_HOTEL_STANDARDS: {
            return {
                ...state,
                standards: action.payload
            };
        }
        case HotelsActions.ADD_HOTEL_STANDARD: {
            return {
                ...state,
                standards: [...state.standards, action.payload]
            };
        }
        case HotelsActions.UPDATE_HOTEL_STANDARD: {
            return {
                ...state,
                standards: state.standards.map(o => {
                    if (o.standard_id == action.payload.standard_id) {
                        o = action.payload;
                    }

                    return o;
                })
            };
        }
        case HotelsActions.REMOVE_HOTEL_STANDARD: {
            return {
                ...state,
                standards: state.standards.filter(o => o.standard_id !== action.payload)
            };
        }
        case HotelsActions.STATUS_HOTEL_STANDARD: {
            return {
                ...state,
                standards: state.standards.map(o => {
                    if (o.standard_id == action.payload) {
                        o.status = o.status == 'A' ? 'B' : 'A';
                    }
                    return o;
                })
            };
        }

        case HotelsActions.ALL_HOTEL_CATEGORIES: {
            return {
                ...state,
                categories: action.payload
            };
        }
        case HotelsActions.ADD_HOTEL_CATEGORY: {
            return {
                ...state,
                categories: [...state.categories, action.payload]
            };
        }
        case HotelsActions.UPDATE_HOTEL_CATEGORY: {
            return {
                ...state,
                categories: state.categories.map(o => {
                    if (o.category_id == action.payload.category_id) {
                        o = action.payload;
                    }

                    return o;
                })
            };
        }
        case HotelsActions.REMOVE_HOTEL_CATEGORY: {
            return {
                ...state,
                categories: state.categories.filter(o => o.category_id !== action.payload)
            };
        }
        case HotelsActions.STATUS_HOTEL_CATEGORY: {
            return {
                ...state,
                categories: state.categories.map(o => {
                    if (o.category_id == action.payload) {
                        o.status = o.status == 'A' ? 'B' : 'A';
                    }
                    return o;
                })
            };
        }



        case HotelsActions.ALL_HOTEL_TYPES_ROOM: {
           
            return {
                ...state,
                rooms: action.payload
            };
        }
        case HotelsActions.ADD_HOTEL_TYPE_ROOM: {
            return {
                ...state,
                rooms: [...state.rooms, action.payload]
            };
        }
        case HotelsActions.UPDATE_HOTEL_TYPE_ROOM: {
            return {
                ...state,
                rooms: state.rooms.map(o => {
                    if (o.room_id == action.payload.room_id) {
                        o = action.payload;
                    }

                    return o;
                })
            };
        }
        case HotelsActions.REMOVE_HOTEL_TYPE_ROOM: {
            return {
                ...state,
                rooms: state.rooms.filter(o => o.room_id !== action.payload)
            };
        }
        case HotelsActions.STATUS_HOTEL_TYPE_ROOM: {
            return {
                ...state,
                rooms: state.rooms.map(o => {
                    if (o.room_id == action.payload) {
                        o.status = o.status == 'A' ? 'B' : 'A';
                    }
                    return o;
                })
            };
        }


        case HotelsActions.ALL_HOTEL_TYPES_MEAL: {
        
            return {
                ...state,
                meals: action.payload
            };
        }
        case HotelsActions.ADD_HOTEL_TYPE_MEAL: {
            return {
                ...state,
                meals: [...state.meals, action.payload]
            };
        }
        case HotelsActions.UPDATE_HOTEL_TYPE_MEAL: {
            return {
                ...state,
                meals: state.meals.map(o => {
                    if (o.meal_id == action.payload.meal_id) {
                        o = action.payload;
                    }

                    return o;
                })
            };
        }
        case HotelsActions.REMOVE_HOTEL_TYPE_MEAL: {
            return {
                ...state,
                meals: state.meals.filter(o => o.meal_id !== action.payload)
            };
        }
        case HotelsActions.STATUS_HOTEL_TYPE_MEAL: {
            return {
                ...state,
                meals: state.meals.map(o => {
                    if (o.meal_id == action.payload) {
                        o.status = o.status == 'A' ? 'B' : 'A';
                    }
                    return o;
                })
            };
        }
        default: {
            return state;
        }

    }
};

export default hotelsReducer;
