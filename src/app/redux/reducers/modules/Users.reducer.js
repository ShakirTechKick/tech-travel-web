import { ALL_USERS, ADD_USER, UPDATE_USER, REMOVE_USER, STATUS_USER } from "../../actions/modules/Users.actions";

const initialState = {
    list: []
};

const usersReducer = function (state = initialState, action) {
    switch (action.type) {
        case ALL_USERS: {
            return {
                ...state,
                list: action.payload
            };
        }
        case ADD_USER: {
            return {
                ...state,
                list: [...state.list, action.payload]
            };
        }
        case UPDATE_USER: {
            return {
                ...state,
                list: state.list.map(u => {
                    if (u.user_id == action.payload.user_id) {
                        u = action.payload;
                    }

                    return u;
                })
            };
        }
        case REMOVE_USER: {
            return {
                ...state,
                list: state.list.filter(u => u.user_id !== action.payload)
            };
        }
        case STATUS_USER: {
            return {
                ...state,
                list: state.list.map(u => {
                    if (u.user_id == action.payload) {
                        u.status = u.status == 'A' ? 'B' : 'A';
                    }
                    return u;
                })
            };
        }
        default: {
            return state;
        }
    }
};

export default usersReducer;
