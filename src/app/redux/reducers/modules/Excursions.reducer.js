import * as ExcursionsActions from "../../actions/modules/Excursions.action";

const data = {
    list: []
};


const excursionsReducer = function (state = data, action) {

    switch (action.type) {
        case ExcursionsActions.ALL_EXCURSIONS: {
            return {
                ...state,
                list: action.payload
            };
        }
        case ExcursionsActions.ADD_EXCURSION: {
            return {
                ...state,
                list: [...state.list, action.payload]
            };
        }
        case ExcursionsActions.UPDATE_EXCURSION: {
            return {
                ...state,
                list: state.list.map(o => {
                    if (o.excursion_id == action.payload.excursion_id) {
                        o = action.payload;
                    }

                    return o;
                })
            };
        }
        case ExcursionsActions.REMOVE_EXCURSION: {
            return {
                ...state,
                list: state.list.filter(o => o.excursion_id !== action.payload)
            };
        }
        case ExcursionsActions.STATUS_EXCURSION: {
            return {
                ...state,
                list: state.list.map(o => {
                    if (o.excursion_id == action.payload) {
                        o.status = o.status == 'A' ? 'B' : 'A';
                    }
                    return o;
                })
            };
        }
        default: {
            return state;
        }

    }
};

export default excursionsReducer;
