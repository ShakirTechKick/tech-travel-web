import { ALL_DRIVERS, ADD_DRIVER, UPDATE_DRIVER, REMOVE_DRIVER, STATUS_DRIVER } from "../../actions/modules/Drivers.action";

const initialState = {
    list: []
};

const driversReducer = function (state = initialState, action) {
    switch (action.type) {
        case ALL_DRIVERS: {
            return {
                ...state,
                list: action.payload
            };
        }
        case ADD_DRIVER: {
            return {
                ...state,
                list: [...state.list, action.payload]
            };
        }
        case UPDATE_DRIVER: {
            return {
                ...state,
                list: state.list.map(o => {
                    if (o.driver_id == action.payload.driver_id) {
                        o = action.payload;
                    }

                    return o;
                })
            };
        }
        case REMOVE_DRIVER: {
            return {
                ...state,
                list: state.list.filter(o => o.driver_id !== action.payload)
            };
        }
        case STATUS_DRIVER: {
            return {
                ...state,
                list: state.list.map(o => {
                    if (o.driver_id == action.payload) {
                        o.status = o.status == 'A' ? 'B' : 'A';
                    }
                    return o;
                })
            };
        }
        default: {
            return state;
        }
    }
};

export default driversReducer;
