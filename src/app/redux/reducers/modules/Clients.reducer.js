import { ALL_CLIENTS, ADD_CLIENT, UPDATE_CLIENT, REMOVE_CLIENT, STATUS_CLIENT } from "../../actions/modules/Clients.action";

const initialState = {
    list: []
};

const clientsReducer = function (state = initialState, action) {
    switch (action.type) {
        case ALL_CLIENTS: {
            return {
                ...state,
                list: action.payload
            };
        }
        case ADD_CLIENT: {
            return {
                ...state,
                list: [...state.list, action.payload]
            };
        }
        case UPDATE_CLIENT: {
            return {
                ...state,
                list: state.list.map(o => {
                    if (o.client_id == action.payload.client_id) {
                        o = action.payload;
                    }

                    return o;
                })
            };
        }
        case REMOVE_CLIENT: {
            return {
                ...state,
                list: state.list.filter(o => o.client_id !== action.payload)
            };
        }
        case STATUS_CLIENT: {
            return {
                ...state,
                list: state.list.map(o => {
                    if (o.client_id == action.payload) {
                        o.status = o.status == 'A' ? 'B' : 'A';
                    }
                    return o;
                })
            };
        }
        default: {
            return state;
        }
    }
};

export default clientsReducer;
