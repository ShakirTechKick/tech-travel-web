import { ALL_VEHICLECATEGORIES, ADD_VEHICLECATEGORY, UPDATE_VEHICLECATEGORY, REMOVE_VEHICLECATEGORY, STATUS_VEHICLECATEGORY } from "../../actions/modules/VehicleCategories.actions";

const initialState = {
    list: []
};

const categoriesReducer = function (state = initialState, action) {
    switch (action.type) {
        case ALL_VEHICLECATEGORIES: {
            return {
                ...state,
                list: action.payload
            };
        }
        case ADD_VEHICLECATEGORY: {
            return {
                ...state,
                list: [...state.list, action.payload]
            };
        }
        case UPDATE_VEHICLECATEGORY: {
            return {
                ...state,
                list: state.list.map(o => {
                    if (o.v_category_id == action.payload.v_category_id) {
                        o = action.payload;
                    }

                    return o;
                })
            };
        }
        case REMOVE_VEHICLECATEGORY: {
            return {
                ...state,
                list: state.list.filter(o => o.v_category_id !== action.payload)
            };
        }
        case STATUS_VEHICLECATEGORY: {
            return {
                ...state,
                list: state.list.map(o => {
                    if (o.v_category_id == action.payload) {
                        o.status = o.status == 'A' ? 'B' : 'A';
                    }
                    return o;
                })
            };
        }
        default: {
            return state;
        }
    }
};

export default categoriesReducer;
