import * as SettingsActions from "../../actions/modules/Settings.action";

const data = {
    email_settings: []
};


const settingsReducer = function (state = data, action) {

    switch (action.type) {
        case SettingsActions.EMAIL_SETTINGS: {
            return {
                ...state,
                email_settings: action.payload
            };
        }
        case SettingsActions.UPDATE_EMAIL_SETTINGS: {
            return {
                ...state,
                email_settings: state.email_settings.map(o => {
                    if (o.email_setting_id == action.payload.email_setting_id) {
                        o = action.payload;
                    }

                    return o;
                })
            };
        }       
        default: {
            return state;
        }

    }
};

export default settingsReducer;
