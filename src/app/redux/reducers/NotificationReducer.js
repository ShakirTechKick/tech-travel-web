import {
  GET_NOTIFICATION,
  CREATE_NOTIFICATION,
  DELETE_ALL_NOTIFICATION,
  DIRTY_NOTIFICATION,
  DELETE_NOTIFICATION
} from "../actions/NotificationActions";

const notifications = [];

const NotificationReducer = function (state = notifications, action) {
  switch (action.type) {
    case GET_NOTIFICATION: {
      return [...action.payload];
    }
    case CREATE_NOTIFICATION: {
      return [...state, action.payload];
    }
    case DIRTY_NOTIFICATION: {
      return [...state.map(n => {
        if (n.notification_id === action.payload) {
          n.dirty = 'YES';
        }

        return n;

      })];
    }
    case DELETE_NOTIFICATION: {
      return [...state.filter(n => n.notification_id !== action.payload)];
    }
    case DELETE_ALL_NOTIFICATION: {
      return [];
    }
    default: {
      return [...state];
    }
  }
};

export default NotificationReducer;
