import { combineReducers } from "redux";
import UserReducer from "./UserReducer";
import LayoutReducer from "./LayoutReducer";
import NotificationReducer from "./NotificationReducer";

import BuilderReducer from "./modules/Builder.reducer";
import RolesReducer from "./modules/Roles.reducer";
import UsersReducer from "./modules/Users.reducer";
import CompanyReducer from "./modules/Company.reducer";
import PlacesReducer from "./modules/Places.reducer";
import HotelsReducer from "./modules/Hotels.reducer";
import AttractionsReducer from "./modules/Attractions.reducer";
import ExcursionsReducer from "./modules/Excursions.reducer";
import ActivitiesReducer from "./modules/Activities.reducer";

import ClientsReducer from "./modules/Clients.reducer";
import DriversReducer from "./modules/Drivers.reducer";
import GuidesReducer from "./modules/Guides.reducer";
import VehicleCategoriesReducer from "./modules/VehicleCategories.reducer";
import VehiclesReducer from "./modules/Vehicles.reducer";
import TripsReducer from "./modules/Trips.reducer";


import SettingsReducer from "./modules/Settings.reducer";

const RootReducer = combineReducers({
  user: UserReducer,
  layout: LayoutReducer,
  notification: NotificationReducer,
  roles: RolesReducer,
  users: UsersReducer,
  places: PlacesReducer,
  company: CompanyReducer,
  hotels: HotelsReducer,
  attractions: AttractionsReducer,
  excursions: ExcursionsReducer,
  activities: ActivitiesReducer,
  builder: BuilderReducer,
  clients: ClientsReducer,
  drivers: DriversReducer,
  guides: GuidesReducer,
  vehicle_categories: VehicleCategoriesReducer,
  vehicles: VehiclesReducer,
  trips: TripsReducer,
  settings: SettingsReducer
});

export default RootReducer;
