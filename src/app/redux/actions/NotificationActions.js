import { Request } from "app/config/Request";

export const GET_NOTIFICATION = "GET_NOTIFICATION";
export const CREATE_NOTIFICATION = "CREATE_NOTIFICATION";
export const DIRTY_NOTIFICATION = "DIRTY_NOTIFICATION";
export const DELETE_NOTIFICATION = "DELETE_NOTIFICATION";
export const DELETE_ALL_NOTIFICATION = "DELETE_ALL_NOTIFICATION";

export const getNotification = (payload) => dispatch => {
  Request.post(`utility/notification/all`, payload).then(res => {
    dispatch({
      type: GET_NOTIFICATION,
      payload: res.data.notifications
    });
  });
};

export const dirtyNotification = payload => dispatch => {
  Request.post(`utility/notification/dirty`, payload).then(res => {
    dispatch({
      type: DIRTY_NOTIFICATION,
      payload: payload.notification_id
    });
  });
};

export const deleteNotification = payload => dispatch => {
  Request.post(`utility/notification/destroy`, payload).then(res => {
    dispatch({
      type: DELETE_NOTIFICATION,
      payload: payload.notification_id
    });
  });
};

export const deleteAllNotification = () => dispatch => {
  Request.post(`utility/notification/deleteAll`).then(res => {
    dispatch({
      type: DELETE_ALL_NOTIFICATION,
      payload: res.data
    });
  });
};

export const createNotification = notification => dispatch => {
  Request.post(`utility/notification/notify`, { notification }).then(res => {
    // dispatch({
    //   type: CREATE_NOTIFICATION,
    //   payload: res.data.notification
    // });
  });
};
