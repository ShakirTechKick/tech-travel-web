import { Request } from "app/config/Request";
import { ToastsStore } from "react-toasts";

export const ALL_ATTRACTIONS = "ALL_ATTRACTIONS";
export const ADD_ATTRACTION = "ADD_ATTRACTION";
export const UPDATE_ATTRACTION = "UPDATE_ATTRACTION";
export const REMOVE_ATTRACTION = "REMOVE_ATTRACTION";
export const STATUS_ATTRACTION = "STATUS_ATTRACTION";


export function fetchAttractions(company_id) {
  return dispatch => {

    Request.get(`places/attractions/all/${company_id}`)
      .then(result => {

        if (result.data.status == 'success') {
          dispatch({
            type: ALL_ATTRACTIONS,
            payload: result.data.attractions
          });
        }
      })
      .catch(errors => {
        ToastsStore.warning("Error fetching countries from server", 2000)
      })
  };
}

export function addAttraction(payload) {
  return dispatch => {

    Request.post('places/attractions/add', payload)
      .then(result => {
        if (result.data.status == 'success') {
          ToastsStore.success("Attraction is added successfully", 2000)
          dispatch({
            type: ADD_ATTRACTION,
            payload: result.data.attraction
          });

        } else {
          ToastsStore.warning("Error: couln't add attraction", 2000)
        }
      })
      .catch(errors => {
        ToastsStore.warning("Error: server problem occured", 2000)
      })
  };
}

export function updateAttraction(payload) {
  return dispatch => {

    Request.post(`places/attractions/edit/${payload.attraction_id}`, payload)
      .then(result => {

        if (result.data.status == 'success') {

          ToastsStore.success("Attraction updated successfully", 2000)
          dispatch({
            type: UPDATE_ATTRACTION,
            payload: result.data.attraction
          });

        } else {
          ToastsStore.warning("Error: couln't update attraction", 2000)
        }
      })
      .catch(errors => {
        ToastsStore.warning("Error: server problem occured", 2000)
      })
  };
}


export function removeAttraction(payload) {
  return dispatch => {

    Request.post(`places/attractions/destroy`, payload)
      .then(result => {

        if (result.data.status == 'success') {
          ToastsStore.success("Successfully attraction deleted", 2000)
          dispatch({
            type: REMOVE_ATTRACTION,
            payload: payload.attraction_id
          });
        } else {
          ToastsStore.warning("Error: couln't delete attraction", 2000)
        }
      })
      .catch(errors => {
        ToastsStore.warning("Error: server problem occured", 2000)
      })
  };
}

export function statusAttraction(payload) {
  return dispatch => {

    Request.post(`places/attractions/status`, payload)
      .then(result => {

        if (result.data.status == 'success') {
          ToastsStore.success("Status changed successfully", 2000)
          dispatch({
            type: STATUS_ATTRACTION,
            payload: payload.attraction_id
          });
        } else {
          ToastsStore.warning("Error: couln't update attraction", 2000)
        }
      })
      .catch(errors => {
        ToastsStore.warning("Error: server problem occured", 2000)
      })
  };
}


