import { Request } from "app/config/Request";
import { ToastsStore } from "react-toasts";

export const EMAIL_SETTINGS = "EMAIL_SETTINGS";
export const UPDATE_EMAIL_SETTINGS = "UPDATE_EMAIL_SETTINGS";


export function fetchEmailSettings(company_id) {
    return dispatch => {

        Request.get(`settings/email/all/${company_id}`)
            .then(result => {
                if (result.data.status == 'success') {
                    dispatch({
                        type: EMAIL_SETTINGS,
                        payload: result.data.settings
                    });
                }
            })
            .catch(errors => {
              ToastsStore.warning("Error: server problem occured", 2000)
            })
    };
}

export function updateEmailSettings(payload) {
    return dispatch => {

        Request.post('settings/email/edit', payload)
            .then(result => {

                console.log(result.data)

                if (result.data.status == 'success') {

                    ToastsStore.success("Email Settings updated successfully", 2000)
                    dispatch({
                        type: UPDATE_EMAIL_SETTINGS,
                        payload: result.data.setting
                    });

                } else {
                    ToastsStore.warning("Error: couln't update setting", 2000)
                }
            })
            .catch(errors => {
              ToastsStore.warning("Error: server problem occured", 2000)
            })
    };
}

