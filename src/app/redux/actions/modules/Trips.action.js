import { Request } from "app/config/Request";
import { ToastsStore } from "react-toasts";

export const ALL_TRIPS = "ALL_TRIPS";
export const ADD_TRIP = "ADD_TRIP";
export const UPDATE_TRIP = "UPDATE_TRIP";
export const REMOVE_TRIP = "REMOVE_TRIP";
export const STATUS_TRIP = "STATUS_TRIP";

export function fetchTrips(company_id) {
  return dispatch => {

    Request.get(`trips/all/${company_id}`)
      .then(result => {

        if (result.data.status == 'success') {
          dispatch({
            type: ALL_TRIPS,
            payload: result.data.trips
          });
        }
      })
      .catch(errors => {
        ToastsStore.warning("Error: server problem occured", 2000)
      })
  };
}

export function addTrip(payload, callback) {
  return dispatch => {

    Request.post('trips/add', payload)
      .then(result => {

        if (result.data.status == 'success') {
          ToastsStore.success("Trip is added successfully", 2000)
          dispatch({
            type: ADD_TRIP,
            payload: result.data.trip
          });

          callback();
        } else {
          ToastsStore.warning("Error: couln't add trip", 2000)
        }
      })
      .catch(errors => {
        ToastsStore.warning("Error: server problem occured", 2000)
      })
  };
}

export function updateTrip(payload, callback) {
  return dispatch => {

    Request.post(`trips/edit/${payload.trip_id}`, payload)
      .then(result => {

        if (result.data.status == 'success') {

          ToastsStore.success("Trip updated successfully", 2000)
          dispatch({
            type: UPDATE_TRIP,
            payload: result.data.trip
          });

          callback();
        } else {
          ToastsStore.warning("Error: couln't update trip", 2000)
        }
      })
      .catch(errors => {
        ToastsStore.warning("Error: server problem occured", 2000)
      })
  };
}


export function removeTrip(payload) {
  return dispatch => {

    Request.post(`trips/destroy`, payload)
      .then(result => {

        if (result.data.status == 'success') {
          ToastsStore.success("Successfully trip deleted", 2000)
          dispatch({
            type: REMOVE_TRIP,
            payload: payload.trip_id
          });
        } else {
          ToastsStore.warning("Error: couln't delete trip", 2000)
        }
      })
      .catch(errors => {
        ToastsStore.warning("Error: server problem occured", 2000)
      })
  };
}

export function statusTrip(payload, callback) {
  return dispatch => {

    Request.post(`trips/status`, payload)
      .then(result => {

        if (result.data.status == 'success') {
          ToastsStore.success("Status changed successfully", 2000)
          dispatch({
            type: STATUS_TRIP,
            payload: payload
          });

          callback();
          
        } else {
          ToastsStore.warning("Error: couln't update trip", 2000)
        }
      })
      .catch(errors => {
        ToastsStore.warning("Error: server problem occured", 2000)
      })
  };
}
