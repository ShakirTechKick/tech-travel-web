import { Request } from "app/config/Request";
import { ToastsStore } from "react-toasts";

export const ALL_VEHICLECATEGORIES = "ALL_VEHICLECATEGORIES";
export const ADD_VEHICLECATEGORY = "ADD_VEHICLECATEGORY";
export const UPDATE_VEHICLECATEGORY = "UPDATE_VEHICLECATEGORY";
export const REMOVE_VEHICLECATEGORY = "REMOVE_VEHICLECATEGORY";
export const STATUS_VEHICLECATEGORY = "STATUS_VEHICLECATEGORY";

export function fetchVehicleCategories(company_id) {
  return dispatch => {

    Request.get(`vehicle-categories/all/${company_id}`)
      .then(result => {

        if (result.data.status == 'success') {
          dispatch({
            type: ALL_VEHICLECATEGORIES,
            payload: result.data.categories
          });
        }
      })
      .catch(errors => {
        ToastsStore.warning("Error: server problem occured", 2000)
      })
  };
}

export function addVehicleCategory(payload, callback) {
  return dispatch => {

    Request.post('vehicle-categories/add', payload)
      .then(result => {

        if (result.data.status == 'success') {
          ToastsStore.success("Vehicle Category is added successfully", 2000)
          dispatch({
            type: ADD_VEHICLECATEGORY,
            payload: result.data.category
          });

          callback();
        } else {
          ToastsStore.warning("Error: couln't add category", 2000)
        }
      })
      .catch(errors => {
        ToastsStore.warning("Error: server problem occured", 2000)
      })
  };
}

export function updateVehicleCategory(payload, callback) {
  return dispatch => {

    Request.post(`vehicle-categories/edit/${payload.v_category_id}`, payload)
      .then(result => {

        if (result.data.status == 'success') {

          ToastsStore.success("Vehicle Category updated successfully", 2000)
          dispatch({
            type: UPDATE_VEHICLECATEGORY,
            payload: result.data.category
          });

          callback();
        } else {
          ToastsStore.warning("Error: couln't update category", 2000)
        }
      })
      .catch(errors => {
        ToastsStore.warning("Error: server problem occured", 2000)
      })
  };
}


export function removeVehicleCategory(payload) {
  return dispatch => {

    Request.post(`vehicle-categories/destroy`, payload)
      .then(result => {

        if (result.data.status == 'success') {
          ToastsStore.success("Successfully vehicle category deleted", 2000)
          dispatch({
            type: REMOVE_VEHICLECATEGORY,
            payload: payload.v_category_id
          });
        } else {
          ToastsStore.warning("Error: couln't delete vehicle category", 2000)
        }
      })
      .catch(errors => {
        ToastsStore.warning("Error: server problem occured", 2000)
      })
  };
}

export function statusVehicleCategory(payload) {
  return dispatch => {

    Request.post(`vehicle-categories/status`, payload)
      .then(result => {

        if (result.data.status == 'success') {
          ToastsStore.success("Status changed successfully", 2000)
          dispatch({
            type: STATUS_VEHICLECATEGORY,
            payload: payload.v_category_id
          });
        } else {
          ToastsStore.warning("Error: couln't update vehicle category", 2000)
        }
      })
      .catch(errors => {
        ToastsStore.warning("Error: server problem occured", 2000)
      })
  };
}
