import { Request } from "app/config/Request";
import { ToastsStore } from "react-toasts";

export const ALL_VEHICLES = "ALL_VEHICLES";
export const ADD_VEHICLES = "ADD_VEHICLES";
export const UPDATE_VEHICLES = "UPDATE_VEHICLES";
export const REMOVE_VEHICLES = "REMOVE_VEHICLES";
export const STATUS_VEHICLES = "STATUS_VEHICLES";

export function fetchVehicles(company_id) {
  return dispatch => {

    Request.get(`vehicles/all/${company_id}`)
      .then(result => {

        if (result.data.status == 'success') {
          dispatch({
            type: ALL_VEHICLES,
            payload: result.data.vehicles
          });
        }
      })
      .catch(errors => {
        ToastsStore.warning("Error: server problem occured", 2000)
      })
  };
}

export function addVehicles(payload, callback) {
  return dispatch => {

    Request.post('vehicles/add', payload)
      .then(result => {

        if (result.data.status == 'success') {
          ToastsStore.success("Vehicles is added successfully", 2000)
          dispatch({
            type: ADD_VEHICLES,
            payload: result.data.vehicle
          });

          callback();
        } else {
          ToastsStore.warning("Error: couln't add vehicles", 2000)
        }
      })
      .catch(errors => {
        ToastsStore.warning("Error: server problem occured", 2000)
      })
  };
}

export function updateVehicles(payload, callback) {
  return dispatch => {

    Request.post(`vehicles/edit/${payload.vehicle_id}`, payload)
      .then(result => {

        if (result.data.status == 'success') {

          ToastsStore.success("Vehicles updated successfully", 2000)
          dispatch({
            type: UPDATE_VEHICLES,
            payload: result.data.vehicle
          });

          callback();
        } else {
          ToastsStore.warning("Error: couln't update vehicles", 2000)
        }
      })
      .catch(errors => {
        ToastsStore.warning("Error: server problem occured", 2000)
      })
  };
}


export function removeVehicles(payload) {
  return dispatch => {

    Request.post(`vehicles/destroy`, payload)
      .then(result => {

        if (result.data.status == 'success') {
          ToastsStore.success("Successfully vehicle deleted", 2000)
          dispatch({
            type: REMOVE_VEHICLES,
            payload: payload.vehicle_id
          });
        } else {
          ToastsStore.warning("Error: couln't delete vehicles", 2000)
        }
      })
      .catch(errors => {
        ToastsStore.warning("Error: server problem occured", 2000)
      })
  };
}

export function statusVehicles(payload) {
  return dispatch => {

    Request.post(`vehicles/status`, payload)
      .then(result => {

        if (result.data.status == 'success') {
          ToastsStore.success("Status changed successfully", 2000)
          dispatch({
            type: STATUS_VEHICLES,
            payload: payload.vehicle_id
          });
        } else {
          ToastsStore.warning("Error: couln't update vehicles", 2000)
        }
      })
      .catch(errors => {
        ToastsStore.warning("Error: server problem occured", 2000)
      })
  };
}
