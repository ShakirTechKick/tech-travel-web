import { Request } from "app/config/Request";
import { ToastsStore } from "react-toasts";

export const ALL_COMPANIES = "ALL_COMPANIES";
export const UPDATE_COMPANY = "UPDATE_COMPANY";

export function fetchCompanies(callback = null) {


  return dispatch => {

    Request.get('companies/all')
      .then(result => {

        if (result.data.status == 'success') {
          if (typeof callback == 'function') {
            callback(result.data.companies)
          }
          dispatch({
            type: ALL_COMPANIES,
            payload: result.data.companies
          });
        }
      })
      .catch(errors => {
        ToastsStore.warning("Error fetching roles from server", 2000)
      })
  };
}


export function updateCompany(payload, callback = null) {
  return dispatch => {

    Request.post(`companies/edit`, payload)
      .then(result => {

        if (result.data.status == 'success') {

          ToastsStore.success("Company updated successfully", 2000)
          dispatch({
            type: UPDATE_COMPANY,
            payload: result.data.company
          });

          if(callback) callback();
        } else {
          ToastsStore.warning("Error: couln't update user", 2000)
        }
      })
      .catch(errors => {
        ToastsStore.warning("Error: server problem occured", 2000)
      })
  };
}