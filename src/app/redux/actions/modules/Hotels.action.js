import { Request } from "app/config/Request";
import { ToastsStore } from "react-toasts";

export const ALL_HOTELS = "ALL_HOTELS";
export const ADD_HOTEL = "ADD_HOTEL";
export const UPDATE_HOTEL = "UPDATE_HOTEL";
export const REMOVE_HOTEL = "REMOVE_HOTEL";
export const STATUS_HOTEL = "STATUS_HOTEL";

export const ALL_HOTEL_STANDARDS = "ALL_HOTEL_STANDARDS";
export const ADD_HOTEL_STANDARD = "ADD_HOTEL_STANDARD";
export const UPDATE_HOTEL_STANDARD = "UPDATE_HOTEL_STANDARD";
export const REMOVE_HOTEL_STANDARD = "REMOVE_HOTEL_STANDARD";
export const STATUS_HOTEL_STANDARD = "STATUS_HOTEL_STANDARD";

export const ALL_HOTEL_CATEGORIES = "ALL_HOTEL_CATEGORIES";
export const ADD_HOTEL_CATEGORY = "ADD_HOTEL_CATEGORY";
export const UPDATE_HOTEL_CATEGORY = "UPDATE_HOTEL_CATEGORY";
export const REMOVE_HOTEL_CATEGORY = "REMOVE_HOTEL_CATEGORY";
export const STATUS_HOTEL_CATEGORY = "STATUS_HOTEL_CATEGORY";

export const ALL_HOTEL_TYPES_ROOM = "ALL_HOTELS_TYPES_ROOM";
export const ADD_HOTEL_TYPE_ROOM = "ADD_HOTEL_TYPE_ROOM";
export const UPDATE_HOTEL_TYPE_ROOM = "UPDATE_HOTEL_TYPE_ROOM";
export const REMOVE_HOTEL_TYPE_ROOM = "REMOVE_HOTEL_TYPE_ROOM";
export const STATUS_HOTEL_TYPE_ROOM = "STATUS_HOTEL_TYPE_ROOM";

export const ALL_HOTEL_TYPES_MEAL = "ALL_HOTELS_TYPES_MEAL";
export const ADD_HOTEL_TYPE_MEAL = "ADD_HOTEL_TYPE_MEAL";
export const UPDATE_HOTEL_TYPE_MEAL = "UPDATE_HOTEL_TYPE_MEAL";
export const REMOVE_HOTEL_TYPE_MEAL = "REMOVE_HOTEL_TYPE_MEAL";
export const STATUS_HOTEL_TYPE_MEAL = "STATUS_HOTEL_TYPE_MEAL";

export function fetchHotels(company_id) {
  return dispatch => {

    Request.get(`accomodations/hotels/all/${company_id}`)
      .then(result => {

        if (result.data.status == 'success') {
          dispatch({
            type: ALL_HOTELS,
            payload: result.data.hotels
          });
        }
      })
      .catch(errors => {
        ToastsStore.warning("Error fetching countries from server", 2000)
      })
  };
}

export function addHotel(payload, callback) {
  return dispatch => {

    Request.post('accomodations/hotels/add', payload)
      .then(result => {
        if (result.data.status == 'success') {
          ToastsStore.success("Hotel is added successfully", 2000)
          dispatch({
            type: ADD_HOTEL,
            payload: result.data.hotel
          });

          callback();

        } else {
          ToastsStore.warning("Error: couln't add hotel", 2000)
        }
      })
      .catch(errors => {
        ToastsStore.warning("Error: server problem occured", 2000)
      })
  };
}

export function updateHotel(payload, callback) {
  return dispatch => {

    Request.post(`accomodations/hotels/edit/${payload.hotel_id}`, payload)
      .then(result => {

        if (result.data.status == 'success') {

          ToastsStore.success("Hotel updated successfully", 2000)
          dispatch({
            type: UPDATE_HOTEL,
            payload: result.data.hotel
          });

          callback();

        } else {
          ToastsStore.warning("Error: couln't update hotel", 2000)
        }
      })
      .catch(errors => {
        ToastsStore.warning("Error: server problem occured", 2000)
      })
  };
}


export function removeHotel(payload) {
  return dispatch => {

    Request.post(`accomodations/hotels/destroy`, payload)
      .then(result => {

        if (result.data.status == 'success') {
          ToastsStore.success("Successfully hotel deleted", 2000)
          dispatch({
            type: REMOVE_HOTEL,
            payload: payload.hotel_id
          });
        } else {
          ToastsStore.warning("Error: couln't delete hotel", 2000)
        }
      })
      .catch(errors => {
        ToastsStore.warning("Error: server problem occured", 2000)
      })
  };
}

export function statusHotel(payload) {
  return dispatch => {

    Request.post(`accomodations/hotels/status`, payload)
      .then(result => {

        if (result.data.status == 'success') {
          ToastsStore.success("Status changed successfully", 2000)
          dispatch({
            type: STATUS_HOTEL,
            payload: payload.hotel_id
          });
        } else {
          ToastsStore.warning("Error: couln't update hotel", 2000)
        }
      })
      .catch(errors => {
        ToastsStore.warning("Error: server problem occured", 2000)
      })
  };
}



export function fetchHotelStandards(company_id) {

  return dispatch => {

    Request.get(`accomodations/hotels/standards/all/${company_id}`)
      .then(result => {
        if (result.data.status == 'success') {
          dispatch({
            type: ALL_HOTEL_STANDARDS,
            payload: result.data.standards
          });
        }
      })
      .catch(errors => {
        ToastsStore.warning("Error fetching hotel standards from server", 2000)
      })
  };
}

export function addHotelStandard(payload) {
  return dispatch => {

    Request.post('accomodations/hotels/standards/add', payload)
      .then(result => {
        if (result.data.status == 'success') {
          ToastsStore.success("Standard is added successfully", 2000)
          dispatch({
            type: ADD_HOTEL_STANDARD,
            payload: result.data.standard
          });

        } else {
          ToastsStore.warning("Error: couln't add standard", 2000)
        }
      })
      .catch(errors => {
        ToastsStore.warning("Error: server problem occured", 2000)
      })
  };
}

export function updateHotelStandard(payload) {
  return dispatch => {

    Request.post(`accomodations/hotels/standards/edit/${payload.standard_id}`, payload)
      .then(result => {

        if (result.data.status == 'success') {

          ToastsStore.success("Standard updated successfully", 2000)
          dispatch({
            type: UPDATE_HOTEL_STANDARD,
            payload: result.data.standard
          });

        } else {
          ToastsStore.warning("Error: couln't update standard", 2000)
        }
      })
      .catch(errors => {
        ToastsStore.warning("Error: server problem occured", 2000)
      })
  };
}


export function removeHotelStandard(payload) {
  return dispatch => {

    Request.post(`accomodations/hotels/standards/destroy`, payload)
      .then(result => {

        if (result.data.status == 'success') {
          ToastsStore.success("Successfully standard deleted", 2000)
          dispatch({
            type: REMOVE_HOTEL_STANDARD,
            payload: payload.standard_id
          });
        } else {
          ToastsStore.warning("Error: couln't delete hotel standard", 2000)
        }
      })
      .catch(errors => {
        ToastsStore.warning("Error: server problem occured", 2000)
      })
  };
}

export function statusHotelStandard(payload) {
  return dispatch => {

    Request.post(`accomodations/hotels/standards/status`, payload)
      .then(result => {

        if (result.data.status == 'success') {
          ToastsStore.success("Status changed successfully", 2000)
          dispatch({
            type: STATUS_HOTEL_STANDARD,
            payload: payload.standard_id
          });
        } else {
          ToastsStore.warning("Error: couln't update hotel standard", 2000)
        }
      })
      .catch(errors => {
        ToastsStore.warning("Error: server problem occured", 2000)
      })
  };
}





export function fetchHotelCategories(company_id) {
  return dispatch => {

    Request.get(`accomodations/hotels/categories/all/${company_id}`)
      .then(result => {

        if (result.data.status == 'success') {
          dispatch({
            type: ALL_HOTEL_CATEGORIES,
            payload: result.data.categories
          });
        }
      })
      .catch(errors => {
        ToastsStore.warning("Error fetching hotel categories from server", 2000)
      })
  };
}

export function addHotelCategory(payload) {
  return dispatch => {

    Request.post('accomodations/hotels/categories/add', payload)
      .then(result => {
        if (result.data.status == 'success') {
          ToastsStore.success("Category is added successfully", 2000)
          dispatch({
            type: ADD_HOTEL_CATEGORY,
            payload: result.data.category
          });

        } else {
          ToastsStore.warning("Error: couln't add category", 2000)
        }
      })
      .catch(errors => {
        ToastsStore.warning("Error: server problem occured", 2000)
      })
  };
}

export function updateHotelCategory(payload) {
  return dispatch => {

    Request.post(`accomodations/hotels/categories/edit/${payload.category_id}`, payload)
      .then(result => {

        if (result.data.status == 'success') {

          ToastsStore.success("Category updated successfully", 2000)
          dispatch({
            type: UPDATE_HOTEL_CATEGORY,
            payload: result.data.category
          });

        } else {
          ToastsStore.warning("Error: couln't update category", 2000)
        }
      })
      .catch(errors => {
        ToastsStore.warning("Error: server problem occured", 2000)
      })
  };
}


export function removeHotelCategory(payload) {
  return dispatch => {

    Request.post(`accomodations/hotels/categories/destroy`, payload)
      .then(result => {

        if (result.data.status == 'success') {
          ToastsStore.success("Successfully category deleted", 2000)
          dispatch({
            type: REMOVE_HOTEL_CATEGORY,
            payload: payload.category_id
          });
        } else {
          ToastsStore.warning("Error: couln't delete hotel category", 2000)
        }
      })
      .catch(errors => {
        ToastsStore.warning("Error: server problem occured", 2000)
      })
  };
}

export function statusHotelCategory(payload) {
  return dispatch => {

    Request.post(`accomodations/hotels/categories/status`, payload)
      .then(result => {

        if (result.data.status == 'success') {
          ToastsStore.success("Status changed successfully", 2000)
          dispatch({
            type: STATUS_HOTEL_CATEGORY,
            payload: payload.category_id
          });
        } else {
          ToastsStore.warning("Error: couln't update hotel category", 2000)
        }
      })
      .catch(errors => {
        ToastsStore.warning("Error: server problem occured", 2000)
      })
  };
}



export function fetchHotelRooms(company_id) {
  return dispatch => {

    Request.get(`accomodations/hotels/rooms/all/${company_id}`)
      .then(result => {

        if (result.data.status == 'success') {
          dispatch({
            type: ALL_HOTEL_TYPES_ROOM,
            payload: result.data.rooms
          });
        }
      })
      .catch(errors => {
        ToastsStore.warning("Error: server problem occured", 2000)
      })
  };
}

export function addHotelRoom(payload) {
  return dispatch => {

    Request.post('accomodations/hotels/rooms/add', payload)
      .then(result => {
        if (result.data.status == 'success') {
          ToastsStore.success("Room is added successfully", 2000)
          dispatch({
            type: ADD_HOTEL_TYPE_ROOM,
            payload: result.data.room
          });

        } else {
          ToastsStore.warning("Error: couln't add room", 2000)
        }
      })
      .catch(errors => {
        ToastsStore.warning("Error: server problem occured", 2000)
      })
  };
}

export function updateHotelRoom(payload) {
  return dispatch => {

    Request.post(`accomodations/hotels/rooms/edit/${payload.room_id}`, payload)
      .then(result => {

        if (result.data.status == 'success') {

          ToastsStore.success("Room updated successfully", 2000)
          dispatch({
            type: UPDATE_HOTEL_TYPE_ROOM,
            payload: result.data.room
          });

        } else {
          ToastsStore.warning("Error: couln't update room", 2000)
        }
      })
      .catch(errors => {
        ToastsStore.warning("Error: server problem occured", 2000)
      })
  };
}


export function removeHotelRoom(payload) {
  return dispatch => {

    Request.post(`accomodations/hotels/rooms/destroy`, payload)
      .then(result => {

        if (result.data.status == 'success') {
          ToastsStore.success("Successfully room deleted", 2000)
          dispatch({
            type: REMOVE_HOTEL_TYPE_ROOM,
            payload: payload.room_id
          });
        } else {
          ToastsStore.warning("Error: couln't delete hotel room", 2000)
        }
      })
      .catch(errors => {
        ToastsStore.warning("Error: server problem occured", 2000)
      })
  };
}

export function statusHotelRoom(payload) {
  return dispatch => {

    Request.post(`accomodations/hotels/rooms/status`, payload)
      .then(result => {

        if (result.data.status == 'success') {
          ToastsStore.success("Status changed successfully", 2000)
          dispatch({
            type: STATUS_HOTEL_TYPE_ROOM,
            payload: payload.room_id
          });
        } else {
          ToastsStore.warning("Error: couln't update hotel room", 2000)
        }
      })
      .catch(errors => {
        ToastsStore.warning("Error: server problem occured", 2000)
      })
  };
}



export function fetchHotelMeals(company_id) {
  return dispatch => {

    Request.get(`accomodations/hotels/meals/all/${company_id}`)
      .then(result => {

        if (result.data.status == 'success') {
          dispatch({
            type: ALL_HOTEL_TYPES_MEAL,
            payload: result.data.meals
          });
        }
      })
      .catch(errors => {
        ToastsStore.warning("Error: server problem occured", 2000)
      })
  };
}

export function addHotelMeal(payload) {
  return dispatch => {

    Request.post('accomodations/hotels/meals/add', payload)
      .then(result => {
        if (result.data.status == 'success') {
          ToastsStore.success("Meal Type is added successfully", 2000)
          dispatch({
            type: ADD_HOTEL_TYPE_MEAL,
            payload: result.data.meal
          });

        } else {
          ToastsStore.warning("Error: couln't add type meal", 2000)
        }
      })
      .catch(errors => {
        ToastsStore.warning("Error: server problem occured", 2000)
      })
  };
}

export function updateHotelMeal(payload) {
  return dispatch => {

    Request.post(`accomodations/hotels/meals/edit/${payload.meal_id}`, payload)
      .then(result => {

        if (result.data.status == 'success') {

          ToastsStore.success("Meals Type updated successfully", 2000)
          dispatch({
            type: UPDATE_HOTEL_TYPE_MEAL,
            payload: result.data.meal
          });

        } else {
          ToastsStore.warning("Error: couln't update type meal", 2000)
        }
      })
      .catch(errors => {
        ToastsStore.warning("Error: server problem occured", 2000)
      })
  };
}


export function removeHotelMeal(payload) {
  return dispatch => {

    Request.post(`accomodations/hotels/meals/destroy`, payload)
      .then(result => {

        if (result.data.status == 'success') {
          ToastsStore.success("Successfully meal type deleted", 2000)
          dispatch({
            type: REMOVE_HOTEL_TYPE_MEAL,
            payload: payload.meal_id
          });
        } else {
          ToastsStore.warning("Error: couln't delete hotel type meal", 2000)
        }
      })
      .catch(errors => {
        ToastsStore.warning("Error: server problem occured", 2000)
      })
  };
}

export function statusHotelMeal(payload) {
  return dispatch => {

    Request.post(`accomodations/hotels/meals/status`, payload)
      .then(result => {

        if (result.data.status == 'success') {
          ToastsStore.success("Status changed successfully", 2000)
          dispatch({
            type: STATUS_HOTEL_TYPE_MEAL,
            payload: payload.meal_id
          });
        } else {
          ToastsStore.warning("Error: couln't update hotel type meal", 2000)
        }
      })
      .catch(errors => {
        ToastsStore.warning("Error: server problem occured", 2000)
      })
  };
}
