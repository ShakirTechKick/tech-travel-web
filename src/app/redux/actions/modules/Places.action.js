import { Request } from "app/config/Request";
import { ToastsStore } from "react-toasts";

export const ALL_COUNTRIES = "ALL_COUNTRIES";
export const ADD_COUNTRY = "ADD_COUNTRY";
export const UPDATE_COUNTRY = "UPDATE_COUNTRY";
export const REMOVE_COUNTRY = "REMOVE_COUNTRY";
export const STATUS_COUNTRY = "STATUS_COUNTRY";
export const ALL_CITIES = "ALL_CITIES";
export const ADD_CITY = "ADD_CITY";
export const UPDATE_CITY = "UPDATE_CITY";
export const REMOVE_CITY = "REMOVE_CITY";
export const STATUS_CITY = "STATUS_CITY";

export function fetchCountries(company_id) {
  return dispatch => {

    Request.get(`places/countries/all/${company_id}`)
      .then(result => {

        if (result.data.status == 'success') {
          dispatch({
            type: ALL_COUNTRIES,
            payload: result.data.countries
          });
        }
      })
      .catch(errors => {
        ToastsStore.warning("Error: server problem occured", 2000)
      })
  };
}

export function addCountry(payload, callback) {
  return dispatch => {

    Request.post('places/countries/add', payload)
      .then(result => {
        if (result.data.status == 'success') {
          ToastsStore.success("Country is added successfully", 2000)
          dispatch({
            type: ADD_COUNTRY,
            payload: result.data.country
          });

          callback();

        } else {
          ToastsStore.warning("Error: couln't add country", 2000)
        }
      })
      .catch(errors => {
        ToastsStore.warning("Error: server problem occured", 2000)
      })
  };
}

export function updateCountry(payload, callback) {
  return dispatch => {

    Request.post(`places/countries/edit/${payload.country_id}`, payload)
      .then(result => {

        if (result.data.status == 'success') {

          ToastsStore.success("Country updated successfully", 2000)
          dispatch({
            type: UPDATE_COUNTRY,
            payload: result.data.country
          });

          callback();

        } else {
          ToastsStore.warning("Error: couln't update country", 2000)
        }
      })
      .catch(errors => {
        ToastsStore.warning("Error: server problem occured", 2000)
      })
  };
}


export function removeCountry(payload) {
  return dispatch => {

    Request.post(`places/countries/destroy`, payload)
      .then(result => {

        if (result.data.status == 'success') {
          ToastsStore.success("Successfully country deleted", 2000)
          dispatch({
            type: REMOVE_COUNTRY,
            payload: payload.country_id
          });
        } else {
          ToastsStore.warning("Error: couln't delete country", 2000)
        }
      })
      .catch(errors => {
        ToastsStore.warning("Error: server problem occured", 2000)
      })
  };
}

export function statusCountry(payload) {
  return dispatch => {

    Request.post(`places/countries/status`, payload)
      .then(result => {

        if (result.data.status == 'success') {
          ToastsStore.success("Status changed successfully", 2000)
          dispatch({
            type: STATUS_COUNTRY,
            payload: payload.country_id
          });
        } else {
          ToastsStore.warning("Error: couln't update country", 2000)
        }
      })
      .catch(errors => {
        ToastsStore.warning("Error: server problem occured", 2000)
      })
  };
}


export function fetchCities(company_id) {
  return dispatch => {

    Request.get(`places/cities/all/${company_id}`)
      .then(result => {

        if (result.data.status == 'success') {
          dispatch({
            type: ALL_CITIES,
            payload: result.data.cities
          });
        }
      })
      .catch(errors => {
        ToastsStore.warning("Error: server problem occured", 2000)
      })
  };
}

export function addCity(payload, callback) {
  return dispatch => {

    Request.post('places/cities/add', payload)
      .then(result => {

        if (result.data.status == 'success') {
          ToastsStore.success("City is added successfully", 2000)
          dispatch({
            type: ADD_CITY,
            payload: result.data.city
          });

          callback();

        } else {
          ToastsStore.warning("Error: couln't add city", 2000)
        }
      })
      .catch(errors => {
        ToastsStore.warning("Error: server problem occured", 2000)
      })
  };
}

export function updateCity(payload, callback) {
  return dispatch => {

    Request.post(`places/cities/edit/${payload.city_id}`, payload)
      .then(result => {

        if (result.data.status == 'success') {

          ToastsStore.success("City updated successfully", 2000)
          dispatch({
            type: UPDATE_CITY,
            payload: result.data.city
          });

          callback();

        } else {
          ToastsStore.warning("Error: couln't update city", 2000)
        }
      })
      .catch(errors => {
        ToastsStore.warning("Error: server problem occured", 2000)
      })
  };
}


export function removeCity(payload) {
  return dispatch => {

    Request.post(`places/cities/destroy`, payload)
      .then(result => {

        if (result.data.status == 'success') {
          ToastsStore.success("Successfully city deleted", 2000)
          dispatch({
            type: REMOVE_CITY,
            payload: payload.city_id
          });
        } else {
          ToastsStore.warning("Error: couln't delete city", 2000)
        }
      })
      .catch(errors => {
        ToastsStore.warning("Error: server problem occured", 2000)
      })
  };
}

export function statusCity(payload) {
  return dispatch => {

    Request.post(`places/cities/status`, payload)
      .then(result => {

        if (result.data.status == 'success') {
          ToastsStore.success("Status changed successfully", 2000)
          dispatch({
            type: STATUS_CITY,
            payload: payload.city_id
          });
        } else {
          ToastsStore.warning("Error: couln't update city", 2000)
        }
      })
      .catch(errors => {
        ToastsStore.warning("Error: server problem occured", 2000)
      })
  };
}
