import { Request } from "app/config/Request";
import { ToastsStore } from "react-toasts";

export const ALL_ROLES = "ALL_ROLES";
export const ADD_ROLES = "ADD_ROLES";
export const UPDATE_ROLES = "UPDATE_ROLES";
export const REMOVE_ROLES = "REMOVE_ROLES";
export const STATUS_ROLES = "STATUS_ROLES";
export const AVAILABLE_MODULES = "AVAILABLE_MODULES";
export const PERMITTED_MODULES = "PERMITTED_MODULES";

export function fetchRoles(payload) {
  return dispatch => {

    Request.get(`roles/all/${payload}`)
      .then(result => {

        if (result.data.status == 'success') {
          dispatch({
            type: ALL_ROLES,
            payload: result.data.list
          });
        }
      })
      .catch(errors => {
        ToastsStore.warning("Error: server problem occured", 2000)
      })
  };
}

export function addRoles(payload, callback) {
  return dispatch => {

    Request.post('roles/add', payload)
      .then(result => {

        if (result.data.status == 'success') {
          ToastsStore.success("Role is added successfully", 2000)
          dispatch({
            type: ADD_ROLES,
            payload: result.data.role_user
          });

          callback();
        } else {
          ToastsStore.error(result.data.msg, 2000)
        }
      })
      .catch(errors => {
        ToastsStore.warning("Error: server problem occured", 2000)
      })
  };
}

export function updateRoles(payload, callback) {
  return dispatch => {

    Request.post(`roles/edit/${payload.role_id}`, payload)
      .then(result => {

        if (result.data.status == 'success') {

          ToastsStore.success("Role updated successfully", 2000)
          dispatch({
            type: UPDATE_ROLES,
            payload: result.data.role_user
          });

          callback();
        } else {
          ToastsStore.error(result.data.msg, 2000)
        }
      })
      .catch(errors => {
        ToastsStore.warning("Error: server problem occured", 2000)
      })
  };
}


export function removeRoles(payload) {
  return dispatch => {

    Request.post(`roles/destroy`, payload)
      .then(result => {

        if (result.data.status == 'success') {
          ToastsStore.success("Successfully role deleted", 2000)
          dispatch({
            type: REMOVE_ROLES,
            payload: payload.role_id
          });
        }
      })
      .catch(errors => {
        ToastsStore.warning("Error: server problem occured", 2000)
      })
  };
}

export function statusRoles(payload) {
  return dispatch => {

    Request.post(`roles/status`, payload)
      .then(result => {

        if (result.data.status == 'success') {
          ToastsStore.success("Status changed successfully", 2000)
          dispatch({
            type: STATUS_ROLES,
            payload: payload.role_id
          });
        }
      })
      .catch(errors => {
        ToastsStore.warning("Error: server problem occured", 2000)
      })
  };
}

export function availableModules() {
  return dispatch => {

    Request.get(`roles/get_available_modules`)
      .then(result => {

        if (result.data.status == 'success') {
          dispatch({
            type: AVAILABLE_MODULES,
            payload: result.data.modules
          });
        }
      })
      .catch(errors => {
        ToastsStore.warning("Error: server problem occured", 2000)
      })
  };
}

export function permittedModules(payload, callback) {
  return dispatch => {

    Request.get(`roles/get_permitted_modules/${payload}`, payload)
      .then(result => {

        if (result.data.status == 'success') {
          dispatch({
            type: PERMITTED_MODULES,
            payload: result.data.modules
          });

          callback(result.data.modules);
        }
      })
      .catch(errors => {
        ToastsStore.warning("Error: server problem occured", 2000)
      })
  };
}