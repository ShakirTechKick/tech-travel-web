import { Request } from "app/config/Request";
import { ToastsStore } from "react-toasts";

export const ALL_CLIENTS = "ALL_CLIENTS";
export const ADD_CLIENT = "ADD_CLIENT";
export const UPDATE_CLIENT = "UPDATE_CLIENT";
export const REMOVE_CLIENT = "REMOVE_CLIENT";
export const STATUS_CLIENT = "STATUS_CLIENT";

export function fetchClients(company_id) {
  return dispatch => {

    Request.get(`clients/all/${company_id}`)
      .then(result => {

        if (result.data.status == 'success') {
          dispatch({
            type: ALL_CLIENTS,
            payload: result.data.clients
          });
        }
      })
      .catch(errors => {
        ToastsStore.warning("Error fetching clients from server", 2000)
      })
  };
}

export function addClient(payload, callback) {
  return dispatch => {

    Request.post('clients/add', payload)
      .then(result => {

        if (result.data.status == 'success') {
          ToastsStore.success("Client is added successfully", 2000)
          dispatch({
            type: ADD_CLIENT,
            payload: result.data.client
          });

          callback();
        } else {
          ToastsStore.warning("Error: couln't add client", 2000)
        }
      })
      .catch(errors => {
        ToastsStore.warning("Error: server problem occured", 2000)
      })
  };
}

export function updateClient(payload, callback) {
  return dispatch => {

    Request.post(`clients/edit/${payload.client_id}`, payload)
      .then(result => {

        if (result.data.status == 'success') {

          ToastsStore.success("Client updated successfully", 2000)
          dispatch({
            type: UPDATE_CLIENT,
            payload: result.data.client
          });

          callback();
        } else {
          ToastsStore.warning("Error: couln't update client", 2000)
        }
      })
      .catch(errors => {
        ToastsStore.warning("Error: server problem occured", 2000)
      })
  };
}


export function removeClient(payload) {
  return dispatch => {

    Request.post(`clients/destroy`, payload)
      .then(result => {

        if (result.data.status == 'success') {
          ToastsStore.success("Successfully client deleted", 2000)
          dispatch({
            type: REMOVE_CLIENT,
            payload: payload.client_id
          });
        } else {
          ToastsStore.warning("Error: couln't delete client", 2000)
        }
      })
      .catch(errors => {
        ToastsStore.warning("Error: server problem occured", 2000)
      })
  };
}

export function statusClient(payload) {
  return dispatch => {

    Request.post(`clients/status`, payload)
      .then(result => {

        if (result.data.status == 'success') {
          ToastsStore.success("Status changed successfully", 2000)
          dispatch({
            type: STATUS_CLIENT,
            payload: payload.client_id
          });
        } else {
          ToastsStore.warning("Error: couln't update client", 2000)
        }
      })
      .catch(errors => {
        ToastsStore.warning("Error: server problem occured", 2000)
      })
  };
}
