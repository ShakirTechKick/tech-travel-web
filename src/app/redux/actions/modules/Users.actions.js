import { Request } from "app/config/Request";
import { ToastsStore } from "react-toasts";

export const ALL_USERS = "ALL_USERS";
export const ADD_USER = "ADD_USER";
export const UPDATE_USER = "UPDATE_USER";
export const UPDATE_PROFILE_USER = "UPDATE_PROFILE_USER";
export const REMOVE_USER = "REMOVE_USER";
export const STATUS_USER = "STATUS_USER";

export function fetchUsers() {
  return dispatch => {

    Request.get('users/all')
      .then(result => {

        if (result.data.status == 'success') {
          dispatch({
            type: ALL_USERS,
            payload: result.data.users
          });
        }
      })
      .catch(errors => {
        ToastsStore.warning("Error: server problem occured", 2000)
      })
  };
}

export function addUsers(payload, callback) {
  return dispatch => {

    Request.post('users/add', payload)
      .then(result => {

        if (result.data.status == 'success') {
          ToastsStore.success("User is added successfully", 2000)
          dispatch({ 
            type: ADD_USER,
            payload: result.data.user
          });

          callback();
        } else {
          ToastsStore.warning("Error: couln't add user", 2000)
        }
      })
      .catch(errors => {
        ToastsStore.warning("Error: server problem occured", 2000)
      })
  };
}

export function updateUser(payload, callback, user = null) {
  return dispatch => {

    Request.post(`users/edit/${payload.user_id}`, payload)
      .then(result => {

        if (result.data.status == 'success') {

          ToastsStore.success("User updated successfully", 2000)
          dispatch({
            type: UPDATE_USER,
            payload: result.data.user
          });

          if (user) {
            dispatch({
              type: UPDATE_PROFILE_USER,
              payload: result.data.user
            });
          }

          callback();
        } else {
          ToastsStore.warning("Error: couln't update user", 2000)
        }
      })
      .catch(errors => {
        ToastsStore.warning("Error: server problem occured", 2000)
      })
  };
}


export function removeUser(payload) {
  return dispatch => {

    Request.post(`users/destroy`, payload)
      .then(result => {

        if (result.data.status == 'success') {
          ToastsStore.success("Successfully user deleted", 2000)
          dispatch({
            type: REMOVE_USER,
            payload: payload.user_id
          });
        } else {
          ToastsStore.warning("Error: couln't delete user", 2000)
        }
      })
      .catch(errors => {
        ToastsStore.warning("Error: server problem occured", 2000)
      })
  };
}

export function statusUser(payload) {
  return dispatch => {

    Request.post(`users/status`, payload)
      .then(result => {

        if (result.data.status == 'success') {
          ToastsStore.success("Status changed successfully", 2000)
          dispatch({
            type: STATUS_USER,
            payload: payload.user_id
          });
        } else {
          ToastsStore.warning("Error: couln't delete user", 2000)
        }
      })
      .catch(errors => {
        ToastsStore.warning("Error: server problem occured", 2000)
      })
  };
}
