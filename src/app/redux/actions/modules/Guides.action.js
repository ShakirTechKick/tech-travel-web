import { Request } from "app/config/Request";
import { ToastsStore } from "react-toasts";

export const ALL_GUIDES = "ALL_GUIDES";
export const ADD_GUIDES = "ADD_GUIDES";
export const UPDATE_GUIDES = "UPDATE_GUIDES";
export const REMOVE_GUIDES = "REMOVE_GUIDES";
export const STATUS_GUIDES = "STATUS_GUIDES";

export function fetchGuides(company_id) {
  return dispatch => {

    Request.get(`guides/all/${company_id}`)
      .then(result => {

        if (result.data.status == 'success') {
          dispatch({
            type: ALL_GUIDES,
            payload: result.data.guides
          });
        }
      })
      .catch(errors => {
        ToastsStore.warning("Error fetching guides from server", 2000)
      })
  };
}

export function addGuides(payload, callback) {
  return dispatch => {

    Request.post('guides/add', payload)
      .then(result => {

        if (result.data.status == 'success') {
          ToastsStore.success("Guide is added successfully", 2000)
          dispatch({
            type: ADD_GUIDES,
            payload: result.data.guide
          });

          callback();
        } else {
          ToastsStore.warning("Error: couln't add guides", 2000)
        }
      })
      .catch(errors => {
        ToastsStore.warning("Error: server problem occured", 2000)
      })
  };
}

export function updateGuides(payload, callback) {
  return dispatch => {

    Request.post(`guides/edit/${payload.guide_id}`, payload)
      .then(result => {

        if (result.data.status == 'success') {

          ToastsStore.success("Guide is updated successfully", 2000)
          dispatch({
            type: UPDATE_GUIDES,
            payload: result.data.guide
          });

          callback();
        } else {
          ToastsStore.warning("Error: couln't update guides", 2000)
        }
      })
      .catch(errors => {
        ToastsStore.warning("Error: server problem occured", 2000)
      })
  };
}


export function removeGuides(payload) {
  return dispatch => {

    Request.post(`guides/destroy`, payload)
      .then(result => {

        if (result.data.status == 'success') {
          ToastsStore.success("Successfully guides deleted", 2000)
          dispatch({
            type: REMOVE_GUIDES,
            payload: payload.guide_id
          });
        } else {
          ToastsStore.warning("Error: couln't delete guides", 2000)
        }
      })
      .catch(errors => {
        ToastsStore.warning("Error: server problem occured", 2000)
      })
  };
}

export function statusGuides(payload) {
  return dispatch => {

    Request.post(`guides/status`, payload)
      .then(result => {

        if (result.data.status == 'success') {
          ToastsStore.success("Status changed successfully", 2000)
          dispatch({
            type: STATUS_GUIDES,
            payload: payload.guide_id
          });
        } else {
          ToastsStore.warning("Error: couln't update guides", 2000)
        }
      })
      .catch(errors => {
        ToastsStore.warning("Error: server problem occured", 2000)
      })
  };
}
