import { Request } from "app/config/Request";
import { ToastsStore } from "react-toasts";

export const ALL_ACTIVITIES = "ALL_ACTIVITIES";
export const ADD_ACTIVITY = "ADD_ACTIVITY";
export const UPDATE_ACTIVITY = "UPDATE_ACTIVITY";
export const REMOVE_ACTIVITY = "REMOVE_ACTIVITY";
export const STATUS_ACTIVITY = "STATUS_ACTIVITY";


export function fetchActivities(company_id) {
  return dispatch => {

    Request.get(`places/activities/all/${company_id}`)
      .then(result => {

        if (result.data.status == 'success') {
          dispatch({
            type: ALL_ACTIVITIES,
            payload: result.data.activities
          });
        }
      }).catch(errors => {
        ToastsStore.warning("Error fetching activities from server", 2000)
      })
  };
}

export function addActivity(payload) {
  return dispatch => {

    Request.post('places/activities/add', payload)
      .then(result => {
        if (result.data.status == 'success') {
          ToastsStore.success("Activity is added successfully", 2000)
          dispatch({
            type: ADD_ACTIVITY,
            payload: result.data.activity
          });

        } else {
          ToastsStore.warning("Error: couln't add activity", 2000)
        }
      })
      .catch(errors => {
        ToastsStore.warning("Error: server problem occured", 2000)
      })
  };
}

export function updateActivity(payload) {
  return dispatch => {

    Request.post(`places/activities/edit/${payload.activity_id}`, payload)
      .then(result => {

        if (result.data.status == 'success') {

          ToastsStore.success("Activity updated successfully", 2000)
          dispatch({
            type: UPDATE_ACTIVITY,
            payload: result.data.activity
          });

        } else {
          ToastsStore.warning("Error: couln't update activity", 2000)
        }
      })
      .catch(errors => {
        ToastsStore.warning("Error: server problem occured", 2000)
      })
  };
}

export function updateActivitiesList(payload) {
  return dispatch => {

    Request.post(`places/activities/update_activity_list`, payload)
      .then(result => {

        if (result.data.status == 'success') {

          ToastsStore.success("Activity updated successfully", 2000)
          dispatch({
            type: UPDATE_ACTIVITY,
            payload: result.data.activity
          });

        } else {
          ToastsStore.warning("Error: couln't update activity", 2000)
        }
      })
      .catch(errors => {
        ToastsStore.warning("Error: server problem occured", 2000)
      })
  };
}


export function removeActivity(payload) {
  return dispatch => {

    Request.post(`places/activities/destroy`, payload)
      .then(result => {

        if (result.data.status == 'success') {
          ToastsStore.success("Successfully activity deleted", 2000)
          dispatch({
            type: REMOVE_ACTIVITY,
            payload: payload.activity_id
          });
        } else {
          ToastsStore.warning("Error: couln't delete activity", 2000)
        }
      })
      .catch(errors => {
        ToastsStore.warning("Error: server problem occured", 2000)
      })
  };
}

export function statusActivity(payload) {
  return dispatch => {

    Request.post(`places/activities/status`, payload)
      .then(result => {

        if (result.data.status == 'success') {
          ToastsStore.success("Status changed successfully", 2000)
          dispatch({
            type: STATUS_ACTIVITY,
            payload: payload.activity_id
          });
        } else {
          ToastsStore.warning("Error: couln't update activity", 2000)
        }
      })
      .catch(errors => {
        ToastsStore.warning("Error: server problem occured", 2000)
      })
  };
}


