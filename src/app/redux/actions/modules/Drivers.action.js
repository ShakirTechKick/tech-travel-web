import { Request } from "app/config/Request";
import { ToastsStore } from "react-toasts";

export const ALL_DRIVERS = "ALL_DRIVERS";
export const ADD_DRIVER = "ADD_DRIVER";
export const UPDATE_DRIVER = "UPDATE_DRIVER";
export const REMOVE_DRIVER = "REMOVE_DRIVER";
export const STATUS_DRIVER = "STATUS_DRIVER";

export function fetchDrivers(company_id) {
  return dispatch => {

    Request.get(`drivers/all/${company_id}`)
      .then(result => {

        if (result.data.status == 'success') {
          dispatch({
            type: ALL_DRIVERS,
            payload: result.data.drivers
          });
        }
      })
      .catch(errors => {
        ToastsStore.warning("Error fetching drivers from server", 2000)
      })
  };
}

export function addDriver(payload, callback) {
  return dispatch => {

    Request.post('drivers/add', payload)
      .then(result => {

        if (result.data.status == 'success') {
          ToastsStore.success("Driver is added successfully", 2000)
          dispatch({
            type: ADD_DRIVER,
            payload: result.data.driver
          });

          callback();
        } else {
          ToastsStore.warning("Error: couln't add driver", 2000)
        }
      })
      .catch(errors => {
        ToastsStore.warning("Error: server problem occured", 2000)
      })
  };
}

export function updateDriver(payload, callback) {
  return dispatch => {

    Request.post(`drivers/edit/${payload.driver_id}`, payload)
      .then(result => {

        if (result.data.status == 'success') {

          ToastsStore.success("Driver updated successfully", 2000)
          dispatch({
            type: UPDATE_DRIVER,
            payload: result.data.driver
          });

          callback();
        } else {
          ToastsStore.warning("Error: couln't update driver", 2000)
        }
      })
      .catch(errors => {
        ToastsStore.warning("Error: server problem occured", 2000)
      })
  };
}


export function removeDriver(payload) {
  return dispatch => {

    Request.post(`drivers/destroy`, payload)
      .then(result => {

        if (result.data.status == 'success') {
          ToastsStore.success("Successfully driver deleted", 2000)
          dispatch({
            type: REMOVE_DRIVER,
            payload: payload.driver_id
          });
        } else {
          ToastsStore.warning("Error: couln't delete driver", 2000)
        }
      })
      .catch(errors => {
        ToastsStore.warning("Error: server problem occured", 2000)
      })
  };
}

export function statusDriver(payload) {
  return dispatch => {

    Request.post(`drivers/status`, payload)
      .then(result => {

        if (result.data.status == 'success') {
          ToastsStore.success("Status changed successfully", 2000)
          dispatch({
            type: STATUS_DRIVER,
            payload: payload.driver_id
          });
        } else {
          ToastsStore.warning("Error: couln't update driver", 2000)
        }
      })
      .catch(errors => {
        ToastsStore.warning("Error: server problem occured", 2000)
      })
  };
}
