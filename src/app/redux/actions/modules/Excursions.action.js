import { Request } from "app/config/Request";
import { ToastsStore } from "react-toasts";

export const ALL_EXCURSIONS = "ALL_EXCURSIONS";
export const ADD_EXCURSION = "ADD_EXCURSION";
export const UPDATE_EXCURSION = "UPDATE_EXCURSION";
export const REMOVE_EXCURSION = "REMOVE_EXCURSION";
export const STATUS_EXCURSION = "STATUS_EXCURSION";


export function fetchExcursions(company_id) {
  return dispatch => {

    Request.get(`places/excursions/all/${company_id}`)
      .then(result => {

        if (result.data.status == 'success') {
          dispatch({
            type: ALL_EXCURSIONS,
            payload: result.data.excursions
          });
        }
      })
      .catch(errors => {
        ToastsStore.warning("Error fetching countries from server", 2000)
      })
  };
}

export function addExcursion(payload) {
  return dispatch => {

    Request.post('places/excursions/add', payload)
      .then(result => {
        if (result.data.status == 'success') {
          ToastsStore.success("Excursion is added successfully", 2000)
          dispatch({
            type: ADD_EXCURSION,
            payload: result.data.excursion
          });

        } else {
          ToastsStore.warning("Error: couln't add excursion", 2000)
        }
      })
      .catch(errors => {
        ToastsStore.warning("Error: server problem occured", 2000)
      })
  };
}

export function updateExcursion(payload) {
  return dispatch => {

    Request.post(`places/excursions/edit/${payload.excursion_id}`, payload)
      .then(result => {

        if (result.data.status == 'success') {

          ToastsStore.success("Excursion updated successfully", 2000)
          dispatch({
            type: UPDATE_EXCURSION,
            payload: result.data.excursion
          });

        } else {
          ToastsStore.warning("Error: couln't update excursion", 2000)
        }
      })
      .catch(errors => {
        ToastsStore.warning("Error: server problem occured", 2000)
      })
  };
}


export function removeExcursion(payload) {
  return dispatch => {

    Request.post(`places/excursions/destroy`, payload)
      .then(result => {

        if (result.data.status == 'success') {
          ToastsStore.success("Successfully excursion deleted", 2000)
          dispatch({
            type: REMOVE_EXCURSION,
            payload: payload.excursion_id
          });
        } else {
          ToastsStore.warning("Error: couln't delete excursion", 2000)
        }
      })
      .catch(errors => {
        ToastsStore.warning("Error: server problem occured", 2000)
      })
  };
}

export function statusExcursion(payload) {
  return dispatch => {

    Request.post(`places/excursions/status`, payload)
      .then(result => {

        if (result.data.status == 'success') {
          ToastsStore.success("Status changed successfully", 2000)
          dispatch({
            type: STATUS_EXCURSION,
            payload: payload.excursion_id
          });
        } else {
          ToastsStore.warning("Error: couln't update excursion", 2000)
        }
      })
      .catch(errors => {
        ToastsStore.warning("Error: server problem occured", 2000)
      })
  };
}


