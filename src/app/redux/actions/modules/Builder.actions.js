import { Request } from "app/config/Request";
import { ToastsStore } from "react-toasts";

export const ALL_ITINERARIES = "ALL_ITINERARIES";
export const AUTO_SAVE_ITINERARY = "AUTO_SAVE_ITINERARY";
export const ADD_ITINERARY = "ADD_ITINERARY";
export const UPDATE_ITINERARY = "UPDATE_ITINERARY";
export const REMOVE_ITINERARY = "REMOVE_ITINERARY";
export const STATUS_ITINERARY = "STATUS_ITINERARY";

export function fetchItineraries(payload, callback = null) {
  return dispatch => {

    Request.get(`builder/itinerary/all/${payload.company_id}${payload.user_id ? '/' + payload.user_id : null}`)
      .then(result => {

        if (result.data.status == 'success') {
          dispatch({
            type: ALL_ITINERARIES,
            payload: result.data.itineraries
          });

          if (callback) callback();
        }
      })
      .catch(errors => {
        ToastsStore.warning("Error fetching itineraries from server", 2000)
      })
  };
}

export function autoSaveItinerary(payload, callback) {
  return dispatch => {

    Request.post('builder/itinerary/autosavedraft', payload)
      .then(result => {

        if (result.data.status == 'success') {
          dispatch({
            type: AUTO_SAVE_ITINERARY,
            payload: result.data.itinerary
          });

          callback(result.data.itinerary.itinerary_id.toString());

        } else {
          ToastsStore.warning("Error: auto save failed", 2000)
        }
      })
      .catch(errors => {
        ToastsStore.warning("Error: server problem occured", 2000)
      })
  };
}

export function addItinerary(payload, callback) {
  return dispatch => {

    Request.post('builder/itinerary/add', payload)
      .then(result => {

        if (result.data.status == 'success') {
          ToastsStore.success("Itinerary is added successfully", 2000)
          dispatch({
            type: ADD_ITINERARY,
            payload: result.data.itinerary
          });

          if (payload.user.admin && payload.user.admin.length) {

            Request.post('utility/notification/notify', {
              company_id: result.data.itinerary.company_id,
              user_id: payload.user.admin[0].user_id, context: 'Itinerary created', description: `Token number: ${result.data.itinerary.itinerary_id}`,
              path: '/itinerary/builder', heading: `Itinerary (${result.data.itinerary.itinerary_id})`
            }).subscribe();

          }

          callback();
        } else {
          ToastsStore.warning("Error: couln't add itinerary", 2000)
        }
      })
      .catch(errors => {
        ToastsStore.warning("Error: server problem occured", 2000)
      })
  };
}

export function updateItinerary(payload, callback) {
  return dispatch => {

    Request.post(`builder/itinerary/edit/${payload.itinerary_id}`, payload)
      .then(result => {

        if (result.data.status == 'success') {

          ToastsStore.success("Itinerary updated successfully", 2000)
          dispatch({
            type: UPDATE_ITINERARY,
            payload: result.data.itinerary
          });

          if (payload.user.admin && payload.user.admin.length) {

            Request.post('utility/notification/notify', {
              company_id: result.data.itinerary.company_id,
              user_id: payload.user.admin[0].user_id, context: 'Itinerary updated', description: `Token number: ${result.data.itinerary.itinerary_id}`,
              path: '/itinerary/builder', heading: `Itinerary (${result.data.itinerary.itinerary_id}) edited`
            }).subscribe();

          }

          callback();
        } else {
          ToastsStore.warning("Error: couln't update itinerary", 2000)
        }
      })
      .catch(errors => {
        ToastsStore.warning("Error: server problem occured", 2000)
      })
  };
}


export function removeItinerary(payload, document = "Real", callback = null) {
  return dispatch => {

    Request.post(`builder/itinerary/destroy`, payload)
      .then(result => {

        if (result.data.status == 'success') {

          if (typeof callback === 'function') {
            callback();
          }

          if (document === "Real") {

            ToastsStore.success("Successfully itinerary deleted", 2000);

            if (payload.user.admin && payload.user.admin.length) {

              Request.post('utility/notification/notify', {
                company_id: result.data.itinerary.company_id,
                user_id: payload.user.admin[0].user_id, context: 'Itinerary deleted', description: `Token number: ${result.data.itinerary.itinerary_id}`,
                path: '/itinerary/builder', heading: `Itinerary (${result.data.itinerary.itinerary_id}) deleted`
              }).subscribe();

            }

          }

          dispatch({
            type: REMOVE_ITINERARY,
            payload: payload.itinerary_id
          });

        } else {
          ToastsStore.warning("Error: couln't delete itinerary", 2000)
        }

      })
      .catch(errors => {
        ToastsStore.warning("Error: server problem occured", 2000)
      })
  };
}

export function statusItinerary(payload) {
  return dispatch => {

    Request.post(`builder/itinerary/status`, payload)
      .then(result => {

        if (result.data.status == 'success') {
          ToastsStore.success("Status changed successfully", 2000)
          dispatch({
            type: STATUS_ITINERARY,
            payload: payload
          });

        } else {
          ToastsStore.warning("Error: couln't update itinerary", 2000)
        }
      })
      .catch(errors => {
        ToastsStore.warning("Error: server problem occured", 2000)
      })
  };
}
