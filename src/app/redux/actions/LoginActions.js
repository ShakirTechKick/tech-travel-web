import { setUserData } from "./UserActions";
import history from "history.js";
import { Request } from "app/config/Request";
import { ToastsStore } from "react-toasts";

export function loginWithEmailAndPassword({ email, password }, callback = null) {
  return dispatch => {

    Request.post(`auth/login`, { email, password })
      .then(result => {

        if (result.data.status == 'success') {

          dispatch(setUserData({ authenticated: true, user: result.data.user }));

          sessionStorage.setItem('user', JSON.stringify({ authenticated: true, user: result.data.user }));

          history.push({ pathname: "/dashboard" });

        } else {
          ToastsStore.error("Error: invalid credentials", 2000)
        }
      })
      .catch(errors => {
        ToastsStore.error("Error: server problem occured", 2000)
      })
      .finally(() => {
        if (typeof callback === 'function') {
          callback()
        }
      })
  };
}

export function makeLoginSuccess(payload) {
  return dispatch => {
    dispatch(setUserData(payload));
  }
}

export function verifyUserExists({ email }, callback = null) {
  return dispatch => {

    Request.post(`auth/forgot/verify/user`, { email })
      .then(result => {

        if (result.data.status == 'success') {
          if (typeof callback === 'function') {
            callback()
          }
        } else {
          ToastsStore.error("Error: user not found", 2000)
        }
      })
      .catch(errors => {
        ToastsStore.error("Error: server problem occured", 2000)
      })
  };
}

export function verifyToken({ email, token, password }, callback = null) {
  return dispatch => {

    Request.post(`auth/forgot/verify/token`, { email, token, password })
      .then(result => {

        if (result.data.status == 'success') {
          if (typeof callback === 'function') {
            callback()
          }

        } else {
          ToastsStore.error("Error: Invalid token", 2000)
        }
      })
      .catch(errors => {
        ToastsStore.error("Error: server problem occured", 2000)
      })
      .finally(() => {
        if (typeof callback === 'function') {
          callback()
        }
      })
  };
}

export function updateUserPassword({ email, token, password }, callback = null) {
  return dispatch => {

    Request.post(`auth/forgot/updatePassword`, { email, token, password })
      .then(result => {

        if (result.data.status == 'success') {

          if (typeof callback === 'function') {
            ToastsStore.success("Success: password is updated", 2000)
            callback()
          }

        } else {
          ToastsStore.error("Error: updating password", 2000)
        }
      })
      .catch(errors => {
        ToastsStore.error("Error: server problem occured", 2000)
      })
      .finally(() => {
        if (typeof callback === 'function') {
          callback()
        }
      })
  };
}