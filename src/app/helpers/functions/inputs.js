import React from 'react';

export const limitMaxLength = (param, { target, target: { value: input } }) => {

    if (typeof param === 'object') {
        if (input > param.max) {
            target.value = param.max;
        }
    } else {
        target.value = input.substring(0, param);
    }
}
