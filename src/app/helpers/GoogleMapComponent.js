import React, {useEffect} from 'react';
import { compose, withProps } from "recompose"
import { withScriptjs, withGoogleMap, GoogleMap, Marker } from "react-google-maps";
const GoogleMapComponent = compose(
  withProps({
    // googleMapURL: "https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyBopMvaMBIhAfJ2oNmV3pLChJeClre1F4w&libraries=geometry,drawing,places",
    loadingElement: <div style={{ height: `100%` }} />,
    containerElement: <div style={{ height: `400px` , marginTop: `10px`, marginBottom: `10px`}} />,
    mapElement: <div style={{ height: `100%` }} />,
  }),
//   withScriptjs,
  withGoogleMap
)(({defaultCenter, defaultZoom, isMarkerShown}) =>
  <GoogleMap
    defaultZoom={defaultZoom}
    zoom={defaultZoom}
    defaultCenter={defaultCenter}
    center={defaultCenter}
  >
    {isMarkerShown && <Marker position={defaultCenter} defaultAnimation={'DROP'}/>}
  </GoogleMap>
)

export default GoogleMapComponent;