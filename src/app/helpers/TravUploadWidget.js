import React from 'react';
import FineUploaderTraditional from 'fine-uploader-wrappers';
import Gallery from 'react-fine-uploader';
import 'react-fine-uploader/gallery/gallery.css';
import { Button, Dialog, DialogTitle, DialogActions, DialogContent } from "@material-ui/core";

const uploader = new FineUploaderTraditional({
    options: {
        chunking: {
            enabled: true
        },
        deleteFile: {
            enabled: true,
            endpoint: `${process.env.REACT_APP_API}utilities/fileuploader/destroy`
        },
        request: {
            endpoint: `${process.env.REACT_APP_API}utilities/fileuploader/upload`
        },
        retry: {
            enableAuto: true
        }
    }
});

const Widget = ({ onCloseHandler, onCompleteHandler }) => {

    uploader.on('complete', onCompleteHandler);

    return (
        <Dialog open={true} contentLabel="Trav Upload Widget" fullWidth>

            <DialogTitle>
                <h4>Trav Gallery Widget</h4>
            </DialogTitle>
            <DialogContent>
                <Gallery uploader={uploader} />
            </DialogContent>
            <DialogActions>
                <Button
                    variant="outlined"
                    color="primary" onClick={onCloseHandler}>Close Upload Widget</Button>
            </DialogActions>
        </Dialog>
    )
}

export default Widget;