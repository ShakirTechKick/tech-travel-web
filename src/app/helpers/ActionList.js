import React from "react";
import { IconButton, Icon } from "@material-ui/core";
import { confirmAlert } from 'react-confirm-alert';
import 'react-confirm-alert/src/react-confirm-alert.css';
import history from '../../history';
const ActionList = (props) => {

    return (
        <div style={{
            display: 'flex',flexDirection: 'row', justifyContent: 'flex-start'
        }}>
            {!!props.del && <IconButton onClick={() => {

                confirmAlert({
                    title: 'Delete Confirm',
                    message: 'Are you sure to do this ?',
                    buttons: [
                        {
                            label: 'Yes',
                            onClick: () => props.deleteEvent(props.id)
                        },
                        {
                            label: 'No',
                            onClick: () => null
                        }
                    ]
                });

            }}>
                <Icon style={{ color: 'tomato', fontWeight: 'bold' }}>delete</Icon>
            </IconButton>}
            {!!props.block && <IconButton onClick={async () => {

                confirmAlert({
                    title: 'Change Status Confirm',
                    message: 'Are you sure to change status ?',
                    buttons: [
                        {
                            label: 'Yes',
                            onClick: () => props.blockEvent(props.id)
                        },
                        {
                            label: 'No',
                            onClick: () => null
                        }
                    ]
                });

            }}>
                <Icon style={{ color: props.status === 'A' ? 'green' : 'red', fontWeight: 'bold' }}>block</Icon>
            </IconButton>}
            {!!props.edit && <IconButton onClick={() => props.editEvent(props.id)}>
                <Icon>edit</Icon>
            </IconButton>}
            {!!props.view && <IconButton onClick={() => history.push(props.view)}>
                <Icon>visibility</Icon>
            </IconButton>}
            {props.children}
        </div>
    );
}

export default ActionList;