/* global google */
import React, { useEffect, useState } from 'react';
import { compose, withProps, lifecycle } from "recompose"
import { withScriptjs, withGoogleMap, GoogleMap, Marker, DirectionsRenderer, InfoWindow } from "react-google-maps";
import moment from 'moment';
import { DirectionsBusRounded, FlightOutlined, DriveEtaOutlined, TrainRounded } from '@material-ui/icons';

const GoogleMapDirectionComponent = compose(
  withProps({
    // googleMapURL: "https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyBopMvaMBIhAfJ2oNmV3pLChJeClre1F4w&libraries=geometry,drawing,places",
    loadingElement: <div style={{ height: `100%` }} />,
    containerElement: <div style={{ width: '100%', height: `100%` }} />,
    mapElement: <div style={{ height: `100%` }} />,
  }),
  //   withScriptjs,
  withGoogleMap
)(({ position, destinations }) => {

  const [directions, setDirections] = useState(null);

  const [iwindows, setIwindows] = useState([]);

  const processDirections = async () => {
    const DirectionsService = new google.maps.DirectionsService();

    const places = [];

    destinations.forEach(item => {
      item.forEach(place => {
        places.push({
          location: (place.location.lat && place.location.lng) ?
            place.location : JSON.parse(place.location),
          stopover: true
        });
      })
    });

    if (places.length >= 2) {

      const origin = places.shift().location;
      const destination = places.pop().location;

      DirectionsService.route({
        origin: origin,
        destination: destination,
        travelMode: google.maps.TravelMode.DRIVING,
        provideRouteAlternatives: false,
        waypoints: places
      }, (result, status) => {
        if (status === google.maps.DirectionsStatus.OK) {
          setDirections(result)
        } else {
          console.error(`error fetching directions ${result}`);
        }
      });
    }
  }

  useEffect(() => {
    processDirections();
  }, [])

  const zoom = 8;
  const center = position;


  const setTransportIcon = (mode) => {
    switch (mode) {
      case 'Car':
        return <DriveEtaOutlined style={{ width: 18 }} />;
      case 'Bus':
        return <DirectionsBusRounded style={{ width: 18 }} />;
      case 'Train':
        return <TrainRounded style={{ width: 18 }} />;
      case 'Flight':
        return <FlightOutlined style={{ width: 18 }} />;
      default:
        return <DriveEtaOutlined style={{ width: 18 }} />;
    }
  }

  const destinationslist = [];

  var order = 0;

  for (var i = 0; i < destinations.length; i++) {
    for (var j = 0; j < destinations[i].length; j++) {
      if (destinationslist.find(d => d.place_id === destinations[i][j].place_id)) {

        const d = Object.assign({}, destinations[i][j]);
        d.orders = [];

        destinationslist.filter(d => d.place_id === destinations[i][j].place_id).forEach(ds => {
          ds.orders.forEach(o => {
            d.orders.push(o);
          })
        });

        d.orders.push({order: order++, day: d.day});

        destinationslist.push(d);

      } else {
        destinationslist.push({ ...destinations[i][j], orders: [{order: order++, day: destinations[i][j].day}] });
      }
    }

  }

  destinationslist.forEach(d => {
    if (d.orders.length > 1) {
      destinationslist.filter(item => item.orders.length < d.orders.length && item.place_id === d.place_id && !item.nolabel).forEach(ds => {
        destinationslist.splice(destinationslist.findIndex(m => m.place_id === d.place_id), 1, { ...d, nolabel: true })
      })
    }
  })


  return (<GoogleMap
    defaultZoom={zoom} zoom={zoom}
    defaultCenter={center} center={center}
  >
    {
      directions &&
      <DirectionsRenderer directions={directions} defaultOptions={{
        markerOptions: { visible: !true }
      }} />
    }

    {
      (destinationslist && destinationslist.length) &&
      destinationslist.map((p, key) => {

        if (!p.nolabel) {

          var label = ``; var stamp = ``;

          p.orders.forEach((dorder, key) => {

            if(dorder.day && dorder.day.datestamp){
              stamp = stamp + moment(dorder.day.datestamp).format('YYYY MMM DD');
            }

            if (key === 0) {
              label = label + `Day ${dorder.order + 1}`;
            } else {
              label = label + `${dorder.order + 1}`;
            }

            if (key !== p.orders.length - 1) {
              label = label + ' ,';
              stamp = stamp + ' ,';
            }

          })

          if (p.location) {
            return <Marker
              key={`marker-${key}`}
              title={p.name} name={p.name}
              animation={google.maps.Animation.BOUNCE} onMouseOver={() => setIwindows([...iwindows, p.place_id])} onMouseOut={() => setIwindows([...iwindows.filter(id => id !== p.place_id)])}
              position={JSON.parse(p.location)} label={{ text: label, color: "rgb(237, 72, 50)", fontWeight: "bold" }} icon={{ url: "http://maps.google.com/mapfiles/ms/icons/red-dot.png" }}>

              {!!iwindows.find(id => id === p.place_id) && <InfoWindow
                key={`infowindow-${key}`} onCloseClick={() => setIwindows([...iwindows.filter(id => id !== p.place_id)])}
                visible={true}>
                <div>
                  <div style={{ ['font-weight']: 'bold' }}>({label}) {p.name}</div>
                  <div style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center' }}><span>Reached Location by</span> {setTransportIcon(p.transport ? p.transport.mode : 'Car')}</div>
                  <small>{stamp}</small>
                </div>
              </InfoWindow>
              }
            </Marker>
          }
        }
      })
    }

  </GoogleMap>)
})

export default GoogleMapDirectionComponent;