import React from 'react';

export default ({ children, styles = {} }) => {
    return (
        <div style={{ display: 'flex', justifyContent: 'space-between', flexWrap: 'wrap', alignItems: 'center' , ...styles}}>
            {children}
        </div>
    )
}