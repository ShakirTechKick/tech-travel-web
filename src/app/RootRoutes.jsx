import React from "react";
import { Redirect } from "react-router-dom";

import dashboardRoutes from "./views/dashboard/DashboardRoutes";
import sessionRoutes from "./views/sessions/SessionRoutes";
import userRoutes from "./views/modules/users/Users.routes";
import rolesRoutes from "./views/modules/roles/Roles.routes";
import companyRoutes from "./views/modules/company/Company.routes";
import placesRoutes from "./views/modules/places/Places.routes";
import hotelsRoutes from "./views/modules/hotels/Hotels.routes";
import hotelStandardsRoutes from "./views/modules/hotels/standards/Standards.routes";
import hotelCategoriesRoutes from "./views/modules/hotels/categories/Categories.routes";
import hotelRoomsRoutes from "./views/modules/hotels/rooms/Rooms.routes";
import hotelMealsRoutes from "./views/modules/hotels/meals/Meals.routes";
import attractionsRoutes from "./views/modules/attractions/Attractions.routes";
import excursionsRoutes from "./views/modules/excursions/Excursions.routes";
import activitiesRoutes from "./views/modules/activities/Activities.routes";
import builderRoutes from "./views/modules/itinerary/Itinerary.routes";

import clientsRoutes from "./views/modules/clients/Clients.routes";
import driversRoutes from "./views/modules/drivers/Drivers.routes";
import guidesRoutes from "./views/modules/guides/Guides.routes";
import vCategoriesRoutes from "./views/modules/vehicle_categories/VehicleCategories.routes";
import vehiclesRoutes from "./views/modules/vehicles/Vehicles.routes";
import tripRoutes from "./views/modules/trips/Trips.routes";


import documentViewRoutes from "./views/public/document/DocumentView.routes";
import profileSettingRoutes from "./views/modules/profiles/ProfileSetting.routes";

const errorRoutes = [
  {
    component: () => <Redirect to="/session/404" />,
    access: 'COMMON'
  }
];

const routes = [
  ...sessionRoutes,
  ...dashboardRoutes,
  ...userRoutes,
  ...rolesRoutes,
  ...companyRoutes,
  ...placesRoutes,
  ...hotelsRoutes,
  ...hotelStandardsRoutes,
  ...hotelCategoriesRoutes,
  ...hotelRoomsRoutes,
  ...hotelMealsRoutes,
  ...attractionsRoutes,
  ...excursionsRoutes,
  ...activitiesRoutes,
  ...builderRoutes,
  ...clientsRoutes,
  ...driversRoutes,
  ...guidesRoutes,
  ...vCategoriesRoutes,
  ...vehiclesRoutes,
  ...tripRoutes,
  ...errorRoutes
];



const common = [
  ...documentViewRoutes,
  ...profileSettingRoutes
];

export default { routes, common  };
