import React, { Component, Fragment } from "react";
import { withRouter, Redirect } from "react-router-dom";
import { connect } from "react-redux";
import AppContext from "app/appContext";
import history from "../../history";

class AuthGuard extends Component {
  constructor(props, context) {
    super(props);
    this.state = {
      authenticated: props.user.authenticated
    };
  }


  componentDidMount() {

    const { location } = this.props;

    const is_public_path = (location.pathname.indexOf('public') !== -1);

    if (!this.state.authenticated && !is_public_path) {
      history.push({
        pathname: "/session/signin",
        state: { redirectUrl: '/dashboard' }
      });
    }
  }

  componentDidUpdate() {
    const { location } = this.props;

    const is_public_path = (location.pathname.indexOf('public') !== -1);

    if (!this.state.authenticated && !is_public_path) {
      history.push({
        pathname: "/session/signin",
        state: { redirectUrl: '/dashboard' }
      });
    }
  }

  shouldComponentUpdate(nextProps, nextState) {
    return nextState.authenticated !== this.state.authenticated;
  }


  render() {
    let { children } = this.props;
    return <Fragment>{children}</Fragment>;
  }
}

AuthGuard.contextType = AppContext;

const mapStateToProps = state => ({
  user: state.user
});

export default withRouter(connect(mapStateToProps)(AuthGuard));
