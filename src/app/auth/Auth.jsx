import React, { Component, Fragment } from "react";
import { connect } from "react-redux";
import { PropTypes } from "prop-types";
import { makeLoginSuccess} from "../redux/actions/LoginActions";

class Auth extends Component {
  state = {};

  static checkIfAuthenticated(props) {
    const user = sessionStorage.getItem('user');
    if (user) {
      props.makeLoginSuccess(JSON.parse(user));
    }
  }

  constructor(props) {
    super(props);
    Auth.checkIfAuthenticated(props);
  }

  render() {
    const { children } = this.props;
    return <Fragment>{children}</Fragment>;
  }
}

const mapStateToProps = state => ({
  makeLoginSuccess: PropTypes.func.isRequired,
  user: state.user
});

export default connect(
  mapStateToProps,
  { makeLoginSuccess }
)(Auth);
