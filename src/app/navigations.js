export const navigations = [
  {
    name: "Dashboard",
    path: "/dashboard",
    icon: "dashboard",
    order: 1
  },
  {
    name: "Itinerary",
    path: "/itinerary/builder",
    icon: "dashboard",
    order: 2
  },
  {
    name: "Clients",
    path: "/clients",
    icon: "dashboard",
    order: 3
  },
  {
    name: "Administration",
    icon: "dashboard",
    order: 4,
    children: [
      {
        name: "Users",
        path: "/users",
        order: 1
      },
      {
        name: "Roles",
        path: "/roles",
        order: 2
      },
      {
        name: "Settings",
        path: "/settings",
        order: 3
      }

    ]
  },
  {
    name: "Library",
    icon: "dashboard",
    order: 5,
    children: [
      {
        name: "Places",
        path: "/library/places",
        order: 1
      },
      {
        name: "Hotels",
        path: "/library/hotels",
        order: 2
      },
      {
        name: "Attractions",
        path: "/library/attractions",
        order: 3
      },
      {
        name: "Excursions",
        path: "/library/excursions",
        order: 4
      },
      {
        name: "Activities",
        path: "/library/activities",
        order: 5
      }
    ]
  },
  {
    name: "Transport",
    icon: "dashboard",
    order: 6,
    children: [
      {
        name: "Drivers",
        path: "/transport/drivers",
        order: 1
      },
      {
        name: "Guides",
        path: "/transport/guides",
        order: 2
      },
      {
        name: "Vehicle Categories",
        path: "/transport/vehicle-categories",
        order: 3
      },
      {
        name: "Vehicles",
        path: "/transport/vehicles",
        order: 4
      }
    ]
  },
  {
    name: "Trips",
    icon: "dashboard",
    path: "/trips",
    order: 7
  }
];
