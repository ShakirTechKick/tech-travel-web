import React, { Component, Fragment } from "react";
import Scrollbar from "react-perfect-scrollbar";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import PropTypes from "prop-types";

import { navigations } from "../../navigations";
import { EgretVerticalNav } from "egret";
import { setLayoutSettings } from "app/redux/actions/LayoutActions";
import { unionBy } from "lodash";

class Sidenav extends Component {

  state = {};

  static navigationLinks = [];

  componentWillMount() {
    Sidenav.navigationLinks = [];
    const { user } = this.props.user;
    const permittedModules = user ? user.permissions : [];

    permittedModules.forEach(module => {
      let module_permitted = Object.keys(module)[0];
      let permissions = module[module_permitted];
      if (permissions["read"]) {
        if (permissions["read"] === "ALLOWED") {

          navigations.forEach(nav => {
            if (nav.children) {
              if (!Sidenav.navigationLinks.find(n => n.name == nav.name)) {
                Sidenav.navigationLinks.push({ ...nav, children: [] });
              }

              if (nav.children.find(nav => nav.name == module_permitted)) {
                Sidenav.navigationLinks.find(n => n.name == nav.name).children.push(nav.children.find(nav => nav.name == module_permitted));
              }

            } else {
              Sidenav.navigationLinks.push(navigations.find(nav => nav.name == module_permitted));
            }
          })
        }
      }
    });


    Sidenav.navigationLinks.sort((a, b) => {
      if (a.children) {
        a.children.sort((ab, bb) => {
          return ab.order - bb.order
        })
      }

      return a.order - b.order
    })
  }

  updateSidebarMode = sidebarSettings => {
    let { settings, setLayoutSettings } = this.props;
    let activeLayoutSettingsName = settings.activeLayout + "Settings";
    let activeLayoutSettings = settings[activeLayoutSettingsName];

    setLayoutSettings({
      ...settings,
      [activeLayoutSettingsName]: {
        ...activeLayoutSettings,
        leftSidebar: {
          ...activeLayoutSettings.leftSidebar,
          ...sidebarSettings
        }
      }
    });
  };

  renderOverlay = () => (
    <div
      onClick={() => this.updateSidebarMode({ mode: "close" })}
      className="sidenav__overlay"
    />
  );
  render() {
    return (
      <Fragment>
        <Scrollbar option={{ suppressScrollX: true }} className="scrollable position-relative">
          {this.props.children}
          <EgretVerticalNav navigation={unionBy(Sidenav.navigationLinks, 'name').filter(a => typeof a !== 'undefined')} />
        </Scrollbar>
        {this.renderOverlay()}
      </Fragment>
    );
  }
}
Sidenav.propTypes = {
  setLayoutSettings: PropTypes.func.isRequired,
  settings: PropTypes.object.isRequired
};
const mapStateToProps = state => ({
  setLayoutSettings: PropTypes.func.isRequired,
  settings: state.layout.settings,
  user: state.user
});
export default withRouter(
  connect(
    mapStateToProps,
    {
      setLayoutSettings
    }
  )(Sidenav)
);