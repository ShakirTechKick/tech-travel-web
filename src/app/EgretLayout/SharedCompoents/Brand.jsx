import React, { Component } from "react";
import parent from '../../../assets/icons/Parent.png'

class Brand extends Component {
  state = {};
  render() {
    return (
      <div className="flex flex-middle flex-space-between brand-area">
        <div className="flex flex-middle brand">
          <img src={parent} alt="parent-logo-company" />
          <span className="brand__text">Tech Travel</span>
        </div>
        {this.props.children}
      </div>
    );
  }
}

export default Brand;
