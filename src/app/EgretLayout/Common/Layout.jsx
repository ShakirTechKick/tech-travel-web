import React, { Component } from "react";
import { connect } from "react-redux";
import { Helmet } from "react-helmet";
import AppContext from "app/appContext";
import { Route, Router } from "react-router-dom";
import history from "history.js";
class Layout extends Component {

  render() {
    return (

      <AppContext.Consumer>
        {({ common }) => (


          <React.Fragment>
            <Helmet>
              <link rel="stylesheet" type="text/css" charset="UTF-8" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.css" />
              <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick-theme.min.css" />
              <style type="text/css">
                {`
                  html, body {
                      overflow: hidden !important;
                  }
                  body ::-webkit-scrollbar{width: 2px !important;}
                  div#root{overflow-y: scroll !important;}
                  .MuiBox-root{padding : 0 !important;}
                  .MuiCardContent-root{padding: 0 !important;}
                  div.map-view-button{right: 15px !important;}
              `}
              </style>
            </Helmet>
            <div className='document-root'>
              <Router history={history}>
                {
                  common.map(props => (
                    <Route {...props} />
                  ))
                }
              </Router>
            </div>
          </React.Fragment >


        )}
      </AppContext.Consumer>
    )
  }
}


const mapStateToProps = state => ({
});

export default connect(mapStateToProps, null)(Layout)