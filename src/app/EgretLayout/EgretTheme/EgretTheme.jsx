import React, { Component } from "react";
import { MuiThemeProvider } from "@material-ui/core";
import { connect } from "react-redux";
import { PropTypes } from "prop-types";
import { setLayoutSettings } from "app/redux/actions/LayoutActions";

class EgretTheme extends Component {

  render() {
    const { children, settings } = this.props;
    const styles = { ...settings.themes[settings.activeTheme] };

    const custom = {
      ...styles,
      palette: {
        ...styles.palette, primary: { ...styles.palette.primary, main: 'rgb(135,175,200)', light: 'rgb(135,175,250)', dark: 'rgb(135,175,175)' },
        secondary: { ...styles.palette.secondary, main: 'rgb(135,175,200)', light: 'rgb(135,175,250)', dark: 'rgb(135,175,175)' },
      }
    }

    return (
      <MuiThemeProvider theme={custom}>
        {children}
      </MuiThemeProvider>
    );
  }
}

EgretTheme.propTypes = {
  setLayoutSettings: PropTypes.func.isRequired,
  settings: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  settings: state.layout.settings,
  setLayoutSettings: PropTypes.func.isRequired
});

export default connect(
  mapStateToProps,
  { setLayoutSettings }
)(EgretTheme);
