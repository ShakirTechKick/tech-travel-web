import React, { Component } from "react";
import { EgretHorizontalNav } from "egret";
import { navigations } from "../../navigations";
import { withStyles, MuiThemeProvider } from "@material-ui/core";
import { PropTypes } from "prop-types";
import { connect } from "react-redux";
import { Helmet } from "react-helmet";
import { unionBy } from "lodash";

class Layout2Navbar extends Component {
  static navigationLinks = [];

  componentWillMount() {
    Layout2Navbar.navigationLinks = [];
    const { user } = this.props.user;
    const permittedModules = user ? user.permissions : [];

    permittedModules.forEach(module => {
      let module_permitted = Object.keys(module)[0];
      let permissions = module[module_permitted];
      if (permissions["read"]) {
        if (permissions["read"] === "ALLOWED") {

          navigations.forEach(nav => {
            if (nav.children) {
              if (!Layout2Navbar.navigationLinks.find(n => n.name == nav.name)) {
                Layout2Navbar.navigationLinks.push({ ...nav, children: [] });
              }

              if (nav.children.find(nav => nav.name == module_permitted)) {
                Layout2Navbar.navigationLinks.find(n => n.name == nav.name).children.push(nav.children.find(nav => nav.name == module_permitted));
              }

            } else {
              Layout2Navbar.navigationLinks.push(navigations.find(nav => nav.name == module_permitted));
            }
          })
        }
      }
    });


    Layout2Navbar.navigationLinks.sort((a, b) => {
      if (a.children) {
        a.children.sort((ab, bb) => {
          return ab.order - bb.order
        })
      }

      return a.order - b.order
    })
  }

  state = {};
  render() {
    let { theme, settings } = this.props;
    const navbarTheme =
      settings.themes[settings.layout2Settings.navbar.theme] || theme;
    return (
      <MuiThemeProvider theme={navbarTheme}>
        <Helmet>
          <style>
            {`.horizontal-nav a, 
              .horizontal-nav label {
                color: ${navbarTheme.palette.primary.contrastText};
              }
              .navbar,
              .horizontal-nav ul ul {
                background: ${navbarTheme.palette.primary.main};
              }
              .horizontal-nav ul li ul li:hover,
              .horizontal-nav ul li ul li.open {
                background: ${navbarTheme.palette.primary.dark};
              }
            `}
          </style>
        </Helmet>
        <div className="navbar">
          <div className="container">
            <EgretHorizontalNav navigation={unionBy(Layout2Navbar.navigationLinks, 'name').filter(a => typeof a !== 'undefined')} max={6} />
          </div>
        </div>
      </MuiThemeProvider>
    );
  }
}

Layout2Navbar.propTypes = {
  settings: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  settings: state.layout.settings,
  user: state.user
});

export default withStyles({}, { withTheme: true })(
  connect(
    mapStateToProps,
    {}
  )(Layout2Navbar)
);
