import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import {
  Icon,
  IconButton,
  Badge,
  MenuItem,
  withStyles,
  MuiThemeProvider,
  Button
} from "@material-ui/core";
import { connect } from "react-redux";
import { setLayoutSettings } from "app/redux/actions/LayoutActions";
import { logoutUser } from "app/redux/actions/UserActions";
import { PropTypes } from "prop-types";
import { EgretMenu, EgretSearchBox } from "egret";
import { isMdScreen } from "utils";
import NotificationBar from "../SharedCompoents/NotificationBar";
import { Link } from "react-router-dom";
import history from '../../../history';
import { AssignmentTurnedInOutlined } from "@material-ui/icons";

const styles = theme => ({
  root: {
    backgroundColor: theme.palette.primary.main
  }
});

const Layout1Topbar = (props) => {


  const updateSidebarMode = sidebarSettings => {
    let { settings, setLayoutSettings } = props;

    setLayoutSettings({
      ...settings,
      layout1Settings: {
        ...settings.layout1Settings,
        leftSidebar: {
          ...settings.layout1Settings.leftSidebar,
          ...sidebarSettings
        }
      }
    });
  };

  const CheckPermission = (mod, type) => {

    var module_exists = false;
    var action_permission = false;

    const { user } = props.user;

    if (user) {

      user.permissions.forEach(module => {
        if (Object.keys(module)[0] === mod) {
          module_exists = true;
          if (module[mod][type] === "ALLOWED") {
            action_permission = true;
          }
        }
      });

      if (module_exists && action_permission) {
        return true;
      }

    }

    return false;

  }

  const handleSidebarToggleAction = () => {
    let { settings } = props;
    let { layout1Settings } = settings;

    let mode;
    if (isMdScreen()) {
      mode = layout1Settings.leftSidebar.mode === "close" ? "mobile" : "close";
    } else {
      mode = layout1Settings.leftSidebar.mode === "full" ? "close" : "full";
    }
    updateSidebarMode({ mode });
  };

  const handleSignOut = () => {
    props.logoutUser();
  };


  let { user, theme, settings } = props;
  const topbarTheme =
    settings.themes[settings.layout1Settings.topbar.theme] || theme;

  var avatar = null;

  if (user['user']) {
    avatar = user['user'].data.avatar;
  }


  return (
    <MuiThemeProvider theme={topbarTheme}>
      <div className="topbar">
        <div
          className={`topbar-hold`}
          style={{ backgroundColor: topbarTheme.palette.primary.main }}
        >
          <div className="flex flex-space-between flex-middle h-100">
            <div className="flex">
              <IconButton onClick={handleSidebarToggleAction}>
                <Icon>menu</Icon>
              </IconButton>

              {
                !!CheckPermission('Itinerary', 'read') &&

                <Button variant='text' color='inherit' onClick={() => history.push('/itinerary/builder')}>
                  <AssignmentTurnedInOutlined /> Itinerary
                  </Button>
              }


            </div>
            <div className="flex flex-middle">
              <EgretSearchBox />
              <NotificationBar user={props.user} />
              <EgretMenu menuButton={
                <img className="mx-8 text-middle circular-image-small cursor-pointer" alt="user avatar"
                  src={avatar ? process.env.REACT_APP_STATIC + avatar : "/assets/images/face-7.jpg"} />
              }>
                <MenuItem style={{ minWidth: 185 }}>
                  <Link className="flex flex-middle" to="/dashboard">
                    <Icon> home </Icon>
                    <span className="pl-16"> Home </span>
                  </Link>
                </MenuItem>
                <MenuItem style={{ minWidth: 185 }}>
                  <Link
                    className="flex flex-middle"
                    to="/profile/setting"
                  >
                    <Icon> person </Icon>
                    <span className="pl-16"> Profile Setting</span>
                  </Link>
                </MenuItem>
                <MenuItem
                  onClick={handleSignOut}
                  className="flex flex-middle"
                  style={{ minWidth: 185 }}
                >
                  <Icon> power_settings_new </Icon>
                  <span className="pl-16"> Logout </span>
                </MenuItem>
              </EgretMenu>
            </div>
          </div>
        </div>
      </div>
    </MuiThemeProvider>
  );
}

Layout1Topbar.propTypes = {
  setLayoutSettings: PropTypes.func.isRequired,
  logoutUser: PropTypes.func.isRequired,
  settings: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  setLayoutSettings: PropTypes.func.isRequired,
  logoutUser: PropTypes.func.isRequired,
  settings: state.layout.settings,
  user: state.user
});

export default withStyles(styles, { withTheme: true })(
  withRouter(
    connect(
      mapStateToProps,
      { setLayoutSettings, logoutUser }
    )(Layout1Topbar)
  )
);
