import React, { Component, Fragment, useRef, useState, useEffect } from "react";
import { ValidatorForm, TextValidator, SelectValidator } from "react-material-ui-form-validator";
import { Button, MenuItem, Grid, makeStyles, Switch } from "@material-ui/core";
import CameraIcon from '@material-ui/icons/CameraAltTwoTone';
import { Breadcrumb } from "egret";
import { withStyles } from "@material-ui/styles";
import { useDispatch, useSelector } from "react-redux";
import { updateUser } from "app/redux/actions/modules/Users.actions";

import { Divider } from "semantic-ui-react";
import { ToastsStore } from "react-toasts";
import Widget from 'app/helpers/TravUploadWidget';


const ProfileSetting = (props) => {

    const dispatch = useDispatch();

    const { user: auth_user } = useSelector(state => state.user);

    const [isUploadOpen, setUploadOpen] = useState(false);

    const [user, setUser] = useState({ ...auth_user.data });

    ValidatorForm.addValidationRule('isPasswordMatch', (value) => {
        if (user.repassword !== user.password) {
            return false;
        }
        return true;
    });

    useEffect(() => {
        return () => {
            ValidatorForm.removeValidationRule('isPasswordMatch');
        }
    }, [])


    const updateUserSetting = () => {
        dispatch(updateUser({ ...user }, () => {
            setUser({ ...user, change_password: false });
        }, true))
    }

    const onChangeField = (e) => {
        setUser({ ...user, [e.target.name]: e.target.value });
    }


    const onCompleteHandler = (id, name, response) => {
        if (response.success) {
            setUser({ ...user, avatar: `${response.path}/${name}` });
        }
    }



    return (
        <div className="analytics m-sm-30">
            <div className="mb-sm-30">
                <Breadcrumb
                    routeSegments={[
                        { name: "Dashboard", path: "/dashboard" },
                        { name: "Profile Setting" }
                    ]}
                />
            </div>
            <div className="mb-sm-30">

                <ValidatorForm onSubmit={updateUserSetting}>
                    <Grid container spacing={6}>
                        <div onClick={() => setUploadOpen(true)}>
                            {
                                !!user.avatar ?
                                    <div className="logo-container" style={{ backgroundImage: `url(${process.env.REACT_APP_STATIC + user.avatar})`, backgroundSize: 'cover' }}></div>
                                    : <div className="logo-container"><CameraIcon /></div>
                            }
                        </div>
                    </Grid>

                    <Grid container spacing={6}>
                        <Grid md={4} sm={12} xs={12} item>
                            <TextValidator
                                className="mb-16 w-100"
                                variant="outlined"
                                label="Name"
                                type="text"
                                value={user.name ? user.name : ``}
                                onChange={onChangeField}
                                name="name"
                                validators={["required"]}
                                errorMessages={[
                                    "Name is required"
                                ]}
                            />
                        </Grid>
                        <Grid md={4} sm={12} xs={12} item>
                            <TextValidator
                                className="mb-16 w-100"
                                variant="outlined"
                                label="Email ID"
                                type="text"
                                value={user.email ? user.email : ``}
                                onChange={onChangeField}
                                name="email"
                                validators={["required", "isEmail"]}
                                errorMessages={[
                                    "Email is required",
                                    "Email is not valid"
                                ]}
                            />
                        </Grid>
                        <Grid md={4} sm={12} xs={12} item>
                            <TextValidator
                                className="mb-16 w-100"
                                variant="outlined"
                                label="Phone Number"
                                type="text"
                                value={user.phone_number ? user.phone_number : ``}
                                onChange={onChangeField}
                                name="phone_number"
                                validators={["required"]}
                                errorMessages={[
                                    "Phone number is required"
                                ]}
                            />
                        </Grid>

                    </Grid>
                    <Grid container spacing={6}>
                        <Grid md={12} sm={12} xs={12} item>
                            <Switch name='change_password' checked={user.change_password ? user.change_password : false}
                                onChange={(e) => setUser({ ...user, change_password: e.target.checked, password: null })} /> Change Password ?
                        </Grid>
                    </Grid>
                    <Grid container spacing={6}>
                        {
                            user.change_password &&

                            <React.Fragment>
                                <Grid md={6} sm={12} xs={12} item>
                                    <TextValidator
                                        className="mb-16 w-100"
                                        variant="outlined"
                                        label="Password"
                                        onChange={onChangeField}
                                        name="password"
                                        type="password"
                                        validators={['required']}
                                        errorMessages={['Password is required']}
                                        value={user.password}
                                    />
                                </Grid>
                                <Grid md={6} sm={12} xs={12} item>

                                    <TextValidator
                                        className="mb-16 w-100"
                                        variant="outlined"
                                        label="Repeat password"
                                        name="repassword"
                                        type="password"
                                        validators={['isPasswordMatch', 'required']}
                                        errorMessages={['password mismatch', 'Repeat Password is required']}
                                        onChange={onChangeField}
                                        value={user.repassword}
                                    />
                                </Grid>
                            </React.Fragment>
                        }
                    </Grid>


                    <Button
                        variant="contained"
                        color="primary" style={{ ['margin-top']: user.change_password ? 0 : 50 }}
                        type="submit">Update Profile Setting</Button>

                </ValidatorForm>
            </div>

            {isUploadOpen &&
                <Widget onCloseHandler={() => setUploadOpen(!isUploadOpen)} onCompleteHandler={onCompleteHandler} />
            }

        </div >
    );
}

export default withStyles({}, { withTheme: true })(ProfileSetting);
