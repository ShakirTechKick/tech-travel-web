import { EgretLoadable } from "egret";
import { authRoles } from "../../../auth/authRoles";

const ProfileSetting = EgretLoadable({
  loader: () => import("./ProfileSetting.component")
});

const profileRoutes = [
  {
    name: 'Profile Setting',
    path: "/profile/setting",
    exact: true,
    component: ProfileSetting,
    auth: authRoles.admin,
    access: 'PUBLIC'
  }
];

export default profileRoutes;
