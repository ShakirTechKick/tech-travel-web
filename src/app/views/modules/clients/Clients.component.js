import React, { useState, useEffect } from "react";
import { ValidatorForm, TextValidator } from "react-material-ui-form-validator";
import { Button, Grid, Table, TableRow, TableCell } from "@material-ui/core";
import { Breadcrumb } from "egret";
import { withStyles } from "@material-ui/styles";
import { useDispatch, useSelector } from "react-redux";
import * as ClientActions from "../../../redux/actions/modules/Clients.action";
import TableMD from "mui-datatables";
import ActionList from "app/helpers/ActionList";
import { Dropdown, Label } from "semantic-ui-react";
import moment from "moment";
import FlexRow from "app/helpers/FlexRow";
import history from '../../../../history';

const Clients = (props) => {

    const dispatch = useDispatch();

    const { user: auth_user } = useSelector(state => state.user);
    const { list } = useSelector(state => state.clients);

    const [client, setClient] = useState({
        salutation: 'Mr.', company_id: auth_user.data.company_id
    });


    const [view, setView] = useState('ManageView');

    useEffect(() => {
        dispatch(ClientActions.fetchClients(auth_user.data.company_id));
    }, [])

    const CheckPermission = (type) => {

        var module_exists = false;
        var action_permission = false;

        auth_user.permissions.forEach(module => {
            if (Object.keys(module)[0] === "Clients") {
                module_exists = true;
                if (module['Clients'][type] === "ALLOWED") {
                    action_permission = true;
                }
            }
        });

        if (module_exists && action_permission) {
            return true;
        }

        return false;

    }

    const deleteEvent = (id) => {
        dispatch(ClientActions.removeClient({ client_id: id }))
    };

    const editEvent = (id) => {
        setClient(list.find(c => c.client_id === id));
        setView('EditView');
    };

    const blockEvent = (id) => {
        dispatch(ClientActions.statusClient({ client_id: id }))
    };

    const changeView = (v) => {
        if (v === 'AddView') {
            setClient({ salutation: 'Mr.', company_id: auth_user.data.company_id });
        }
        setView(v)
    }

    const saveClient = () => {


        if (view === 'AddView') {
            dispatch(ClientActions.addClient({ ...client }, () => {
                changeView('ManageView')
            }));
        } else {
            dispatch(ClientActions.updateClient({ ...client }, () => {
                changeView('ManageView')
            }));
        }
    }
    const onChangeField = (e) => {
        setClient({ ...client, [e.target.name]: e.target.value });
    }


    const clients_list = list.sort((a, b) => b.client_id - a.client_id).map(c => ([
        c.client_id, (c.salutation ? c.salutation : ``) + c.name, c.phone_number, c.country,
        <ActionList del={CheckPermission("delete")} edit={CheckPermission("edit")} block={CheckPermission("edit")} id={c.client_id} deleteEvent={deleteEvent} blockEvent={blockEvent}
            editEvent={editEvent} status={c.status} view={`/clients/view/${c.client_id}`} />
    ]));


    const headers = ['Client ID', 'Name', 'Phone Number', 'Country', 'Action'];

    const { path, params } = props.match ? props.match : { path: null, params: null };

    return (
        <div className="analytics m-sm-30">

            {!props.modal &&
                <div className="mb-sm-30">
                    <Breadcrumb
                        routeSegments={[
                            { name: "Dashboard", path: "/dashboard" },
                            { name: "Clients" }
                        ]}
                    />
                </div>
            }

            {(path === '/clients/view/:id') ?

                list.find(c => c.client_id == params.id) ?
                    <Table style={{ border: '.5px solid' }}>
                        <TableRow>
                            <TableCell colspan="2" style={{ padding: '15px' }}>
                                <FlexRow>
                                    <Button variant='outlined' onClick={() => history.push('/clients')}>Back</Button>
                                    <ActionList del={CheckPermission("delete")} block={CheckPermission("edit")}
                                        id={list.find(c => c.client_id == params.id).client_id} deleteEvent={deleteEvent} blockEvent={blockEvent}
                                        status={list.find(c => c.client_id == params.id).status} />
                                </FlexRow>
                            </TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell style={{ padding: '15px' }}>Client Name</TableCell>
                            <TableCell>{list.find(c => c.client_id == params.id).salutation + list.find(c => c.client_id == params.id).name}</TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell style={{ padding: '15px' }}>Email</TableCell>
                            <TableCell>{list.find(c => c.client_id == params.id).email}</TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell style={{ padding: '15px' }}>Country</TableCell>
                            <TableCell>{list.find(c => c.client_id == params.id).country}</TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell style={{ padding: '15px' }}>Phone Number</TableCell>
                            <TableCell>{list.find(c => c.client_id == params.id).phone_number}</TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell style={{ padding: '15px' }}>Status</TableCell>
                            <TableCell>{list.find(c => c.client_id == params.id).status === 'A' ? 'Active' : 'Blocked'}</TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell style={{ padding: '15px' }}>Created On</TableCell>
                            <TableCell>{moment(list.find(c => c.client_id == params.id).createdAt).format('YYYY MMM DD, hh:mm:ss a ddd')}</TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell style={{ padding: '15px' }}>Last Updated On</TableCell>
                            <TableCell>{moment(list.find(c => c.client_id == params.id).updatedAt).format('YYYY MMM DD, hh:mm:ss a ddd')}</TableCell>
                        </TableRow>
                    </Table> : <div className="no-matches">No matched item found !</div> :

                <React.Fragment>
                    <div className="mb-sm-30">
                        {
                            view === 'ManageView' &&
                            <Button
                                onClick={changeView.bind(null, 'AddView')}
                                variant="outlined"
                                color="primary">Add Client</Button>
                        }
                        {
                            (view === 'AddView' || view === 'EditView') &&
                            <Button
                                variant="outlined"
                                onClick={changeView.bind(null, 'ManageView')}
                                color="primary">All Clients</Button>
                        }
                    </div>
                    {
                        view === 'ManageView' &&

                        <TableMD
                            columns={headers}
                            title="Clients"
                            data={clients_list}
                            options={{
                                responsive: 'scrollMaxHeight',
                                selectableRows: !props.modal
                            }}
                        />
                    }

                    {(view === 'AddView' || view == 'EditView') &&
                        <ValidatorForm onSubmit={saveClient}>

                            <Grid container spacing={6}>
                                <Grid md={2} sm={4} xs={4} item>
                                    <Dropdown
                                        placeholder='Salutation'
                                        fluid
                                        selection
                                        value={client.salutation ? client.salutation : `Mr.`}
                                        onChange={(event, { value }) => setClient({ ...client, salutation: value })}
                                        options={['Mr.', 'Mrs.', 'Ms.', 'Rev.'].map((m, index) => {
                                            return { key: index, text: m, value: m }
                                        })}
                                    />
                                </Grid>
                                <Grid md={4} sm={8} xs={8} item>
                                    <TextValidator
                                        className="mb-16 w-100"
                                        variant="outlined"
                                        label="Name"
                                        type="text"
                                        value={client.name ? client.name : ``}
                                        onChange={onChangeField}
                                        name="name"
                                        validators={["required"]}
                                        errorMessages={[
                                            "Name is required"
                                        ]}
                                    />
                                </Grid>
                                <Grid md={6} sm={12} xs={12} item>
                                    <TextValidator
                                        className="mb-16 w-100"
                                        variant="outlined"
                                        label="Country"
                                        type="text"
                                        value={client.country ? client.country : ``}
                                        onChange={onChangeField}
                                        name="country"
                                        validators={["required"]}
                                        errorMessages={[
                                            "Country is required"
                                        ]}
                                    />
                                </Grid>
                            </Grid>
                            <Grid container spacing={6}>
                                <Grid md={6} sm={12} xs={12} item>
                                    <TextValidator
                                        className="mb-16 w-100"
                                        variant="outlined"
                                        label="Email ID"
                                        type="text"
                                        value={client.email ? client.email : ``}
                                        onChange={onChangeField}
                                        name="email"
                                        validators={["required", "isEmail"]}
                                        errorMessages={[
                                            "Email is required",
                                            "Email is not valid"
                                        ]}
                                    />
                                </Grid>
                                <Grid md={6} sm={12} xs={12} item>
                                    <TextValidator
                                        className="mb-16 w-100"
                                        variant="outlined"
                                        label="Phone Number"
                                        type="text"
                                        value={client.phone_number ? client.phone_number : ``}
                                        onChange={onChangeField}
                                        name="phone_number"
                                    />
                                </Grid>
                            </Grid>

                            <Button
                                variant="contained"
                                color="primary"
                                type="submit">{view === 'AddView' ? 'Save' : 'Update'} Client</Button>

                        </ValidatorForm>
                    }
                </React.Fragment>
            }
        </div >
    );
}

export default withStyles({}, { withTheme: true })(Clients);
