import { EgretLoadable } from "egret";
import { authRoles } from "../../../auth/authRoles";

const Clients = EgretLoadable({
    loader: () => import("./Clients.component")
});

const clientsRoutes = [
    {
        name: "Clients",
        path: "/clients",
        exact: true,
        component: Clients,
        auth: authRoles.admin,
        access: 'AUTH'
    },
    {
        name: "Clients",
        path: "/clients/view/:id",
        exact: true,
        component: Clients,
        auth: authRoles.admin,
        access: 'AUTH'
    }
];

export default clientsRoutes;
