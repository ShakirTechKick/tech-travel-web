import { EgretLoadable } from "egret";
import { authRoles } from "../../../auth/authRoles";

const Drivers = EgretLoadable({
    loader: () => import("./Drivers.component")
});

const driversRoutes = [
    {
        name: "Drivers",
        path: "/transport/drivers",
        exact: true,
        component: Drivers,
        auth: authRoles.admin,
        access: 'AUTH'
    },
    {
        name: "Drivers",
        path: "/transport/drivers/view/:id",
        exact: true,
        component: Drivers,
        auth: authRoles.admin,
        access: 'AUTH'
    }
];

export default driversRoutes;
