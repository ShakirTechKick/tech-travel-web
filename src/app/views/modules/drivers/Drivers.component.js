import React, { useState, useEffect } from "react";
import { ValidatorForm, TextValidator } from "react-material-ui-form-validator";
import { Button, Grid, Table, TableRow, TableCell } from "@material-ui/core";
import { Breadcrumb } from "egret";
import { withStyles } from "@material-ui/styles";
import { useDispatch, useSelector } from "react-redux";
import * as DriverActions from "../../../redux/actions/modules/Drivers.action";
import TableMD from "mui-datatables";
import ActionList from "app/helpers/ActionList";
import FlexRow from "app/helpers/FlexRow";
import history from '../../../../history';
import moment from "moment";

const Drivers = (props) => {

    const dispatch = useDispatch();

    const { user: auth_user } = useSelector(state => state.user);
    const { list } = useSelector(state => state.drivers);

    const [driver, setDriver] = useState({
        company_id: auth_user.data.company_id
    });


    const [view, setView] = useState('ManageView');

    useEffect(() => {
        dispatch(DriverActions.fetchDrivers(auth_user.data.company_id));
    }, [])

    const CheckPermission = (type) => {

        var module_exists = false;
        var action_permission = false;

        auth_user.permissions.forEach(module => {
            if (Object.keys(module)[0] === "Drivers") {
                module_exists = true;
                if (module['Drivers'][type] === "ALLOWED") {
                    action_permission = true;
                }
            }
        });

        if (module_exists && action_permission) {
            return true;
        }

        return false;

    }

    const deleteEvent = (id) => {
        dispatch(DriverActions.removeDriver({ driver_id: id }))
    };

    const editEvent = (id) => {
        setDriver(list.find(d => d.driver_id === id));
        setView('EditView');
    };

    const blockEvent = (id) => {
        dispatch(DriverActions.statusDriver({ driver_id: id }))
    };

    const changeView = (v) => {
        if (v === 'AddView') {
            setDriver({ company_id: auth_user.data.company_id });
        }
        setView(v)
    }

    const saveDriver = () => {


        if (view === 'AddView') {
            dispatch(DriverActions.addDriver({ ...driver }, () => {
                changeView('ManageView')
            }));
        } else {
            dispatch(DriverActions.updateDriver({ ...driver }, () => {
                changeView('ManageView')
            }));
        }
    }
    const onChangeField = (e) => {
        setDriver({ ...driver, [e.target.name]: e.target.value });
    }


    const drivers_list = list.sort((a, b) => b.driver_id - a.driver_id).map(d => ([
        d.driver_id, d.name, d.email, d.phone_number,
        <ActionList del={CheckPermission("delete")} edit={CheckPermission("edit")} block={CheckPermission("edit")}
            id={d.driver_id} deleteEvent={deleteEvent} blockEvent={blockEvent}
            editEvent={editEvent} status={d.status}  view={`/transport/drivers/view/${d.driver_id}`} />
    ]))


    const headers = ['Driver ID', 'Name', 'Email', 'Phone Number', 'Action'];


    const { path, params } = props.match ? props.match : { path: null, params: null };

    return (
        <div className="analytics m-sm-30">
            <div className="mb-sm-30">
                <Breadcrumb
                    routeSegments={[
                        { name: "Dashboard", path: "/dashboard" },
                        { name: "Drivers" }
                    ]}
                />
            </div>


            {(path === '/transport/drivers/view/:id') ?

                list.find(c => c.driver_id == params.id) ?
                <Table style={{ border: '.5px solid' }}>
                        <TableRow>
                            <TableCell colspan="2" style={{ padding: '15px' }}>
                                <FlexRow>
                                    <Button variant='outlined' onClick={() => history.push('/transport/drivers')}>Back</Button>
                                    <ActionList del={CheckPermission("delete")} block={CheckPermission("edit")}
                                        id={list.find(c => c.driver_id == params.id).driver_id} deleteEvent={deleteEvent} blockEvent={blockEvent}
                                        status={list.find(c => c.driver_id == params.id).status} />
                                </FlexRow>
                            </TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell style={{ padding: '15px' }}>Driver Name</TableCell>
                            <TableCell>{list.find(c => c.driver_id == params.id).name}</TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell style={{ padding: '15px' }}>Email</TableCell>
                            <TableCell>{list.find(c => c.driver_id == params.id).email}</TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell style={{ padding: '15px' }}>Country</TableCell>
                            <TableCell>{list.find(c => c.driver_id == params.id).country}</TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell style={{ padding: '15px' }}>Phone Number</TableCell>
                            <TableCell>{list.find(c => c.driver_id == params.id).phone_number}</TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell style={{ padding: '15px' }}>Status</TableCell>
                            <TableCell>{list.find(c => c.driver_id == params.id).status === 'A' ? 'Active' : 'Blocked'}</TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell style={{ padding: '15px' }}>Created On</TableCell>
                            <TableCell>{moment(list.find(c => c.driver_id == params.id).createdAt).format('YYYY MMM DD, hh:mm:ss a ddd')}</TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell style={{ padding: '15px' }}>Last Updated On</TableCell>
                            <TableCell>{moment(list.find(c => c.driver_id == params.id).updatedAt).format('YYYY MMM DD, hh:mm:ss a ddd')}</TableCell>
                        </TableRow>
                    </Table> : <div className="no-matches">No matched item found !</div> :

                <React.Fragment>
                    <div className="mb-sm-30">
                        {
                            view === 'ManageView' &&
                            <Button
                                onClick={changeView.bind(null, 'AddView')}
                                variant="outlined"
                                color="primary">Add Driver</Button>
                        }
                        {
                            (view === 'AddView' || view === 'EditView') &&
                            <Button
                                variant="outlined"
                                onClick={changeView.bind(null, 'ManageView')}
                                color="primary">All Drivers</Button>
                        }
                    </div>

                    {
                        view === 'ManageView' &&

                        <TableMD
                            columns={headers}
                            title="Drivers"
                            data={drivers_list}
                            options={{
                                responsive: 'scrollMaxHeight'
                            }}
                        />
                    }

                    {(view === 'AddView' || view == 'EditView') &&
                        <ValidatorForm onSubmit={saveDriver}>

                            <Grid container spacing={6}>
                                <Grid md={4} sm={12} xs={12} item>
                                    <TextValidator
                                        className="mb-16 w-100"
                                        variant="outlined"
                                        label="Name"
                                        type="text"
                                        value={driver.name ? driver.name : ``}
                                        onChange={onChangeField}
                                        name="name"
                                        validators={["required"]}
                                        errorMessages={[
                                            "Name is required"
                                        ]}
                                    />
                                </Grid>
                                <Grid md={4} sm={12} xs={12} item>
                                    <TextValidator
                                        className="mb-16 w-100"
                                        variant="outlined"
                                        label="Email ID"
                                        type="text"
                                        value={driver.email ? driver.email : ``}
                                        onChange={onChangeField}
                                        name="email"
                                        validators={["isEmail"]}
                                        errorMessages={[
                                            "Email is not valid"
                                        ]}
                                    />
                                </Grid>
                                <Grid md={4} sm={12} xs={12} item>
                                    <TextValidator
                                        className="mb-16 w-100"
                                        variant="outlined"
                                        label="Phone Number"
                                        type="text"
                                        value={driver.phone_number ? driver.phone_number : ``}
                                        onChange={onChangeField}
                                        name="phone_number"
                                    />
                                </Grid>
                            </Grid>

                            <Button
                                variant="contained"
                                color="primary"
                                type="submit">{view === 'AddView' ? 'Save' : 'Update'} Driver</Button>

                        </ValidatorForm>
                    }
                </React.Fragment>}

        </div >
    );
}

export default withStyles({}, { withTheme: true })(Drivers);
