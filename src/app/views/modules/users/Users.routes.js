import { EgretLoadable } from "egret";
import { authRoles } from "../../../auth/authRoles";

const Users = EgretLoadable({
    loader: () => import("./Users.component")
});

const usersRoutes = [
    {
        name: "Users",
        path: "/users",
        exact: true,
        component: Users,
        auth: authRoles.admin,
        access: 'AUTH'
    },
    {
        name: "Users",
        path: "/users/view/:id",
        exact: true,
        component: Users,
        auth: authRoles.admin,
        access: 'AUTH'
    }
];

export default usersRoutes;
