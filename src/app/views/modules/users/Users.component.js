import React, { Component, Fragment, useRef, useState, useEffect } from "react";
import { ValidatorForm, TextValidator, SelectValidator } from "react-material-ui-form-validator";
import { Button, MenuItem, Grid, Icon, Switch, Dialog, DialogActions, DialogTitle, DialogContent, useTheme, Table, TableRow, TableCell, Avatar } from "@material-ui/core";
import { Breadcrumb } from "egret";
import { withStyles } from "@material-ui/styles";
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { useDispatch, useSelector } from "react-redux";
import { fetchRoles } from "app/redux/actions/modules/Roles.actions";
import { fetchCompanies } from "app/redux/actions/modules/Company.actions";
import * as UserActions from "app/redux/actions/modules/Users.actions";
import TableMD from "mui-datatables";
import ActionList from "app/helpers/ActionList";
import { Dropdown, Label } from "semantic-ui-react";
import FlexRow from "app/helpers/FlexRow";
import { ToastsStore } from "react-toasts";
import history from '../../../../history';
import moment from "moment";

const Users = (props) => {

    const dispatch = useDispatch();

    const [user, setUser] = useState({});

    const [usersUnderAdmins, setUsersUnderAdmins] = useState({
        open: false, list: [], company: null
    });

    const { user: auth_user } = useSelector(state => state.user);
    const { list } = useSelector(state => state.users);
    const { list: roles } = useSelector(state => state.roles);
    const { companies } = useSelector(state => state.company);

    const [view, setView] = useState('ManageView');

    ValidatorForm.addValidationRule('isPasswordMatch', (value) => {
        if (user.repassword !== user.password) {
            return false;
        }
        return true;
    });

    useEffect(() => {

        dispatch(UserActions.fetchUsers());
        dispatch(fetchRoles(auth_user.data.user_id));
        dispatch(fetchCompanies());

        return () => {
            ValidatorForm.removeValidationRule('isPasswordMatch');
        }

    }, [])

    const CheckPermission = (type) => {

        var module_exists = false;
        var action_permission = false;

        auth_user.permissions.forEach(module => {
            if (Object.keys(module)[0] === "Users") {
                module_exists = true;
                if (module['Users'][type] === "ALLOWED") {
                    action_permission = true;
                }
            }
        });

        if (module_exists && action_permission) {
            return true;
        }

        return false;

    }

    const deleteEvent = (id) => {
        dispatch(UserActions.removeUser({ user_id: id }))
    };

    const editEvent = (id) => {
        setUser(list.find(u => u.user_id === id));
        setView('EditView');
    };

    const blockEvent = (id) => {
        dispatch(UserActions.statusUser({ user_id: id }))
    };

    const changeView = (v) => {
        if (v === 'AddView') {
            setUser({});
        }
        setView(v)
    }

    const saveUser = () => {

        if (user.role_id && (auth_user.data.role === 'SuperAdmin' || user.company_id)) {
            if (view === 'AddView') {
                dispatch(UserActions.addUsers({ ...user }, () => {
                    changeView('ManageView')
                }));
            } else {
                dispatch(UserActions.updateUser({ ...user }, () => {
                    changeView('ManageView')
                }));
            }
        } else {
            ToastsStore.error('Role & Company both are required');
        }
    }
    const onChangeField = (e) => {
        setUser({ ...user, [e.target.name]: e.target.value });
    }


    const roles_list = roles.filter(r => {
        if (auth_user.data.role === 'SuperAdmin') {
            return (r.role !== 'SuperAdmin');
        } else {
            return (r.role !== 'SuperAdmin' && r.role !== 'Admin');
        }
    })

    const companies_list = companies.filter(c => {
        if (auth_user.data.role === 'SuperAdmin') {
            return true;
        } else {
            return c.company_id === auth_user.data.company_id;
        }
    });

    const company_admin_list = list.sort((a, b) => b.user_id - a.user_id).filter(u => u.role === 'Admin').map(u => (
        [u.user_id, u.name,
        <div><h6>{u.email}</h6><h6>{u.phone_number}</h6></div>,
        companies.find(c => c.company_id === u.company_id) ? companies.find(c => c.company_id === u.company_id).name : null,
        <Button variant='outlined' onClick={() => setUsersUnderAdmins({
            list: list.filter(ul => (ul.role !== 'Admin' && ul.role !== 'SuperAdmin' && ul.company_id === u.company_id)),
            company: companies.find(c => c.company_id === u.company_id) ? companies.find(c => c.company_id === u.company_id).name : null, open: true
        })}>
            <span>{list.filter(ul => (ul.role !== 'Admin' && ul.role !== 'SuperAdmin' && ul.company_id === u.company_id)).length}</span> Users</Button>, <ActionList del={CheckPermission("delete")} edit={CheckPermission("edit")} block={CheckPermission("edit")}
                id={u.user_id} deleteEvent={deleteEvent} blockEvent={blockEvent}
                editEvent={editEvent} status={u.status} view={`/users/view/${u.user_id}`} />]
    ));

    const company_users_list = list.sort((a, b) => b.user_id - a.user_id).filter(u => u.role !== 'Admin' && u.company_id === auth_user.data.company_id).map(u => ([
        u.user_id, u.name, u.role, u.email, u.phone_number,
        <ActionList del={CheckPermission("delete")} edit={CheckPermission("edit")} block={CheckPermission("edit")}
            id={u.user_id} deleteEvent={deleteEvent} blockEvent={blockEvent}
            editEvent={editEvent} status={u.status} view={`/users/view/${u.user_id}`} />
    ]));

    const headers = auth_user.data.role === 'SuperAdmin' ?
        ['Compnay ID', 'Name', 'Contact', 'Company', 'Users', 'Action'] :
        ['User ID', 'Name', 'Role Of', 'Email', 'Contact', 'Action'];


        const { path, params } = props.match ? props.match : { path: null, params: null };

    if (path === '/users/view/:id') {

        return (<div className="analytics m-sm-30">
            <div className="mb-sm-30">
                <Breadcrumb
                    routeSegments={[
                        { name: "Dashboard", path: "/dashboard" },
                        { name: auth_user.data.role === 'SuperAdmin' ? "Companies" : "Users" }
                    ]}
                />
            </div>
            {
                !!list.find(c => c.user_id == params.id) ?
                    <Table style={{ border: '.5px solid' }}>
                        <TableRow>
                            <TableCell>
                                {(list.find(c => c.user_id == params.id) && list.find(c => c.user_id == params.id).avatar) &&
                                    <Avatar style={{ width: 100, height: 100, margin: 15 }} src={process.env.REACT_APP_STATIC + list.find(c => c.user_id == params.id).avatar} />
                                }
                            </TableCell>
                            <TableCell style={{ padding: '15px' }}>
                                <FlexRow styles={{ justifyContent: 'flex-end' }}>
                                    <Button variant='outlined' onClick={() => history.push('/users')}>Back</Button>
                                    <ActionList del={CheckPermission("delete")} block={CheckPermission("edit")}
                                        id={list.find(c => c.user_id == params.id).user_id} deleteEvent={deleteEvent} blockEvent={blockEvent}
                                        status={list.find(c => c.user_id == params.id).status} />
                                </FlexRow>
                            </TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell style={{ padding: '15px' }}>Name</TableCell>
                            <TableCell>{list.find(c => c.user_id == params.id).name}</TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell style={{ padding: '15px' }}>Role Of</TableCell>
                            <TableCell>
                                {roles.find(r => r.role_id === list.find(c => c.user_id == params.id).role_id) ?
                                    roles.find(r => r.role_id === list.find(c => c.user_id == params.id).role_id).role : null}
                            </TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell style={{ padding: '15px' }}>Email</TableCell>
                            <TableCell>{list.find(c => c.user_id == params.id).email}</TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell style={{ padding: '15px' }}>Phone Number</TableCell>
                            <TableCell>{list.find(c => c.user_id == params.id).phone_number}</TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell style={{ padding: '15px' }}>Status</TableCell>
                            <TableCell>{list.find(c => c.user_id == params.id).status === 'A' ? 'Active' : 'Blocked'}</TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell style={{ padding: '15px' }}>Created On</TableCell>
                            <TableCell>{moment(list.find(c => c.user_id == params.id).createdAt).format('YYYY MMM DD, hh:mm:ss a ddd')}</TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell style={{ padding: '15px' }}>Last Updated On</TableCell>
                            <TableCell>{moment(list.find(c => c.user_id == params.id).updatedAt).format('YYYY MMM DD, hh:mm:ss a ddd')}</TableCell>
                        </TableRow>
                    </Table> : <div className="no-matches">No matched item found !</div>

            }


        </div>)
    }

    return (
        <div className="analytics m-sm-30">
            <div className="mb-sm-30">
                <Breadcrumb
                    routeSegments={[
                        { name: "Dashboard", path: "/dashboard" },
                        { name: auth_user.data.role === 'SuperAdmin' ? "Companies" : "Users" }
                    ]}
                />
            </div>


            <div className="mb-sm-30">
                {
                    view === 'ManageView' &&
                    <Button
                        onClick={changeView.bind(null, 'AddView')}
                        variant="outlined"
                        color="primary">Add {auth_user.data.role === 'SuperAdmin' ? "Company" : "User"}</Button>
                }
                {
                    (view === 'AddView' || view === 'EditView') &&
                    <Button
                        variant="outlined"
                        onClick={changeView.bind(null, 'ManageView')}
                        color="primary">All {auth_user.data.role === 'SuperAdmin' ? "Companies" : "Users"}</Button>
                }
            </div>
            {
                view === 'ManageView' &&

                <TableMD
                    columns={headers}
                    title={auth_user.data.role === 'SuperAdmin' ? "Companies" : "Users"}
                    data={auth_user.data.role === 'SuperAdmin' ? company_admin_list : company_users_list}
                    options={{
                        responsive: 'scrollMaxHeight'
                    }}
                />
            }

            {(view === 'AddView' || view == 'EditView') &&
                <ValidatorForm onSubmit={saveUser}>

                    <Grid container spacing={6}>
                        <Grid md={4} sm={12} xs={12} item>
                            <TextValidator
                                className="mb-16 w-100"
                                variant="outlined"
                                label="Name"
                                type="text"
                                value={user.name ? user.name : ``}
                                onChange={onChangeField}
                                name="name"
                                validators={["required"]}
                                errorMessages={[
                                    "Name is required"
                                ]}
                            />
                        </Grid>
                        <Grid md={4} sm={12} xs={12} item>
                            <Dropdown
                                placeholder='Role Of User'
                                fluid
                                selection
                                value={user.role_id ? user.role_id : ``}
                                onChange={(event, { value }) => setUser({ ...user, role_id: value })}
                                options={roles_list.map((m, index) => {
                                    return { key: index, text: m.role, value: m.role_id }
                                })}
                            />
                        </Grid>

                        <Grid md={4} sm={12} xs={12} item>
                            {!!(auth_user.data.role !== 'SuperAdmin') ?
                                <Dropdown
                                    placeholder='Company'
                                    fluid
                                    selection
                                    value={user.company_id ? user.company_id : ``}
                                    onChange={(event, { value }) => setUser({ ...user, company_id: value })}
                                    options={companies_list.map((m, index) => {
                                        return { key: index, text: m.name, value: m.company_id }
                                    })}
                                /> :
                                <TextValidator
                                    className="mb-16 w-100"
                                    variant="outlined"
                                    label="Company"
                                    type="text"
                                    value={user.company ? user.company : ``}
                                    onChange={onChangeField}
                                    name="company"
                                    validators={["required"]}
                                    errorMessages={[
                                        "Company Name is required"
                                    ]}
                                />}
                        </Grid>
                    </Grid>
                    <Grid container spacing={6}>
                        <Grid md={6} sm={12} xs={12} item>
                            <TextValidator
                                className="mb-16 w-100"
                                variant="outlined"
                                label="Email ID"
                                type="text"
                                value={user.email ? user.email : ``}
                                onChange={onChangeField}
                                name="email"
                                validators={["required", "isEmail"]}
                                errorMessages={[
                                    "Email is required",
                                    "Email is not valid"
                                ]}
                            />
                        </Grid>
                        <Grid md={6} sm={12} xs={12} item>
                            <TextValidator
                                className="mb-16 w-100"
                                variant="outlined"
                                label="Phone Number"
                                type="text"
                                value={user.phone_number ? user.phone_number : ``}
                                onChange={onChangeField}
                                name="phone_number"
                            />
                        </Grid>
                    </Grid>
                    <Grid container spacing={6}>
                        <Grid md={12} xs={12} item>
                            {
                                view === 'EditView' &&

                                <React.Fragment>
                                    <Switch name='change_password' onChange={(e) => setUser({ ...user, change_password: e.target.checked, password: null })} /> Change Password ?
                                </React.Fragment>

                            }
                        </Grid>

                        {view === 'AddView' &&
                            <React.Fragment>
                                <Grid md={6} sm={12} xs={12} item>
                                    <TextValidator
                                        className="mb-16 w-100"
                                        variant="outlined"
                                        label="Password"
                                        onChange={onChangeField}
                                        name="password"
                                        type="password"
                                        validators={['required']}
                                        value={user.password}
                                        errorMessages={['Password is required']}
                                    />
                                </Grid>
                                <Grid md={6} sm={12} xs={12} item>

                                    <TextValidator
                                        className="mb-16 w-100"
                                        variant="outlined"
                                        label="Repeat password"
                                        name="repassword"
                                        type="password"
                                        value={user.repassword}
                                        validators={['isPasswordMatch', 'required']}
                                        errorMessages={['password mismatch', 'Repeat Password is required']}
                                        onChange={onChangeField}
                                    />
                                </Grid>
                            </React.Fragment>
                        }
                        {
                            (view === 'EditView' && user.change_password) &&

                            <React.Fragment>
                                <Grid md={6} sm={12} xs={12} item>
                                    <TextValidator
                                        className="mb-16 w-100"
                                        variant="outlined"
                                        label="Password"
                                        onChange={onChangeField}
                                        name="password"
                                        type="password"
                                        validators={['required']}
                                        errorMessages={['Password is required']}
                                        defaultValue={user.password}
                                    />
                                </Grid>
                                <Grid md={6} sm={12} xs={12} item>

                                    <TextValidator
                                        className="mb-16 w-100"
                                        variant="outlined"
                                        label="Repeat password"
                                        name="repassword"
                                        type="password"
                                        validators={['isPasswordMatch', 'required']}
                                        errorMessages={['password mismatch', 'Repeat Password is required']}
                                        onChange={onChangeField}
                                        defaultValue={user.repassword}
                                    />
                                </Grid>
                            </React.Fragment>
                        }
                    </Grid>

                    <Button
                        variant="contained"
                        color="primary"
                        type="submit">{view === 'AddView' ? 'Save' : 'Update'} User</Button>

                </ValidatorForm>
            }

            <Dialog open={usersUnderAdmins.open} fullScreen>
                <DialogTitle>Users Of {usersUnderAdmins.company}</DialogTitle>
                <DialogContent draggable dividers={true}>
                    <TableMD
                        columns={['User Id', 'Name', 'Email', 'Phone Number', 'Date Joined']}
                        data={usersUnderAdmins.list.map(u => (
                            [u.user_id, u.name, u.email, u.phone_number, moment(u.createdAt).format('YYYY MMM DD')]
                        ))}
                        options={{
                            responsive: 'scrollMaxHeight'
                        }}
                    />
                </DialogContent>
                <DialogActions>
                    <Button variant='contained' onClick={() => setUsersUnderAdmins({ list: [], open: false, company: null })}>
                        Cancel
                        </Button>
                </DialogActions>
            </Dialog>


        </div >
    );
}

export default withStyles({}, { withTheme: true })(Users);
