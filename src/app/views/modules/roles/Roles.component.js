import React, { Component, Fragment, useRef, useState, useEffect } from "react";
import { ValidatorForm, TextValidator, SelectValidator } from "react-material-ui-form-validator";
import { Button, MenuItem, Grid, Icon } from "@material-ui/core";
import { Breadcrumb } from "egret";
import { withStyles } from "@material-ui/styles";
import { useDispatch, useSelector } from "react-redux";
import { fetchRoles, addRoles, updateRoles, statusRoles, removeRoles, availableModules, permittedModules as fetchPermittedModules } from "app/redux/actions/modules/Roles.actions";
import TableMD from "mui-datatables";
import ActionList from "app/helpers/ActionList";
import { Dropdown, Label } from "semantic-ui-react";
import FlexRow from "app/helpers/FlexRow";

const Roles = (props) => {

    const dispatch = useDispatch();

    const [role_id, setRoleID] = useState(null);
    const [role_user, setRoleUser] = useState(``);
    const [permittedModules, setPermittedModules] = useState([]);

    const { user } = useSelector(state => state.user);
    const { list, availableModules: availableModulesList } = useSelector(state => state.roles);

    const [view, setView] = useState('ManageView');

    useEffect(() => {

        dispatch(fetchRoles(user.data.user_id));
        dispatch(availableModules());
    }, [])

    const CheckPermission = (type) => {

        var module_exists = false;
        var action_permission = false;

        user.permissions.forEach(module => {
            if (Object.keys(module)[0] === "Roles") {
                module_exists = true;
                if (module['Roles'][type] === "ALLOWED") {
                    action_permission = true;
                }
            }
        });

        if (module_exists && action_permission) {
            return true;
        }

        return false;

    }

    const deleteEvent = (id) => {
        dispatch(removeRoles({ role_id: id }))
    };

    const editEvent = (id) => {

        dispatch(fetchPermittedModules(id, pm => {
            console.log(pm)
            setPermittedModules([...pm]);
            setRoleUser(list.filter(r => r.role_id == id)[0].role);
            setRoleID(id);
            setView('EditView');
        }));

    };

    const blockEvent = (id) => {
        dispatch(statusRoles({ role_id: id }))
    };

    const isReservedUserRoles = (role) => {
        return role === 'Admin' || role === 'SuperAdmin';
    }

    const roles = list.map(r => ([
        r.role_id, r.role, r.role !== "SuperAdmin" ?
            <ActionList del={!isReservedUserRoles(r.role) && CheckPermission("delete")} edit={CheckPermission("edit")} block={!isReservedUserRoles(r.role) && CheckPermission("edit")}
                id={r.role_id} deleteEvent={deleteEvent} blockEvent={blockEvent}
                editEvent={editEvent} status={r.status} /> : <ActionList />
    ]))

    const changeView = (v) => {
        if (v === 'AddView') {
            setRoleID(null);
            setPermittedModules([]);
        }
        setView(v)
    }

    const saveRoleSubmit = () => {
        if (view === 'AddView') {
            dispatch(addRoles({ created_by: user.data.user_id, role: role_user, permittedModules: permittedModules }, () => {
                changeView('ManageView')
            }));
        } else {
            dispatch(updateRoles({ created_by: user.data.user_id, role_id, role: role_user, permittedModules: permittedModules }, () => {
                changeView('ManageView')
            }));
        }
    }

    const selectedModules = (e, { value }) => {
        const permittedModules = [];
        value.forEach(module => {
            const temp = permittedModules.filter((item) => {
                return item.module === module;
            });

            if (!temp.length) {
                permittedModules.push(
                    { module: module, read: "NOT SET", write: "NOT SET", edit: "NOT SET", delete: "NOT SET" }
                );
            }
        });

        setPermittedModules([...permittedModules])
    }

    const _changePermission = (event, { value }, field, key) => {

        const temp = permittedModules;

        temp[key][field] = value;

        if (field === "read" && value !== "ALLOWED") {
            temp[key]["write"] = "NOT SET";
            temp[key]["edit"] = "NOT SET";
            temp[key]["delete"] = "NOT SET";
        }

        setPermittedModules([...temp]);
    }

    const modules = permittedModules.map(module => {
        return module.module;
    })

    return (
        <div className="analytics m-sm-30">
            <div className="mb-sm-30">
                <Breadcrumb
                    routeSegments={[
                        { name: "Dashboard", path: "/dashboard" },
                        { name: "Roles" }
                    ]}
                />
            </div>


            <div className="mb-sm-30">
                {
                    view === 'ManageView' &&
                    <Button
                        onClick={changeView.bind(null, 'AddView')}
                        variant="outlined"
                        color="primary">Add Roles</Button>
                }
                {
                    (view === 'AddView' || view === 'EditView') &&
                    <Button
                        variant="outlined"
                        onClick={changeView.bind(null, 'ManageView')}
                        color="primary">All Roles</Button>
                }
            </div>
            {
                view === 'ManageView' &&

                <TableMD
                    columns={['Role ID', 'Name', 'Action']}
                    title='Roles'
                    data={roles}
                    options={{
                        responsive: 'scrollMaxHeight'
                    }}
                />
            }

            {(view === 'AddView' || view == 'EditView') &&
                <ValidatorForm onSubmit={saveRoleSubmit}>

                    <Grid container spacing={6}>
                        <Grid md={6} sm={12} xs={12} item>
                            <TextValidator
                                className="mb-16 w-100"
                                variant="outlined"
                                label="Role"
                                type="text"
                                value={role_user}
                                onChange={(e) => setRoleUser(e.target.value)}
                                name="role"
                                validators={["required"]}
                                errorMessages={[
                                    "Role is required"
                                ]}
                            />
                        </Grid>
                        <Grid md={6} sm={12} xs={12} item>
                            <Dropdown
                                placeholder='Permitted Modules'
                                fluid
                                multiple
                                search
                                selection
                                value={modules}
                                onChange={selectedModules}
                                multiple options={availableModulesList.filter(m => {

                                    if(user.data.role === 'SuperAdmin'){
                                        return true;
                                    }
                                    
                                    return m.category === 'USERS';

                                }).map((m, index) => {
                                    return { key: index, text: m.module, value: m.module }
                                })}
                            />
                        </Grid>
                    </Grid>
                    {
                        !!permittedModules.length &&

                        permittedModules.map((module, index) => {

                            return (

                                <FlexRow>
                                    <div>{module.module}</div>
                                    <FlexRow styles={{['flex-wrap']: 'wrap'}}>
                                        <div>
                                            <Label>Read</Label>
                                            <Dropdown defaultValue={module.read} onChange={(event, { value }) => _changePermission(event, { value }, "read", index)} placeholder="Read" options={[{ key: 1, text: "NOT SET", value: "NOT SET" },
                                            { key: 2, text: "NOT ALLOWED", value: "NOT ALLOWED" }, { key: 3, text: "ALLOWED", value: "ALLOWED" }
                                            ]} />
                                        </div>
                                        <div>
                                            <Label>Write</Label>
                                            <Dropdown value={module.write} onChange={(event, { value }) => _changePermission(event, { value }, "write", index)} disabled={module.read !== "ALLOWED"} placeholder="Write" options={[{ key: 1, text: "NOT SET", value: "NOT SET" },
                                            { key: 2, text: "NOT ALLOWED", value: "NOT ALLOWED" }, { key: 3, text: "ALLOWED", value: "ALLOWED" }
                                            ]} />
                                        </div>
                                        <div>
                                            <Label>Edit</Label>
                                            <Dropdown value={module.edit} onChange={(event, { value }) => _changePermission(event, { value }, "edit", index)} disabled={module.read !== "ALLOWED"} placeholder="Edit" options={[{ key: 1, text: "NOT SET", value: "NOT SET" },
                                            { key: 2, text: "NOT ALLOWED", value: "NOT ALLOWED" }, { key: 3, text: "ALLOWED", value: "ALLOWED" }
                                            ]} />
                                        </div>
                                        <div>
                                            <Label>Delete</Label>
                                            <Dropdown value={module.delete} onChange={(event, { value }) => _changePermission(event, { value }, "delete", index)} disabled={module.read !== "ALLOWED"} placeholder="Delete" options={[{ key: 1, text: "NOT SET", value: "NOT SET" },
                                            { key: 2, text: "NOT ALLOWED", value: "NOT ALLOWED" }, { key: 3, text: "ALLOWED", value: "ALLOWED" }
                                            ]} />
                                        </div>
                                    </FlexRow>
                                </FlexRow>

                            )
                        })
                    }
                    <Button
                        variant="contained"
                        color="primary" style={{['margin-top']: '15px'}}
                        type="submit">{view === 'AddView' ? 'Save' : 'Update'} Roles</Button>

                </ValidatorForm>
            }


        </div >
    );
}

export default withStyles({}, { withTheme: true })(Roles);
