import { EgretLoadable } from "egret";
import { authRoles } from "../../../auth/authRoles";

const Roles = EgretLoadable({
  loader: () => import("./Roles.component")
});

const rolesRoutes = [
  {
    name: 'Roles',
    path: "/roles",
    exact: true,
    component: Roles,
    auth: authRoles.admin,
    access: 'AUTH'
  }
];

export default rolesRoutes;
