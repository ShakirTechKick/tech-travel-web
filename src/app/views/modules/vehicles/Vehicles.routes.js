import { EgretLoadable } from "egret";
import { authRoles } from "../../../auth/authRoles";

const Vehicles = EgretLoadable({
    loader: () => import("./Vehicles.component")
});

const vehiclesRoutes = [
    {
        name: "Vehicles",
        path: "/transport/vehicles",
        exact: true,
        component: Vehicles,
        auth: authRoles.admin,
        access: 'AUTH'
    },
    {
        name: "Vehicles",
        path: "/transport/vehicles/view/:id",
        exact: true,
        component: Vehicles,
        auth: authRoles.admin,
        access: 'AUTH'
    }
];

export default vehiclesRoutes;
