import React, { useState, useEffect } from "react";
import { ValidatorForm, TextValidator } from "react-material-ui-form-validator";
import { Button, Grid, Table, TableRow, TableCell } from "@material-ui/core";
import { Breadcrumb } from "egret";
import { withStyles } from "@material-ui/styles";
import { useDispatch, useSelector } from "react-redux";
import * as VehicleActions from "../../../redux/actions/modules/Vehicles.action";
import { fetchVehicleCategories } from "../../../redux/actions/modules/VehicleCategories.actions";
import TableMD from "mui-datatables";
import ActionList from "app/helpers/ActionList";
import { Dropdown } from "semantic-ui-react";
import history from '../../../../history';
import moment from "moment";
import FlexRow from "app/helpers/FlexRow";

const Vehicles = (props) => {

    const dispatch = useDispatch();

    const { user: auth_user } = useSelector(state => state.user);
    const { list } = useSelector(state => state.vehicles);
    const { list: categories } = useSelector(state => state.vehicle_categories);

    const [vehicles, setVehicles] = useState({
        company_id: auth_user.data.company_id
    });


    const [view, setView] = useState('ManageView');

    useEffect(() => {
        dispatch(VehicleActions.fetchVehicles(auth_user.data.company_id));
        dispatch(fetchVehicleCategories(auth_user.data.company_id));
    }, [])

    const CheckPermission = (type) => {

        var module_exists = false;
        var action_permission = false;

        auth_user.permissions.forEach(module => {
            if (Object.keys(module)[0] === "Vehicles") {
                module_exists = true;
                if (module['Vehicles'][type] === "ALLOWED") {
                    action_permission = true;
                }
            }
        });

        if (module_exists && action_permission) {
            return true;
        }

        return false;

    }

    const deleteEvent = (id) => {
        dispatch(VehicleActions.removeVehicles({ vehicle_id: id }))
    };

    const editEvent = (id) => {
        setVehicles(list.find(v => v.vehicle_id === id));
        setView('EditView');
    };

    const blockEvent = (id) => {
        dispatch(VehicleActions.statusVehicles({ vehicle_id: id }))
    };

    const changeView = (v) => {
        if (v === 'AddView') {
            setVehicles({ company_id: auth_user.data.company_id });
        }
        setView(v)
    }

    const saveVehicles = () => {


        if (view === 'AddView') {
            dispatch(VehicleActions.addVehicles({ ...vehicles }, () => {
                changeView('ManageView')
            }));
        } else {
            dispatch(VehicleActions.updateVehicles({ ...vehicles }, () => {
                changeView('ManageView')
            }));
        }
    }
    const onChangeField = (e) => {
        setVehicles({ ...vehicles, [e.target.name]: e.target.value });
    }


    const vehicles_list = list.sort((a, b) => b.vehicle_id - a.vehicle_id).map(v => ([
        v.vehicle_id, v.name, v.vehicle_number, categories.find(category => category.v_category_id === v.v_category_id) ?
            categories.find(category => category.v_category_id === v.v_category_id).name : null,
        <ActionList del={CheckPermission("delete")} edit={CheckPermission("edit")} block={CheckPermission("edit")}
            id={v.vehicle_id} deleteEvent={deleteEvent} blockEvent={blockEvent}
            editEvent={editEvent} status={v.status} view={`/transport/vehicles/view/${v.vehicle_id}`} />
    ]))


    const headers = ['Vehicle ID', 'Name', 'Vehicle Number', 'Category', 'Action'];

    const { path, params } = props.match ? props.match : { path: null, params: null };

    return (
        <div className="analytics m-sm-30">
            <div className="mb-sm-30">
                <Breadcrumb
                    routeSegments={[
                        { name: "Dashboard", path: "/dashboard" },
                        { name: "Vehicles" }
                    ]}
                />
            </div>
            {(path === '/transport/vehicles/view/:id') ?

                list.find(c => c.vehicle_id == params.id) ?
                    <Table style={{ border: '.5px solid' }}>
                        <TableRow>
                            <TableCell colspan="2" style={{ padding: '15px' }}>
                                <FlexRow>
                                    <Button variant='outlined' onClick={() => history.push('/transport/vehicles')}>Back</Button>
                                    <ActionList del={CheckPermission("delete")} block={CheckPermission("edit")}
                                        id={list.find(c => c.vehicle_id == params.id).vehicle_id} deleteEvent={deleteEvent} blockEvent={blockEvent}
                                        status={list.find(c => c.vehicle_id == params.id).status} />
                                </FlexRow>
                            </TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell style={{ padding: '15px' }}>Vehicle Name</TableCell>
                            <TableCell>{list.find(c => c.vehicle_id == params.id).name}</TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell style={{ padding: '15px' }}>Vehicle Category</TableCell>
                            <TableCell>{
                                categories.find(c => c.v_category_id === list.find(c => c.vehicle_id == params.id).v_category_id) ?
                                    categories.find(c => c.v_category_id === list.find(c => c.vehicle_id == params.id).v_category_id).name : null
                            }</TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell style={{ padding: '15px' }}>Vehicle Number</TableCell>
                            <TableCell>{list.find(c => c.vehicle_id == params.id).vehicle_number}</TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell style={{ padding: '15px' }}>Status</TableCell>
                            <TableCell>{list.find(c => c.vehicle_id == params.id).status === 'A' ? 'Active' : 'Blocked'}</TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell style={{ padding: '15px' }}>Created On</TableCell>
                            <TableCell>{moment(list.find(c => c.vehicle_id == params.id).createdAt).format('YYYY MMM DD, hh:mm:ss a ddd')}</TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell style={{ padding: '15px' }}>Last Updated On</TableCell>
                            <TableCell>{moment(list.find(c => c.vehicle_id == params.id).updatedAt).format('YYYY MMM DD, hh:mm:ss a ddd')}</TableCell>
                        </TableRow>
                    </Table> : <div className="no-matches">No matched item found !</div> :
                <React.Fragment>
                    <div className="mb-sm-30">
                        {
                            view === 'ManageView' &&
                            <Button
                                onClick={changeView.bind(null, 'AddView')}
                                variant="outlined"
                                color="primary">Add Vehicles</Button>
                        }
                        {
                            (view === 'AddView' || view === 'EditView') &&
                            <Button
                                variant="outlined"
                                onClick={changeView.bind(null, 'ManageView')}
                                color="primary">All Vehicles</Button>
                        }
                    </div>
                    {
                        view === 'ManageView' &&

                        <TableMD
                            columns={headers}
                            title="Vehicles"
                            data={vehicles_list}
                            options={{
                                responsive: 'scrollMaxHeight'
                            }}
                        />
                    }

                    {(view === 'AddView' || view == 'EditView') &&
                        <ValidatorForm onSubmit={saveVehicles}>

                            <Grid container spacing={6}>
                                <Grid md={4} sm={12} xs={12} item>
                                    <TextValidator
                                        className="mb-16 w-100"
                                        variant="outlined"
                                        label="Name"
                                        type="text"
                                        value={vehicles.name ? vehicles.name : ``}
                                        onChange={onChangeField}
                                        name="name"
                                        validators={["required"]}
                                        errorMessages={[
                                            "Name is required"
                                        ]}
                                    />
                                </Grid>
                                <Grid md={4} sm={12} xs={12} item>
                                    <TextValidator
                                        className="mb-16 w-100"
                                        variant="outlined"
                                        label="Vehicle Number"
                                        type="text"
                                        value={vehicles.vehicle_number ? vehicles.vehicle_number : ``}
                                        onChange={onChangeField}
                                        name="vehicle_number"
                                        validators={["required"]}
                                        errorMessages={[
                                            "Vehicle Number is required"
                                        ]}
                                    />
                                </Grid>
                                <Grid md={4} sm={12} xs={12} item>
                                    <Dropdown
                                        placeholder='Vehicle Category'
                                        fluid
                                        selection
                                        value={vehicles.v_category_id ? vehicles.v_category_id : ``}
                                        onChange={(event, { value }) => setVehicles({ ...vehicles, v_category_id: value })}
                                        options={categories.map((m, index) => {
                                            return { key: index, text: m.name, value: m.v_category_id }
                                        })}
                                    />
                                </Grid>
                            </Grid>

                            <Button
                                variant="contained"
                                color="primary"
                                type="submit">{view === 'AddView' ? 'Save' : 'Update'} Vehicles</Button>

                        </ValidatorForm>
                    }
                </React.Fragment>
            }

        </div >
    );
}

export default withStyles({}, { withTheme: true })(Vehicles);
