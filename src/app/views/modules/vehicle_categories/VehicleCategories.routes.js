import { EgretLoadable } from "egret";
import { authRoles } from "../../../auth/authRoles";

const VehicleCategories = EgretLoadable({
    loader: () => import("./VehicleCategories.component")
});

const categoriesRoutes = [
    {
        name: "Vehicle Categories",
        path: "/transport/vehicle-categories",
        exact: true,
        component: VehicleCategories,
        auth: authRoles.admin,
        access: 'AUTH'
    },
    {
        name: "Vehicle Categories",
        path: "/transport/vehicle-categories/view/:id",
        exact: true,
        component: VehicleCategories,
        auth: authRoles.admin,
        access: 'AUTH'
    }
];

export default categoriesRoutes;
