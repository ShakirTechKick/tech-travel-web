import React, { useState, useEffect } from "react";
import { ValidatorForm, TextValidator } from "react-material-ui-form-validator";
import { Button, Grid, Table, TableRow, TableCell } from "@material-ui/core";
import { Breadcrumb } from "egret";
import { withStyles } from "@material-ui/styles";
import { useDispatch, useSelector } from "react-redux";
import * as CategoryActions from "../../../redux/actions/modules/VehicleCategories.actions";
import TableMD from "mui-datatables";
import ActionList from "app/helpers/ActionList";
import history from '../../../../history';
import moment from "moment";
import FlexRow from "app/helpers/FlexRow";

const VehicleCategories = (props) => {


    const dispatch = useDispatch();

    const { user: auth_user } = useSelector(state => state.user);
    const { list } = useSelector(state => state.vehicle_categories);

    const [category, setCategory] = useState({
        company_id: auth_user.data.company_id
    });


    const [view, setView] = useState('ManageView');

    useEffect(() => {
        dispatch(CategoryActions.fetchVehicleCategories(auth_user.data.company_id));
    }, [])

    const CheckPermission = (type) => {

        var module_exists = false;
        var action_permission = false;

        auth_user.permissions.forEach(module => {
            if (Object.keys(module)[0] === "Vehicle Categories") {
                module_exists = true;
                if (module['Vehicle Categories'][type] === "ALLOWED") {
                    action_permission = true;
                }
            }
        });

        if (module_exists && action_permission) {
            return true;
        }

        return false;

    }

    const deleteEvent = (id) => {
        dispatch(CategoryActions.removeVehicleCategory({ v_category_id: id }))
    };

    const editEvent = (id) => {
        setCategory(list.find(c => c.v_category_id === id));
        setView('EditView');
    };

    const blockEvent = (id) => {
        dispatch(CategoryActions.statusVehicleCategory({ v_category_id: id }))
    };

    const changeView = (v) => {
        if (v === 'AddView') {
            setCategory({ company_id: auth_user.data.company_id });
        }
        setView(v)
    }

    const saveCategory = () => {


        if (view === 'AddView') {
            dispatch(CategoryActions.addVehicleCategory({ ...category }, () => {
                changeView('ManageView')
            }));
        } else {
            dispatch(CategoryActions.updateVehicleCategory({ ...category }, () => {
                changeView('ManageView')
            }));
        }
    }
    const onChangeField = (e) => {
        setCategory({ ...category, [e.target.name]: e.target.value });
    }


    const category_list = list.map(c => ([
        c.v_category_id, c.name, c.number_of_seats,
        <ActionList del={CheckPermission("delete")} edit={CheckPermission("edit")} block={CheckPermission("edit")}
            id={c.v_category_id} deleteEvent={deleteEvent} blockEvent={blockEvent}
            editEvent={editEvent} status={c.status} view={`/transport/vehicle-categories/view/${c.v_category_id}`} />
    ]))


    const headers = ['Category ID', 'Name', 'Number Of Seats', 'Action'];

    const { path, params } = props.match ? props.match : { path: null, params: null };

    return (
        <div className="analytics m-sm-30">
            <div className="mb-sm-30">
                <Breadcrumb
                    routeSegments={[
                        { name: "Dashboard", path: "/dashboard" },
                        { name: "Vehicle Categories" }
                    ]}
                />
            </div>

            {(path === '/transport/vehicle-categories/view/:id') ?

                list.find(c => c.v_category_id == params.id) ?
                <Table style={{ border: '.5px solid' }}>
                        <TableRow>
                            <TableCell colspan="2" style={{ padding: '15px' }}>
                                <FlexRow>
                                    <Button variant='outlined' onClick={() => history.push('/transport/vehicle-categories')}>Back</Button>
                                    <ActionList del={CheckPermission("delete")} block={CheckPermission("edit")}
                                        id={list.find(c => c.v_category_id == params.id).v_category_id} deleteEvent={deleteEvent} blockEvent={blockEvent}
                                        status={list.find(c => c.v_category_id == params.id).status} />
                                </FlexRow>
                            </TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell style={{ padding: '15px' }}>Vehicle Category</TableCell>
                            <TableCell>{list.find(c => c.v_category_id == params.id).name}</TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell style={{ padding: '15px' }}>Number Of Seats</TableCell>
                            <TableCell>{list.find(c => c.v_category_id == params.id).number_of_seats}</TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell style={{ padding: '15px' }}>Status</TableCell>
                            <TableCell>{list.find(c => c.v_category_id == params.id).status === 'A' ? 'Active' : 'Blocked'}</TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell style={{ padding: '15px' }}>Created On</TableCell>
                            <TableCell>{moment(list.find(c => c.v_category_id == params.id).createdAt).format('YYYY MMM DD, hh:mm:ss a ddd')}</TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell style={{ padding: '15px' }}>Last Updated On</TableCell>
                            <TableCell>{moment(list.find(c => c.v_category_id == params.id).updatedAt).format('YYYY MMM DD, hh:mm:ss a ddd')}</TableCell>
                        </TableRow>
                    </Table> : <div className="no-matches">No matched item found !</div> :
                <React.Fragment>
                    <div className="mb-sm-30">
                        {
                            view === 'ManageView' &&
                            <Button
                                onClick={changeView.bind(null, 'AddView')}
                                variant="outlined"
                                color="primary">Add Category</Button>
                        }
                        {
                            (view === 'AddView' || view === 'EditView') &&
                            <Button
                                variant="outlined"
                                onClick={changeView.bind(null, 'ManageView')}
                                color="primary">All V Categories</Button>
                        }
                    </div>
                    {
                        view === 'ManageView' &&

                        <TableMD
                            columns={headers}
                            title="Vehicle Categories"
                            data={category_list}
                            options={{
                                responsive: 'scrollMaxHeight'
                            }}
                        />
                    }

                    {(view === 'AddView' || view == 'EditView') &&
                        <ValidatorForm onSubmit={saveCategory}>

                            <Grid container spacing={6}>
                                <Grid md={6} sm={12} xs={12} item>
                                    <TextValidator
                                        className="mb-16 w-100"
                                        variant="outlined"
                                        label="Name"
                                        type="text"
                                        value={category.name ? category.name : ``}
                                        onChange={onChangeField}
                                        name="name"
                                        validators={["required"]}
                                        errorMessages={[
                                            "Name is required"
                                        ]}
                                    />
                                </Grid>
                                <Grid md={6} sm={12} xs={12} item>
                                    <TextValidator
                                        className="mb-16 w-100"
                                        variant="outlined"
                                        label="Number Of Seats"
                                        type="text"
                                        value={category.number_of_seats ? category.number_of_seats : ``}
                                        onChange={onChangeField}
                                        name="number_of_seats"
                                        validators={["required"]}
                                        errorMessages={[
                                            "Seat Count is required"
                                        ]}
                                    />
                                </Grid>
                            </Grid>

                            <Button
                                variant="contained"
                                color="primary"
                                type="submit">{view === 'AddView' ? 'Save' : 'Update'} Category</Button>

                        </ValidatorForm>
                    }
                </React.Fragment>}
        </div >
    );
}

export default withStyles({}, { withTheme: true })(VehicleCategories);
