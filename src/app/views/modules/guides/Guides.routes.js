import { EgretLoadable } from "egret";
import { authRoles } from "../../../auth/authRoles";

const Guides = EgretLoadable({
    loader: () => import("./Guides.component")
});

const guidesRoutes = [
    {
        name: "Guides",
        path: "/transport/guides",
        exact: true,
        component: Guides,
        auth: authRoles.admin,
        access: 'AUTH'
    },
    {
        name: "Guides",
        path: "/transport/guides/view/:id",
        exact: true,
        component: Guides,
        auth: authRoles.admin,
        access: 'AUTH'
    }
];

export default guidesRoutes;
