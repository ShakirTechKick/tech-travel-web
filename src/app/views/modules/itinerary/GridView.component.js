import React, { useState } from "react";
import TableMD from "mui-datatables";
import {
    Icon, IconButton, MenuItem, Button, Card,
    CardHeader, makeStyles, Avatar, CardContent, Typography, CardActions
} from "@material-ui/core";
import { withStyles } from "@material-ui/styles";
import { EgretMenu } from "egret";
import FlexRow from "app/helpers/FlexRow";
import ActionList from "app/helpers/ActionList";
import { useSelector } from "react-redux";
import { red } from "@material-ui/core/colors";
import { FileCopy, PictureInPictureAlt, UpdateOutlined, TrendingUpOutlined } from "@material-ui/icons";


const useStyles = makeStyles((theme) => ({
    root: {
        maxWidth: 345,
        marginRight: 10,
        marginBottom: 10
    },
    avatar: {
        backgroundColor: red[500],
    },
    small: {
        fontSize: 8
    }
}));


const GridView = (props) => {

    const { user } = useSelector(state => state.user);
    const { list } = useSelector(state => state.builder);

    const CheckPermission = (type) => {

        var module_exists = false;
        var action_permission = false;

        user.permissions.forEach(module => {
            if (Object.keys(module)[0] === "Itinerary") {
                module_exists = true;
                if (module['Itinerary'][type] === "ALLOWED") {
                    action_permission = true;
                }
            }
        });

        if (module_exists && action_permission) {
            return true;
        }

        return false;

    }

    const classes = useStyles();

    return (
        <div className="content">

            <FlexRow styles={{ ['justify-content']: 'flex-start' }}>

                {list.map(item => (
                    <Card className={classes.root}>
                        <CardHeader
                            avatar={
                                <Avatar aria-label="recipe" className={classes.avatar}>
                                    {item.days} <div className={classes.small}>Days</div>
                                </Avatar>
                            }
                            title="3 Days/4 Nights Sri Lanka"
                            action={
                                <EgretMenu menuButton={
                                    <IconButton>
                                        <Icon>more_vert</Icon>
                                    </IconButton>
                                }>
                                    <MenuItem style={{ minWidth: 185 }}>
                                        <Icon> cancel </Icon>
                                        <span className="pl-16">Cancel</span>
                                    </MenuItem>
                                    {!['DRAFT'].includes(item.status) &&
                                        <React.Fragment>
                                            <MenuItem style={{ minWidth: 185 }} onClick={() => props.setPreview(item.itinerary_id)}>
                                                <PictureInPictureAlt />
                                                <span className="pl-16">Preview</span>
                                            </MenuItem>
                                            <MenuItem style={{ minWidth: 185 }} onClick={() => props.makeCopy(item.itinerary_id)}>
                                                <FileCopy />
                                                <span className="pl-16">Clone This</span>
                                            </MenuItem>
                                            <MenuItem style={{ minWidth: 185 }} onClick={() => props.updateItinerary(item.itinerary_id)}>
                                                <UpdateOutlined />
                                                <span className="pl-16">Update Status</span>
                                            </MenuItem>
                                            {(item.status === 'APPROVED') &&
                                                <MenuItem style={{ minWidth: 185 }} disabled={item.converted === 'Y'} onClick={() => props.convertToTrip(item.itinerary_id)}>
                                                    <TrendingUpOutlined />
                                                    <span className="pl-16" style={item.converted === 'Y' ? { color: 'green', fontWeight: 'bold' } : {}}>
                                                        {item.converted === 'N' ? 'Convert To Trip' : 'Converted'}
                                                    </span>
                                                </MenuItem>
                                            }
                                        </React.Fragment>
                                    }
                                </EgretMenu>

                            } />

                        <CardContent>
                            <Typography>
                                By Kannan  On July 23, 2020  For Pradeepan
                            </Typography>
                        </CardContent>
                        <CardActions disableSpacing>
                            <ActionList del={CheckPermission('delete')} edit={CheckPermission('edit')}
                                id={item.itinerary_id} deleteEvent={props.deleteEvent}
                                editEvent={props.editEvent} status={item.status}  view={`/itinerary/builder/view/${item.itinerary_id}`}/>
                        </CardActions>
                    </Card>
                ))
                }

            </FlexRow>
        </div>
    )
}

export default withStyles({}, { withTheme: true })(GridView);