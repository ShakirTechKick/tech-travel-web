import { EgretLoadable } from "egret";
import { authRoles } from "../../../auth/authRoles";

const Itinerary = EgretLoadable({
  loader: () => import("./Itinerary.component")
});

const builderRoutes = [
  {
    name: 'Itinerary',
    path: "/itinerary/builder",
    exact: true,
    component: Itinerary,
    auth: authRoles.admin,
    access: 'AUTH'
  },
  {
    name: 'Itinerary',
    path: "/itinerary/builder/view/:id",
    exact: true,
    component: Itinerary,
    auth: authRoles.admin,
    access: 'AUTH'
  }
];

export default builderRoutes;
