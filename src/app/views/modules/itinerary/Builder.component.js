/* global google */
import React, { useState, useEffect } from 'react';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import { useSelector, useDispatch } from 'react-redux';
import { ValidatorForm, TextValidator } from 'react-material-ui-form-validator';
import {
    Grid, Paper, Icon, IconButton, Dialog, DialogTitle, CardHeader,
    DialogActions, DialogContent, Switch, TextField, Table, TableHead, TableRow, TableCell, TableBody, Card, CardContent, CardActionArea, FormControl, FormLabel, Radio, RadioGroup, FormControlLabel, ButtonGroup, InputAdornment
} from '@material-ui/core';
import Alert from '@material-ui/lab/Alert';
import FlexRow from 'app/helpers/FlexRow';
import { Dropdown, Divider } from 'semantic-ui-react';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Box from '@material-ui/core/Box';
import MD5 from "md5";
import {
    PlaceOutlined, Filter, LandscapeOutlined, Streetview, DeleteForever,
    LocalActivity, HotelOutlined, FileCopyOutlined, CalendarToday, Add, CancelOutlined, CheckBoxOutlineBlankOutlined, CheckCircleOutlineRounded, ClearAllRounded, MapRounded, DoneAllSharp, NotesOutlined, TimelapseRounded, TimeToLeave, CardTravel, TimelapseOutlined, CardTravelOutlined, DriveEtaRounded, DirectionsBusRounded, TrainRounded, Flight
} from '@material-ui/icons'

import { fetchClients } from 'app/redux/actions/modules/Clients.action';
import { fetchCountries, fetchCities } from 'app/redux/actions/modules/Places.action';
import { fetchHotels, fetchHotelStandards, fetchHotelCategories, fetchHotelRooms, fetchHotelMeals } from 'app/redux/actions/modules/Hotels.action';
import { fetchAttractions } from 'app/redux/actions/modules/Attractions.action';
import { fetchExcursions } from 'app/redux/actions/modules/Excursions.action';
import { fetchActivities, updateActivitiesList } from 'app/redux/actions/modules/Activities.action';
import { ToastsStore } from 'react-toasts';
import TokenGenerator from "rand-token";
import DateFnsUtils from '@date-io/date-fns';
import {
    MuiPickersUtilsProvider,
    KeyboardDatePicker,
} from '@material-ui/pickers';
import moment from 'moment';
import GoogleMapDirectionComponent from 'app/helpers/GoogleMapDirectionComponent';
import Geosuggest from 'react-geosuggest';
import Slider from 'react-slick';
import ClientsComponent from '../clients/Clients.component';
import PlacesComponent from '../places/Places.component';
import AttractionsComponent from '../attractions/Attractions.component';
import ExcursionsComponent from '../excursions/Excursions.component';
import HotelsComponent from '../hotels/Hotels.component';
import { isUndefined, isNull } from 'lodash';
import { Request } from 'app/config/Request';

const useStyles = makeStyles((theme) => ({
    root: {
        width: '100%',
        marginTop: 15
    },
    button: {
        marginRight: theme.spacing(1),
    },
    instructions: {
        marginTop: theme.spacing(1),
        marginBottom: theme.spacing(1),
    },
    rootvertical: {
        flexGrow: 1,
        backgroundColor: theme.palette.background.paper,
        display: 'flex',
        height: '100%',
        padding: 15
    },
    verticaltabs: {
        borderRight: `1px solid ${theme.palette.divider}`,
    },
    small: { fontSize: 12 },
    rootHorizontal: {
        flexGrow: 1,
        maxWidth: '100%'
    },
    horizontaltabs: {
        width: '100%',
        borderBottomWidth: 0
    },
    block: {
        width: '100%',
        padding: 15,
        background: 'rgb(236, 236, 225)'
    },
    bottom: { marginBottom: 10 }
}));


const Builder = (props) => {
    const dispatch = useDispatch();

    const [repeat, setRepeat] = React.useState(null);

    const [datepicker, setDatePicker] = React.useState({
        datestamp: new Date(), open: 'closed',
        onCancel: () => setDatePicker({ ...datepicker, open: 'closed' }),
    });

    const pickDateStamp = (datestamp) => {
        setDatePicker({ ...datepicker, datestamp });
    };

    const classes = useStyles();
    const [activeStep, setActiveStep] = React.useState(0);
    const [skipped, setSkipped] = React.useState(new Set());


    const { countries, cities } = useSelector(state => state.places);

    const { list: clients } = useSelector(state => state.clients);
    const { list: hotels, standards, categories: all_categories, rooms: all_rooms, meals: all_meals } = useSelector(state => state.hotels);
    const { list: attractions } = useSelector(state => state.attractions);
    const { list: excursions } = useSelector(state => state.excursions);
    const { list: activities } = useSelector(state => state.activities);

    const [activities_list, setActivitiesList] = useState([...activities]);

    const [hotelFilter, setHotelFilter] = useState({ apply: false, standard: null });


    const [DistanceMatrix, setDistanceMatrix] = useState({
        closeView: () => setDistanceMatrix({ ...DistanceMatrix, open: 'closed' }),
        open: 'closed'
    });

    const [MapView, setMapView] = useState({
        closeView: () => setMapView({ ...MapView, open: 'closed' }),
        open: 'closed'
    });

    const [ErrorDialog, setErrorDialog] = useState({
        header: 'Not Allowed',
        body: 'Please check what went wrong !',
        onCloseDialog: () => setErrorDialog({ ...ErrorDialog, open: 'closed' }),
        open: 'closed'
    });

    const [ComponentDialog, setComponentDialog] = useState({
        header: 'Component',
        body: 'Component Body',
        component: null,
        closeView: () => setComponentDialog({ ...ComponentDialog, open: 'closed' }),
        open: 'closed'
    });

    const [TransportDialog, setTransportDialog] = useState({
        closeView: () => setTransportDialog({ ...TransportDialog, open: 'closed' }),
        open: 'closed', day: null, destination_id: null
    });

    const [DialogConfirm, setConfirmationDialog] = useState({
        header: 'Confirm',
        body: 'Do you confirm the action',
        onCancel: () => setConfirmationDialog({ ...DialogConfirm, open: 'closed' }),
        onSuccess: null,
        open: 'closed'
    })

    const [activityModal, setActivityModal] = useState({
        day: null,
        activity: null,
        open: 'closed',
        save: false,
        item: null
    });

    const [InclusionText, setInclusionText] = useState(null);
    const [ExclusionText, setExclusionText] = useState(null);
    const [NoteText, setNoteText] = useState(null);

    const onAddActivityItem = () => {

        const { day, activity, save, item } = activityModal;

        setActivitiesList([...activities_list.filter(a => a.activity_id !== activity.activity_id), {
            ...activity,
            activities: activity.activities ? [...activity.activities.split(','), item].join() : item
        }]);

        const activities = itinerary.content.days.find(d => d.id === day.id).activities;

        const days = [...itinerary.content.days];

        activities.splice(activities.findIndex(a => a.activity_id === activity.activity_id), 1, {
            ...activity, activities: activity.activities ? [...activity.activities.split(','), item].join() : item
        });

        days.splice(days.findIndex(d => d.id === day.id), 1, { ...day, activities });

        setItinerary({
            ...itinerary, content: {
                ...itinerary.content,
                days: days
            }
        });

        if (save) {
            dispatch(updateActivitiesList({
                activity_id: activity.activity_id,
                activities: [...activities.find(a => a.activity_id === activity.activity_id).activities.split(',')]
            }));
        }

        setActivityModal({ day: null, activity: null, item: null, save: false, open: 'closed' });

    }

    const onClickAddActivity = (day, activity) => {
        setActivityModal({ ...activityModal, day, activity, open: 'open' });
    }

    const { user: auth_user } = useSelector(state => state.user);

    const original = {
        company_id: auth_user.data.company_id,
        created_by: auth_user.data.user_id,
        countries: [],
        pax: 1,
        days: 1,
        name: null,
        client_id: null,
        currency: 'USD',
        content: {
            days: [],
            costing: {
                accomodation: [],
                excursions: [],
                transportation: [],
                subcost: null,
                taxes: [],
                method: 'amount',
                percent: null,
                profit: null,
                sub_package_cost: null,
                grand_package_cost: null,
                process: null, status: "NOTCONFIRMED"
            },
            notes: [],
            inclusions: [],
            exclusions: [],
            distance_duration_matrix: { locations: [], total: null }
        },
        status: 'DRAFT',
        datasource_provider: 'Local'
    };

    const build = props.current ? props.current : original;

    const [itinerary, setItinerary] = useState({ ...build });

    const getSteps = () => {
        if (props.current) {
            return ['Edit Itinerary', 'Complete Itinerary'];
        }
        return ['Basic Tour Details', 'Build Itinerary', 'Complete Itinerary'];
    }

    const TabPanelVertical = (props) => {
        const { children, value, index, ...other } = props;

        return (
            <div
                component="div"
                role="tabpanel"
                style={{ width: '100%' }}
                hidden={value !== index}
                id={`vertical-tabpanel-${index}`}
                aria-labelledby={`vertical-tab-${index}`}
                {...other}
            >
                {value === index && <Box p={3}>{children}</Box>}
            </div>
        );
    }

    const verticalTabProps = (index) => {

        return {
            id: `vertical-tab-${index}`,
            'aria-controls': `vertical-tabpanel-${index}`,
        };

    }


    const TabPanelHorizontal = (props) => {
        const { children, value, index, ...other } = props;

        return (
            <Typography
                component="div"
                role="tabpanel"
                hidden={value !== index}
                id={`scrollable-prevent-tabpanel-${index}`}
                aria-labelledby={`scrollable-prevent-tab-${index}`}
                {...other}
            >
                {value === index && <Box p={3}>{children}</Box>}
            </Typography>
        );
    }

    const horizontalTabProps = (index) => {
        return {
            id: `scrollable-prevent-tab-${index}`,
            'aria-controls': `scrollable-prevent-tabpanel-${index}`,
        };
    }



    const [valueVerticalTab, setValueVerticalTab] = React.useState(0);
    const onChangeVerticalTabs = (event, index) => {
        setValueHorizontal(0);
        setValueVerticalTab(index);
    }


    const [valueHorizontal, setValueHorizontal] = React.useState(0);
    const onChangeHorizontalTabs = (event, index) => {
        setValueHorizontal(index);
    }


    const [valueHorizontalFinal, setValueHorizontalFinal] = React.useState(0);
    const onChangeHorizontalTabsFinal = (event, index) => {
        setValueHorizontalFinal(index);
    }

    const onChangeField = (e) => {
        setItinerary({ ...itinerary, [e.target.name]: e.target.value });
    }

    const onSubmitBasicComponent = () => {

        if (itinerary.countries.length) {

            const Temp = [];

            for (var i = 0; i < itinerary.days; i++) {
                Temp.push(i)
            }

            setItinerary({
                ...itinerary, content: {
                    days: Temp.map((day) => {
                        return {
                            id: day, destinations: [], hotel: { selection: null, rooms: [] }, attractions: [], excursions: [], activities: []
                        }
                    }),
                    costing: {
                        accomodation: [],
                        excursions: [],
                        transportation: [],
                        subcost: null,
                        taxes: [],
                        method: 'amount',
                        percent: null,
                        profit: null,
                        sub_package_cost: null,
                        grand_package_cost: null,
                        process: null
                    },
                    notes: [],
                    inclusions: [],
                    exclusions: [],
                    distance_duration_matrix: {
                        locations: [], total: null
                    }
                }
            });

            handleNext();
        } else {

            setErrorDialog({
                ...ErrorDialog, open: 'open', body: 'Please select atleast 1 country'
            });

        }
    }

    const openComponentDialog = (component, header) => {
        setComponentDialog({ ...ComponentDialog, open: 'open', component, header })
    }

    const BasicComponent = () => (
        <div className={classes.root}>
            <ValidatorForm onSubmit={onSubmitBasicComponent}>
                <Grid container spacing={6}>
                    <Grid md={4} sm={12} item>
                        Local DB <Switch defaultChecked={itinerary.datasource_provider === 'Global'}
                            onChange={event => setItinerary({ ...itinerary, datasource_provider: event.target.checked ? 'Global' : 'Local' })} /> Global DB
                    </Grid>
                    <Grid md={4} sm={12} item>
                        <TextValidator
                            className="mb-16 w-100"
                            variant="outlined"
                            label="Name (Optional)"
                            type="text"
                            value={itinerary.name}
                            onChange={onChangeField}
                            name="name"
                        />
                    </Grid>
                    <Grid md={4} sm={12} item>
                        <Dropdown
                            placeholder='Select Client'
                            fluid search clearable selection
                            value={itinerary.client_id ? itinerary.client_id : null}
                            onChange={(event, { value }) => setItinerary({ ...itinerary, client_id: value })}
                            options={clients.map((m, index) => {
                                return { key: index, text: m.name, value: m.client_id }
                            })}
                            noResultsMessage={<Button fullWidth variant='outlined' onClick={openComponentDialog.bind(null, <ClientsComponent modal />, 'Add Client')}>Add Client</Button>}
                        />
                    </Grid>
                </Grid>
                <Grid container spacing={6}>
                    <Grid md={2} sm={12} item>
                        <TextValidator
                            className="mb-16 w-100"
                            variant="outlined"
                            label="Number Of Days"
                            type="text"
                            value={itinerary.days}
                            onChange={onChangeField}
                            name="days"
                            validators={["required", "minNumber:1"]}
                            errorMessages={[
                                "Days is required",
                                "Minimum number of days is 1"
                            ]}
                        />
                    </Grid>
                    <Grid md={4} sm={12} item>
                        <Dropdown
                            placeholder='Select Countries'
                            fluid selection
                            multiple clearable basic search
                            value={itinerary.countries ? itinerary.countries : []}
                            onChange={(event, { value }) => setItinerary({ ...itinerary, countries: value })}
                            options={countries.map((m, index) => {
                                return { key: index, text: m.name, value: m.country_id }
                            })}
                        />
                    </Grid>
                    <Grid md={4} sm={12} item>
                        <TextValidator
                            className="mb-16 w-100"
                            variant="outlined"
                            label="Number Of Pax"
                            type="text"
                            value={itinerary.pax}
                            onChange={onChangeField}
                            name="pax"
                            min={1}
                            validators={["isNumber"]}
                            errorMessages={[
                                "Pax count must be a number"
                            ]}
                        />
                    </Grid>
                    <Grid md={2} sm={12} item>
                        <Dropdown
                            placeholder='Select Currency'
                            fluid
                            selection
                            search
                            value={itinerary.currency ? itinerary.currency : 'USD'}
                            onChange={(event, { value }) => setItinerary({ ...itinerary, currency: value })}
                            options={['USD', 'EURO', 'AUD', 'SGD', 'MYR', 'INR', 'LKR', 'AED'].map((currency, index) => {
                                return { key: index, text: currency, value: currency }
                            })}
                        />
                    </Grid>
                </Grid>
                <Button
                    style={{['margin-top']: '10px'}} variant="outlined" color="primary"
                    type="submit">Submit</Button>
            </ValidatorForm>
        </div>
    )





    const onClickAddRoom = (day) => {
        const rooms = itinerary.content.days.find(d => d.id === day.id).hotel.rooms;
        const id = !!rooms.length ? rooms[rooms.length - 1].id + 1 : rooms.length;
        const room = { id, category: null, type_room: null, type_meal: null, number_of_rooms: 1, rate_per_day: null };

        const days = [...itinerary.content.days];

        days.splice(days.findIndex(d => d.id === day.id), 1, { ...day, hotel: { selection: day.hotel.selection, rooms: [...rooms, room] } });

        setItinerary({
            ...itinerary, content: {
                ...itinerary.content,
                days: days
            }
        })
    }


    const onClickRemoveRoom = (day, room) => {
        const rooms = itinerary.content.days.find(d => d.id === day.id).hotel.rooms;

        const days = [...itinerary.content.days];

        days.splice(days.findIndex(d => d.id === day.id), 1, { ...day, hotel: { selection: day.hotel.selection, rooms: [...rooms.filter(r => r.id !== room.id)] } });

        setItinerary({
            ...itinerary, content: {
                ...itinerary.content,
                days: days
            }
        })
    }

    const onClickRemoveAttraction = (day, attraction) => {
        const attractions = itinerary.content.days.find(d => d.id === day.id).attractions;

        const days = [...itinerary.content.days];

        days.splice(days.findIndex(d => d.id === day.id), 1, { ...day, attractions: [...attractions.filter(a => a.attraction_id !== attraction.attraction_id)] });

        setItinerary({
            ...itinerary, content: {
                ...itinerary.content,
                days: days
            }
        })
    }

    const onClickRemoveExcursion = (day, excursion) => {
        const excursions = itinerary.content.days.find(d => d.id === day.id).excursions;

        const days = [...itinerary.content.days];

        days.splice(days.findIndex(d => d.id === day.id), 1, { ...day, excursions: [...excursions.filter(e => e.excursion_id !== excursion.excursion_id)] });

        setItinerary({
            ...itinerary, content: {
                ...itinerary.content,
                days: days
            }
        })
    }

    const onRemoveActivityItem = (day, activity, item) => {
        const activities = itinerary.content.days.find(d => d.id === day.id).activities;

        const days = [...itinerary.content.days];

        activities.splice(activities.findIndex(a => a.activity_id === activity.activity_id), 1, {
            ...activity, activities: activity.activities.split(',').filter(a => a !== item).join()
        });

        if (!activities.find(a => a.activity_id === activity.activity_id).activities) {
            activities.splice(activities.findIndex(a => a.activity_id === activity.activity_id), 1);
        }

        days.splice(days.findIndex(d => d.id === day.id), 1, { ...day, activities });

        setItinerary({
            ...itinerary, content: {
                ...itinerary.content,
                days: days
            }
        });

    }



    const onAddMoreDays = () => {

        const days = itinerary.content.days;

        const day = { id: (days[days.length - 1].id + 1), destinations: [], hotel: { selection: null, rooms: [] }, attractions: [], excursions: [], activities: [] };
        setItinerary({
            ...itinerary, content: { ...itinerary.content, days: [...days, day] }, days: itinerary.days + 1
        });
    }



    const onDeleteCurrentDay = (day) => {

        const days = itinerary.content.days;

        days.splice(days.findIndex(d => d.id === day.id), 1);

        setItinerary({
            ...itinerary, content: { ...itinerary.content, days: [...days] }, days: itinerary.days - 1
        });

        DialogConfirm.onCancel();
    }

    const onCopyToNextDay = (day) => {
        const days = itinerary.content.days;

        const nextIndex = days.findIndex(d => d.id === day.id) + 1;

        if (typeof days[nextIndex] !== 'undefined') {
            const copy = { ...day, id: days[nextIndex].id };

            days.splice(nextIndex, 1, copy);

            setItinerary({
                ...itinerary, content: { ...itinerary.content, days: [...days] }
            });

            ToastsStore.info("Current day's data is copies to next day", 2000);

        } else {
            ToastsStore.error("There no next day exists !", 2000);
        }

        DialogConfirm.onCancel();
    }

    const onClickDatePicker = (day) => {
        setDatePicker({ ...datepicker, open: 'open', day });
    }

    const onDatePicked = (datestamp) => {

        const days = itinerary.content.days;

        const { day } = datepicker;

        const current = days.findIndex(d => d.id === day.id);

        const start = moment(datestamp).subtract('day', current);

        var counter = 0;

        const datestamp_updated_days = days.map(d => {
            d.datestamp = moment(start).add('day', counter++);

            return d;
        });

        setItinerary({
            ...itinerary, content: { ...itinerary.content, days: [...datestamp_updated_days] }
        });

        setDatePicker({ ...datepicker, open: 'closed', day: null });
    }

    const onRemoveDateStamps = () => {
        const days = itinerary.content.days;

        const datestamp_updated_days = days.map(d => {
            delete d.datestamp
            return d;
        });

        setItinerary({
            ...itinerary, content: { ...itinerary.content, days: [...datestamp_updated_days] }
        });

    }

    const suggestSelect = (day, category, suggest) => {

        if (suggest) {


            const { location, gmaps } = suggest;

            const days = [...itinerary.content.days];

            const images = [];
            var counter = 0;

            const caption = gmaps.name.replace(/\s/g, String.apply());

            gmaps.photos.filter((item, index) => index < 2).forEach(photo => {
                Request.post(`utilities/fileuploader/download`, { url: photo.getUrl(), filepath: `google-${caption}-${counter++}-attraction-photo.jpg` })
                    .then(result => {
                        if (result.data.status === 'success') {
                            images.push(`/${result.data.path.replace('public/uploads/', '')}`);
                        }

                        if (images.length == 2) {

                            const photos = images.join();

                            if (category === 'destination') {

                                const destination = {
                                    id: day.destinations.length ? day.destinations[day.destinations.length - 1].id + 1 : 0,
                                    name: gmaps.name, place_id: gmaps.place_id, location, type: 'Global', images: photos
                                }

                                days.splice(days.findIndex(d => d.id === day.id), 1, { ...day, destinations: [...day.destinations, destination] });

                            }

                            if (category === 'hotel') {

                                const hotel = {
                                    name: gmaps.name, place_id: gmaps.place_id, location, type: 'Global',
                                    images: photos, url: gmaps.url, website: gmaps.website, address: gmaps.formatted_address, phone_number: gmaps.international_phone_number
                                }

                                days.splice(days.findIndex(d => d.id === day.id), 1, { ...day, hotel: { selection: hotel, rooms: day.hotel.rooms } });

                            }

                            if (category === 'attraction') {

                                const attraction = {
                                    id: day.attractions.length ? day.attractions[day.attractions.length - 1].id + 1 : 0,
                                    name: gmaps.name, place_id: gmaps.place_id, location, type: 'Global',
                                    images: photos, url: gmaps.url, website: gmaps.website, address: gmaps.formatted_address, phone_number: gmaps.international_phone_number
                                }

                                days.splice(days.findIndex(d => d.id === day.id), 1, { ...day, attractions: [...day.attractions, attraction] });

                            }

                            setItinerary({
                                ...itinerary, content: {
                                    ...itinerary.content,
                                    days: days
                                }
                            })

                        }

                    }).catch(error => null);

            })

        }
    }


    const onRemoveGlobalItem = (day, category, id) => {
        const days = [...itinerary.content.days];

        if (category === 'destination') {

            days.splice(days.findIndex(d => d.id === day.id), 1, { ...day, destinations: [...day.destinations.filter(d => d.id !== id)] });

        }

        if (category === 'attraction') {

            days.splice(days.findIndex(d => d.id === day.id), 1, { ...day, attractions: [...day.attractions.filter(d => d.id !== id)] });

        }

        setItinerary({
            ...itinerary, content: {
                ...itinerary.content,
                days: days
            }
        })
    }



    const calculateDistanceMatrix = (modal = null) => {


        const DistanceMatrixSystem = new google.maps.DistanceMatrixService();

        const places = [];

        itinerary.content.days.forEach(day => {
            day.destinations.forEach(destination => {

                var place = destination;

                if (!(place.location.lat && place.location.lng)) {
                    place = { ...place, location: JSON.parse(place.location) };
                }

                if (places.length) {

                    if (!(places[places.length - 1].lat === place.location.lat && places[places.length - 1].lng === place.location.lng)) {
                        places.push(place);
                    }

                } else {
                    places.push(place);
                }

            })
        })

        DistanceMatrixSystem.getDistanceMatrix({
            origins: [...places.map(p => p.location)],
            destinations: [...places.map(p => p.location)],
            travelMode: 'DRIVING'
        }, (result, status) => {
            if (status === google.maps.DistanceMatrixStatus.OK) {

                const { distance_duration_matrix } = itinerary.content;

                var total = 0;

                const distance_matrix = [];

                places.forEach((p, index) => {
                    if (index === 0) {
                        distance_matrix.push({
                            name: p.name, distance: 0, duration: 0, city_tours: distance_duration_matrix.locations.find(dm => dm.name === p.name) ?
                                distance_duration_matrix.locations.find(dm => dm.name === p.name).city_tours : 0
                        });
                    } else {

                        total += (result.rows[index - 1].elements[index].distance.value);

                        distance_matrix.push({
                            name: p.name, distance: result.rows[index - 1].elements[index].distance,
                            duration: result.rows[index - 1].elements[index].duration, city_tours: distance_duration_matrix.locations.find(dm => dm.name === p.name) ?
                                distance_duration_matrix.locations.find(dm => dm.name === p.name).city_tours : 0
                        })
                    }
                });

                setItinerary({ ...itinerary, content: { ...itinerary.content, distance_duration_matrix: { locations: distance_matrix, total: Math.floor((total / 1000)), total_city_tours: distance_duration_matrix.total_city_tours ? distance_duration_matrix.total_city_tours : 0 } } });

            } else {
                console.error(`error fetching directions ${result}`);
            }
        })

        setDistanceMatrix({ ...DistanceMatrix, open: modal ? 'open' : 'closed' });
    }



    const loadTransportIcon = (mode) => {
        switch (mode) {
            case 'Car':
                return <DriveEtaRounded />;
            case 'Bus':
                return <DirectionsBusRounded />;
            case 'Train':
                return <TrainRounded />;
            case 'Flight':
                return <Flight />;
            default:
                return <DriveEtaRounded />;
        }
    }

    const BuilderComponent = () => (
        <div className={classes.rootvertical}>
            <Tabs
                orientation="vertical"
                variant="scrollable"
                value={valueVerticalTab}
                onChange={onChangeVerticalTabs}
                aria-label="Days Vertical Tabs"
                className={classes.verticaltabs}>
                {
                    itinerary.content.days.map((day, index) => (
                        <Tab className='vertical-itinerary-tabs' label={'Day ' + (index + 1)} {...verticalTabProps(index)} wrapped />
                    ))
                }

            </Tabs>

            {
                itinerary.content.days.map((day, index) => (
                    <TabPanelVertical value={valueVerticalTab} index={index}>

                        <FlexRow>
                            <FlexRow>
                                <Button variant='outlined' className={classes.button} onClick={onAddMoreDays}>
                                    <Add /> Add
                                </Button>
                                <Button variant='outlined' className={classes.button} onClick={() => {
                                    setConfirmationDialog({ ...DialogConfirm, open: 'open', onSuccess: onCopyToNextDay.bind(null, day) });
                                }}>
                                    <FileCopyOutlined /> Copy to Next
                                    </Button>
                                <Button variant='outlined' className={classes.button} onClick={() => {
                                    setConfirmationDialog({ ...DialogConfirm, open: 'open', onSuccess: onDeleteCurrentDay.bind(null, day) });
                                }}>
                                    <DeleteForever /> Delete Day
                                </Button>

                                <Button variant='outlined' className={classes.button} onClick={() => {
                                    if ((!!itinerary.content.days.length && !!itinerary.content.days[0].destinations.length)) {
                                        setMapView({ ...MapView, open: 'open' })
                                    }
                                }}>
                                    <MapRounded /> Open Map
                                </Button>
                                <IconButton onClick={calculateDistanceMatrix.bind(null, 'modal')} title="Distance & Travel Time Calculator">
                                    <TimelapseRounded />
                                </IconButton>
                            </FlexRow>
                            <FlexRow>
                                {((!!day.datestamp) &&
                                    <IconButton onClick={onRemoveDateStamps}>
                                        <CancelOutlined />
                                    </IconButton>
                                )}
                                <IconButton onClick={onClickDatePicker.bind(null, day)}>
                                    <CalendarToday />
                                </IconButton>
                                {
                                    (!!day.datestamp ?
                                        <div style={{ fontSize: 25, fontWeight: 'bold' }}>
                                            {moment(day.datestamp).format('MMM DD YYYY, ddd')}
                                        </div> : <div style={{ fontSize: 15, fontWeight: 'bold' }}>Date Not Set</div>)
                                }


                            </FlexRow>
                        </FlexRow>

                        <Divider clearing section />

                        <Tabs
                            value={valueHorizontal}
                            onChange={onChangeHorizontalTabs}
                            variant="fullWidth"
                            indicatorColor="secondary"
                            textColor="secondary"
                            aria-label="Horizontal Itineray Builder Tabs"
                        >
                            <Tab icon={<PlaceOutlined />} label="Destination" {...horizontalTabProps(0)} />
                            <Tab icon={<HotelOutlined />} label="Hotel"  {...horizontalTabProps(1)} />
                            <Tab icon={<LandscapeOutlined />} label="Attractions"  {...horizontalTabProps(2)} />
                            <Tab icon={<Streetview />} label="Excursion"  {...horizontalTabProps(3)} />
                            <Tab icon={<LocalActivity />} label="Activities"  {...horizontalTabProps(4)} />

                        </Tabs>
                        <TabPanelHorizontal value={valueHorizontal} index={0}>

                            <Grid container spacing={6}>

                                <Grid md={6} sm={12} item>

                                    {
                                        itinerary.datasource_provider === 'Global' ?

                                            <Geosuggest
                                                placeholder="Find Destination"
                                                country={[...countries.filter(c => itinerary.countries.includes(c.country_id)).map(c => c.code)]}
                                                types={['(cities)']}
                                                onSuggestSelect={suggestSelect.bind(null, day, 'destination')}
                                            /> :

                                            <Dropdown
                                                placeholder='Select Destination'
                                                fluid selection
                                                multiple clearable basic search
                                                noResultsMessage={<Button fullWidth variant='outlined' onClick={openComponentDialog.bind(null, <PlacesComponent modal />, 'Add Destination')}>Add Destination</Button>}
                                                value={!!itinerary.content.days.find(d => d.id === day.id).destinations.length ?
                                                    itinerary.content.days.find(d => d.id === day.id).destinations.map(d => d.city_id) : []}
                                                onChange={(event, { value }) => {

                                                    const days = [...itinerary.content.days];

                                                    days.splice(days.findIndex(d => d.id === day.id), 1, {
                                                        ...day, destinations: value.map(city_id => {
                                                            return { ...cities.find(city => city.city_id === city_id), transport: { mode: 'Car' } }
                                                        })
                                                    });

                                                    setItinerary({
                                                        ...itinerary, content: {
                                                            ...itinerary.content,
                                                            days: days
                                                        }
                                                    })
                                                }}
                                                options={cities.filter(c => [...itinerary.countries].includes(c.country_id)).map((m, index) => {
                                                    return { key: index, text: m.name, value: m.city_id }
                                                })}
                                            />

                                    }


                                </Grid>

                            </Grid>

                            <Grid container spacing={6}>
                                {
                                    (!!itinerary.content.days.find(d => d.id === day.id)) &&
                                    (
                                        (!!itinerary.content.days.find(d => d.id === day.id).destinations.length) &&

                                        itinerary.content.days.find(d => d.id === day.id).destinations.map(destination => (
                                            <Grid md={12} item>
                                                <div className={classes.block}>
                                                    <FlexRow>
                                                        <FlexRow styles={{ justifyContent: 'flex-start' }}>
                                                            <PlaceOutlined />
                                                            <span style={{ fontWeight: 'bold' }}>{destination.name}</span>
                                                        </FlexRow>
                                                        <ButtonGroup>
                                                            <Button variant='contained' disabled>
                                                                {destination.transport ?
                                                                    <React.Fragment>
                                                                        {loadTransportIcon(destination.transport.mode)}
                                                                        <span>{destination.transport.mode}</span>
                                                                    </React.Fragment> : <React.Fragment>{loadTransportIcon('Car')} <span>Car</span></React.Fragment>}
                                                            </Button>
                                                            <Button variant='outlined' onClick={() => setTransportDialog({ ...TransportDialog, open: 'open', day, destination_id: destination.id })}>
                                                                <CardTravelOutlined /> Add Transport
                                                            </Button>
                                                        </ButtonGroup>


                                                        {
                                                            (destination.type && destination.type === 'Global') &&
                                                            <IconButton onClick={onRemoveGlobalItem.bind(null, day, 'destination', destination.id)}>
                                                                <Icon>cancel</Icon>
                                                            </IconButton>
                                                        }
                                                    </FlexRow>
                                                </div>
                                            </Grid>
                                        ))
                                    )
                                }
                            </Grid>

                        </TabPanelHorizontal>
                        <TabPanelHorizontal value={valueHorizontal} index={1}>
                            {(itinerary.content.days.length && itinerary.content.days[itinerary.content.days.length - 1].id === day.id) ?
                                <div className="final-day-tour">Final day of the tour. Please add a day to add hotel !.</div> :
                                <React.Fragment>

                                    <div className="hotel-filter-bar">
                                        <FlexRow style={{ padding: '5px', background: 'whitesmoke', borderRadius: '5ps' }}>
                                            <FlexRow styles={{ justifyContent: 'flex-start' }}>
                                                <Filter />
                                                <span style={{ fontWeight: 'bold' }}>Filter</span>
                                            </FlexRow>
                                            <div style={{ width: '200px' }}>
                                                <Dropdown
                                                    placeholder='By Hotel Standards'
                                                    fluid selection
                                                    multiple clearable basic search
                                                    onChange={(event, { value }) => { }}
                                                    options={standards.map((m, index) => {
                                                        return { key: index, text: m.name, value: m.standard_id }
                                                    })}
                                                />
                                            </div>
                                        </FlexRow>
                                    </div>

                                    <Divider clearing section />

                                    <Grid container spacing={6}>

                                        <Grid md={6} sm={12} item>
                                            {day.destinations.length ?

                                                (itinerary.datasource_provider === 'Global' ?

                                                    <Geosuggest
                                                        placeholder="Find Hotel"
                                                        country={[...countries.filter(c => itinerary.countries.includes(c.country_id)).map(c => c.code)]}
                                                        types={['establishment']}
                                                        onSuggestSelect={suggestSelect.bind(null, day, 'hotel')}
                                                    /> :

                                                    <Dropdown
                                                        placeholder='Select Hotel'
                                                        fluid search
                                                        clearable selection
                                                        noResultsMessage={<Button fullWidth variant='outlined' onClick={openComponentDialog.bind(null, <HotelsComponent modal />, 'Add Hotel')}>Add Hotel</Button>}
                                                        value={itinerary.content.days.find(d => d.id === day.id).hotel.selection ?
                                                            itinerary.content.days.find(d => d.id === day.id).hotel.selection.hotel_id : null}
                                                        onChange={(event, { value }) => {

                                                            const days = [...itinerary.content.days];

                                                            days.splice(days.findIndex(d => d.id === day.id), 1, { ...day, hotel: { selection: hotels.find(c => c.hotel_id === value), rooms: day.hotel.rooms } });

                                                            setItinerary({
                                                                ...itinerary, content: {
                                                                    ...itinerary.content,
                                                                    days: days
                                                                }
                                                            })
                                                        }}
                                                        options={hotels.filter(h => day.destinations.map(d => d.city_id).includes(h.city_id)).map((m, index) => {
                                                            return { key: index, text: `${m.name} (${m.standard})`, value: m.hotel_id }
                                                        })}
                                                    />
                                                ) : <div>Please select a destination to pick a hotel !</div>
                                            }
                                        </Grid>

                                    </Grid>

                                    <Grid container spacing={6}>
                                        {
                                            (!!itinerary.content.days.find(d => d.id === day.id)) &&
                                            (
                                                !!itinerary.content.days.find(d => d.id === day.id).hotel.selection &&

                                                <Grid md={12} item>
                                                    <div className={classes.block}>
                                                        <FlexRow styles={{ justifyContent: 'flex-start' }}>
                                                            <HotelOutlined />
                                                            <span style={{ fontWeight: 'bold' }}>{itinerary.content.days.find(d => d.id === day.id).hotel.selection.name}</span>
                                                        </FlexRow>
                                                        <FlexRow style={{ backgroundColor: 'rgba(240,240,240, 1)' }}>
                                                            <div>
                                                                <div>{itinerary.content.days.find(d => d.id === day.id).hotel.selection.address}</div>
                                                                <div>{itinerary.content.days.find(d => d.id === day.id).hotel.selection.phone_number}</div>
                                                            </div>
                                                            <div style={{ width: 250 }}>

                                                                <Dropdown
                                                                    placeholder='Standard'
                                                                    fluid clearable selection
                                                                    value={itinerary.content.days.find(d => d.id === day.id).hotel.selection.standard ?
                                                                        itinerary.content.days.find(d => d.id === day.id).hotel.selection.standard : null}
                                                                    onChange={(event, { value }) => {

                                                                        const days = [...itinerary.content.days];

                                                                        days.splice(days.findIndex(d => d.id === day.id), 1,
                                                                            { ...day, hotel: { selection: { ...day.hotel.selection, standard: value }, rooms: day.hotel.rooms } });


                                                                        setItinerary({
                                                                            ...itinerary, content: {
                                                                                ...itinerary.content,
                                                                                days: days
                                                                            }
                                                                        })
                                                                    }}
                                                                    options={standards.map((m, index) => {
                                                                        return { key: index, text: m.name, value: m.name }
                                                                    })}
                                                                />

                                                                <Divider clearing />

                                                                {!(itinerary.content.days.find(d => d.id === day.id).hotel.selection.type &&
                                                                    itinerary.content.days.find(d => d.id === day.id).hotel.selection.type === 'Global') &&
                                                                    <React.Fragment>
                                                                        <div>Contact Person: {itinerary.content.days.find(d => d.id === day.id).hotel.selection.contact_person}</div>
                                                                        <div>{itinerary.content.days.find(d => d.id === day.id).hotel.selection.contact_person_email}</div>
                                                                        <div>{itinerary.content.days.find(d => d.id === day.id).hotel.selection.contact_person_phone_number}</div>
                                                                    </React.Fragment>
                                                                }
                                                                {!!itinerary.content.days.find(d => d.id === day.id).hotel.selection.website &&
                                                                    <div>
                                                                        <a target="_blank" href={itinerary.content.days.find(d => d.id === day.id).hotel.selection.website}>Visit Web</a>
                                                                    </div>
                                                                }

                                                            </div>
                                                        </FlexRow>
                                                        <Divider horizontal />

                                                        <FlexRow>
                                                            <Button variant='outlined' onClick={onClickAddRoom.bind(null, day)}>Add Room</Button>
                                                            <div>
                                                                <TextField placeholder="Checkin"
                                                                    style={{ ['margin-left']: 10 }} variant='filled'
                                                                    defaultValue={itinerary.content.days.find(d => d.id === day.id).hotel.selection.checkin ? itinerary.content.days.find(d => d.id === day.id).hotel.selection.checkin : null}
                                                                    onBlur={e => {
                                                                        const days = [...itinerary.content.days];

                                                                        days.splice(days.findIndex(d => d.id === day.id), 1,
                                                                            { ...day, hotel: { selection: { ...day.hotel.selection, checkin: e.target.value }, rooms: day.hotel.rooms } });


                                                                        setItinerary({
                                                                            ...itinerary, content: {
                                                                                ...itinerary.content,
                                                                                days: days
                                                                            }
                                                                        })
                                                                    }}
                                                                />

                                                                <TextField placeholder="Checkout"
                                                                    defaultValue={itinerary.content.days.find(d => d.id === day.id).hotel.selection.checkout ? itinerary.content.days.find(d => d.id === day.id).hotel.selection.checkout : null}
                                                                    style={{ ['margin-left']: 10 }} variant='filled'
                                                                    onBlur={e => {
                                                                        const days = [...itinerary.content.days];

                                                                        days.splice(days.findIndex(d => d.id === day.id), 1,
                                                                            { ...day, hotel: { selection: { ...day.hotel.selection, checkout: e.target.value }, rooms: day.hotel.rooms } });


                                                                        setItinerary({
                                                                            ...itinerary, content: {
                                                                                ...itinerary.content,
                                                                                days: days
                                                                            }
                                                                        })
                                                                    }}
                                                                />
                                                            </div>
                                                        </FlexRow>


                                                    </div>
                                                </Grid>
                                            )
                                        }
                                    </Grid>


                                    {!!itinerary.content.days.find(d => d.id === day.id).hotel.rooms.length &&
                                        <React.Fragment>
                                            <Divider horizontal>
                                                <h6>Room Selection</h6>
                                            </Divider>

                                            {itinerary.content.days.find(d => d.id === day.id).hotel.rooms.map((room, index) => {

                                                const hotel = itinerary.content.days.find(d => d.id === day.id).hotel.selection;

                                                var categories = hotel.categories ? JSON.parse(hotel.categories) : [];
                                                var room_types = hotel.rooms ? JSON.parse(hotel.rooms) : [];
                                                var meals = hotel.meals ? JSON.parse(hotel.meals) : [];

                                                if (hotel.type && hotel.type === 'Global') {
                                                    categories = all_categories.map(object => (object.name));
                                                    room_types = all_rooms.map(object => (object.name));
                                                    meals = all_meals.map(object => (object.name));
                                                }

                                                return (<Paper>
                                                    <ValidatorForm>
                                                        <Grid container spacing={6}>
                                                            <Grid md={3} item>
                                                                <Dropdown
                                                                    placeholder='Category'
                                                                    fluid clearable selection
                                                                    value={room.category}
                                                                    onChange={(event, { value }) => {

                                                                        const days = [...itinerary.content.days];

                                                                        const rooms = day.hotel.rooms;

                                                                        rooms.splice(rooms.findIndex(r => r.id === room.id), 1, { ...room, category: value });

                                                                        days.splice(days.findIndex(d => d.id === day.id), 1, { ...day, hotel: { selection: day.hotel.selection, rooms: rooms } });

                                                                        setItinerary({
                                                                            ...itinerary, content: {
                                                                                ...itinerary.content,
                                                                                days: days
                                                                            }
                                                                        })
                                                                    }}
                                                                    options={categories.map((m, index) => {
                                                                        return { key: index, text: m, value: m }
                                                                    })}
                                                                />
                                                            </Grid>
                                                            <Grid md={2} item>
                                                                <Dropdown
                                                                    placeholder='Room Type'
                                                                    fluid clearable selection
                                                                    value={room.type_room}
                                                                    onChange={(event, { value }) => {

                                                                        const days = [...itinerary.content.days];

                                                                        const rooms = day.hotel.rooms;

                                                                        rooms.splice(rooms.findIndex(r => r.id === room.id), 1, { ...room, type_room: value });

                                                                        days.splice(days.findIndex(d => d.id === day.id), 1, { ...day, hotel: { selection: day.hotel.selection, rooms: rooms } });

                                                                        setItinerary({
                                                                            ...itinerary, content: {
                                                                                ...itinerary.content,
                                                                                days: days
                                                                            }
                                                                        })
                                                                    }}
                                                                    options={room_types.map((m, index) => {
                                                                        return { key: index, text: m, value: m }
                                                                    })}
                                                                />
                                                            </Grid>
                                                            <Grid md={2} item>
                                                                <Dropdown
                                                                    placeholder='Meal Type'
                                                                    fluid clearable selection
                                                                    value={room.type_meal}
                                                                    onChange={(event, { value }) => {

                                                                        const days = [...itinerary.content.days];

                                                                        const rooms = day.hotel.rooms;

                                                                        rooms.splice(rooms.findIndex(r => r.id === room.id), 1, { ...room, type_meal: value });

                                                                        days.splice(days.findIndex(d => d.id === day.id), 1, { ...day, hotel: { selection: day.hotel.selection, rooms: rooms } });

                                                                        setItinerary({
                                                                            ...itinerary, content: {
                                                                                ...itinerary.content,
                                                                                days: days
                                                                            }
                                                                        })
                                                                    }}
                                                                    options={meals.map((m, index) => {
                                                                        return { key: index, text: m, value: m }
                                                                    })}
                                                                />
                                                            </Grid>
                                                            <Grid md={2} item>
                                                                <TextValidator
                                                                    className="mb-16 w-100"
                                                                    variant="outlined"
                                                                    label="Rooms"
                                                                    type="number"
                                                                    name="number_of_rooms"
                                                                    min={1}
                                                                    onBlur={event => {
                                                                        const days = [...itinerary.content.days];

                                                                        const rooms = day.hotel.rooms;

                                                                        rooms.splice(rooms.findIndex(r => r.id === room.id), 1, { ...room, number_of_rooms: event.target.value });

                                                                        days.splice(days.findIndex(d => d.id === day.id), 1, { ...day, hotel: { selection: day.hotel.selection, rooms: rooms } });

                                                                        setItinerary({
                                                                            ...itinerary, content: {
                                                                                ...itinerary.content,
                                                                                days: days
                                                                            }
                                                                        })
                                                                    }}
                                                                    value={room.number_of_rooms}
                                                                />
                                                            </Grid>
                                                            <Grid md={2} item>
                                                                <TextValidator
                                                                    className="mb-16 w-100"
                                                                    variant="outlined"
                                                                    label="Rate Per Day"
                                                                    type="text"
                                                                    name="rate_per_day"
                                                                    value={room.rate_per_day}
                                                                    onBlur={event => {
                                                                        const days = [...itinerary.content.days];

                                                                        const rooms = day.hotel.rooms;

                                                                        rooms.splice(rooms.findIndex(r => r.id === room.id), 1, { ...room, rate_per_day: event.target.value });

                                                                        days.splice(days.findIndex(d => d.id === day.id), 1, { ...day, hotel: { selection: day.hotel.selection, rooms: rooms } });

                                                                        setItinerary({
                                                                            ...itinerary, content: {
                                                                                ...itinerary.content,
                                                                                days: days
                                                                            }
                                                                        })
                                                                    }}
                                                                />
                                                            </Grid>
                                                            <Grid md={1} item>
                                                                <IconButton onClick={onClickRemoveRoom.bind(null, day, room)}>
                                                                    <Icon>cancel</Icon>
                                                                </IconButton>
                                                            </Grid>
                                                        </Grid>
                                                    </ValidatorForm>
                                                </Paper>
                                                )
                                            })}
                                        </React.Fragment>
                                    }

                                </React.Fragment>
                            }
                        </TabPanelHorizontal>
                        <TabPanelHorizontal value={valueHorizontal} index={2}>

                            <Grid container spacing={6}>

                                <Grid md={6} sm={12} item>
                                    {day.destinations.length ?

                                        (
                                            itinerary.datasource_provider === 'Global' ?

                                                <Geosuggest
                                                    placeholder="Find Attraction"
                                                    country={[...countries.filter(c => itinerary.countries.includes(c.country_id)).map(c => c.code)]}
                                                    types={['establishment']}
                                                    onSuggestSelect={suggestSelect.bind(null, day, 'attraction')}
                                                /> :
                                                <Dropdown
                                                    placeholder='Pick Attractions'
                                                    fluid selection
                                                    multiple clearable basic search
                                                    noResultsMessage={<Button fullWidth variant='outlined' onClick={openComponentDialog.bind(null, <AttractionsComponent modal />, 'Add Attraction')}>Add Attraction</Button>}
                                                    value={itinerary.content.days.find(d => d.id === day.id).attractions ?
                                                        itinerary.content.days.find(d => d.id === day.id).attractions.map(a => a.attraction_id) : []}
                                                    onChange={(event, { value }) => {

                                                        const days = [...itinerary.content.days];

                                                        days.splice(days.findIndex(d => d.id === day.id), 1, { ...day, attractions: attractions.filter(a => value.includes(a.attraction_id)) });

                                                        setItinerary({
                                                            ...itinerary, content: {
                                                                ...itinerary.content,
                                                                days: days
                                                            }
                                                        })
                                                    }}
                                                    options={attractions.filter(a => day.destinations.map(d => d.city_id).includes(a.city_id)).map((m, index) => {
                                                        return { key: index, text: m.name, value: m.attraction_id }
                                                    })}
                                                />
                                        )
                                        : <div>Please select a destination to pick attractions !</div>
                                    }
                                </Grid>

                            </Grid>

                            <Grid container spacing={6}>
                                {
                                    !!itinerary.content.days.find(d => d.id === day.id) &&
                                    (
                                        (!!itinerary.content.days.find(d => d.id === day.id).attractions.length) &&


                                        itinerary.content.days.find(d => d.id === day.id).attractions.map(attraction => (
                                            <Grid md={12} item>
                                                <div className={classes.block}>
                                                    <FlexRow>
                                                        <div>
                                                            <LandscapeOutlined />
                                                            <span style={{ fontWeight: 'bold' }}>{attraction.name}</span>
                                                        </div>
                                                        <div>

                                                            {
                                                                (attraction.type && attraction.type === 'Global') ?
                                                                    <IconButton onClick={onRemoveGlobalItem.bind(null, day, 'attraction', attraction.id)}>
                                                                        <Icon>cancel</Icon>
                                                                    </IconButton> :
                                                                    <IconButton onClick={onClickRemoveAttraction.bind(null, day, attraction)}>
                                                                        <Icon>cancel</Icon>
                                                                    </IconButton>
                                                            }

                                                        </div>
                                                    </FlexRow>
                                                </div>
                                            </Grid>
                                        ))

                                    )
                                }
                            </Grid>

                        </TabPanelHorizontal>
                        <TabPanelHorizontal value={valueHorizontal} index={3}>
                            <Grid container spacing={6}>

                                <Grid md={6} sm={12} item>
                                    {day.destinations.length ?
                                        <Dropdown
                                            placeholder='Pick Excursions'
                                            fluid selection
                                            multiple clearable basic search
                                            noResultsMessage={<Button fullWidth variant='outlined' onClick={openComponentDialog.bind(null, <ExcursionsComponent modal />, 'Add Excursion')}>Add Excursion</Button>}
                                            value={itinerary.content.days.find(d => d.id === day.id).excursions ?
                                                itinerary.content.days.find(d => d.id === day.id).excursions.map(e => e.excursion_id) : []}
                                            onChange={(event, { value }) => {

                                                const days = [...itinerary.content.days];

                                                days.splice(days.findIndex(d => d.id === day.id), 1, { ...day, excursions: excursions.filter(e => value.includes(e.excursion_id)) });

                                                setItinerary({
                                                    ...itinerary, content: {
                                                        ...itinerary.content,
                                                        days: days
                                                    }
                                                })
                                            }}
                                            options={excursions.filter(e => day.destinations.map(d => d.city_id).includes(e.city_id)).map((m, index) => {
                                                return { key: index, text: m.name, value: m.excursion_id }
                                            })}
                                        /> : <div>Please select a destination to pick excursions !</div>
                                    }
                                </Grid>

                            </Grid>

                            <Grid container spacing={6}>
                                {
                                    (!!itinerary.content.days.find(d => d.id === day.id)) &&
                                    (
                                        (!!itinerary.content.days.find(d => d.id === day.id).excursions.length) &&


                                        itinerary.content.days.find(d => d.id === day.id).excursions.map(excursion => (
                                            <Grid md={12} item>
                                                <div className={classes.block}>
                                                    <FlexRow>
                                                        <div>
                                                            <Streetview />
                                                            <span style={{ fontWeight: 'bold' }}>{excursion.name}</span>
                                                        </div>
                                                        <div>
                                                            <IconButton onClick={onClickRemoveExcursion.bind(null, day, excursion)}>
                                                                <Icon>cancel</Icon>
                                                            </IconButton>
                                                        </div>
                                                    </FlexRow>
                                                    <FlexRow>
                                                        <div>
                                                            <Switch name='cost_applied'
                                                                defaultChecked={excursion.cost_applied ? excursion.cost_applied : false}
                                                                onChange={(e) => {

                                                                    const days = [...itinerary.content.days];

                                                                    days.splice(days.findIndex(d => d.id === day.id), 1, {
                                                                        ...day, excursions: excursions.map(ex => {
                                                                            if (ex.excursion_id === excursion.excursion_id) {
                                                                                ex.cost_applied = e.target.checked;
                                                                            }
                                                                            return ex;
                                                                        })
                                                                    });

                                                                    setItinerary({
                                                                        ...itinerary, content: {
                                                                            ...itinerary.content,
                                                                            days: days
                                                                        }
                                                                    })

                                                                }} /> Apply Cost ?
                                                        </div>

                                                        {
                                                            excursion.cost_applied &&
                                                            <TextField
                                                                defaultValue={excursion.cost ? excursion.cost : null}
                                                                type="number" onBlur={event => {
                                                                    const days = [...itinerary.content.days];

                                                                    days.splice(days.findIndex(d => d.id === day.id), 1, {
                                                                        ...day, excursions: excursions.map(ex => {
                                                                            if (ex.excursion_id === excursion.excursion_id) {
                                                                                ex.cost = event.target.value;
                                                                            }
                                                                            return ex;
                                                                        })
                                                                    });

                                                                    setItinerary({
                                                                        ...itinerary, content: {
                                                                            ...itinerary.content,
                                                                            days: days
                                                                        }
                                                                    })
                                                                }} placeholder='Enter amount' />
                                                        }

                                                    </FlexRow>
                                                </div>
                                            </Grid>
                                        ))

                                    )
                                }
                            </Grid>


                        </TabPanelHorizontal>
                        <TabPanelHorizontal value={valueHorizontal} index={4}>
                            <Grid container spacing={6}>
                                <FlexRow>
                                    {
                                        activities_list.sort((a, b) => a.activity_id - b.activity_id).map(activity => {

                                            const styles = {
                                                width: 150, height: 50, borderRadius: 5, alignItems: 'center', cursor: 'pointer',
                                                borderWidth: 2, borderColor: 'black', justifyContent: 'center', borderWidth: 2,
                                                textAlign: 'center', paddingTop: '15px', marginTop: '10px', marginRight: '10px'
                                            };


                                            return (
                                                <div style={styles} onClick={() => {

                                                    const days = [...itinerary.content.days];

                                                    const day_activities = [...itinerary.content.days.find(d => d.id === day.id).activities];

                                                    if (day_activities.find(a => a.activity_id === activity.activity_id)) {
                                                        day_activities.splice(day_activities.findIndex(a => a.activity_id === activity.activity_id), 1);
                                                    } else {
                                                        day_activities.splice(day_activities.length, 1, activity);
                                                    }


                                                    days.splice(days.findIndex(d => d.id === day.id), 1, { ...day, activities: day_activities });

                                                    setItinerary({
                                                        ...itinerary, content: {
                                                            ...itinerary.content,
                                                            days: days
                                                        }
                                                    });

                                                }}>
                                                    {activity.category}
                                                </div>
                                            )
                                        })
                                    }
                                </FlexRow>
                            </Grid>
                            <Divider horizontal clearing section />
                            <Grid container spacing={6}>

                                {
                                    (!!itinerary.content.days.find(d => d.id === day.id)) &&
                                    (
                                        (!!itinerary.content.days.find(d => d.id === day.id).activities.length) &&


                                        itinerary.content.days.find(d => d.id === day.id).activities.map(activity => (
                                            <div style={{ width: '100%' }}>
                                                <div className='activity-header'>
                                                    <FlexRow>
                                                        <div style={{ fontWeight: 'bold' }}>{activity.category}</div>
                                                        <div>
                                                            <IconButton onClick={onClickAddActivity.bind(null, day, activity)}>
                                                                <Icon>add</Icon>
                                                            </IconButton>
                                                        </div>
                                                    </FlexRow>
                                                </div>
                                                <div className='activity-list'>
                                                    {
                                                        (!!activity.activities &&
                                                            activity.activities.split(',').map(item => (
                                                                <FlexRow>
                                                                    <div>{item}</div>
                                                                    <div>
                                                                        <IconButton onClick={onRemoveActivityItem.bind(null, day, activity, item)}>
                                                                            <Icon>cancel</Icon>
                                                                        </IconButton>
                                                                    </div>
                                                                </FlexRow>
                                                            )))
                                                    }
                                                </div>
                                                <Divider horizontal clearing section />
                                            </div>
                                        ))

                                    )
                                }

                            </Grid>

                        </TabPanelHorizontal>
                    </TabPanelVertical>
                ))
            }
        </div >
    )



    const onRemoveTaxItem = (tax) => {
        setItinerary({
            ...itinerary,
            content: {
                ...itinerary.content,
                costing: {
                    ...itinerary.content.costing, process: 'process',
                    taxes: [...itinerary.content.costing.taxes.filter(t => t.id !== tax.id)]
                }
            }
        })
    }

    const processCosting = () => {

        var subcost = 0;

        itinerary.content.days.forEach((day, index) => {

            const hotel = day.hotel.selection;
            const rooms = hotel ? day.hotel.rooms : null;

            if (rooms) {
                rooms.forEach(room => {
                    if ((room.number_of_rooms && !isNaN(room.number_of_rooms))
                        && (room.rate_per_day && !isNaN(room.rate_per_day))) {
                        subcost += parseFloat((room.number_of_rooms * room.rate_per_day));
                    }
                })
            }
        });


        itinerary.content.days.forEach((day, index) => {

            const excursions = day.excursions;

            if (excursions.length) {
                excursions.forEach(excursion => {
                    if (excursion.cost_applied) {
                        if (excursion.cost && !isNaN(excursion.cost)) {
                            subcost += parseFloat(excursion.cost);
                        }
                    }
                })
            }
        });

        itinerary.content.costing.transportation.forEach(item => {
            if (item.cost && !isNaN(item.cost)) {
                subcost += parseFloat(item.cost);
            }
        });

        var taxes = 0;

        itinerary.content.costing.taxes.forEach(tax => {
            if (tax.amount && !isNaN(tax.amount)) {
                taxes += (parseFloat(tax.amount) * subcost) / 100;
            }
        });


        setItinerary({
            ...itinerary, content: {
                ...itinerary.content, costing:
                {
                    ...itinerary.content.costing, process: null,
                    sub_package_cost: subcost, status: "CONFIRMED",
                    grand_package_cost: (itinerary.content.costing.percent ? (subcost + (parseFloat(itinerary.content.costing.percent) / 100) * subcost) :
                        (itinerary.content.costing.profit ? (subcost + parseFloat(itinerary.content.costing.profit)) : subcost)) + taxes,
                    profit: itinerary.content.costing.percent ? (parseFloat(itinerary.content.costing.percent) / 100) * subcost : itinerary.content.costing.profit ? itinerary.content.costing.profit : 0
                }
            }
        })

    }


    useEffect(() => {
        if (itinerary.content.costing && itinerary.content.costing.process) {
            processCosting();
        }
    }, [itinerary])

    const onRemoveNoteText = (id) => {
        setItinerary({
            ...itinerary, content: {
                ...itinerary.content, notes: [...itinerary.content.notes.filter(note => note.id !== id)]
            }
        })
    }

    const onRemoveInclusionText = (id) => {
        setItinerary({
            ...itinerary, content: {
                ...itinerary.content, inclusions: [...itinerary.content.inclusions.filter(note => note.id !== id)]
            }
        })
    }

    const onRemoveExclusionText = (id) => {
        setItinerary({
            ...itinerary, content: {
                ...itinerary.content, exclusions: [...itinerary.content.exclusions.filter(note => note.id !== id)]
            }
        })
    }


    const onResetCosting = () => {

        setItinerary({
            ...itinerary, content: {
                ...itinerary.content, costing: original.content.costing ? original.content.costing : null
            }
        })
    }

    const FinalBuilderComponent = () => {

        return (
            <React.Fragment>
                <Tabs
                    value={valueHorizontalFinal}
                    onChange={onChangeHorizontalTabsFinal}
                    variant="fullWidth"
                    indicatorColor="secondary"
                    textColor="secondary"
                    aria-label="Horizontal Itineray Builder Final Component Tabs"
                >
                    <Tab icon={<DoneAllSharp />} label="Costing" {...horizontalTabProps(0)} />
                    <Tab icon={<NotesOutlined />} label="Notes"  {...horizontalTabProps(1)} />

                </Tabs>
                <TabPanelHorizontal value={valueHorizontalFinal} index={0}>
                    <Paper className='paper-block'>
                        <div className='header-builder'>Costing</div>

                        <Divider horizontal clearing />

                        <Paper className='paper-block'>
                            <div style={{ fontWeight: 'bold' }}>Accomodation</div>

                            {
                                (!!itinerary.content.days.length) &&

                                <Table cellPadding={10} cellSpacing={10} className='table-cost-detail'>
                                    <TableHead>
                                        <TableRow>
                                            <TableCell>Day</TableCell>
                                            <TableCell>Hotel</TableCell>
                                            <TableCell>Rooms</TableCell>
                                            <TableCell align='right'>Cost</TableCell>
                                        </TableRow>
                                    </TableHead>
                                    <TableBody>
                                        {itinerary.content.days.map((day, index) => {

                                            const hotel = day.hotel.selection;
                                            const rooms = hotel ? day.hotel.rooms : null;

                                            return (
                                                <TableRow>
                                                    <TableCell>Day {index + 1}</TableCell>
                                                    <TableCell>
                                                        {hotel ? hotel.name : 'Hotel Not Selected'}
                                                    </TableCell>
                                                    <TableCell>
                                                        {
                                                            rooms ? rooms.map(room => (
                                                                <FlexRow>
                                                                    <div>{room.category}</div>
                                                                    <div>{room.type_room}</div>
                                                                    <div>{room.type_meal}</div>
                                                                    <div>{room.number_of_rooms} rooms</div>
                                                                </FlexRow>
                                                            )) : 'Rooms Not Selected'
                                                        }
                                                    </TableCell>
                                                    <TableCell align='right'>
                                                        {
                                                            rooms ? rooms.map(room => (
                                                                <div>{room.number_of_rooms * room.rate_per_day}</div>
                                                            )) : 'Rooms Not Selected'
                                                        }
                                                    </TableCell>
                                                </TableRow>
                                            )

                                        })}
                                    </TableBody>
                                </Table>
                            }

                        </Paper>
                        <Paper className='paper-block'>

                            <div style={{ fontWeight: 'bold' }}>Excursion Costs</div>

                            {
                                (!!itinerary.content.days.length) &&

                                <Table cellPadding={10} cellSpacing={10} className='table-cost-detail'>
                                    <TableHead>
                                        <TableRow>
                                            <TableCell>Day</TableCell>
                                            <TableCell>Excursion</TableCell>
                                            <TableCell>Cost Applied</TableCell>
                                            <TableCell align='right'>Cost</TableCell>
                                        </TableRow>
                                    </TableHead>
                                    <TableBody>
                                        {itinerary.content.days.map((day, index) => {

                                            const excursions = day.excursions;

                                            return (
                                                <TableRow>
                                                    <TableCell>Day {index + 1}</TableCell>
                                                    <TableCell>
                                                        {
                                                            excursions.map(excursion => (
                                                                <FlexRow>
                                                                    <div>{excursion.name}</div>
                                                                    <div>{excursion.distance}</div>
                                                                </FlexRow>
                                                            ))
                                                        }
                                                    </TableCell>
                                                    <TableCell>
                                                        {
                                                            excursions.map(excursion => (
                                                                <FlexRow>
                                                                    <div>{excursion.cost_applied ? 'Applied' : 'Not Applied'}</div>
                                                                </FlexRow>
                                                            ))
                                                        }
                                                    </TableCell>
                                                    <TableCell align='right'>
                                                        {
                                                            excursions.map(excursion => (
                                                                <FlexRow>
                                                                    <div>{excursion.cost ? excursion.cost : 'Not Set'}</div>
                                                                </FlexRow>
                                                            ))
                                                        }
                                                    </TableCell>
                                                </TableRow>
                                            )

                                        })}
                                    </TableBody>
                                </Table>
                            }

                        </Paper>

                        <Paper className='paper-block'>
                            <h5>Transportation</h5>

                            <FlexRow>
                                <div>
                                    All Inclusive Transport Cost(Vehice, Drivers, Guides...)
                                </div>

                                <div>
                                    <TextField
                                        variant='outlined'
                                        type='number'
                                        defaultValue={itinerary.content.costing.transportation.find(transport => transport.item === 'all') ?
                                            itinerary.content.costing.transportation.find(transport => transport.item === 'all').cost : null
                                        }
                                        placeholder="Amount"
                                        InputProps={{
                                            endAdornment: <InputAdornment position="end">{itinerary.currency ? itinerary.currency : 'USD'}</InputAdornment>,
                                        }} inputProps={{ style: { ['text-align']: 'right' } }}
                                        onBlur={event => {

                                            setItinerary({
                                                ...itinerary,
                                                content: {
                                                    ...itinerary.content, costing: {
                                                        ...itinerary.content.costing, process: 'process',
                                                        transportation: [...itinerary.content.costing.transportation.filter(t => t.item !== 'all'), { item: 'all', cost: event.target.value }]
                                                    }
                                                }
                                            })
                                        }}
                                    />
                                </div>
                            </FlexRow>
                        </Paper>


                        <Paper className='paper-block'>
                            <FlexRow>
                                <div>Sub Total</div>
                                <div>
                                    <span className="currency-text">{itinerary.currency ? itinerary.currency : 'USD'}</span>
                                    <span>{itinerary.content.costing.sub_package_cost}</span>
                                </div>
                            </FlexRow>
                        </Paper>

                        <Paper className='paper-block'>

                            <FlexRow styles={{ ['margin-bottom']: '15px' }}>
                                <h5>Taxes</h5>
                                <Button variant='outlined' onClick={() => {
                                    setItinerary({
                                        ...itinerary,
                                        content: {
                                            ...itinerary.content,
                                            costing: {
                                                ...itinerary.content.costing, process: 'process',
                                                taxes: [...itinerary.content.costing.taxes,
                                                {
                                                    id: itinerary.content.costing.taxes.length ? itinerary.content.costing.taxes[itinerary.content.costing.taxes.length - 1].id + 1 : 0,
                                                    item: null, amount: null
                                                }]
                                            }
                                        }

                                    })
                                }}>
                                    <Add /> Add Tax
                            </Button>
                            </FlexRow>

                            {
                                itinerary.content.costing.taxes.sort((a, b) => a - b).map((tax, index) => (
                                    <FlexRow styles={{ ['margin-bottom']: '15px' }}>
                                        <FlexRow styles={{ ['justify-content']: 'flex-start' }}>

                                            {(index === itinerary.content.costing.taxes.length - 1) &&
                                                < IconButton onClick={onRemoveTaxItem.bind(null, tax)}>
                                                    <Icon>cancel</Icon>
                                                </IconButton>
                                            }
                                            <div>
                                                <TextField
                                                    placeholder="Tax item"
                                                    variant='standard'
                                                    type='text'
                                                    defaultValue={tax.item}
                                                    style={{ ['outline']: 'none', ['border']: '0' }}
                                                    onBlur={event => setItinerary({
                                                        ...itinerary,
                                                        content: {
                                                            ...itinerary.content,
                                                            costing: {
                                                                ...itinerary.content.costing, process: 'process',
                                                                taxes: [...itinerary.content.costing.taxes.filter(t => t.id !== tax.id), { ...tax, item: event.target.value }]
                                                            }
                                                        }
                                                    }
                                                    )}
                                                />
                                            </div>
                                        </FlexRow>
                                        <div>
                                            <TextField
                                                placeholder="Tax Percent"
                                                inputProps={{ style: { ['text-align']: 'right' } }}
                                                variant='outlined'
                                                type='number'
                                                InputProps={{
                                                    endAdornment: <InputAdornment position="end">%</InputAdornment>,
                                                }}
                                                defaultValue={tax.amount}
                                                onBlur={event => setItinerary({
                                                    ...itinerary,
                                                    content: {
                                                        ...itinerary.content,
                                                        costing: {
                                                            ...itinerary.content.costing, process: 'process',
                                                            taxes: [...itinerary.content.costing.taxes.filter(t => t.id !== tax.id), { ...tax, amount: event.target.value }]
                                                        }
                                                    }
                                                })}
                                            />
                                        </div>
                                    </FlexRow>

                                ))
                            }

                        </Paper>

                        <Paper className='paper-block'>
                            <h5>Profit</h5>
                            <FlexRow>
                                <div>
                                    Amount <Switch defaultChecked={itinerary.content.costing.method === 'percent'} onChange={event => {
                                        setItinerary({
                                            ...itinerary,
                                            content: {
                                                ...itinerary.content,
                                                costing: {
                                                    ...itinerary.content.costing, process: 'process',
                                                    method: event.target.checked ? 'percent' : 'amount',
                                                    percent: event.target.checked ? null : itinerary.content.costing.percent
                                                }
                                            }
                                        })
                                    }} /> Percent
                                </div>
                                <FlexRow>

                                    {
                                        itinerary.content.costing.method === 'percent' &&
                                        <TextField
                                            variant='outlined'
                                            type='number' style={{ ['margin-right']: '10px' }}
                                            defaultValue={itinerary.content.costing.percent ? itinerary.content.costing.percent : null}
                                            inputProps={{ style: { ['text-align']: 'right' } }}
                                            placeholder="Percent"
                                            InputProps={{
                                                endAdornment: <InputAdornment position="end">%</InputAdornment>,
                                            }}
                                            onBlur={event => {
                                                setItinerary({
                                                    ...itinerary,
                                                    content: {
                                                        ...itinerary.content,
                                                        costing: {
                                                            ...itinerary.content.costing, process: 'process',
                                                            percent: event.target.value
                                                        }
                                                    }
                                                })
                                            }}
                                        />
                                    }

                                    <TextField
                                        variant='outlined'
                                        type='number'
                                        defaultValue={itinerary.content.costing.profit ? itinerary.content.costing.profit : 0}
                                        placeholder="Amount" inputProps={{ style: { ['text-align']: 'right' } }}
                                        min={0} onBlur={event => {
                                            setItinerary({
                                                ...itinerary,
                                                content: {
                                                    ...itinerary.content,
                                                    costing: {
                                                        ...itinerary.content.costing, process: 'process',
                                                        profit: event.target.value
                                                    }
                                                }
                                            })
                                        }}
                                        InputProps={{
                                            endAdornment: <InputAdornment position="end">{itinerary.currency ? itinerary.currency : 'USD'}</InputAdornment>,
                                        }}
                                    />
                                </FlexRow>
                            </FlexRow>
                        </Paper>
                        <Paper className='paper-block'>
                            <FlexRow>
                                <div>Package Grand Cost</div>
                                <div>
                                    <span className="currency-text">{itinerary.currency ? itinerary.currency : 'USD'}</span>
                                    <span>{itinerary.content.costing.grand_package_cost}</span>
                                </div>
                            </FlexRow>
                        </Paper>
                        <Paper className='paper-block'>
                            {
                                <Button variant='outlined' onClick={onResetCosting}>
                                    <ClearAllRounded /> Reset Costing
                                </Button>
                            }
                        </Paper>
                    </Paper >

                </TabPanelHorizontal>
                <TabPanelHorizontal value={valueHorizontalFinal} index={1}>
                    <Paper className='paper-block'>
                        <div className='header-builder'>Inclusions</div>

                        <Divider horizontal clearing />

                        <Grid container>
                            <Grid item md={10}>
                                <TextField
                                    placeholder="Enter inclusion to add"
                                    variant="outlined"
                                    defaultValue={InclusionText}
                                    onBlur={event => setInclusionText(event.target.value)}
                                    fullWidth
                                />
                            </Grid>
                            <Grid item md={2}>
                                <Button className='notes-add-button' variant="outlined" onClick={() => {
                                    setItinerary({
                                        ...itinerary, content: {
                                            ...itinerary.content, inclusions: [...itinerary.content.inclusions, {
                                                id: itinerary.content.inclusions.length ? itinerary.content.inclusions[itinerary.content.inclusions.length - 1].id + 1 : 0,
                                                text: InclusionText
                                            }]
                                        }
                                    })
                                }}><Add /> Add Inclusion
                                </Button>
                            </Grid>
                        </Grid>
                        <Divider horizontal clearing />
                        {
                            itinerary.content.inclusions.map(note => {
                                return (
                                    <FlexRow styles={{
                                        ['margin-bottom']: '15px', background: 'whitesmoke', ['padding-left']: '5px',
                                        ['border-radius']: '5px'
                                    }}>
                                        <div>{note.text}</div>
                                        <IconButton onClick={onRemoveInclusionText.bind(null, note.id)}>
                                            <Icon>cancel</Icon>
                                        </IconButton>
                                    </FlexRow>
                                )
                            })
                        }
                    </Paper>
                    <Paper className='paper-block'>
                        <div className='header-builder'>Exclusions</div>

                        <Divider horizontal clearing />

                        <Grid container>
                            <Grid item md={10}>
                                <TextField
                                    placeholder="Enter exclusion to add"
                                    variant="outlined"
                                    defaultValue={ExclusionText}
                                    onBlur={event => setExclusionText(event.target.value)}
                                    fullWidth
                                />
                            </Grid>
                            <Grid item md={2}>
                                <Button className='notes-add-button' variant="outlined" onClick={() => {
                                    setItinerary({
                                        ...itinerary, content: {
                                            ...itinerary.content, exclusions: [...itinerary.content.exclusions, {
                                                id: itinerary.content.exclusions.length ? itinerary.content.exclusions[itinerary.content.exclusions.length - 1].id + 1 : 0,
                                                text: ExclusionText
                                            }]
                                        }
                                    })
                                }}><Add /> Add Exclusion
                                </Button>
                            </Grid>
                        </Grid>
                        <Divider horizontal clearing />
                        {
                            itinerary.content.exclusions.map(note => {
                                return (
                                    <FlexRow styles={{
                                        ['margin-bottom']: '15px', background: 'whitesmoke', ['padding-left']: '5px',
                                        ['border-radius']: '5px'
                                    }}>
                                        <div>{note.text}</div>
                                        <IconButton onClick={onRemoveExclusionText.bind(null, note.id)}>
                                            <Icon>cancel</Icon>
                                        </IconButton>
                                    </FlexRow>
                                )
                            })
                        }
                    </Paper>
                    <Paper className='paper-block'>
                        <div className='header-builder'>Notes</div>

                        <Divider horizontal clearing />

                        <Grid container>
                            <Grid item md={10}>
                                <TextField
                                    placeholder="Enter note text to add"
                                    variant="outlined"
                                    defaultValue={NoteText}
                                    onBlur={event => setNoteText(event.target.value)}
                                    fullWidth
                                />
                            </Grid>
                            <Grid item md={2}>
                                <Button className='notes-add-button' variant="outlined" onClick={() => {
                                    setItinerary({
                                        ...itinerary, content: {
                                            ...itinerary.content, notes: [...itinerary.content.notes, {
                                                id: itinerary.content.notes.length ? itinerary.content.notes[itinerary.content.notes.length - 1].id + 1 : 0,
                                                text: NoteText
                                            }]
                                        }
                                    })
                                }}><Add /> Add Notes
                                </Button>
                            </Grid>
                        </Grid>
                        <Divider horizontal clearing />
                        {
                            itinerary.content.notes.map(note => {
                                return (
                                    <FlexRow styles={{
                                        ['margin-bottom']: '15px', background: 'whitesmoke', ['padding-left']: '5px',
                                        ['border-radius']: '5px'
                                    }}>
                                        <div>{note.text}</div>
                                        <IconButton onClick={onRemoveNoteText.bind(null, note.id)}>
                                            <Icon>cancel</Icon>
                                        </IconButton>
                                    </FlexRow>
                                )
                            })
                        }
                    </Paper>
                </TabPanelHorizontal>

            </React.Fragment>
        )

    }


    const getStepContent = (step) => {
        if (props.current) {

            switch (step) {
                case 0:
                    return BuilderComponent();
                case 1:
                    return FinalBuilderComponent();
                default:
                    return 'Unknown step';
            }

        } else {
            switch (step) {
                case 0:
                    return BasicComponent();
                case 1:
                    return BuilderComponent();
                case 2:
                    return FinalBuilderComponent();
                default:
                    return 'Unknown step';
            }
        }
    }

    const steps = getSteps();


    const isStepOptional = (step) => {
        return step === 1;
    };

    const isStepSkipped = (step) => {
        return skipped.has(step);
    };

    const handleNext = () => {
        let newSkipped = skipped;
        if (isStepSkipped(activeStep)) {
            newSkipped = new Set(newSkipped.values());
            newSkipped.delete(activeStep);
        }

        setActiveStep((prevActiveStep) => {

            if ((props.current && prevActiveStep === 0) || (!props.current && prevActiveStep === 1)) {
                calculateDistanceMatrix();
            }

            if (prevActiveStep === 0 && !props.current) {

                if (itinerary.countries.length < 1) {

                    setErrorDialog({
                        ...ErrorDialog, open: 'open', body: 'Please select atleast 1 country'
                    });

                    return 0;
                }
            }

            return prevActiveStep + 1
        });

        setSkipped(newSkipped);
    };

    const handleBack = () => {
        setActiveStep((prevActiveStep) => prevActiveStep - 1);
    };


    const handleResetAction = () => {

        setConfirmationDialog({
            ...DialogConfirm, header: 'Confirm',
            body: 'Do you confirm to reset ?', onSuccess: () => {
                setItinerary({ ...original })
                setActiveStep(0);

                DialogConfirm.onCancel();
            },
            open: 'open'
        })
    };


    useEffect(() => {
        setActivitiesList(activities);
    }, [activities])


    const createCompletedItinerary = () => {

        const data_itinerary = { ...itinerary, status: 'CREATED', content: JSON.stringify(itinerary.content), countries: JSON.stringify(itinerary.countries) };

        if (!(data_itinerary.name && data_itinerary.name.length)) {
            data_itinerary.name = `${data_itinerary.days} Days / ${parseInt(data_itinerary.days) - 1} Nights Tour Of ${itinerary.countries.map(c => countries.find(country => country.country_id == c).name).join()}`;
        }

        if (props.current) {

            if (data_itinerary.status === 'DRAFT') {

                data_itinerary.status = 'CREATED';

            } else if (data_itinerary.status === 'CREATED') {
                data_itinerary.status = 'UPDATED (NEW)';

            } else if (['APPROVED', 'CANCELLED'].includes(data_itinerary.status)) {
                delete data_itinerary.status;
            }

            dispatch(props.onBuildEdit({ ...data_itinerary, user: auth_user }, () => {
                props.onCompleteAction();
            }))

        } else {

            const draft_id = localStorage.getItem(MD5('current-draft-id'));

            if (draft_id && props.deleteDraft) {
                props.deleteDraft(draft_id, () => {
                    dispatch(props.onBuildFinish({ ...data_itinerary, token: TokenGenerator.generate(32), user: auth_user }, () => {
                        props.onCompleteAction();
                    }))
                });
            } else {
                dispatch(props.onBuildFinish({ ...data_itinerary, token: TokenGenerator.generate(32), user: auth_user }, () => {
                    props.onCompleteAction();
                }))
            }
        }

    }


    const processCompleteItinerary = () => {
        setConfirmationDialog({
            ...DialogConfirm, header: 'Confirm',
            body: `Do you confirm to ${props.current ? 'update itinerary' : 'finish'} ?`, onSuccess: () => {
                createCompletedItinerary()
            },
            open: 'open'
        })
    }



    const onChangeCityTours = (location, e) => {

        var total_city_tours = 0;

        itinerary.content.distance_duration_matrix.locations.forEach(loc => {
            if (loc.name !== location.name) {
                total_city_tours += parseFloat(loc.city_tours);
            }
        });

        if (e.target.value && !isNaN(e.target.value)) {
            total_city_tours += parseFloat(e.target.value);
        }


        setItinerary({
            ...itinerary, content: {
                ...itinerary.content,
                distance_duration_matrix: {
                    ...itinerary.content.distance_duration_matrix,
                    locations: [...itinerary.content.distance_duration_matrix.locations.map(loc => {
                        if (loc.name === location.name) {
                            loc.city_tours = e.target.value;
                        }

                        return loc;
                    })], total_city_tours
                }
            }
        })
    }

    const saveAsDraft = () => {

        const draft = { ...itinerary, status: 'DRAFT', content: JSON.stringify(itinerary.content), countries: JSON.stringify(itinerary.countries) };

        const current_draft_id = localStorage.getItem(MD5('current-draft-id'));

        if (current_draft_id && !props.current) {
            draft.itinerary_id = current_draft_id;
        }

        if (!(draft.name && draft.name.length)) {
            draft.name = `${draft.days} Days / ${parseInt(draft.days) - 1} Nights Tour Of ${itinerary.countries.map(c => countries.find(country => country.country_id == c).name).join()}`;
        }


        dispatch(props.onAutoSaveDraft({ ...draft, token: TokenGenerator.generate(32) }, (id) => {
            localStorage.setItem(MD5('autosavetimestamp'), Date.now().toString());
            localStorage.setItem(MD5('current-draft-id'), id);
            ToastsStore.success('Saved as Draft');
            props.onCompleteAction();
        }));

    }

    const onSaveAsDraft = () => {
        setConfirmationDialog({
            ...DialogConfirm, header: 'Save Draft',
            body: `Do you want to save as draft for later access ?`, onSuccess: () => {

                if (itinerary.content.days.length) {
                    saveAsDraft();
                } else {
                    DialogConfirm.onCancel();
                    setErrorDialog({ ...ErrorDialog, open: 'open', body: 'Please complete few steps before save as draft' });
                }

            },
            open: 'open'
        })
    }

    const autoSaveDraft = (data = null) => {

        const draft = { ...itinerary, status: itinerary.status ? itinerary.status : 'DRAFT', content: JSON.stringify(itinerary.content), countries: JSON.stringify(itinerary.countries) };

        const current_draft_id = localStorage.getItem(MD5('current-draft-id'));

        if (current_draft_id && !props.current) {
            draft.itinerary_id = current_draft_id;
        }

        if (!(draft.name && draft.name.length)) {
            draft.name = `${draft.days} Days / ${parseInt(draft.days) - 1} Nights Tour Of ${itinerary.countries.map(c => countries.find(country => country.country_id == c).name).join()}`;
        }

        dispatch(props.onAutoSaveDraft({ ...draft, token: TokenGenerator.generate(32) }, (id) => {
            localStorage.setItem(MD5('autosavetimestamp'), Date.now().toString());
            localStorage.setItem(MD5('current-draft-id'), id);
            ToastsStore.info('Auto saved');
        }));


    }

    const saveKeyCombination = (e) => {

        if (e.key === 's' && e.ctrlKey) {

            e.preventDefault();

            if (itinerary.content.days.length) {
                autoSaveDraft();
            } else {
                DialogConfirm.onCancel();
                setErrorDialog({ ...ErrorDialog, open: 'open', body: 'Please complete few steps before save as draft' });
            }

        }
    }



    useEffect(() => {

        const saved = localStorage.getItem(MD5('autosavetimestamp'));

        if (itinerary.status === 'DRAFT' || isUndefined(itinerary.status) || isNull(itinerary.status)) {

            if (itinerary.content.days.length) {
                if (saved) {

                    if (moment(Date.now()).diff(parseInt(saved), 'seconds') >= 30) {
                        autoSaveDraft();
                    }

                } else {
                    if (repeat == null) {
                        setTimeout(() => autoSaveDraft(), 1000);
                        setRepeat(Date.now());
                    }
                }
            }
        }

        window.addEventListener('keydown', saveKeyCombination);

        return () => {
            window.removeEventListener('keydown', saveKeyCombination);
        }

    }, [itinerary])

    useEffect(() => {
        [
            fetchClients(auth_user.data.company_id),
            fetchCountries(auth_user.data.company_id),
            fetchCities(auth_user.data.company_id),
            fetchHotels(auth_user.data.company_id),
            fetchHotelStandards(auth_user.data.company_id),
            fetchHotelCategories(auth_user.data.company_id),
            fetchHotelRooms(auth_user.data.company_id),
            fetchHotelMeals(auth_user.data.company_id),
            fetchAttractions(auth_user.data.company_id),
            fetchExcursions(auth_user.data.company_id),
            fetchActivities(auth_user.data.company_id)
        ].forEach(fetch => dispatch(fetch));


        window.addEventListener('keydown', saveKeyCombination);

        return () => {
            window.removeEventListener('keydown', saveKeyCombination);

            if (localStorage.getItem(MD5('autosavetimestamp'))) {
                localStorage.removeItem(MD5('autosavetimestamp'));
            }

            if (localStorage.getItem(MD5('current-draft-id'))) {
                localStorage.removeItem(MD5('current-draft-id'));
            }
        }

    }, [])

    return (
        <div className={classes.root}>
            <FlexRow styles={{ margin: '15px 0' }}>
                <FlexRow>
                    <Button onClick={props.viewAll} color='primary' className={classes.button} variant='outlined'>
                        View All
                    </Button>
                    <Button onClick={handleResetAction} color='red' className={classes.button} variant='outlined'>
                        Reset
                    </Button>
                </FlexRow>
                <div>
                    <Button disabled={activeStep === 0} onClick={handleBack} className={classes.button}>
                        Back
                    </Button>
                    {!(props.current && props.current.status !== 'DRAFT') &&
                        <Button variant='contained' onClick={onSaveAsDraft} className={classes.button}>
                            Save As Draft
                    </Button>
                    }

                    {(activeStep < steps.length - 1) &&
                        <Button
                            variant="contained"
                            color="primary"
                            onClick={handleNext}
                            className={classes.button}
                        >
                            Next
                    </Button>
                    }

                    {activeStep === steps.length - 1 &&
                        <Button
                            variant="contained"
                            color="primary"
                            onClick={processCompleteItinerary}
                            className={classes.button}
                        >
                            Finish
                    </Button>
                    }
                </div>
            </FlexRow>

            <Stepper activeStep={activeStep}>
                {steps.map((label, index) => {
                    const stepProps = {};
                    const labelProps = {};

                    switch (index) {
                        case 0:
                            labelProps.optional = <Typography variant="caption">Introductory</Typography>;
                            break;
                        case 1:
                            labelProps.optional = <Typography variant="caption">Itinerary Informations</Typography>;
                            break;
                        case 2:
                            labelProps.optional = <Typography variant="caption">Final Touches ..!</Typography>;
                            break;
                        default: labelProps.optional = null
                    }

                    if (isStepSkipped(index)) {
                        stepProps.completed = false;
                    }
                    return (
                        <Step key={label} {...stepProps}>
                            <StepLabel {...labelProps}>{label}</StepLabel>
                        </Step>
                    );
                })}
            </Stepper>

            <div>
                {getStepContent(activeStep)}
            </div>


            <Dialog open={activityModal.open === 'open'} fullWidth>
                <ValidatorForm>
                    <DialogTitle>Add Activity</DialogTitle>
                    <DialogContent>
                        <TextValidator
                            className="mb-16 w-100"
                            fullWidth
                            variant="outlined"
                            label="Activity Item"
                            type="text"
                            value={activityModal.item}
                            onChange={(e) => setActivityModal({ ...activityModal, item: e.target.value, open: 'open' })}
                            name="activity_item"
                            validators={["required"]}
                            errorMessages={[
                                "Activity item is required"
                            ]}
                        />

                        <Divider clearing section />

                        <Switch name='save_in_db'
                            onChange={(e) => setActivityModal({ ...activityModal, save: e.target.checked })} /> Save in DB ?

                    </DialogContent>
                    <DialogActions>
                        <Button onClick={() => setActivityModal('closed')} color="primary">
                            Cancel
                                        </Button>
                        <Button onClick={onAddActivityItem} color="primary" type="submit">
                            Add
                        </Button>
                    </DialogActions>
                </ValidatorForm>
            </Dialog>

            <Dialog
                open={DialogConfirm.open === 'open'} disableBackdropClick fullWidth>
                <DialogTitle>{DialogConfirm.header}</DialogTitle>
                <DialogContent>
                    {DialogConfirm.body}
                </DialogContent>
                <DialogActions>
                    <Button variant='outlined' onClick={DialogConfirm.onCancel}>Cancel</Button>
                    <Button variant='contained' onClick={DialogConfirm.onSuccess}>Ok</Button>
                </DialogActions>
            </Dialog>

            <Dialog
                open={datepicker.open === 'open'} disableBackdropClick>
                <DialogContent>
                    <MuiPickersUtilsProvider utils={DateFnsUtils}>
                        <KeyboardDatePicker
                            variant='static'
                            format="yyyy-mm-dd"
                            value={datepicker.datestamp}
                            onChange={pickDateStamp}
                            KeyboardButtonProps={{
                                'aria-label': 'set datestamp',
                            }}
                            minDate={new Date()}
                            onChange={onDatePicked}
                        />
                    </MuiPickersUtilsProvider>
                </DialogContent>
                <DialogActions>
                    <Button variant='outlined' onClick={datepicker.onCancel}>Cancel</Button>
                </DialogActions>
            </Dialog>


            <Dialog
                open={ErrorDialog.open === 'open'} disableBackdropClick>
                <DialogTitle>{ErrorDialog.header}</DialogTitle>
                <DialogContent>
                    {ErrorDialog.body}
                </DialogContent>
                <DialogActions>
                    <Button variant='contained' onClick={ErrorDialog.onCloseDialog}>Ok</Button>
                </DialogActions>
            </Dialog>

            {!!(itinerary.content.days.length && itinerary.content.days[0].destinations.length) &&
                <Dialog
                    open={MapView.open === 'open'} disableBackdropClick fullScreen className="max-width">
                    <DialogTitle className="map-dialog">
                        <IconButton onClick={MapView.closeView}>
                            <CancelOutlined />
                        </IconButton>
                    </DialogTitle>
                    <DialogContent>
                        <GoogleMapDirectionComponent
                            position={
                                countries.find(c => c.country_id === itinerary.countries[0]) ?
                                    JSON.parse(countries.find(c => c.country_id === itinerary.countries[0]).location) : null}
                            destinations={itinerary.content.days.map(day => {
                                return day.destinations.map(d => {
                                    return { ...d, day };
                                });
                            })} />
                    </DialogContent>
                </Dialog>

            }

            <Dialog
                open={DistanceMatrix.open === 'open'} disableBackdropClick fullWidth className="max-width">
                <DialogTitle className="distance-modal">
                    <FlexRow>
                        <div>Distance and Travel Time Calculator</div>
                        <IconButton onClick={DistanceMatrix.closeView}>
                            <CancelOutlined />
                        </IconButton>
                    </FlexRow>
                </DialogTitle>
                <DialogContent>

                    <Slider {...{
                        slidesPerRow: 4, autoplay: false, responsive: [{
                            breakpoint: 1024,
                            settings: {
                                slidesToShow: 2,
                                slidesToScroll: 1
                            }
                        }, {
                            breakpoint: 600,
                            settings: {
                                slidesToShow: 1,
                                slidesToScroll: 1
                            }
                        }],
                        slidesToScroll: 1, dots: true, arrows: true, centerPadding: 15
                    }}>

                        {
                            itinerary.content.distance_duration_matrix.locations.map((location, index) => {

                                return (
                                    <Card style={{ margin: 15 }}>
                                        <CardHeader
                                            avatar={<PlaceOutlined />} title={(location.name)}
                                            action={<div className="index-icon">{index + 1}</div>} />

                                        <CardContent>
                                            <TextField label="City Tour (KM)" variant='filled' placeholder='City Tour (KM)'
                                                type="number" defaultValue={location.city_tours} onChange={onChangeCityTours.bind(null, location)} />
                                        </CardContent>
                                        <CardActionArea style={{ height: 50, padding: 15, textAlign: 'center' }}>
                                            {index > 0 ?
                                                <FlexRow>
                                                    <div>
                                                        <FlexRow>
                                                            <TimeToLeave /> {location.distance.text}
                                                        </FlexRow>
                                                    </div>
                                                    <div>
                                                        <TimelapseOutlined /> {location.duration.text}
                                                    </div>
                                                </FlexRow> : 'Start City Location'
                                            }
                                        </CardActionArea>
                                    </Card>

                                )
                            })
                        }

                    </Slider>

                    <Divider clearing />

                    <TextField label="Total Distance (KM)" variant='filled' placeholder='Total Distance (KM)' contentEditable={false}
                        value={(itinerary.content.distance_duration_matrix.total + itinerary.content.distance_duration_matrix.total_city_tours)} />

                </DialogContent>
            </Dialog>


            <Dialog
                open={ComponentDialog.open === 'open'} disableBackdropClick fullWidth className="max-width">
                <DialogTitle className="component-modal">
                    <FlexRow>
                        <div>{ComponentDialog.header}</div>
                        <div>
                            <IconButton variant='outlined' onClick={ComponentDialog.closeView}>
                                <CancelOutlined />
                            </IconButton>
                        </div>
                    </FlexRow>
                </DialogTitle>
                <DialogContent>
                    {ComponentDialog.component}
                </DialogContent>
            </Dialog>

            <Dialog
                open={TransportDialog.open === 'open'} disableBackdropClick fullWidth>
                <DialogTitle style={{ width: '100%', background: 'rgba(180, 180, 180, 0.5)' }}>
                    <FlexRow>
                        <div>Add Transport</div>
                        <div>
                            <IconButton variant='outlined' onClick={TransportDialog.closeView}>
                                <CancelOutlined />
                            </IconButton>
                        </div>
                    </FlexRow>
                </DialogTitle>
                <DialogContent style={{ ['overflow-x']: 'hidden' }}>
                    {TransportDialog.day &&
                        <Grid spacing="10" container>
                            <Grid md="12" item>
                                <FormControl component="fieldset">
                                    <FormLabel component="legend">Select Transport</FormLabel>
                                    <RadioGroup aria-label="transport" name="transport" defaultValue={
                                        TransportDialog.day.destinations.find(d => d.id === TransportDialog.destination_id) ?
                                            TransportDialog.day.destinations.find(d => d.id === TransportDialog.destination_id).transport.mode : 'car'
                                    } onChange={({ target: { value } }) => {

                                        const days = itinerary.content.days;

                                        days.splice(days.findIndex(d => d.id === TransportDialog.day.id), 1, {
                                            ...TransportDialog.day,
                                            destinations: [...TransportDialog.day.destinations.filter(d => d.id !== TransportDialog.destination_id),
                                            { ...TransportDialog.day.destinations.find(d => d.id === TransportDialog.destination_id), transport: { mode: value } }]
                                        })

                                        const updated_with_transport = {
                                            ...itinerary, content: {
                                                ...itinerary.content, days
                                            }
                                        }

                                        setItinerary({ ...updated_with_transport })
                                    }}>
                                        <FormControlLabel value="Car" control={<Radio />} label="Car" />
                                        <FormControlLabel value="Bus" control={<Radio />} label="Bus" />
                                        <FormControlLabel value="Train" control={<Radio />} label="Train" />
                                        <FormControlLabel value="Flight" control={<Radio />} label="Flight" />
                                    </RadioGroup>
                                </FormControl>
                            </Grid>
                        </Grid>
                    }
                </DialogContent>
            </Dialog>

            <Alert severity="info" className="save-alert">Press <span>CTRL + S</span> to save now !</Alert>

        </div >
    );
}


export default withStyles({}, { withTheme: true })(Builder);