import React, { useCallback, useState, useEffect } from "react";
import { ValidatorForm, TextValidator, SelectValidator } from "react-material-ui-form-validator";
import { ButtonGroup, Button, MenuItem, Grid, Divider, Icon, Switch, Dialog, DialogTitle, DialogContent, DialogActions, Table, TableBody, TableRow, TableCell, Avatar, IconButton } from "@material-ui/core";
import CameraIcon from '@material-ui/icons/CameraAltTwoTone';
import AddBox from '@material-ui/icons/AddBox';
import MapRounded from '@material-ui/icons/MapRounded';
import { Breadcrumb, EgretMenu } from "egret";
import { withStyles } from "@material-ui/styles";
import { useDispatch, useSelector } from "react-redux";
import { fetchClients } from "app/redux/actions/modules/Clients.action";
import { fetchUsers } from "app/redux/actions/modules/Users.actions";
import * as BuilderActions from 'app/redux/actions/modules/Builder.actions';
import { addTrip } from 'app/redux/actions/modules/Trips.action';
import FlexRow from "app/helpers/FlexRow";
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
import 'react-google-places-autocomplete/dist/assets/index.css';
import TableView from "./TableView.component";
import GridView from "./GridView.component";
import Builder from "./Builder.component";
import DocumentView from "../../public/document/DocumentView.component";
import { ToastsStore } from "react-toasts";
import { Dropdown } from "semantic-ui-react";
import history from '../../../../history';
import { setLayoutSettings } from "app/redux/actions/LayoutActions";
import { EgretLayoutSettings } from "app/EgretLayout/settings";
import { Link } from "react-router-dom";
import moment from "moment";
import ActionList from "app/helpers/ActionList";
import { fetchCountries, fetchCities } from "app/redux/actions/modules/Places.action";
import { PictureAsPdfOutlined, FileCopy, UpdateOutlined, TrendingUpOutlined } from "@material-ui/icons";


const getMuiThemeCustom = () => createMuiTheme({
    overrides: {
        MUIDataTableHeadCell: {
            root: {
                '&:nth-child(5)': {
                    width: '100px'
                }
            }
        }
    }
})

const Itinerary = (props) => {

    const dispatch = useDispatch();

    const { user: auth_user } = useSelector(state => state.user);

    const { list } = useSelector(state => state.builder);
    const { list: clients } = useSelector(state => state.clients);
    const { list: users } = useSelector(state => state.users);
    const { countries, cities } = useSelector(state => state.places);

    const [views, setViews] = useState('ManageView');

    const [designListView, setDesignListView] = useState('rows');

    const [document, setDocument] = useState(null);

    const [current, setCurrent] = useState('rows');

    const [DialogConfirm, setConfirmationDialog] = useState({
        header: 'Confirm',
        body: 'Do you confirm the action',
        onCancel: () => setConfirmationDialog({ ...DialogConfirm, open: 'closed' }),
        onSuccess: null,
        open: 'closed'
    })

    const [UpdateStatus, setUpdateStatus] = useState({
        onCancel: () => setUpdateStatus({ ...UpdateStatus, open: 'closed' }),
        onSuccess: null, open: 'closed'
    });

    const [ViewClient, setViewClient] = useState({
        onCancel: () => setViewClient({ ...ViewClient, open: 'closed' }),
        open: 'closed', client_id: null, clients: []
    });

    const [status, setStatus] = useState({
        id: null, text: null
    });


    const [PreviewDialog, setPreviewDialog] = useState({
        closeView: () => setPreviewDialog({ ...PreviewDialog, open: 'closed' }),
        onSuccess: null, open: 'closed'
    });



    useEffect(() => {

        if (['SuperAdmin', 'Admin'].includes(auth_user.data.role)) {
            dispatch(BuilderActions.fetchItineraries({ company_id: auth_user.data.company_id }))
        } else {
            dispatch(BuilderActions.fetchItineraries({ company_id: auth_user.data.company_id, user_id: auth_user.data.user_id }))
        }

        dispatch(fetchUsers({ company_id: auth_user.data.company_id }));
        dispatch(fetchClients(auth_user.data.company_id));
        dispatch(fetchCountries(auth_user.data.company_id));
        dispatch(fetchCities(auth_user.data.company_id));

    }, [])


    const toggleListViewDesign = () => {
        setDesignListView(designListView === 'rows' ? 'blocks' : 'rows');
    }

    const editEvent = (id) => {

        const c = {
            ...list.find(b => b.itinerary_id === id), content: JSON.parse(list.find(b => b.itinerary_id === id).content),
            countries: JSON.parse(list.find(b => b.itinerary_id === id).countries)
        };
        setCurrent(c);
        setViews('EditView');
    }

    const deleteEvent = (id) => {
        dispatch(BuilderActions.removeItinerary({ itinerary_id: id, user: auth_user }));
    }

    const deleteDraft = (id, callback) => {
        dispatch(BuilderActions.removeItinerary({ itinerary_id: id }, 'Draft', callback));
    }

    const blockEvent = (id) => { }


    const onBuildItinerary = () => {
        setLayoutSettings({
            ...EgretLayoutSettings,
            layout1Settings: {
                ...EgretLayoutSettings.layout1Settings,
                leftSidebar: { ...EgretLayoutSettings.layout1Settings.leftSidebar, mode: 'compact' }
            }
        })(dispatch);

        setViews('BuilderView');
    }


    const onCompleteAction = () => {
        setViews('ManageView');
    }

    const viewAll = () => {
        setLayoutSettings({
            ...EgretLayoutSettings,
            layout1Settings: {
                ...EgretLayoutSettings.layout1Settings,
                leftSidebar: { ...EgretLayoutSettings.layout1Settings.leftSidebar, mode: 'full' }
            }
        })(dispatch);

        setViews('ManageView');
    }

    const setPreview = (document) => {
        setDocument(list.find(doc => doc.itinerary_id === document));
        setPreviewDialog({ ...PreviewDialog, open: 'open' });
    }


    const makeCopy = (id) => {

        setConfirmationDialog({
            ...DialogConfirm, open: 'open', onSuccess: () => {

                const copy = list.find(doc => doc.itinerary_id === id);

                if (copy) {
                    delete copy.itinerary_id;
                    dispatch(BuilderActions.addItinerary({ ...copy, copy: id, user: auth_user }, () => {
                        ToastsStore.info('Itinerary is cloned successfully');
                        DialogConfirm.onCancel();
                    }));
                }
            }
        });

    }


    const assignUpdatedStatus = () => {
        dispatch(BuilderActions.updateItinerary({ ...list.find(doc => doc.itinerary_id === status.id), status: status.text, user: auth_user }, () => {
            setUpdateStatus({ ...UpdateStatus, open: 'closed' });

            setStatus({ id: null, text: null });
        }))
    }


    const updateStatusItinerary = (id) => {
        setUpdateStatus({ ...UpdateStatus, open: 'open' });
        setStatus({ ...status, id });
    }


    const convertToTrip = (id) => {

        const doc = list.find(doc => doc.itinerary_id === id);

        if (doc) {
            dispatch(addTrip({
                company_id: auth_user.data.company_id, client_id: doc.client_id,
                status: 'PENDING', itinerary_id: id, name: doc.name, created_by: doc.created_by
            }, () => {
                dispatch(BuilderActions.updateItinerary({ ...doc, converted: 'Y', user: auth_user }, () => {
                    history.push('/trips');
                }))
            }))
        }

    }


    const viewClient = (client_id, clients) => {

        setViewClient({ ...ViewClient, open: 'open', client_id, clients })

    }

    const CheckPermission = (type) => {

        var module_exists = false;
        var action_permission = false;

        auth_user.permissions.forEach(module => {
            if (Object.keys(module)[0] === "Itinerary") {
                module_exists = true;
                if (module['Itinerary'][type] === "ALLOWED") {
                    action_permission = true;
                }
            }
        });

        if (module_exists && action_permission) {
            return true;
        }

        return false;

    }


    const { path, params } = props.match ? props.match : { path: null, params: null };

    const modals = (

        <React.Fragment>
            <Dialog
                open={DialogConfirm.open === 'open'} disableBackdropClick fullWidth>
                <DialogTitle>{DialogConfirm.header}</DialogTitle>
                <DialogContent>
                    {DialogConfirm.body}
                </DialogContent>
                <DialogActions>
                    <Button variant='outlined' onClick={DialogConfirm.onCancel}>Cancel</Button>
                    <Button variant='contained' onClick={DialogConfirm.onSuccess}>Ok</Button>
                </DialogActions>
            </Dialog>


            <Dialog
                open={UpdateStatus.open === 'open'} disableBackdropClick fullWidth>
                <DialogTitle>Update Status</DialogTitle>
                <DialogContent style={{ height: 300 }}>
                    <Dropdown
                        placeholder='Select Status'
                        fluid
                        selection
                        value={status.text ? status.text : null}
                        onChange={(event, { value }) => setStatus({ ...status, text: value })}
                        options={['UPDATED (NEW)', 'APPROVED', 'PENDING', 'SALES', 'CANCELLED'].map((m, index) => {
                            return { key: index, text: m, value: m }
                        })}
                    />
                </DialogContent>
                <DialogActions>
                    <Button variant='outlined' onClick={UpdateStatus.onCancel}>Cancel</Button>
                    <Button variant='contained' onClick={assignUpdatedStatus}>Ok</Button>
                </DialogActions>
            </Dialog>

            <Dialog
                className="preview-dialog"
                open={PreviewDialog.open === 'open'} disableBackdropClick fullScreen>
                <DialogContent>
                    <DocumentView control document={document} setBackView={PreviewDialog.closeView} />
                </DialogContent>
            </Dialog>


            <Dialog
                open={ViewClient.open === 'open'} disableBackdropClick fullWidth>
                <DialogContent>
                    <Table>
                        {
                            ViewClient.clients.find(c => c.client_id === ViewClient.client_id) &&
                            <TableBody>
                                <TableRow>
                                    <TableCell>
                                        <Avatar src={process.env.REACT_APP_STATIC + ViewClient.clients.find(c => c.client_id === ViewClient.client_id).avatar} />
                                    </TableCell>
                                    <TableCell>{
                                        ViewClient.clients.find(c => c.client_id === ViewClient.client_id).salutation +
                                        ViewClient.clients.find(c => c.client_id === ViewClient.client_id).name
                                    }</TableCell>
                                </TableRow>
                                <TableRow>
                                    <TableCell>Email</TableCell>
                                    <TableCell>{ViewClient.clients.find(c => c.client_id === ViewClient.client_id).email}</TableCell>
                                </TableRow>
                                <TableRow>
                                    <TableCell>Phone Number</TableCell>
                                    <TableCell>{ViewClient.clients.find(c => c.client_id === ViewClient.client_id).phone_number}</TableCell>
                                </TableRow>
                                <TableRow>
                                    <TableCell>Country</TableCell>
                                    <TableCell>{ViewClient.clients.find(c => c.client_id === ViewClient.client_id).country}</TableCell>
                                </TableRow>
                            </TableBody>
                        }
                    </Table>
                </DialogContent>
                <DialogActions>
                    <Button variant='outlined' onClick={ViewClient.onCancel}>Cancel</Button>
                </DialogActions>
            </Dialog>
        </React.Fragment>

    )

    if (path === "/itinerary/builder/view/:id") {
        return (
            <div className="analytics m-sm-30">
                <div className="mb-sm-30">
                    <Breadcrumb
                        routeSegments={[
                            { name: "Dashboard", path: "/dashboard" },
                            { name: "Itinerary Builder" }
                        ]}
                    />
                </div>

                {
                    list.find(b => b.itinerary_id == params.id) ?
                        <Table style={{ border: '.5px solid' }}>
                            <TableRow>
                                <TableCell colspan="2" style={{ padding: '15px' }}>
                                    <FlexRow>
                                        <Button variant='outlined' onClick={() => history.push('/itinerary/builder')}>Back</Button>
                                        <ActionList del={CheckPermission("delete")} deleteEvent={deleteEvent}
                                            id={list.find(b => b.itinerary_id == params.id).itinerary_id}>
                                            <EgretMenu menuButton={
                                                <IconButton>
                                                    <Icon>more_vert</Icon>
                                                </IconButton>
                                            }>
                                                <MenuItem style={{ minWidth: 185 }}>
                                                    <Icon> cancel </Icon>
                                                    <span className="pl-16">Cancel</span>
                                                </MenuItem>
                                                {!['DRAFT'].includes(list.find(b => b.itinerary_id == params.id).status) &&
                                                    <React.Fragment>
                                                        <MenuItem style={{ minWidth: 185 }} onClick={() => setPreview(list.find(b => b.itinerary_id == params.id).itinerary_id)}>
                                                            <PictureAsPdfOutlined />
                                                            <span className="pl-16">Preview</span>
                                                        </MenuItem>
                                                        <MenuItem style={{ minWidth: 185 }} onClick={() => makeCopy(list.find(b => b.itinerary_id == params.id).itinerary_id)}>
                                                            <FileCopy />
                                                            <span className="pl-16">Clone This</span>
                                                        </MenuItem>
                                                        <MenuItem style={{ minWidth: 185 }} onClick={() => updateStatusItinerary(list.find(b => b.itinerary_id == params.id).itinerary_id)}>
                                                            <UpdateOutlined />
                                                            <span className="pl-16">Update Status</span>
                                                        </MenuItem>

                                                        {(list.find(b => b.itinerary_id == params.id).status === 'APPROVED') &&
                                                            <MenuItem style={{ minWidth: 185 }} disabled={list.find(b => b.itinerary_id == params.id).converted === 'Y'} onClick={() => convertToTrip(list.find(b => b.itinerary_id == params.id).itinerary_id)}>
                                                                <TrendingUpOutlined />
                                                                <span className="pl-16" style={list.find(b => b.itinerary_id == params.id).converted === 'Y' ? { color: 'green', fontWeight: 'bold' } : {}}>
                                                                    {list.find(b => b.itinerary_id == params.id).converted === 'N' ? 'Convert To Trip' : 'Converted'}
                                                                </span>
                                                            </MenuItem>
                                                        }
                                                    </React.Fragment>
                                                }
                                            </EgretMenu>
                                        </ActionList>
                                    </FlexRow>
                                </TableCell>
                            </TableRow>
                            <TableRow>
                                <TableCell style={{ padding: '15px' }}>Name</TableCell>
                                <TableCell>{list.find(b => b.itinerary_id == params.id).name}</TableCell>
                            </TableRow>
                            <TableRow>
                                <TableCell style={{ padding: '15px' }}>Client</TableCell>
                                <TableCell>
                                    {
                                        clients.find(c => c.client_id === list.find(b => b.itinerary_id == params.id).client_id) ?
                                            <Link to={`/itinerary/builder/view/${list.find(b => b.itinerary_id == params.id).client_id}`}>{clients.find(c => c.client_id === list.find(b => b.itinerary_id == params.id).client_id).name}</Link> : null
                                    }
                                </TableCell>
                            </TableRow>
                            <TableRow>
                                <TableCell style={{ padding: '15px' }}>Pax</TableCell>
                                <TableCell>{list.find(b => b.itinerary_id == params.id).pax}</TableCell>
                            </TableRow>
                            <TableRow>
                                <TableCell style={{ padding: '15px' }}>Days / Nights</TableCell>
                                <TableCell>{list.find(b => b.itinerary_id == params.id).days}/{parseInt(list.find(b => b.itinerary_id == params.id).days) - 1}</TableCell>
                            </TableRow>
                            <TableRow>
                                <TableCell style={{ padding: '15px' }}>Countries</TableCell>
                                <TableCell>
                                    {!!list.find(b => b.itinerary_id == params.id).countries &&

                                        JSON.parse(list.find(b => b.itinerary_id == params.id).countries).map(country => (
                                            countries.find(c => c.country_id === country) ? countries.find(c => c.country_id === country).name : ''
                                        )).join()

                                    }
                                </TableCell>
                            </TableRow>
                            <TableRow>
                                <TableCell style={{ padding: '15px' }}>Currency</TableCell>
                                <TableCell>{list.find(b => b.itinerary_id == params.id).currency}</TableCell>
                            </TableRow>
                            <TableRow>
                                <TableCell style={{ padding: '15px' }}>Is converted ?</TableCell>
                                <TableCell>{list.find(b => b.itinerary_id == params.id).converted === 'Y' ? 'Yes' : 'No'}</TableCell>
                            </TableRow>
                            <TableRow>
                                <TableCell style={{ padding: '15px' }}>Is converted ?</TableCell>
                                <TableCell>{list.find(b => b.itinerary_id == params.id).converted === 'Y' ? 'Yes' : 'No'}</TableCell>
                            </TableRow>
                            <TableRow>
                                <TableCell style={{ padding: '15px' }}>Created By</TableCell>
                                <TableCell>
                                    {
                                        users.find(u => u.user_id === list.find(b => b.itinerary_id == params.id).created_by) ?
                                            <Link to={`/users/view/${list.find(b => b.itinerary_id == params.id).created_by}`}>{users.find(u => u.user_id === list.find(b => b.itinerary_id == params.id).created_by).name}</Link> : null
                                    }
                                </TableCell>
                            </TableRow>
                            <TableRow>
                                <TableCell style={{ padding: '15px' }}>Status</TableCell>
                                <TableCell>{list.find(b => b.itinerary_id == params.id).status}</TableCell>
                            </TableRow>
                            <TableRow>
                                <TableCell style={{ padding: '15px' }}>Created On</TableCell>
                                <TableCell>{moment(list.find(b => b.itinerary_id == params.id).createdAt).format('YYYY MMM DD, hh:mm:ss a ddd')}</TableCell>
                            </TableRow>
                            <TableRow>
                                <TableCell style={{ padding: '15px' }}>Last Updated On</TableCell>
                                <TableCell>{moment(list.find(b => b.itinerary_id == params.id).updatedAt).format('YYYY MMM DD, hh:mm:ss a ddd')}</TableCell>
                            </TableRow>
                        </Table> : <div className="no-matches">No matched item found !</div>
                }

                {
                    modals
                }
            </div>
        )
    }

    return (
        <div className="analytics m-sm-30">
            <div className="mb-sm-30">
                <Breadcrumb
                    routeSegments={[
                        { name: "Dashboard", path: "/dashboard" },
                        { name: "Itinerary Builder" }
                    ]}
                />
            </div>

            {
                views === 'ManageView' &&

                <div className="builder-manage-view">
                    <FlexRow>
                        <Button variant='contained' title="List View Design" onClick={onBuildItinerary}>
                            Start Build Itinerary
                        </Button>
                        <Button variant='outlined' title="List View Design" onClick={toggleListViewDesign}>
                            {
                                designListView === 'rows' ?
                                    <Icon>dashboard</Icon> : <Icon>list</Icon>
                            }

                        </Button>
                    </FlexRow>

                    <div className="content">
                        {
                            designListView === 'rows' ?
                                <TableView setPreview={setPreview} viewClient={viewClient} makeCopy={makeCopy} updateItinerary={updateStatusItinerary}
                                    editEvent={editEvent} deleteEvent={deleteEvent} blockEvent={blockEvent} convertToTrip={convertToTrip} /> :
                                <GridView setPreview={setPreview} makeCopy={makeCopy} updateItinerary={updateStatusItinerary}
                                    editEvent={editEvent} deleteEvent={deleteEvent} blockEvent={blockEvent} convertToTrip={convertToTrip} />
                        }
                    </div>
                </div>
            }

            {
                views === 'BuilderView' &&

                <div className="builder-view">
                    <Builder onBuildFinish={BuilderActions.addItinerary} deleteDraft={deleteDraft} onCompleteAction={onCompleteAction} onAutoSaveDraft={BuilderActions.autoSaveItinerary} viewAll={viewAll} />
                </div>
            }


            {
                views === 'EditView' &&

                <div className="builder-view">
                    <Builder current={current} deleteDraft={deleteDraft} onBuildEdit={BuilderActions.updateItinerary} onCompleteAction={onCompleteAction} onAutoSaveDraft={BuilderActions.autoSaveItinerary} viewAll={viewAll} />
                </div>
            }

            {
                modals
            }

        </div>
    );
}

export default withStyles({}, { withTheme: true })(Itinerary);
