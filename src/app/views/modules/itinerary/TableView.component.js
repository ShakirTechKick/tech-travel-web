import React, { useState } from "react";
import TableMD from "mui-datatables";
import { Icon, IconButton, MenuItem, Button } from "@material-ui/core";
import { withStyles } from "@material-ui/styles";
import { EgretMenu } from "egret";
import ActionList from "app/helpers/ActionList";
import { useSelector } from "react-redux";
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
import moment from "moment";
import { PictureInPictureAlt, FileCopy, UpdateOutlined, TrendingUpOutlined } from "@material-ui/icons";


const getMuiStyles = () => createMuiTheme({
    overrides: {
        MUIDataTableHeadCell: {
            root: {
                '&:nth-child(1)': {
                    width: 100
                }
            }
        }
    }
})

const TableView = (props) => {

    const { user } = useSelector(state => state.user);
    const { list } = useSelector(state => state.builder);
    const { list: users } = useSelector(state => state.users);
    const { list: clients } = useSelector(state => state.clients);

    const CheckPermission = (type) => {

        var module_exists = false;
        var action_permission = false;

        user.permissions.forEach(module => {
            if (Object.keys(module)[0] === "Itinerary") {
                module_exists = true;
                if (module['Itinerary'][type] === "ALLOWED") {
                    action_permission = true;
                }
            }
        });

        if (module_exists && action_permission) {
            return true;
        }

        return false;

    }

    const getUserIdentity = user_id => {
        if (user_id === user.data.user_id) {
            return "You";
        } else {
            if (users.find(u => u.user_id === user_id)) {
                return users.find(u => u.user_id === user_id).name;
            } else {
                return `User ID: ${user_id}`
            }
        }
    }

    const getClient = client_id => {
        if (client_id) {

            if (clients.find(c => c.client_id === client_id)) {
                return `${clients.find(c => c.client_id === client_id).name} (${clients.find(c => c.client_id === client_id).country})`;
            } else {
                return `Client ID: ${client_id}`
            }
        } else {
            return "Not Set";
        }
    }

    return (
        <div className="content">
            <MuiThemeProvider theme={getMuiStyles()}>
                <TableMD columns={['ID', 'Name', 'Client',
                    { name: 'Created By', options: { filter: false, sort: false } },
                    { name: 'Created At', options: { filter: false, sort: false } },
                    'Status', { name: 'Actions', options: { filter: false, sort: false } }
                ]} data={[...list.sort((a, b) => b.itinerary_id - a.itinerary_id).map(item => (
                    [item.copy ?
                        <div><div>{item.itinerary_id}</div><div>Clone : {item.copy}</div></div> : item.itinerary_id,
                    <Button variant='text' style={{ fontSize: 12, color: 'green', fontWeight: 'bold' }} onClick={() => props.setPreview(item.itinerary_id)}>{item.name}</Button>,
                    <Button variant='text' style={{ fontSize: 12, color: 'blue', fontWeight: 'bold' }} onClick={() => props.viewClient(item.client_id, clients)}>{getClient(item.client_id)}</Button>,
                    getUserIdentity(item.created_by), moment(item.createdAt).format('YYYY MMM DD'), item.status,
                    <ActionList del={CheckPermission('delete')} edit={CheckPermission('edit')}
                        id={item.itinerary_id} deleteEvent={props.deleteEvent}
                        editEvent={props.editEvent} status={item.status} view={`/itinerary/builder/view/${item.itinerary_id}`}>
                        <EgretMenu menuButton={
                            <IconButton>
                                <Icon>more_vert</Icon>
                            </IconButton>
                        }>
                            <MenuItem style={{ minWidth: 185 }}>
                                <Icon> cancel </Icon>
                                <span className="pl-16">Cancel</span>
                            </MenuItem>
                            {!['DRAFT'].includes(item.status) &&
                                <React.Fragment>
                                    <MenuItem style={{ minWidth: 185 }} onClick={() => props.setPreview(item.itinerary_id)}>
                                        <PictureInPictureAlt />
                                        <span className="pl-16">Preview</span>
                                    </MenuItem>
                                    <MenuItem style={{ minWidth: 185 }} onClick={() => props.makeCopy(item.itinerary_id)}>
                                        <FileCopy />
                                        <span className="pl-16">Clone This</span>
                                    </MenuItem>
                                    <MenuItem style={{ minWidth: 185 }} onClick={() => props.updateItinerary(item.itinerary_id)}>
                                        <UpdateOutlined />
                                        <span className="pl-16">Update Status</span>
                                    </MenuItem>

                                    {(item.status === 'APPROVED') &&
                                        <MenuItem style={{ minWidth: 185 }} disabled={item.converted === 'Y'} onClick={() => props.convertToTrip(item.itinerary_id)}>
                                            <TrendingUpOutlined />
                                            <span className="pl-16" style={item.converted === 'Y' ? { color: 'green', fontWeight: 'bold' } : {}}>
                                                {item.converted === 'N' ? 'Convert To Trip' : 'Converted'}
                                            </span>
                                        </MenuItem>
                                    }
                                </React.Fragment>
                            }
                        </EgretMenu>
                    </ActionList>
                    ]
                ))]} options={{ responsive: 'scrollMaxHeight' }} />
            </MuiThemeProvider>
        </div>
    )
}

export default withStyles({}, { withTheme: true })(TableView);