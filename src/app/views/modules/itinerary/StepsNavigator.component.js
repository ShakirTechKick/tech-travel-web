import React from 'react';
import { Button } from '@material-ui/core';


export default ({ activeStep, steps, isStepOptional, handleSkip, handleNext, handleBack }) => (
    <div>
        {activeStep !== steps.length && (
            <div>
                <Button disabled={activeStep === 0} onClick={handleBack}>
                    Back
                    </Button>

                {isStepOptional(activeStep) && (
                    <Button
                        variant="contained"
                        color="primary"
                        onClick={handleSkip}>
                        Skip
                    </Button>
                )}

                <Button
                    variant="contained"
                    color="primary"
                    type="submit"
                    onClick={handleNext}>
                    {activeStep === steps.length - 1 ? 'Finish' : 'Next'}
                </Button>
            </div>
        )}
    </div>
)