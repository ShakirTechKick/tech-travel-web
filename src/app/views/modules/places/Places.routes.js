import { EgretLoadable } from "egret";
import { authRoles } from "../../../auth/authRoles";

const Places = EgretLoadable({
  loader: () => import("./Places.component")
});

const placesRoutes = [
  {
    name: 'Places',
    path: "/library/places",
    exact: true,
    component: Places,
    auth: authRoles.admin,
    access: 'AUTH'
  },
  {
    name: 'Places',
    path: "/library/places/country/view/:id",
    exact: true,
    component: Places,
    auth: authRoles.admin,
    access: 'AUTH'
  },
  {
    name: 'Places',
    path: "/library/places/city/view/:id",
    exact: true,
    component: Places,
    auth: authRoles.admin,
    access: 'AUTH'
  }
];

export default placesRoutes;
