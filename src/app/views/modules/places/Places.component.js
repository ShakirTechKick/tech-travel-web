import React, { useState, useEffect } from "react";
import { ValidatorForm, TextValidator } from "react-material-ui-form-validator";
import { ButtonGroup, Button, MenuItem, Grid, Divider, Icon, Switch, TextField, Dialog, DialogTitle, DialogContent, DialogActions, Table, TableRow, TableCell, IconButton } from "@material-ui/core";
import AddBox from '@material-ui/icons/AddBox';
import MapRounded from '@material-ui/icons/MapRounded';
import { Breadcrumb } from "egret";
import { withStyles } from "@material-ui/styles";
import { useDispatch, useSelector } from "react-redux";
import TableMD from "mui-datatables";
import ActionList from "app/helpers/ActionList";
import { Dropdown } from "semantic-ui-react";
import { ToastsStore } from "react-toasts";
import Widget from 'app/helpers/TravUploadWidget';
import * as PlacesActions from 'app/redux/actions/modules/Places.action';
import FlexRow from "app/helpers/FlexRow";
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
import 'react-google-places-autocomplete/dist/assets/index.css';
import GoogleMapComponent from "app/helpers/GoogleMapComponent";
import GooglePlacesSuggest from "react-geosuggest";
import { countries as ListOfCountries, languages } from "countries-list";
import { getCode } from "country-list";
import CountryCodes from "app/helpers/country-codes.json";
import { geocodeByPlaceId } from "react-google-places-autocomplete";
import moment from "moment";
import history from '../../../../history';
import { CancelOutlined } from "@material-ui/icons";
import { Request } from "app/config/Request";

const getMuiThemeCustom = () => createMuiTheme({
    overrides: {
        MUIDataTableHeadCell: {
            root: {
                '&:nth-child(5)': {
                    width: '100px'
                }
            }
        }
    }
})

const Places = (props) => {

    const dispatch = useDispatch();

    const { user: auth_user } = useSelector(state => state.user);

    const [current, setCurrent] = useState({});

    const [isMapShown, setToggleMap] = useState({
        visibility: false, location: { lat: 7.873053999999999, lng: 80.77179699999999 }, zoom: 6
    });


    const static_country = {
        company_id: auth_user.data.company_id
    };

    const [country, setCountry] = useState(static_country);

    const static_city = {
        company_id: auth_user.data.company_id, images: []
    };

    const [city, setCity] = useState(static_city);

    const [fetchRemoteImagesCity, setfetchRemoteImagesCity] = useState({
        allowed: false, photos: []
    });

    const [fetchRemoteImagesCountry, setfetchRemoteImagesCountry] = useState({
        allowed: false, photos: []
    });


    const [isUploadOpen, setUploadOpen] = useState({
        open: 'closed', category: 'country'
    });

    const [DetailView, setDetailView] = useState({
        heading: 'Add Country', open: 'closed', view: 'Add',
        closeView: () => setDetailView({ ...DetailView, open: 'closed' })
    })

    const onCompleteHandler = (id, name, response) => {
        if (response.success) {
            if (isUploadOpen.category === 'city') {
                if (city.images) {
                    setCity({ ...city, images: [...city.images, `${response.path}/${name}`] });
                } else {
                    setCity({ ...city, images: [`${response.path}/${name}`] });
                }
            } else {
                if (country.images) {
                    setCountry({ ...country, images: [...country.images, `${response.path}/${name}`] });
                } else {
                    setCountry({ ...country, images: [`${response.path}/${name}`] });
                }
            }
        }
    }
    const onOpenGallery = (category) => {
        setUploadOpen({ open: 'open', category });
    }

    const onRemoveGalleryCity = (img) => {

        const images = city.images.filter(i => i !== img);

        setCity({ ...city, images: [...images] });

        Request.post(`utilities/fileuploader/destroy_resources`, { path: img })
    }

    const onRemoveGalleryCountry = (img) => {

        const images = country.images.filter(i => i !== img);

        setCountry({ ...country, images: [...images] });

        Request.post(`utilities/fileuploader/destroy_resources`, { path: img })
    }

    const { countries, cities } = useSelector(state => state.places);

    const [mainView, setMainView] = useState('CountryView');
    const [subView, setSubView] = useState('AddView');

    useEffect(() => {
        dispatch(PlacesActions.fetchCountries(auth_user.data.company_id));
        dispatch(PlacesActions.fetchCities(auth_user.data.company_id));
    }, [])

    const deleteEventCountry = (id) => {
        dispatch(PlacesActions.removeCountry({ country_id: id }))
    };

    const editEventCountry = (id) => {
        const c = countries.find(c => c.country_id === id);
        setCountry({ ...c, images: c.images ? c.images.split(',').filter(i => !!i) : [], location: JSON.parse(c.location) });

        geocodeByPlaceId(c.place_id).then(result => {

            const location = { lat: result[0].geometry.location.lat(), lng: result[0].geometry.location.lng() };

            setToggleMap({ ...isMapShown, location, zoom: 4 });

        }).catch(error => null)

        setSubView('EditView');

        setDetailView({
            ...DetailView, open: 'open', heading: 'Edit Country', view: 'Edit'
        })
    };

    const blockEventCountry = (id) => {
        dispatch(PlacesActions.statusCountry({ country_id: id }))
    };

    const deleteEventCity = (id) => {
        dispatch(PlacesActions.removeCity({ city_id: id }))
    };

    const editEventCity = (id) => {
        const c = cities.find(c => c.city_id === id);
        setCity({ ...c, images: c.images ? c.images.split(',').filter(i => !!i) : [], location: JSON.parse(c.location) });

        geocodeByPlaceId(c.place_id).then(result => {

            const location = { lat: result[0].geometry.location.lat(), lng: result[0].geometry.location.lng() };

            setToggleMap({ ...isMapShown, location, zoom: 10 });

        }).catch(error => null);

        setSubView('EditView');
        setDetailView({
            ...DetailView, open: 'open', heading: 'Edit City', view: 'Edit'
        })
    };

    const blockEventCity = (id) => {
        dispatch(PlacesActions.statusCity({ city_id: id }))
    };

    const CheckPermission = (type) => {

        var module_exists = false;
        var action_permission = false;

        auth_user.permissions.forEach(module => {
            if (Object.keys(module)[0] === "Places") {
                module_exists = true;
                if (module['Places'][type] === "ALLOWED") {
                    action_permission = true;
                }
            }
        });

        if (module_exists && action_permission) {
            return true;
        }

        return false;

    }


    const onChangeField = (mod, e) => {
        if (mod === 'country') {
            setCountry({ ...country, [e.target.name]: e.target.value });
        } else {
            setCity({ ...city, [e.target.name]: e.target.value });
        }

    }

    const submitCountryForm = () => {

        setfetchRemoteImagesCountry({ allowed: false, name: null, photos: [] })

        if (subView === 'AddView') {

            if (countries.find(c => c.name === country.name)) {
                ToastsStore.error(`${country.name} already exists !`, 2000);
            } else {
                dispatch(PlacesActions.addCountry({ ...country }, () => {
                    setCountry({ ...static_country });
                    setDetailView({ ...DetailView, open: 'closed' });
                }));
            }

        } else if (subView == 'EditView') {
            dispatch(PlacesActions.updateCountry({ ...country }, () => {
                setCountry({ ...static_country });
                setDetailView({ ...DetailView, open: 'closed' });
            }));
        }
    }

    const submitCityForm = () => {

        setfetchRemoteImagesCity({ allowed: false, name: null, photos: [] })

        if (subView === 'AddView') {
            dispatch(PlacesActions.addCity({ ...city, country_id: current.country_id }, () => {
                setCity({ ...static_city });
                setDetailView({ ...DetailView, open: 'closed' });
            }));
        } else if (subView == 'EditView') {
            dispatch(PlacesActions.updateCity({ ...city }, () => {
                setCity({ ...static_city });
                setDetailView({ ...DetailView, open: 'closed' });
            }));
        }
    }


    const viewCities = (selected_country) => {
        setCurrent({ ...selected_country });
        setCity({ ...city, country_id: selected_country.country_id, images: [] });
    }

    useEffect(() => {
        if (current.country_id) {
            setMainView('CityView');
            setSubView('AddView');
        }
    }, [current]);

    const countries_list = countries.map(c => ([
        c.name, c.capital, c.languages, c.currency,
        <FlexRow styles={{ justifyContent: 'flex-start', flexWrap: 'no-wrap' }}>
            <Button variant='outlined' color='secondary' onClick={viewCities.bind(null, c)}>Cities</Button>
            <ActionList del={CheckPermission("delete")} edit={CheckPermission("edit")} block={CheckPermission("edit")}
                id={c.country_id} deleteEvent={deleteEventCountry} blockEvent={blockEventCountry}
                editEvent={editEventCountry} status={c.status} view={`/library/places/country/view/${c.country_id}`} />
        </FlexRow>
    ]))

    const cities_list = cities.sort((a, b) => b.city_id - a.city_id).filter(c => c.country_id === current.country_id).map(c => ([
        c.name, c.type,
        <ActionList del={CheckPermission("delete")} edit={CheckPermission("edit")} block={CheckPermission("edit")}
            id={c.city_id} deleteEvent={deleteEventCity} blockEvent={blockEventCity}
            editEvent={editEventCity} status={c.status} view={`/library/places/city/view/${c.city_id}`} />
    ]))

    const viewCountries = () => {
        setSubView('AddView');
        setMainView('CountryView');
    }

    const makeToggleMap = () => {
        setToggleMap({ ...isMapShown, visibility: !isMapShown.visibility });
    }


    const onSuggestSelectRegion = (region) => {

        if (region) {

            const { gmaps, location } = region;

            const { name, place_id, photos } = gmaps;

            setCountry({ ...country, name, place_id });
            setToggleMap({ ...isMapShown, location, zoom: 4 });

            const code = getCode(name) || CountryCodes[name];

            var capital, currency, country_languages = null;

            if (typeof code !== 'undefined') {

                capital = ListOfCountries[code].capital;
                currency = ListOfCountries[code].currency;
                country_languages = ListOfCountries[code].languages.map(l => {
                    return languages[l].name + ` (${languages[l].native})`
                }).join();

            } else {
                ToastsStore.warning(`Unable to fetch additional information for the country ${name}`, 2000);
            }


            setfetchRemoteImagesCountry({ ...fetchRemoteImagesCountry, name, photos });

            if (fetchRemoteImagesCountry.allowed) {

                const images = [];
                var counter = 0;

                ToastsStore.info('Fetching images, please wait...!', 2000);

                const caption = `c${auth_user.data.company_id}-${name.replace(/\s/g, String.apply())}`;

                const pictures = photos.filter((p, index) => index < 5);

                pictures.filter((p, index) => index < 5).forEach(photo => {
                    Request.post(`utilities/fileuploader/download`, { url: photo.getUrl(), filepath: `google-${caption}-${counter++}-places-photo.jpg` })
                        .then(result => {
                            if (result.data.status === 'success') {
                                images.push(`/${result.data.path.replace('public/uploads/', '')}`);

                                if (counter == pictures.length) {
                                    setCountry({ ...country, images, name, place_id, location: JSON.stringify(location), code, capital, currency, languages: country_languages });
                                    ToastsStore.info('Successfully images are fetched !', 2000);
                                }
                            }
                        }).catch(error => null);

                })

            } else {
                setCountry({ ...country, name, place_id, location: JSON.stringify(location), code, capital, currency, languages: country_languages });
            }
        }

    }


    const onSuggestSelectCity = (selected) => {

        if (selected) {

            const { gmaps, location } = selected;
            const { name, photos, place_id, url, vicinity } = gmaps;

            setToggleMap({ ...isMapShown, location, zoom: 10 });

            setfetchRemoteImagesCity({ ...fetchRemoteImagesCity, name, photos });

            if (fetchRemoteImagesCity.allowed) {

                const images = [];
                var counter = 0;

                ToastsStore.info('Fetching images, please wait...!', 2000);

                const caption = `c${auth_user.data.company_id}-${name.replace(/\s/g, String.apply())}`;

                const pictures = photos.filter((p, index) => index < 5);

                pictures.forEach(photo => {
                    Request.post(`utilities/fileuploader/download`, { url: photo.getUrl(), filepath: `google-${caption}-${counter++}-places-photo.jpg` })
                        .then(result => {
                            if (result.data.status === 'success') {
                                images.push(`/${result.data.path.replace('public/uploads/', '')}`);

                                if (counter == pictures.length) {
                                    setCity({ ...city, name, place_id, location: JSON.stringify(location), url, vicinity, images });
                                    ToastsStore.info('Successfully images are fetched !', 2000);
                                }
                            }
                        }).catch(error => null);

                })

            } else {
                setCity({ ...city, name, place_id, location: JSON.stringify(location), url, vicinity });
            }

        }

    }


    const onClickOnMap = (e) => {

    }

    const onChangeImageFetchCity = e => {

        setfetchRemoteImagesCity({ ...fetchRemoteImagesCity, allowed: e.target.checked });

        if (e.target.checked) {
            if (fetchRemoteImagesCity.photos && fetchRemoteImagesCity.photos.length) {

                const photos = fetchRemoteImagesCity.photos;

                ToastsStore.info('Fetching images, please wait...!', 2500);
                const images = [];

                var counter = 0;
                const caption = `c${auth_user.data.company_id}-${fetchRemoteImagesCity.name.replace(/\s/g, String.apply())}`;

                const pictures = photos.filter((p, index) => index < 5);

                pictures.forEach(photo => {
                    Request.post(`utilities/fileuploader/download`, { url: photo.getUrl(), filepath: `google-${caption}-${counter++}-places-photo.jpg` })
                        .then(result => {
                            if (result.data.status === 'success') {
                                images.push(`/${result.data.path.replace('public/uploads/', '')}`);

                                if (counter == pictures.length) {
                                    setCity({
                                        ...city, images
                                    });
                                    ToastsStore.info('Successfully images are fetched !', 2000);
                                }
                            }
                        }, error => null);

                });

            }
        } else {

            if (city.images && city.images.length) {
                const googleimages = city.images.filter(img => img.indexOf('google') !== -1);

                if (googleimages.length) {
                    ToastsStore.info('Removing google images fetched', 2500);

                    setCity({ ...city, images: [...city.images.filter(img => img.indexOf('google') === -1)] });

                    googleimages.forEach(img => {
                        Request.post(`utilities/fileuploader/destroy_resources`, { path: img })
                            .then(null, null);
                    })
                }
            }

        }
    }


    const onChangeImageFetchCountry = e => {

        setfetchRemoteImagesCountry({ ...fetchRemoteImagesCountry, allowed: e.target.checked });

        if (e.target.checked) {
            if (fetchRemoteImagesCountry.photos && fetchRemoteImagesCountry.photos.length) {

                const photos = fetchRemoteImagesCountry.photos;

                ToastsStore.info('Fetching images, please wait...!', 2500);
                const images = [];

                var counter = 0;
                const caption = `c${auth_user.data.company_id}-${fetchRemoteImagesCountry.name.replace(/\s/g, String.apply())}`;

                const pictures = photos.filter((p, index) => index < 5);

                pictures.forEach(photo => {
                    Request.post(`utilities/fileuploader/download`, { url: photo.getUrl(), filepath: `google-${caption}-${counter++}-places-photo.jpg` })
                        .then(result => {
                            if (result.data.status === 'success') {

                                images.push(`/${result.data.path.replace('public/uploads/', '')}`);

                                if (counter == pictures.length) {
                                    setCountry({
                                        ...country, images
                                    });
                                    ToastsStore.info('Successfully images are fetched !', 2000);
                                }
                            }
                        }, error => null);
                });

            }
        } else {

            if (country.images && country.images.length) {
                const googleimages = country.images.filter(img => img.indexOf('google') !== -1);

                if (googleimages.length) {
                    ToastsStore.info('Removing google images fetched', 2500);

                    setCountry({ ...country, images: [...country.images.filter(img => img.indexOf('google') === -1)] });

                    googleimages.forEach(img => {
                        Request.post(`utilities/fileuploader/destroy_resources`, { path: img })
                            .then(null, null);
                    })
                }
            }

        }
    }


    const addCityView = () => {
        setCity({ ...static_city });
        setDetailView({
            ...DetailView, open: 'open', heading: 'Add City', view: 'Add'
        });
        setSubView('AddView');
    }

    const addCountryView = () => {
        setCountry({ ...static_country });
        setDetailView({
            ...DetailView, open: 'open', heading: 'Add Country', view: 'Add'
        });
    }

    const { path, params } = props.match ? props.match : { path: null, params: null };


    if (path === "/library/places/country/view/:id") {
        return (
            <div className="analytics m-sm-30">
                {!props.modal &&
                    <div className="mb-sm-30">
                        <Breadcrumb
                            routeSegments={[
                                { name: "Dashboard", path: "/dashboard" },
                                { name: "Places" }
                            ]}
                        />
                    </div>
                }

                {!!countries.find(c => c.country_id == params.id) ?
                    <React.Fragment>
                        {!!countries.find(c => c.country_id == params.id).location &&
                            <GoogleMapComponent isMarkerShown defaultZoom={4} defaultCenter={JSON.parse(countries.find(c => c.country_id == params.id).location)} />}

                        <Table style={{ border: '.5px solid' }}>
                            <TableRow>
                                <TableCell colspan="2" style={{ padding: '15px' }}>
                                    <FlexRow>
                                        <Button variant='outlined' onClick={() => history.push('/library/places')}>Back</Button>
                                        <ActionList del={CheckPermission("delete")} block={CheckPermission("edit")}
                                            id={countries.find(c => c.country_id == params.id).country_id} deleteEvent={deleteEventCountry} blockEvent={blockEventCountry}
                                            status={countries.find(c => c.country_id == params.id).status} />
                                    </FlexRow>
                                </TableCell>
                            </TableRow>
                            <TableRow>
                                <TableCell style={{ padding: '15px' }}>Name</TableCell>
                                <TableCell>{countries.find(c => c.country_id == params.id).name}</TableCell>
                            </TableRow>
                            <TableRow>
                                <TableCell style={{ padding: '15px' }}>Description</TableCell>
                                <TableCell>{countries.find(c => c.country_id == params.id).description}</TableCell>
                            </TableRow>
                            <TableRow>
                                <TableCell style={{ padding: '15px' }}>Currency</TableCell>
                                <TableCell>{countries.find(c => c.country_id == params.id).currency}</TableCell>
                            </TableRow>
                            <TableRow>
                                <TableCell style={{ padding: '15px' }}>Capital</TableCell>
                                <TableCell>{countries.find(c => c.country_id == params.id).capital}</TableCell>
                            </TableRow>
                            <TableRow>
                                <TableCell style={{ padding: '15px' }}>Languages</TableCell>
                                <TableCell>{countries.find(c => c.country_id == params.id).languages}</TableCell>
                            </TableRow>
                            <TableRow>
                                <TableCell style={{ padding: '15px' }}>Flag</TableCell>
                                <TableCell>{countries.find(c => c.country_id == params.id).flag}</TableCell>
                            </TableRow>

                            <TableRow>
                                <TableCell style={{ padding: '15px' }}>Status</TableCell>
                                <TableCell>{countries.find(c => c.country_id == params.id).status === 'A' ? 'Active' : 'Blocked'}</TableCell>
                            </TableRow>
                            <TableRow>
                                <TableCell style={{ padding: '15px' }}>Created On</TableCell>
                                <TableCell>{moment(countries.find(c => c.country_id == params.id).createdAt).format('YYYY MMM DD, hh:mm:ss a ddd')}</TableCell>
                            </TableRow>
                            <TableRow>
                                <TableCell style={{ padding: '15px' }}>Last Updated On</TableCell>
                                <TableCell>{moment(countries.find(c => c.country_id == params.id).updatedAt).format('YYYY MMM DD, hh:mm:ss a ddd')}</TableCell>
                            </TableRow>
                        </Table>
                    </React.Fragment>
                    : <div className="no-matches">No matched item found !</div>}
            </div>
        )
    }

    if (path === "/library/places/city/view/:id") {
        return (
            <div className="analytics m-sm-30">
                {!props.modal &&
                    <div className="mb-sm-30">
                        <Breadcrumb
                            routeSegments={[
                                { name: "Dashboard", path: "/dashboard" },
                                { name: "Places" }
                            ]}
                        />
                    </div>
                }

                {!!cities.find(c => c.city_id == params.id) ?
                    <React.Fragment>
                        {!!cities.find(c => c.city_id == params.id).location &&
                            <GoogleMapComponent isMarkerShown defaultZoom={8} defaultCenter={JSON.parse(cities.find(c => c.city_id == params.id).location)} />}

                        <Table style={{ border: '2px solid' }}>
                            <TableRow>
                                <TableCell colspan="2" style={{ padding: '15px' }}>
                                    <FlexRow>
                                        <Button variant='outlined' onClick={() => history.push('/library/places')}>Back</Button>
                                        <ActionList del={CheckPermission("delete")} block={CheckPermission("edit")}
                                            id={cities.find(c => c.city_id == params.id).country_id} deleteEvent={deleteEventCity} blockEvent={blockEventCity}
                                            status={cities.find(c => c.city_id == params.id).status} />
                                    </FlexRow>
                                </TableCell>
                            </TableRow>
                            <TableRow>
                                <TableCell style={{ padding: '15px' }}>Name</TableCell>
                                <TableCell>{cities.find(c => c.city_id == params.id).name}</TableCell>
                            </TableRow>
                            <TableRow>
                                <TableCell style={{ padding: '15px' }}>Country</TableCell>
                                <TableCell>
                                    {countries.find(cn => cn.country_id == cities.find(c => c.city_id == params.id).country_id) ?
                                        countries.find(cn => cn.country_id == cities.find(c => c.city_id == params.id).country_id).name : null}
                                </TableCell>
                            </TableRow>
                            <TableRow>
                                <TableCell style={{ padding: '15px' }}>Description</TableCell>
                                <TableCell>{cities.find(c => c.city_id == params.id).description}</TableCell>
                            </TableRow>
                            <TableRow>
                                <TableCell style={{ padding: '15px' }}>Type Of City</TableCell>
                                <TableCell>{cities.find(c => c.city_id == params.id).type}</TableCell>
                            </TableRow>
                            <TableRow>
                                <TableCell style={{ padding: '15px' }}>Google Map Link</TableCell>
                                <TableCell><a hreh={cities.find(c => c.city_id == params.id).description}>Link</a></TableCell>
                            </TableRow>

                            <TableRow>
                                <TableCell style={{ padding: '15px' }}>Status</TableCell>
                                <TableCell>{cities.find(c => c.city_id == params.id).status === 'A' ? 'Active' : 'Blocked'}</TableCell>
                            </TableRow>
                            <TableRow>
                                <TableCell style={{ padding: '15px' }}>Created On</TableCell>
                                <TableCell>{moment(cities.find(c => c.city_id == params.id).createdAt).format('YYYY MMM DD, hh:mm:ss a ddd')}</TableCell>
                            </TableRow>
                            <TableRow>
                                <TableCell style={{ padding: '15px' }}>Last Updated On</TableCell>
                                <TableCell>{moment(cities.find(c => c.city_id == params.id).updatedAt).format('YYYY MMM DD, hh:mm:ss a ddd')}</TableCell>
                            </TableRow>
                        </Table>
                    </React.Fragment>
                    : <div className="no-matches">No matched item found !</div>
                }

            </div>
        )
    }

    return (
        <div className="analytics m-sm-30">
            {!props.modal &&
                <div className="mb-sm-30">
                    <Breadcrumb
                        routeSegments={[
                            { name: "Dashboard", path: "/dashboard" },
                            { name: "Places" }
                        ]}
                    />
                </div>
            }

            <FlexRow>
                <ButtonGroup>
                    <Button variant='outlined' onClick={makeToggleMap}>
                        <MapRounded />
                    </Button>
                </ButtonGroup>

                {
                    mainView === 'CountryView' &&

                    <Button variant='outlined' onClick={addCountryView}>
                        Add Country
                    </Button>
                }

                {
                    mainView === 'CityView' &&
                    <ButtonGroup>
                        <Button variant='outlined' onClick={addCityView}>
                            Add City
                         </Button>
                        <Button variant='outlined' onClick={viewCountries}>
                            View Countries
                        </Button>
                    </ButtonGroup>
                }

            </FlexRow>

            {
                isMapShown.visibility &&

                <GoogleMapComponent isMarkerShown defaultZoom={isMapShown.zoom}
                    defaultCenter={isMapShown.location} onClickOnMap={onClickOnMap} />

            }

            <div style={{ marginTop: `20px`, marginBottom: `20px` }}>


                {

                    mainView === 'CountryView' &&

                    <React.Fragment>

                        <MuiThemeProvider theme={getMuiThemeCustom}>
                            <TableMD
                                columns={['Name', 'Capital', 'Languages', 'Currency', {
                                    label: 'Action', options: {
                                        sort: false, filter: false
                                    }
                                }]}
                                options={{
                                    responsive: 'scrollMaxHeight',
                                    selectableRows: !props.modal
                                }}
                                title='Countries'
                                data={countries_list}
                            />
                        </MuiThemeProvider>
                    </React.Fragment>

                }


                {

                    mainView === 'CityView' &&

                    <React.Fragment>

                        <TableMD
                            columns={['Name', 'Type Of City', {
                                label: 'Action', options: {
                                    sort: false, filter: false
                                }
                            }]}
                            options={{
                                responsive: 'scrollMaxHeight',
                                selectableRows: !props.modal
                            }}
                            title={`Cities (${current.name})`}
                            data={cities_list}
                        />
                    </React.Fragment>

                }

                {isUploadOpen.open === 'open' &&
                    <Widget onCloseHandler={() => setUploadOpen({ ...isUploadOpen, open: 'closed' })} onCompleteHandler={onCompleteHandler} />
                }


                <Dialog open={DetailView.open === 'open'} fullWidth className="max-width">
                    <DialogTitle className="places-modal">
                        <FlexRow>
                            {DetailView.heading}
                            <IconButton variant='outlined' color='secondary' onClick={DetailView.closeView}>
                                <CancelOutlined />
                            </IconButton>
                        </FlexRow>
                    </DialogTitle>
                    <DialogContent>

                        {mainView === 'CityView' &&

                            <ValidatorForm onSubmit={submitCityForm}>

                                <Grid container spacing={6}>
                                    <Grid md={3} sm={12} xs={12} item>
                                        <div className='add-image-button' onClick={onOpenGallery.bind(null, 'city')}>
                                            <Icon>camera</Icon> Add Gallery
                                            </div>
                                        <Switch value={fetchRemoteImagesCity.allowed} onChange={onChangeImageFetchCity} />
                                        <span>Google images?</span>
                                    </Grid>
                                    <Grid md={9} sm={12} xs={12} item>

                                        <div className='gallery-images-list'>
                                            {
                                                !!city.images &&
                                                city.images.map(img => (
                                                    <React.Fragment>
                                                        <div className='gallery-item' style={{ background: `url(${img.indexOf('http') !== -1 ? img : process.env.REACT_APP_STATIC + img})`, backgroundSize: 'cover' }}>
                                                            <div className='gallery-remove-button' onClick={onRemoveGalleryCity.bind(null, img)}>
                                                                <Icon>close</Icon>
                                                            </div>
                                                        </div>
                                                    </React.Fragment>
                                                ))
                                            }
                                        </div>

                                    </Grid>
                                </Grid>

                                <Grid container spacing={6}>
                                    <Grid md={6} sm={12} xs={12} item>
                                        <GooglePlacesSuggest
                                            types={["(cities)"]}
                                            country={current.code}
                                            defaultValue={city.name ? city.name : null}
                                            placeholder={city.name ? city.name : 'Search city'}
                                            onSuggestSelect={onSuggestSelectCity} />
                                    </Grid>
                                    <Grid md={6} sm={12} xs={12} item>
                                        <Dropdown
                                            placeholder='Type Of City'
                                            fluid
                                            selection
                                            value={city.type ? city.type : ``}
                                            onChange={(event, { value }) => setCity({ ...city, type: value })}
                                            options={['TRAVEL POINT', 'STAY POINT'].map((m, index) => {
                                                return { key: index, text: m, value: m }
                                            })}
                                        />
                                    </Grid>
                                </Grid>
                                <Grid container spacing={6}>
                                    <Grid md={12} xs={12} item>
                                        <TextField variant='outlined' placeholder="City Description"
                                            style={{ width: '100%', ['margin-bottom']: '15px' }} inputProps={{ maxLength: 275 }}
                                            multiline rowsMax={5} onChange={(e) => setCity({ ...city, description: e.target.value })}
                                            defaultValue={city.description ? city.description : ``} />
                                        <span>* 275 characters ({city.description ? city.description.length : 0})</span>
                                    </Grid>
                                </Grid>
                                <Button
                                    variant="contained"
                                    color="primary"
                                    style={{ marginTop: '15px' }}
                                    type="submit">{DetailView.view === 'Add' ? 'Save' : 'Update'} City</Button>

                            </ValidatorForm>
                        }



                        {

                            mainView === 'CountryView' &&

                            <ValidatorForm onSubmit={submitCountryForm}>

                                <Grid container spacing={6}>
                                    <Grid md={3} sm={12} xs={12} item>
                                        <div className='add-image-button' onClick={onOpenGallery.bind(null, 'country')}>
                                            <Icon>camera</Icon> Add Gallery
                                        </div>
                                        <Switch value={fetchRemoteImagesCountry.allowed} onChange={onChangeImageFetchCountry} />
                                        <span>Google images?</span>
                                    </Grid>
                                    <Grid md={9} sm={12} xs={12} item>

                                        <div className='gallery-images-list'>
                                            {
                                                !!country.images &&
                                                country.images.map(img => (
                                                    <React.Fragment>
                                                        <div className='gallery-item' style={{ background: `url(${img.indexOf('http') !== -1 ? img : process.env.REACT_APP_STATIC + img})`, backgroundSize: 'cover' }}>
                                                            <div className='gallery-remove-button' onClick={onRemoveGalleryCountry.bind(null, img)}>
                                                                <Icon>close</Icon>
                                                            </div>
                                                        </div>
                                                    </React.Fragment>
                                                ))
                                            }
                                        </div>

                                    </Grid>
                                </Grid>


                                <Grid container spacing={6}>
                                    <Grid md={6} sm={12} xs={12} item>
                                        <GooglePlacesSuggest
                                            types={['(regions)']}
                                            placeholder={country.name ? country.name : 'Search country'}
                                            defaultValue={country.name ? country.name : null}
                                            onSuggestSelect={onSuggestSelectRegion} />
                                    </Grid>
                                    <Grid md={6} sm={12} xs={12} item>
                                        <TextValidator
                                            className="mb-16 w-100"
                                            variant="outlined"
                                            label="Currency"
                                            type="text"
                                            value={country.currency ? country.currency : ``}
                                            onChange={onChangeField.bind(null, 'country')}
                                            name="currency"
                                        />
                                    </Grid>
                                </Grid>
                                <Grid container spacing={6}>
                                    <Grid md={6} sm={12} xs={12} item>
                                        <TextValidator
                                            className="mb-16 w-100"
                                            variant="outlined"
                                            label="Capital"
                                            type="text"
                                            value={country.capital ? country.capital : ``}
                                            onChange={onChangeField.bind(null, 'country')}
                                            name="capital"
                                        />
                                    </Grid>
                                    <Grid md={6} sm={12} xs={12} item>
                                        <TextValidator
                                            className="mb-16 w-100"
                                            variant="outlined"
                                            label="Languages (Comma , seperated)"
                                            type="text"
                                            value={country.languages ? country.languages : ``}
                                            onChange={onChangeField.bind(null, 'country')}
                                            name="languages"
                                        />
                                    </Grid>
                                </Grid>
                                <Grid container spacing={6}>
                                    <Grid md={12} sm={12} xs={12} item>
                                        <TextField variant='outlined' placeholder="Country / Region Description"
                                            style={{ width: '100%', ['margin-bottom']: '15px' }}
                                            multiline rowsMax={5} onChange={(e) => setCountry({ ...country, description: e.target.value })}
                                            defaultValue={country.description ? country.description : ``} />
                                    </Grid>
                                </Grid>
                                <Button
                                    variant="contained"
                                    color="primary"
                                    type="submit">{DetailView.view === 'Add' ? 'Save' : 'Update'} Country</Button>

                            </ValidatorForm>

                        }

                    </DialogContent>
                </Dialog>
            </div>
        </div >
    );
}

export default withStyles({}, { withTheme: true })(Places);
