import { EgretLoadable } from "egret";
import { authRoles } from "../../../auth/authRoles";

const Attractions = EgretLoadable({
  loader: () => import("./Attractions.component")
});

const attractionsRoutes = [
  {
    name: 'Hotels',
    path: "/library/attractions",
    exact: true,
    component: Attractions,
    auth: authRoles.admin,
    access: 'AUTH'
  },
  {
    name: 'Hotels',
    path: "/library/attractions/view/:id",
    exact: true,
    component: Attractions,
    auth: authRoles.admin,
    access: 'AUTH'
  }
];

export default attractionsRoutes;
