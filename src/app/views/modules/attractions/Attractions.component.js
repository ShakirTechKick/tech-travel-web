import React, { useCallback, useState, useEffect } from "react";
import { ValidatorForm, TextValidator, SelectValidator } from "react-material-ui-form-validator";
import { ButtonGroup, Button, MenuItem, Grid, Switch, Icon, Table, TableRow, TableCell, TextField } from "@material-ui/core";
import MapRounded from '@material-ui/icons/MapRounded';
import { Breadcrumb } from "egret";
import { withStyles } from "@material-ui/styles";
import { useDispatch, useSelector } from "react-redux";
import TableMD from "mui-datatables";
import ActionList from "app/helpers/ActionList";
import { Dropdown, Label, Divider } from "semantic-ui-react";
import { ToastsStore } from "react-toasts";
import * as AttractionActions from 'app/redux/actions/modules/Attractions.action';
import * as PlacesActions from 'app/redux/actions/modules/Places.action';
import FlexRow from "app/helpers/FlexRow";
import history from '../../../../history';
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
import 'react-google-places-autocomplete/dist/assets/index.css';
import GoogleMapComponent from "app/helpers/GoogleMapComponent";
import GooglePlacesSuggest from "react-geosuggest";
import { geocodeByPlaceId } from "react-google-places-autocomplete";
import Widget from 'app/helpers/TravUploadWidget';
import moment from "moment";
import { Request } from "app/config/Request";

const getMuiThemeCustom = () => createMuiTheme({
    overrides: {
        MUIDataTableHeadCell: {
            root: {
                '&:nth-child(5)': {
                    width: '100px'
                }
            }
        }
    }
})

const Attractions = (props) => {

    const dispatch = useDispatch();

    const { user: auth_user } = useSelector(state => state.user);

    const [current, setCurrent] = useState({
        city: null, country: null
    });

    const [isMapShown, setToggleMap] = useState({
        visibility: false, location: { lat: 7.873053999999999, lng: 80.77179699999999 }, zoom: 10
    });

    const static_attraction = {
        company_id: auth_user.data.company_id, images: []
    };

    const [attraction, setAttraction] = useState(static_attraction);

    const [fetchRemoteImages, setFetchRemoteImages] = useState({
        allowed: false, photos: []
    });


    const [isUploadOpen, setUploadOpen] = useState(false);
    const onCompleteHandler = (id, name, response) => {
        if (response.success) {
            if (attraction.images) {
                setAttraction({ ...attraction, images: [...attraction.images, `${response.path}/${name}`] });
            } else {
                setAttraction({ ...attraction, images: [`${response.path}/${name}`] });
            }
        }
    }

    const { countries, cities } = useSelector(state => state.places);
    const { list } = useSelector(state => state.attractions);

    const [view, setView] = useState('ManageView');

    useEffect(() => {
        dispatch(AttractionActions.fetchAttractions(auth_user.data.company_id));
        dispatch(PlacesActions.fetchCountries(auth_user.data.company_id));
        dispatch(PlacesActions.fetchCities(auth_user.data.company_id));
    }, [])

    const deleteEvent = (id) => {
        dispatch(AttractionActions.removeAttraction({ attraction_id: id }))
    };

    const editEvent = (id) => {
        const a = list.find(a => a.attraction_id === id);
        setAttraction({ ...a, images: a.images ? a.images.split(',').filter(i => !!i) : [] });

        geocodeByPlaceId(a.place_id).then(result => {

            const location = { lat: result[0].geometry.location.lat(), lng: result[0].geometry.location.lng() };

            setToggleMap({ ...isMapShown, location, zoom: 16 });

        }).catch(error => null);

        setView('EditView');
    };

    const blockEvent = (id) => {
        dispatch(AttractionActions.statusAttraction({ attraction_id: id }))
    };

    const CheckPermission = (type) => {

        var module_exists = false;
        var action_permission = false;

        auth_user.permissions.forEach(module => {
            if (Object.keys(module)[0] === "Attractions") {
                module_exists = true;
                if (module['Attractions'][type] === "ALLOWED") {
                    action_permission = true;
                }
            }
        });

        if (module_exists && action_permission) {
            return true;
        }

        return false;

    }


    const onChangeField = (e) => {
        setAttraction({ ...attraction, [e.target.name]: e.target.value });
    }

    const saveAttractionForm = () => {

        setFetchRemoteImages({ allowed: false, name: null, photos: [] });

        if (attraction.country_id && attraction.city_id) {
            if (view === 'AddView') {
                dispatch(AttractionActions.addAttraction({ ...attraction }));
            } else if (view == 'EditView') {
                dispatch(AttractionActions.updateAttraction({ ...attraction }));
            }

            setAttraction({ ...static_attraction });
            setView('ManageView')
        } else {
            ToastsStore.error('Country and city are required !', 2000);
        }
    }



    const attractions_list = list.sort((a, b) => b.attraction_id - a.attraction_id).map(a => ([
        a.name, `${countries.find(c => c.country_id === a.country_id) ? countries.find(c => c.country_id === a.country_id).name : ''} : ${cities.find(c => c.city_id === a.city_id) ? cities.find(c => c.city_id === a.city_id).name : ''}`,
        a.rates_applied, <ActionList del={CheckPermission("delete")} edit={CheckPermission("edit")} block={CheckPermission("edit")}
            id={a.attraction_id} deleteEvent={deleteEvent} blockEvent={blockEvent}
            editEvent={editEvent} status={a.status} view={`/library/attractions/view/${a.attraction_id}`} />
    ]))

    const toggleView = () => {

        setAttraction({ ...static_attraction });

        view === 'ManageView' ? setView('AddView') : setView('ManageView');
    }

    const makeToggleMap = () => {
        setToggleMap({ ...isMapShown, visibility: !isMapShown.visibility });
    }

    const onOpenGallery = () => {
        setUploadOpen(true);
    }
    const onRemoveGallery = (img) => {

        const images = attraction.images.filter(i => i !== img);

        setAttraction({ ...attraction, images: [...images] });

        Request.post(`utilities/fileuploader/destroy_resources`, { path: img });
    }



    const onSelectEstablishment = (establishment) => {
        if (establishment) {
            const { location, gmaps: { name, place_id, formatted_address,
                international_phone_number, photos, website, url } } = establishment;


            setToggleMap({ ...isMapShown, location, zoom: 16 });

            setAttraction({
                ...attraction, name, place_id, url, website, location: JSON.stringify(location),
                address: formatted_address, phone_number: international_phone_number
            });

            setFetchRemoteImages({ ...fetchRemoteImages, photos });

            if (fetchRemoteImages.allowed) {

                const images = [];
                var counter = 0;

                ToastsStore.info('Fetching images, please wait...!', 2000);

                const caption = `c${auth_user.data.company_id}-${name.replace(/\s/g, String.apply())}`;

                const pictures = photos.filter((p, index) => index < 5);

                pictures.forEach(photo => {
                    Request.post(`utilities/fileuploader/download`, { url: photo.getUrl(), filepath: `google-${caption}-${counter++}-attraction-photo.jpg` })
                        .then(result => {
                            if (result.data.status === 'success') {
                                images.push(`/${result.data.path.replace('public/uploads/', '')}`);

                                if (counter == pictures.length) {
                                    setAttraction({
                                        ...attraction, name, place_id, url, website, images,
                                        address: formatted_address, phone_number: international_phone_number
                                    });
                                    ToastsStore.info('Successfully images are fetched !', 2000);
                                }
                            }
                        })
                        .catch(error => null);

                })
            }


        }



    }



    const onChangeImageFetch = e => {

        setFetchRemoteImages({ ...fetchRemoteImages, allowed: e.target.checked });

        if (e.target.checked) {
            if (fetchRemoteImages.photos && fetchRemoteImages.photos.length) {

                const photos = fetchRemoteImages.photos;

                ToastsStore.info('Fetching images, please wait...!', 2500);
                const images = [];

                var counter = 0;
                const caption = `c${auth_user.data.company_id}-${attraction.name.replace(/\s/g, String.apply())}`;

                const pictures = photos.filter((p, index) => index < 5);

                pictures.forEach(photo => {
                    Request.post(`utilities/fileuploader/download`, { url: photo.getUrl(), filepath: `google-${caption}-${counter++}-attraction-photo.jpg` })
                        .then(result => {
                            if (result.data.status === 'success') {
                                images.push(`/${result.data.path.replace('public/uploads/', '')}`);

                                if (counter == pictures.length) {
                                    setAttraction({
                                        ...attraction, images
                                    });
                                    ToastsStore.info('Successfully images are fetched !', 2000);
                                }
                            }
                        })
                        .catch(error => null);

                });

            }
        } else {

            if (attraction.images.length) {
                const googleimages = attraction.images.filter(img => img.indexOf('google') !== -1);

                if (googleimages.length) {
                    ToastsStore.info('Removing google images fetched', 2500);

                    setAttraction({ ...attraction, images: [...attraction.images.filter(img => img.indexOf('google') === -1)] });

                    googleimages.forEach(img => {
                        Request.post(`utilities/fileuploader/destroy_resources`, { path: img });
                    })
                }
            }

        }
    }


    const { path, params } = props.match ? props.match : { path: null, params: null };

    return (
        <div className="analytics m-sm-30">

            {!props.modal &&
                <div className="mb-sm-30">
                    <Breadcrumb
                        routeSegments={[
                            { name: "Dashboard", path: "/dashboard" },
                            { name: "Attractions" }
                        ]}
                    />
                </div>
            }


            {(path === '/library/attractions/view/:id') ?

                list.find(c => c.attraction_id == params.id) ?

                    <React.Fragment>
                        {!!list.find(c => c.attraction_id == params.id).location &&
                            <GoogleMapComponent isMarkerShown defaultZoom={10} defaultCenter={JSON.parse(list.find(c => c.attraction_id == params.id).location)} />}
                        <Table style={{ border: '.5px solid' }}>
                            <TableRow>
                                <TableCell colspan="2" style={{ padding: '15px' }}>
                                    <FlexRow>
                                        <Button variant='outlined' onClick={() => history.push('/library/attractions')}>Back</Button>
                                        <ActionList del={CheckPermission("delete")} block={CheckPermission("edit")}
                                            id={list.find(c => c.attraction_id == params.id).attraction_id} deleteEvent={deleteEvent} blockEvent={blockEvent}
                                            status={list.find(c => c.attraction_id == params.id).status} />
                                    </FlexRow>
                                </TableCell>
                            </TableRow>
                            <TableRow>
                                <TableCell style={{ padding: '15px' }}>Name</TableCell>
                                <TableCell>{list.find(c => c.attraction_id == params.id).name}</TableCell>
                            </TableRow>
                            <TableRow>
                                <TableCell style={{ padding: '15px' }}>Place ID</TableCell>
                                <TableCell>{list.find(c => c.attraction_id == params.id).place_id}</TableCell>
                            </TableRow>
                            <TableRow>
                                <TableCell style={{ padding: '15px' }}>Google Map Link</TableCell>
                                <TableCell><a href={list.find(c => c.attraction_id == params.id).url}>Link</a></TableCell>
                            </TableRow>
                            <TableRow>
                                <TableCell style={{ padding: '15px' }}>Phone Number</TableCell>
                                <TableCell>{list.find(c => c.attraction_id == params.id).phone_number}</TableCell>
                            </TableRow>
                            <TableRow>
                                <TableCell style={{ padding: '15px' }}>Rate Applied</TableCell>
                                <TableCell>{list.find(c => c.attraction_id == params.id).rates_applied}</TableCell>
                            </TableRow>
                            <TableRow>
                                <TableCell style={{ padding: '15px' }}>Rate Full Local</TableCell>
                                <TableCell>{list.find(c => c.attraction_id == params.id).rate_full_local}</TableCell>
                            </TableRow>
                            <TableRow>
                                <TableCell style={{ padding: '15px' }}>Rate Half Local</TableCell>
                                <TableCell>{list.find(c => c.attraction_id == params.id).rate_half_local}</TableCell>
                            </TableRow>
                            <TableRow>
                                <TableCell style={{ padding: '15px' }}>Rate Full Foriegn</TableCell>
                                <TableCell>{list.find(c => c.attraction_id == params.id).rate_full_foriegn}</TableCell>
                            </TableRow>
                            <TableRow>
                                <TableCell style={{ padding: '15px' }}>Rate Half Foriegn</TableCell>
                                <TableCell>{list.find(c => c.attraction_id == params.id).rate_half_foriegn}</TableCell>
                            </TableRow>
                            <TableRow>
                                <TableCell style={{ padding: '15px' }}>Description</TableCell>
                                <TableCell>{list.find(c => c.attraction_id == params.id).description}</TableCell>
                            </TableRow>
                            <TableRow>
                                <TableCell style={{ padding: '15px' }}>Status</TableCell>
                                <TableCell>{list.find(c => c.attraction_id == params.id).status === 'A' ? 'Active' : 'Blocked'}</TableCell>
                            </TableRow>
                            <TableRow>
                                <TableCell style={{ padding: '15px' }}>Created On</TableCell>
                                <TableCell>{moment(list.find(c => c.attraction_id == params.id).createdAt).format('YYYY MMM DD, hh:mm:ss a ddd')}</TableCell>
                            </TableRow>
                            <TableRow>
                                <TableCell style={{ padding: '15px' }}>Last Updated On</TableCell>
                                <TableCell>{moment(list.find(c => c.attraction_id == params.id).updatedAt).format('YYYY MMM DD, hh:mm:ss a ddd')}</TableCell>
                            </TableRow>
                        </Table>
                    </React.Fragment> : <div className="no-matches">No matched item found !</div> :


                <React.Fragment>
                    <FlexRow>
                        <ButtonGroup>
                            <Button variant='outlined' onClick={makeToggleMap}>
                                <MapRounded />
                            </Button>
                        </ButtonGroup>
                        <Button variant='outlined' onClick={toggleView}>
                            {(view === 'AddView' || view === 'EditView') ? 'All' : 'Add'} Attractions
                        </Button>
                    </FlexRow>

                    {
                        isMapShown.visibility &&

                        <GoogleMapComponent isMarkerShown defaultZoom={isMapShown.zoom} defaultCenter={isMapShown.location} />

                    }

                    <div style={{ marginTop: `20px`, marginBottom: `20px` }}>
                        <React.Fragment>

                            {(view === 'AddView' || view === 'EditView') &&
                                <ValidatorForm onSubmit={saveAttractionForm}>

                                    <Grid container spacing={6}>
                                        <Grid md={3} sm={12} xs={12} item>
                                            <div className='add-image-button' onClick={onOpenGallery}>
                                                <Icon>camera</Icon> Add Gallery
                                    </div>
                                            <Switch value={fetchRemoteImages.allowed} onChange={onChangeImageFetch} />
                                            <span>Google images?</span>
                                        </Grid>
                                        <Grid md={9} sm={12} xs={12} item>

                                            <div className='gallery-images-list'>
                                                {
                                                    (!!attraction.images && !!attraction.images.length) &&
                                                    attraction.images.map(img => (
                                                        <React.Fragment>
                                                            <div className='gallery-item' style={{ background: `url(${process.env.REACT_APP_STATIC + img})`, backgroundSize: 'cover' }}>
                                                                <div className='gallery-remove-button' onClick={onRemoveGallery.bind(null, img)}>
                                                                    <Icon>close</Icon>
                                                                </div>
                                                            </div>
                                                        </React.Fragment>
                                                    ))
                                                }
                                            </div>

                                        </Grid>
                                    </Grid>


                                    <Grid container spacing={6}>
                                        <Grid md={4} sm={12} xs={12} item>
                                            <Dropdown
                                                placeholder='Country'
                                                fluid clearable selection search
                                                value={attraction.country_id ? attraction.country_id : ``}
                                                onChange={(event, { value }) => setAttraction({ ...attraction, country_id: value })}
                                                options={countries.map((m, index) => {
                                                    return { key: index, text: m.name, value: m.country_id }
                                                })}
                                            />
                                        </Grid>
                                        <Grid md={4} sm={12} xs={12} item>
                                            <Dropdown
                                                placeholder='City'
                                                fluid clearable selection search
                                                value={attraction.city_id ? attraction.city_id : ``}
                                                onChange={(event, { value }) => setAttraction({ ...attraction, city_id: value })}
                                                options={attraction.country_id ? cities.filter(c => c.country_id === attraction.country_id).map((m, index) => {
                                                    return { key: index, text: m.name, value: m.city_id }
                                                }) : []}
                                            />
                                        </Grid>
                                        <Grid md={4} sm={12} xs={12} item>
                                            <GooglePlacesSuggest
                                                country={countries.find(c => c.country_id === attraction.country_id) ?
                                                    countries.find(c => c.country_id === attraction.country_id).code : 'LK'}
                                                types={['establishment']}
                                                placeholder={attraction.name ? attraction.name : 'Search Attraction'}
                                                disabled={!(attraction.country_id || attraction.city_id)}
                                                onClick={() => (
                                                    (attraction.country_id || attraction.city_id) ? null :
                                                        window.alert("Please select country and city first !")
                                                )}
                                                onSuggestSelect={onSelectEstablishment} />
                                        </Grid>
                                    </Grid>


                                    <Grid container spacing={6}>
                                        <Grid md={6} sm={12} xs={12} item>
                                            <TextValidator
                                                className="mb-16 w-100"
                                                variant="outlined"
                                                label="Phone Number"
                                                type="text"
                                                value={attraction.phone_number ? attraction.phone_number : ``}
                                                onChange={onChangeField}
                                                name="phone_number"
                                            />
                                        </Grid>
                                        <Grid md={6} sm={12} xs={12} item>
                                            <TextValidator
                                                className="mb-16 w-100"
                                                variant="outlined"
                                                label="Website"
                                                type="text"
                                                value={attraction.website ? attraction.website : ``}
                                                onChange={onChangeField}
                                                name="website"
                                            />
                                        </Grid>
                                    </Grid>
                                    <Grid container spacing={6}>

                                        <Grid md={12} sm={12} xs={12} item>

                                            No <Switch name='rates_applied' defaultChecked={attraction.rates_applied ?
                                                attraction.rates_applied === 'YES' ? true : false : false}
                                                onChange={(e) => setAttraction({ ...attraction, rates_applied: e.target.checked ? 'YES' : 'NO' })} /> Yes,  Rates Applied ?
                                </Grid>


                                        {(typeof attraction.rates_applied !== 'undefined' && attraction.rates_applied === 'YES') &&
                                            < React.Fragment >

                                                <Grid md={6} sm={12} xs={12} item>
                                                    <TextValidator
                                                        className="mb-16 w-100"
                                                        variant="outlined"
                                                        label="Rate For Local (Half)"
                                                        type="text"
                                                        value={attraction.rate_half_local ? attraction.rate_half_local : ``}
                                                        onChange={onChangeField}
                                                        name="rate_half_local"
                                                    />
                                                </Grid>

                                                <Grid md={6} sm={12} xs={12} item>
                                                    <TextValidator
                                                        className="mb-16 w-100"
                                                        variant="outlined"
                                                        label="Rate For Local (Full)"
                                                        type="text"
                                                        value={attraction.rate_full_local ? attraction.rate_full_local : ``}
                                                        onChange={onChangeField}
                                                        name="rate_full_local"
                                                    />
                                                </Grid>

                                                <Grid md={6} sm={12} xs={12} item>
                                                    <TextValidator
                                                        className="mb-16 w-100"
                                                        variant="outlined"
                                                        label="Rate For Foriegn (Half)"
                                                        type="text"
                                                        value={attraction.rate_half_foriegn ? attraction.rate_half_foriegn : ``}
                                                        onChange={onChangeField}
                                                        name="rate_half_foriegn"
                                                    />
                                                </Grid>

                                                <Grid md={6} sm={12} xs={12} item>
                                                    <TextValidator
                                                        className="mb-16 w-100"
                                                        variant="outlined"
                                                        label="Rate For Foriegn (Full)"
                                                        type="text"
                                                        value={attraction.rate_full_foriegn ? attraction.rate_full_foriegn : ``}
                                                        onChange={onChangeField}
                                                        name="rate_full_foriegn"
                                                    />
                                                </Grid>

                                            </React.Fragment>}

                                    </Grid>

                                    <Grid container spacing={6}>
                                        <Grid md={12} sm={12} xs={12} item>
                                            <TextField variant='outlined' placeholder="Attraction Description"
                                                style={{ width: '100%', ['margin-bottom']: '15px' }} inputProps={{ maxLength: 275 }}
                                                multiline rowsMax={5} onChange={(e) => setAttraction({ ...attraction, description: e.target.value })}
                                                defaultValue={attraction.description ? attraction.description : ``} />
                                            <span>* 275 characters ({attraction.description ? attraction.description.length : 0})</span>
                                        </Grid>
                                    </Grid>

                                    <Divider horizontal clearing />

                                    <Button
                                        variant="contained"
                                        color="primary"
                                        type="submit">{view === 'AddView' ? 'Save' : 'Update'} Attraction</Button>

                                </ValidatorForm>
                            }
                            {view === 'ManageView' &&
                                <TableMD
                                    columns={['Name', 'Country | City', 'Is Rates Applied', {
                                        label: 'Action', options: {
                                            sort: false, filter: false
                                        }
                                    }]}
                                    options={{
                                        responsive: 'scrollMaxHeight',
                                        selectableRows: !props.modal
                                    }}
                                    title='Attractions'
                                    data={attractions_list}
                                />
                            }

                            {isUploadOpen &&
                                <Widget onCloseHandler={() => setUploadOpen(!isUploadOpen)} onCompleteHandler={onCompleteHandler} />
                            }
                        </React.Fragment>
                    </div>
                </React.Fragment>
            }
        </div >
    );
}

export default withStyles({}, { withTheme: true })(Attractions);
