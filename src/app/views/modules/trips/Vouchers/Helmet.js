export default () => {
    return (
        `
         <head>
                <title>Quotation Document</title>
                <style>
                 * {
                    -webkit-box-sizing: border-box;
                }
                
                *{
                    font-size: 10px !important;
                }
                svg{
                    width: 25px !important;
                    font-size: 15px !important;
                }
                div.document-container-pdf {
                    width: 795pt;
                    border: 0.25px solid rgba(128, 128, 128, 0.55);
                    background-color: rgba(255, 255, 255, 0.5);
                    padding: 15px;
                }
                
                div.flex-header {
                    display: -webkit-box;
                    display: -webkit-flex;
                    display: flex;
                      flex-direction: row;
                      -webkit-box-pack: justify; 
                      -webkit-justify-content: flex-start;
                              justify-content: flex-start;
                }
                
                div.flex-header .logo {
                    width: 150px;
                }
                
                div.document-title-header {
                    text-align: center;
                    font-size: 20px;
                    font-weight: 800;
                }
                
                div.comments-list {
                    margin-top: 25px;
                }
                
                div.comments-list .comments-header {
                    font-size: 16px;
                    font-weight: 600;
                    margin-bottom: 10px;
                }
                
                div.comments-list .comments-list-item{
                    margin-bottom: 10px;
                }
                
                div.document-footer{
                    text-align: center;
                }
                
                div.document-footer .address{
                    display: -webkit-box;
                    display: -webkit-flex;
                    display: flex;
                      flex-direction: row;
                      -webkit-box-pack: justify; 
                      -webkit-justify-content: center;
                              justify-content: center;
                }
                
                div.document-footer .address > div{
                    margin-left: 10px;
                    display: -webkit-box;
                    display: -webkit-flex;
                    display: flex;
                      flex-direction: row;
                      -webkit-box-pack: justify; 
                      -webkit-justify-content: center;
                              justify-content: center;
                }
                
                div.document-footer .address > div span{
                    margin-left: 10px;
                }

                 </style>
        </head>`
    )
}