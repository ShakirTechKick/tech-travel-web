import React from 'react';
import { RoomOutlined, PhoneInTalkOutlined, AlternateEmailOutlined, LanguageOutlined, Facebook, Instagram, Twitter } from "@material-ui/icons"
import { Divider } from "semantic-ui-react";
import { Table, TableHead, TableRow, TableCell, TableBody } from "@material-ui/core";

export default ({ trip, clients, itinerary, content, company }) => {
    return (
        <div className="document-container-pdf" style={{ padding: '15px' }}>

            <table style={{ width: '100%' }}>
                <tr>
                    <td>
                        <img src={`${process.env.REACT_APP_STATIC}${company.logo}`} style={{ width: 150 }} />
                    </td>
                    <td align="right">
                        {
                            clients.find(c => c.client_id === trip.client_id) &&
                            <div style={{ textAlign: 'right' }}>
                                <div>{clients.find(c => c.client_id === trip.client_id).name}</div>
                                <div>{clients.find(c => c.client_id === trip.client_id).country}</div>
                                <div>{clients.find(c => c.client_id === trip.client_id).email}</div>
                                <div>{clients.find(c => c.client_id === trip.client_id).phone_number}</div>
                            </div>
                        }
                    </td>
                </tr>
            </table>

            <Divider clearing horizontal section >
                <div className="document-title-header" style={{ width: '100%', textAlign: 'center', marginBottom: '15px', marginTop: '15px', fontWeight: 'bold' }}>
                    {itinerary ? `${itinerary.name} Quotation` : 'Tour Quotation'}
                </div>
            </Divider>


            <table style={{ width: '100%', border: '1px solid' }}>
                <thead>
                    <tr>
                        <td colSpan={2}>Summary</td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>
                            {itinerary ? `${itinerary.name} Tour Cost` : 'Tour Package Cost'}
                        </td>
                        <td align="right">
                            {content ? content.costing ? (itinerary.currency + content.costing.grand_package_cost)
                                : 'Cost not defined yet' : 'Cost not defined yet'}
                        </td>
                    </tr>
                </tbody>
            </table>

            <div className="comments-list" style={{ margin: '15px 0' }}>
                <div className="comments-header" style={{ fontWeight: 'bold' }}>Inclusions</div>
                {
                    content.inclusions.map((item, key) => (
                        <div className="comments-list-item">{item.text}</div>
                    ))
                }
            </div>

            <div className="comments-list" style={{ margin: '15px 0' }}>
                <div className="comments-header" style={{ fontWeight: 'bold' }}>Exclusions</div>
                {
                    content.exclusions.map((item, key) => (
                        <div className="comments-list-item">{item.text}</div>
                    ))
                }
            </div>

            <div className="comments-list" style={{ margin: '15px 0' }}>
                <div className="comments-header" style={{ fontWeight: 'bold' }}>Notes</div>
                {
                    content.notes.map((item, key) => (
                        <div className="comments-list-item">{item.text}</div>
                    ))
                }
            </div>

            <Divider clearing />

            <table className="document-footer" style={{ width: '100%' }}>
                <tr className="company-footer-heading">
                    <td align="center">{company ? company.name : 'MAB Technologies Pvt Ltd'}</td>
                </tr>
                <tr className="address">
                    <td align="center">
                        <span style={{ marginRight: '10px' }}>{company ? company.address : 'NewYork, USA'}</span>
                        <span style={{ marginRight: '10px' }}>{company ? company.phone_number : '+1 76321353982'}</span>
                        <span>{company ? company.email : 'support@travdynamics.com'}</span>
                    </td>
                </tr>
            </table>

        </div>

    )
}