import React, { Component, Fragment, useRef, useState, useEffect } from "react";
import { renderToString } from "react-dom/server";
import { ValidatorForm, TextValidator, SelectValidator } from "react-material-ui-form-validator";
import { Button, Table, TableHead, TableRow, TableCell, TableBody, Tabs, Tab, Typography, Box } from "@material-ui/core";
import { Breadcrumb, EgretMenu } from "egret";
import { withStyles } from "@material-ui/styles";
import { useDispatch, useSelector } from "react-redux";
import { Dropdown, Label, Divider } from "semantic-ui-react";
import { createMuiTheme, makeStyles } from '@material-ui/core/styles';
import {  PictureAsPdfOutlined, CloseOutlined, RoomOutlined, PhoneInTalkOutlined, AlternateEmailOutlined, LanguageOutlined, Facebook, Instagram, Twitter } from "@material-ui/icons";
import moment from "moment";
import Html2Canvas from 'html2canvas';
import JSPdf from 'jspdf';
import FlexRow from "app/helpers/FlexRow";

const getMuiStyles = () => createMuiTheme({
    overrides: {
        MUIDataTableHeadCell: {
            root: {
                '&:nth-child(1)': {
                    width: 100
                }
            }
        }
    }
})


const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
        maxWidth: '100%'
    },
    tabs: {
        width: '100%',
        borderBottomWidth: 0
    },
}))


const TabPanel = (props) => {
    const { children, value, index, ...other } = props;


    return (
        <Typography
            component="div"
            role="tabpanel"
            hidden={value !== index}
            id={`scrollable-prevent-tabpanel-${index}`}
            aria-labelledby={`scrollable-prevent-tab-${index}`}
            {...other}
        >
            {value === index && <Box p={3}>{children}</Box>}
        </Typography>
    );
}
const horizontalTabProps = (index) => {
    return {
        id: `scrollable-prevent-tab-${index}`,
        'aria-controls': `scrollable-prevent-tabpanel-${index}`,
    };
}


const getTotalPayable = (hotel) => {
    var total = 0;

    hotel.bookings.forEach(b => {
        b.rooms.forEach(r => {
            if (r.number_of_rooms && r.rate_per_day) {
                total += parseFloat(r.number_of_rooms) * parseFloat(r.rate_per_day);
            }
        })
    })

    return total;
}


const DocumentProvider = ({ company, h, itinerary, client }) => {
    return (
        <div id='text-block'>
            <div style={{ textAlign: 'center' }}>
                <div>
                    {company &&
                        <img src={`${process.env.REACT_APP_STATIC}${company.logo}`} style={{ width: 150 }} className="logo" />
                    }
                </div>
            </div>

            <Divider clearing horizontal section>
                <div style={{ fontWeight: 'bold', fontSize: 20 }}>
                    Reservation Voucher - {h.hotel}
                </div>
            </Divider>

            {
                !!client &&
                <FlexRow>
                    <div>
                        <div>Client: {`${client.salutation} ${client.name}`}</div>
                        <div>{itinerary.pax} Pax</div>
                    </div>
                    {!!h.bookings.length &&
                        <div>
                            {h.bookings.length} Nights
                    </div>
                    }
                </FlexRow>
            }

            <table border="1" style={{ width: '100%' }} cellPadding="10">
                <thead>
                    <th>Date</th><th>Standard</th>
                    <th>Category</th><th>Meal Types</th>
                    <th>Rooms</th><th>Price</th>
                </thead>
                <tbody>
                    {
                        h.bookings.map((booking, key) => {

                            console.log(h, booking)

                            return (<tr key={KeyframeEffect}>
                                <td>{moment(booking.datestamp).format('MMM DD, YYYY')}</td>
                                <td align='center'>
                                    {
                                        booking.rooms.map((room, key) => (
                                            <div key={key}>{room.category}</div>
                                        ))
                                    }
                                </td>
                                <td align='center'>
                                    {
                                        booking.rooms.map((room, key) => (
                                            <div key={key}>{room.type_room}</div>
                                        ))
                                    }
                                </td>
                                <td align='center'>
                                    {
                                        booking.rooms.map((room, key) => (
                                            <div key={key}>{room.type_meal}</div>
                                        ))
                                    }
                                </td>
                                <td align='center'>
                                    {
                                        booking.rooms.map((room, key) => (
                                            <div key={key}>{room.number_of_rooms}</div>
                                        ))
                                    }
                                </td>
                                <td align='right'>
                                    {
                                        booking.rooms.map((room, key) => (
                                            <div key={key}>{room.number_of_rooms * room.rate_per_day}</div>
                                        ))
                                    }
                                </td>
                            </tr>)
                        })
                    }
                </tbody>
                <tfoot>
                    <div>Total Amount Payable: <span style={{ fontWeight: 'bold' }}>{itinerary ? itinerary.currency : 'USD '}</span> {getTotalPayable(h)}</div>
                </tfoot>
            </table>

            <Divider clearing />

            <div className="document-footer">
                <div className="company-footer-heading">
                    {company ? company.name : 'MAB Technologies Pvt Ltd'}
                </div>
                <div className="address">
                    <div><RoomOutlined /> <span>{company ? company.address : 'NewYork, USA'}</span></div>
                    <div><PhoneInTalkOutlined /> <span>{company ? company.phone_number : '+1 76321353982'}</span></div>
                    <div><AlternateEmailOutlined /> <span>{company ? company.email : 'support@travdynamics.com'}</span></div>
                </div>
                <div className="social-media-icon">
                    <a href={company ? company.web : 'travdynamics.com'} target="_blank"><LanguageOutlined style={{ color: 'rgba(59,89,152,1)' }} /></a>
                    <a href={company ? company.facebook : 'facebook.com'} target="_blank"><Facebook style={{ color: 'rgba(59,89,152,1)' }} /></a>
                    <a href={company ? company.linkedin : 'linkedin.com'} target="_blank"><Instagram style={{ color: 'rgba(193,53,132, 1)' }} /></a>
                    <a href={company ? company.Instagram : 'instagram'} target="_blank"><Twitter style={{ color: 'rgba(29,161,242, 1)' }} /></a>
                </div>
            </div>
        </div>
    )
}

const Vouchers = ({ trip, itineraries, clients, users, companies, closeView }) => {

    const dispatch = useDispatch();

    const { user: auth_user } = useSelector(state => state.user);

    const [TabCurrent, setValueTab] = React.useState(0);
    const onChangeTab = (event, index) => {
        setValueTab(index);
    }

    const itinerary = itineraries.find(i => i.itinerary_id === trip.itinerary_id) ?
        itineraries.find(i => i.itinerary_id === trip.itinerary_id) : null;

    const content = itinerary ? JSON.parse(itinerary.content) : null;

    const company = companies.find(c => c.company_id === trip.company_id) ?
        companies.find(c => c.company_id === trip.company_id) : null;

    const client = clients.find(c => c.client_id === trip.client_id) ?
        clients.find(c => c.client_id === trip.client_id) : null;

    var hotels = [];

    content.days.forEach((day) => {

        if (day.hotel.selection) {
            if (!!hotels.find(h => h.hotel === day.hotel.selection.name)) {

                var h = hotels.find(h => h.hotel === day.hotel.selection.name);
                h.bookings.push({ rooms: h.rooms, datestamp: day.datestamp });

                hotels = [h, ...hotels.filter(r => r.hotel === h.hotel)];

            } else {
                hotels.push({ hotel: day.hotel.selection.name, bookings: [{ rooms: day.hotel.rooms, datestamp: day.datestamp }] });
            }
        }

    })

    const exportPDF = () => {

        Html2Canvas(document.getElementById(`scrollable-prevent-tabpanel-${TabCurrent}`))
            .then(canvas => {
                const data = canvas.toDataURL('image/png');
                const pdf = new JSPdf();
                pdf.addImage(data, 'JPEG', 0, 0);
                pdf.save("hotel-voucher.pdf");
            }).catch(error => console.log(error))
    }


   

    return (
        <div className="analytics m-sm-30">
            <div style={{ display: 'flex', justifyContent: 'space-between', flexWrap: 'wrap' }}>
                <div className="action-bar">
                    <Button variant='outlined' onClick={exportPDF}><PictureAsPdfOutlined /> Export PDF</Button>
                    <Button variant='outlined' onClick={closeView}><CloseOutlined /> Close Back</Button>
                </div>

                <div className="document-container-pdf">


                    <React.Fragment>
                        <Tabs
                            value={TabCurrent}
                            onChange={onChangeTab} variant="scrollable" indicatorColor="secondary"
                            textColor="secondary" aria-label="Horizontal Itineray Builder Final Component Tabs"
                        >
                            {
                                hotels.map((h, key) => (
                                    <Tab label={h.hotel} {...horizontalTabProps(key)} />
                                ))
                            }

                        </Tabs>

                        {
                            hotels.map((h, key) => (
                                <TabPanel value={TabCurrent} index={key}>
                                    <DocumentProvider {...{ itinerary, client, h, company }} />
                                </TabPanel>
                            ))
                        }
                    </React.Fragment>

                </div>
            </div>
        </div>
    );
}

export default withStyles({}, { withTheme: true })(Vouchers);
