import { EgretLoadable } from "egret";
import { authRoles } from "../../../auth/authRoles";

const Vouchers = EgretLoadable({
    loader: () => import("./Vouchers.component")
});

const vouchersRoutes = [
    {
        name: "Vouchers",
        path: "/tour/hotel/vouchers",
        exact: true,
        component: Vouchers,
        auth: authRoles.admin,
        access: 'AUTH'
    }
];

export default vouchersRoutes;
