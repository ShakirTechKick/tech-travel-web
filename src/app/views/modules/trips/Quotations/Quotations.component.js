import React, { Component, Fragment, useRef, useState, useEffect } from "react";
import { renderToString } from "react-dom/server";
import { ValidatorForm, TextValidator, SelectValidator } from "react-material-ui-form-validator";
import { Button, Table, TableHead, TableRow, TableCell, TableBody } from "@material-ui/core";
import { Breadcrumb, EgretMenu } from "egret";
import { withStyles } from "@material-ui/styles";
import { useDispatch, useSelector } from "react-redux";
import { Dropdown, Label, Divider } from "semantic-ui-react";
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
import { ReceiptOutlined, CommuteOutlined, InsertDriveFileOutlined, DescriptionOutlined, PictureAsPdfOutlined, CloseOutlined, RoomOutlined, PhoneInTalkOutlined, AlternateEmailOutlined, LanguageOutlined, Facebook, Instagram, Twitter } from "@material-ui/icons";
import HTMLView from "./HTMLView";
import { saveAs } from 'file-saver';
import { Request } from "app/config/Request";

const getMuiStyles = () => createMuiTheme({
    overrides: {
        MUIDataTableHeadCell: {
            root: {
                '&:nth-child(1)': {
                    width: 100
                }
            }
        }
    }
})

const Quotations = ({ trip, itineraries, clients, users, companies, closeView }) => {

    const dispatch = useDispatch();

    const { user: auth_user } = useSelector(state => state.user);

    const itinerary = itineraries.find(i => i.itinerary_id === trip.itinerary_id) ?
        itineraries.find(i => i.itinerary_id === trip.itinerary_id) : null;

    const content = itinerary ? JSON.parse(itinerary.content) : null;


    const company = companies.find(c => c.company_id === trip.company_id) ?
        companies.find(c => c.company_id === trip.company_id) : null;


    const exportPDF = () => {

        const html = renderToString(<HTMLView {...{ trip, itinerary, clients, content, company }} />);

        Request.post(`utilities/document/generate/pdf`, { html, document: 'quotation' })
            .then(result => Request.get(`utilities/document/download/quotation.pdf`, { responseType: 'blob' }))
            .then(result => {

                const pdf = new Blob([result.data], { type: 'application/pdf' });

                saveAs(pdf, 'document-quotation.pdf');
            })
    }


    return (
        <div className="analytics m-sm-30">
            <div style={{ display: 'flex', justifyContent: 'space-between', flexWrap: 'wrap' }}>
                <div className="action-bar">
                    <Button variant='outlined' onClick={exportPDF}><PictureAsPdfOutlined /> Export PDF</Button>
                    <Button variant='outlined' onClick={closeView}><CloseOutlined /> Close Back</Button>
                </div>

                <div className="document-container-pdf">

                    <div className="flex-header">
                        <div>
                            {company &&
                                <img src={`${process.env.REACT_APP_STATIC}${company.logo}`} style={{ width: 150 }} className="logo" />
                            }
                        </div>
                        <div>
                            {
                                clients.find(c => c.client_id === trip.client_id) &&
                                <div style={{ textAlign: 'right' }}>
                                    <div>{clients.find(c => c.client_id === trip.client_id).name}</div>
                                    <div>{clients.find(c => c.client_id === trip.client_id).country}</div>
                                    <div>{clients.find(c => c.client_id === trip.client_id).email}</div>
                                    <div>{clients.find(c => c.client_id === trip.client_id).phone_number}</div>
                                </div>
                            }
                        </div>
                    </div>

                    <Divider clearing horizontal section >
                        <div className="document-title-header">
                            {itinerary ? `${itinerary.name} Quotation` : 'Tour Quotation'}
                        </div>
                    </Divider>


                    <Table>
                        <TableHead>
                            <TableRow>
                                <TableCell colSpan={2}>Summary</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            <TableRow>
                                <TableCell>
                                    {itinerary ? `${itinerary.name} Tour Cost` : 'Tour Package Cost'}
                                </TableCell>
                                <TableCell align="right">
                                    {content ? content.costing ? (itinerary.currency + content.costing.grand_package_cost)
                                        : 'Cost not defined yet' : 'Cost not defined yet'}
                                </TableCell>
                            </TableRow>
                        </TableBody>
                    </Table>

                    <div className="comments-list">
                        <div className="comments-header">Inclusions</div>
                        {
                            content?.inclusions?.map((item, key) => (
                                <div className="comments-list-item">{item.text}</div>
                            ))
                        }
                    </div>

                    <div className="comments-list">
                        <div className="comments-header">Exclusions</div>
                        {
                            content?.exclusions?.map((item, key) => (
                                <div className="comments-list-item">{item.text}</div>
                            ))
                        }
                    </div>

                    <div className="comments-list">
                        <div className="comments-header">Notes</div>
                        {
                            content?.notes?.map((item, key) => (
                                <div className="comments-list-item">{item.text}</div>
                            ))
                        }
                    </div>

                    <Divider clearing />

                    <div className="document-footer">
                        <div className="company-footer-heading">
                            {company ? company.name : 'MAB Technologies Pvt Ltd'}
                        </div>
                        <div className="address">
                            <div><RoomOutlined /> <span>{company ? company.address : 'NewYork, USA'}</span></div>
                            <div><PhoneInTalkOutlined /> <span>{company ? company.phone_number : '+1 76321353982'}</span></div>
                            <div><AlternateEmailOutlined /> <span>{company ? company.email : 'support@travdynamics.com'}</span></div>
                        </div>
                        <div className="social-media-icon">
                            <a href={company ? company.web : 'travdynamics.com'} target="_blank"><LanguageOutlined style={{ color: 'rgba(59,89,152,1)' }} /></a>
                            <a href={company ? company.facebook : 'facebook.com'} target="_blank"><Facebook style={{ color: 'rgba(59,89,152,1)' }} /></a>
                            <a href={company ? company.linkedin : 'linkedin.com'} target="_blank"><Instagram style={{ color: 'rgba(193,53,132, 1)' }} /></a>
                            <a href={company ? company.Instagram : 'instagram'} target="_blank"><Twitter style={{ color: 'rgba(29,161,242, 1)' }} /></a>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    );
}

export default withStyles({}, { withTheme: true })(Quotations);
