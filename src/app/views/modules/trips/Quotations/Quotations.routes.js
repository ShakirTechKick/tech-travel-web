import { EgretLoadable } from "egret";
import { authRoles } from "../../../auth/authRoles";

const Quotations = EgretLoadable({
    loader: () => import("./Quotations.component")
});

const quotationsRoutes = [
    {
        name: "Quotations",
        path: "/tour/quotations",
        exact: true,
        component: Quotations,
        auth: authRoles.admin,
        access: 'AUTH'
    }
];

export default quotationsRoutes;
