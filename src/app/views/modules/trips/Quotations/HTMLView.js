import React from 'react';
import { RoomOutlined, PhoneInTalkOutlined, AlternateEmailOutlined, LanguageOutlined, Facebook, Instagram, Twitter } from "@material-ui/icons"
import { Divider } from "semantic-ui-react";

export default ({ trip, clients, itinerary, content, company }) => {
    return (
        <div className="document-container-pdf" style={{ padding: '15px' }}>

            <table style={{ width: '100%' }}>
                <tr>
                    <td>
                        <img src={`${process.env.REACT_APP_STATIC}${company.logo}`} style={{ width: 150 }} />
                    </td>
                    <td align="right">
                        {
                            clients.find(c => c.client_id === trip.client_id) &&
                            <div style={{ textAlign: 'right' }}>
                                <div>{clients.find(c => c.client_id === trip.client_id).name}</div>
                                <div>{clients.find(c => c.client_id === trip.client_id).country}</div>
                                <div>{clients.find(c => c.client_id === trip.client_id).email}</div>
                                <div>{clients.find(c => c.client_id === trip.client_id).phone_number}</div>
                            </div>
                        }
                    </td>
                </tr>
            </table>

            <div className="document-title-header" style={{ width: '100%', textAlign: 'center', marginBottom: '15px', marginTop: '15px', fontWeight: 'bold' }}>
                {itinerary ? `${itinerary.name} Quotation` : 'Tour Quotation'}
            </div>


            <table style={{ width: '100%' }} cellPadding='10' border='1'>
                <thead>
                    <tr>
                        <td colSpan={2}>Summary</td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>
                            {itinerary ? `${itinerary.name} Tour Cost` : 'Tour Package Cost'}
                        </td>
                        <td align="right">
                            {content ? content.costing ? (itinerary.currency + content.costing.grand_package_cost)
                                : 'Cost not defined yet' : 'Cost not defined yet'}
                        </td>
                    </tr>
                </tbody>
            </table>

            <div className="comments-list" style={{ margin: '15px 0' }}>
                <div className="comments-header" style={{ fontWeight: 'bold' }}>Inclusions</div>
                {
                    content.inclusions.map((item, key) => (
                        <div className="comments-list-item">{item.text}</div>
                    ))
                }
            </div>

            <div className="comments-list" style={{ margin: '15px 0' }}>
                <div className="comments-header" style={{ fontWeight: 'bold' }}>Exclusions</div>
                {
                    content.exclusions.map((item, key) => (
                        <div className="comments-list-item">{item.text}</div>
                    ))
                }
            </div>

            <div className="comments-list" style={{ margin: '15px 0' }}>
                <div className="comments-header" style={{ fontWeight: 'bold' }}>Notes</div>
                {
                    content.notes.map((item, key) => (
                        <div className="comments-list-item">{item.text}</div>
                    ))
                }
            </div>

            <Divider clearing />


            <table className="footer-block" style={{ width: '100%', ['border-top']: '1px solid grey' }} cellPadding='15'>
                <tr>
                    <td colSpan="3" align='center'>
                        <div style={{ textAlign: 'center', fontSize: '16px', fontWeight: 'bold' }}>
                            {company ? company.name : 'MAB Technologies Pvt Ltd'}
                        </div>
                    </td>
                </tr>
                <tr>
                    <td align='right'>
                        <div style={{ height: 15 }}><RoomOutlined style={{ width: 20, fontSize: '15px' }} /> <span>{company ? company.address : 'NewYork, USA'}</span></div>
                    </td>
                    <td align='center'>
                        <div style={{ height: 15 }}><PhoneInTalkOutlined style={{ width: 20, fontSize: '15px' }} /> <span>{company ? company.phone_number : '+1 76321353982'}</span></div>
                    </td>
                    <td align='left'>
                        <div style={{ height: 15 }}><AlternateEmailOutlined style={{ width: 20, fontSize: '15px' }} /> <span>{company ? company.email : 'support@travdynamics.com'}</span></div>
                    </td>
                </tr>
                <tr>
                    <td colSpan="3" align='center'>
                        <div>
                            <a href={company ? company.web : 'travdynamics.com'} target="_blank"><LanguageOutlined style={{ color: 'rgba(59,89,152,1)', width: 20, fontSize: '15px' }} /></a>
                            <a href={company ? company.facebook : 'facebook.com'} target="_blank"><Facebook style={{ color: 'rgba(59,89,152,1)', width: 20, fontSize: '15px' }} /></a>
                            <a href={company ? company.linkedin : 'linkedin.com'} target="_blank"><Instagram style={{ color: 'rgba(193,53,132, 1)', width: 20, fontSize: '15px' }} /></a>
                            <a href={company ? company.Instagram : 'instagram'} target="_blank"><Twitter style={{ color: 'rgba(29,161,242, 1)', width: 20, fontSize: '15px' }} /></a>
                        </div>
                    </td>
                </tr>
            </table>


        </div>

    )
}