import React from 'react';
import { RoomOutlined, PhoneInTalkOutlined, AlternateEmailOutlined, LanguageOutlined, Facebook, Instagram, Twitter } from "@material-ui/icons"
import { Divider } from "semantic-ui-react";
import { Table, TableHead, TableRow, TableCell, TableBody } from "@material-ui/core";
import moment from 'moment';

export default ({ trip, clients, itinerary, content, company }) => {
    return (
        <div className="document-container-pdf" style={{ padding: '15px' }}>

            <div className="document-container-pdf">

                <div style={{ textAlign: 'center' }}>
                    <div>
                        {company &&
                            <img src={`${process.env.REACT_APP_STATIC}${company.logo}`} style={{ width: 150 }} className="logo" />
                        }
                    </div>
                </div>

                <div style={{
                    fontWeight: 'bold', fontSize: 20,
                    textAlign: 'center', width: '100%', margin: 'auto'
                }}>Driver Log</div>

                <table border='1' cellPadding='10' style={{ width: '100%', margin: '25px auto' }}>
                    <tbody>
                        <tr>
                            <td style={{ fontWeight: 'bold', padding: 15 }}>
                                {itinerary ? `${itinerary.name} Tour` : 'Tour'}
                            </td>
                        </tr>
                    </tbody>
                </table>


                <table border='1' cellPadding='10' style={{ width: '100%', margin: '25px auto' }}>
                    <tbody>
                        {
                            content.days.map((day, index) => (
                                <tr>
                                    <td>
                                        Day {index + 1} {!!day.datestamp && `(${moment(day.datestamp).format('MMM DD, YYYY')})`}
                                    </td>
                                    <td>
                                        {
                                            day.destinations.map((destination, index) => (
                                                <div>{`(${index + 1}) ` + destination.name}</div>
                                            ))
                                        }
                                    </td>
                                </tr>
                            ))
                        }
                    </tbody>
                </table>


                <table className="footer-block" style={{ width: '100%', ['border-top']: '1px solid grey' }} cellPadding='15'>
                    <tr>
                        <td colSpan="3" align='center'>
                            <div style={{ textAlign: 'center', fontSize: '16px', fontWeight: 'bold' }}>
                                {company ? company.name : 'MAB Technologies Pvt Ltd'}
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td align='right'>
                            <div style={{ height: 15 }}><RoomOutlined style={{ width: 20, fontSize: '15px' }} /> <span>{company ? company.address : 'NewYork, USA'}</span></div>
                        </td>
                        <td align='center'>
                            <div style={{ height: 15 }}><PhoneInTalkOutlined style={{ width: 20, fontSize: '15px' }} /> <span>{company ? company.phone_number : '+1 76321353982'}</span></div>
                        </td>
                        <td align='left'>
                            <div style={{ height: 15 }}><AlternateEmailOutlined style={{ width: 20, fontSize: '15px' }} /> <span>{company ? company.email : 'support@travdynamics.com'}</span></div>
                        </td>
                    </tr>
                    <tr>
                        <td colSpan="3" align='center'>
                            <div>
                                <a href={company ? company.web : 'travdynamics.com'} target="_blank"><LanguageOutlined style={{ color: 'rgba(59,89,152,1)', width: 20, fontSize: '15px' }} /></a>
                                <a href={company ? company.facebook : 'facebook.com'} target="_blank"><Facebook style={{ color: 'rgba(59,89,152,1)', width: 20, fontSize: '15px' }} /></a>
                                <a href={company ? company.linkedin : 'linkedin.com'} target="_blank"><Instagram style={{ color: 'rgba(193,53,132, 1)', width: 20, fontSize: '15px' }} /></a>
                                <a href={company ? company.Instagram : 'instagram'} target="_blank"><Twitter style={{ color: 'rgba(29,161,242, 1)', width: 20, fontSize: '15px' }} /></a>
                            </div>
                        </td>
                    </tr>
                </table>

            </div>

        </div>

    )
}