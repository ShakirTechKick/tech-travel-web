import React, { Component, Fragment, useRef, useState, useEffect } from "react";
import { renderToString } from "react-dom/server";
import { ValidatorForm, TextValidator, SelectValidator } from "react-material-ui-form-validator";
import { Button, Table, TableHead, TableRow, TableCell, TableBody } from "@material-ui/core";
import { Breadcrumb, EgretMenu } from "egret";
import { withStyles } from "@material-ui/styles";
import { useDispatch, useSelector } from "react-redux";
import { Dropdown, Label, Divider } from "semantic-ui-react";
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
import { PictureAsPdfOutlined, CloseOutlined, RoomOutlined, PhoneInTalkOutlined, AlternateEmailOutlined, LanguageOutlined, Facebook, Instagram, Twitter } from "@material-ui/icons";
import HTMLView from "./HTMLView";
import { saveAs } from 'file-saver';
import moment from "moment";

const getMuiStyles = () => createMuiTheme({
    overrides: {
        MUIDataTableHeadCell: {
            root: {
                '&:nth-child(1)': {
                    width: 100
                }
            }
        }
    }
})

const Quotations = ({ trip, itineraries, clients, users, companies, closeView }) => {

    const dispatch = useDispatch();

    const { user: auth_user } = useSelector(state => state.user);

    const itinerary = itineraries.find(i => i.itinerary_id === trip.itinerary_id) ?
        itineraries.find(i => i.itinerary_id === trip.itinerary_id) : null;

    const content = itinerary ? JSON.parse(itinerary.content) : null;


    const company = companies.find(c => c.company_id === trip.company_id) ?
        companies.find(c => c.company_id === trip.company_id) : null;


    const exportPDF = () => {

        const html = renderToString(<HTMLView {...{ trip, itinerary, clients, content, company }} />);

        Request.post(`utilities/document/generate/pdf`, { html, document: 'driver-log' })
            .then(result => Request.get(`utilities/document/download/driver-log.pdf`, { responseType: 'blob' }))
            .then(result => {

                const pdf = new Blob([result.data], { type: 'application/pdf' });

                saveAs(pdf, 'driver-log.pdf');
            })
    }

    return (
        <div className="analytics m-sm-30">
            <div style={{ display: 'flex', justifyContent: 'space-between', flexWrap: 'wrap' }}>
                <div className="action-bar">
                    <Button variant='outlined' onClick={exportPDF}><PictureAsPdfOutlined /> Export PDF</Button>
                    <Button variant='outlined' onClick={closeView}><CloseOutlined /> Close Back</Button>
                </div>

                <div className="document-container-pdf">

                    <div style={{ textAlign: 'center' }}>
                        <div>
                            {company &&
                                <img src={`${process.env.REACT_APP_STATIC}${company.logo}`} style={{width: 150}} className="logo" />
                            }
                        </div>
                    </div>

                    <Divider clearing horizontal section >
                        <div style={{ fontWeight: 'bold', fontSize: 20 }}>
                            Driver Log
                        </div>
                    </Divider>


                    <Table>
                        <TableBody>
                            <TableRow>
                                <TableCell style={{ fontWeight: 'bold' }}>
                                    {itinerary ? `${itinerary.name} Tour` : 'Tour'}
                                </TableCell>
                            </TableRow>
                        </TableBody>
                    </Table>


                    <Table>
                        <TableBody>
                            {
                                content.days.map((day, index) => (
                                    <TableRow>
                                        <TableCell>
                                            Day {index + 1} {!!day.datestamp && `(${moment(day.datestamp).format('MMM DD, YYYY')})`}
                                        </TableCell>
                                        <TableCell>
                                            {
                                                day.destinations.map((destination, index) => (
                                                    <div>{`(${index + 1}) ` + destination.name}</div>
                                                ))
                                            }
                                        </TableCell>
                                    </TableRow>
                                ))
                            }
                        </TableBody>
                    </Table>


                    <div style={{
                        margin: '50px auto', textAlign: 'center', width: '100%',
                        backgroundColor: 'rgba(rgba(50, 50, 50, 0.25))'
                    }}>
                        <div style={{ textAlign: 'center', fontSize: '16px', fontWeight: 'bold' }}>
                            {company ? company.name : 'MAB Technologies Pvt Ltd'}
                        </div>
                        <div style={{ width: '100%', margin: '15px auto' }}>
                            <div style={{ width: '33.33%', float: 'left' }}><RoomOutlined /> <span>{company ? company.address : 'NewYork, USA'}</span></div>
                            <div style={{ width: '33.33%', float: 'left' }}><PhoneInTalkOutlined /> <span>{company ? company.phone_number : '+1 76321353982'}</span></div>
                            <div style={{ width: '33.33%', float: 'right' }}><AlternateEmailOutlined /> <span>{company ? company.email : 'support@travdynamics.com'}</span></div>
                        </div>
                        <div>
                            <a href={company ? company.web : 'travdynamics.com'} target="_blank"><LanguageOutlined style={{ color: 'rgba(59,89,152,1)' }} /></a>
                            <a href={company ? company.facebook : 'facebook.com'} target="_blank"><Facebook style={{ color: 'rgba(59,89,152,1)' }} /></a>
                            <a href={company ? company.linkedin : 'linkedin.com'} target="_blank"><Instagram style={{ color: 'rgba(193,53,132, 1)' }} /></a>
                            <a href={company ? company.Instagram : 'instagram'} target="_blank"><Twitter style={{ color: 'rgba(29,161,242, 1)' }} /></a>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    );
}

export default withStyles({}, { withTheme: true })(Quotations);
