import { EgretLoadable } from "egret";
import { authRoles } from "../../../auth/authRoles";

const Logs = EgretLoadable({
    loader: () => import("./Logs.component")
});

const logsRoutes = [
    {
        name: "Logs",
        path: "/tour/driver/logs",
        exact: true,
        component: Logs,
        auth: authRoles.admin,
        access: 'AUTH'
    }
];

export default logsRoutes;
