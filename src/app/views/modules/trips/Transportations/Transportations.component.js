import React, { useState, useEffect } from "react";
import { Button, Grid, ButtonGroup } from "@material-ui/core";
import { Breadcrumb, EgretMenu } from "egret";
import { withStyles } from "@material-ui/styles";
import { useDispatch, useSelector } from "react-redux";
import { Dropdown, Label, Divider } from "semantic-ui-react";
import { fetchVehicles } from "app/redux/actions/modules/Vehicles.action";
import { fetchDrivers } from "app/redux/actions/modules/Drivers.action";
import { fetchGuides } from "app/redux/actions/modules/Guides.action";
import { updateTrip } from "app/redux/actions/modules/Trips.action";



const Transportation = ({ trip, closeView }) => {

    const dispatch = useDispatch();

    const { user: auth_user } = useSelector(state => state.user);

    const { list: vehicles } = useSelector(state => state.vehicles);
    const { list: drivers } = useSelector(state => state.drivers);
    const { list: guides } = useSelector(state => state.guides);

    const [transportation, setTransportation] = useState(trip.transportation ?
        JSON.parse(trip.transportation) : { vehicle: null, guide: null, driver: null });

    const updateTransport = () => {

        dispatch(updateTrip({ trip_id: trip.trip_id, transportation: JSON.stringify(transportation) }, () => {

        }))


    }


    useEffect(() => {

        [fetchVehicles(auth_user.data.company_id), fetchDrivers(auth_user.data.company_id), fetchGuides(auth_user.data.company_id)].forEach(action => {
            dispatch(action);
        })

    }, [])

    return (
        <div className="analytics m-sm-30">
            <Grid spacing={4} container>
                <Grid md={4} sm={12} xs={12} item>
                    <div className="section-header">
                        Vehicle Selection
                    </div>
                    <Dropdown
                        placeholder="Vehicle Selection"
                        fluid
                        selection
                        value={transportation.vehicle ? transportation.vehicle : null}
                        onChange={(e, { value }) => {
                            setTransportation({ ...transportation, vehicle: vehicles.find(v => v.vehicle_id === value) });
                        }}
                        options={vehicles.map(m => (
                            { key: m.vehicle_id, text: `${m.name} / (${m.vehicle_number})`, value: m.vehicle_id }
                        ))} />
                </Grid>
                <Grid md={4} sm={12} xs={12} item>
                    <div className="section-header">
                        Driver Selection
                    </div>
                    <Dropdown
                        placeholder="Driver Selection"
                        fluid
                        selection
                        value={transportation.driver ? transportation.driver.driver_id : null}
                        onChange={(e, { value }) => {
                            setTransportation({ ...transportation, driver: drivers.find(d => d.driver_id === value) });
                        }}
                        options={drivers.map(m => (
                            { key: m.driver_id, text: `${m.name} / (${m.phone_number})`, value: m.driver_id }
                        ))} />
                </Grid>
                <Grid md={4} sm={12} xs={12} item>
                    <div className="section-header">
                        Guide Selection
                    </div>
                    <Dropdown
                        placeholder="Guide Selection"
                        fluid
                        selection
                        value={transportation.guide ? transportation.guide.guide_id : null}
                        onChange={(e, { value }) => {
                            setTransportation({ ...transportation, vehicle: guides.find(g => g.guide_id === value) });
                        }}
                        options={guides.map(m => (
                            { key: m.guide_id, text: `${m.name} / (${m.phone_number})`, value: m.guide_id }
                        ))} />
                </Grid>
            </Grid>

            <Divider clearing />

            <ButtonGroup>
                <Button variant='outlined' onClick={closeView}>Back</Button>
                <Button variant='outlined' color='primary' onClick={updateTransport}>Update Transportation</Button>
            </ButtonGroup>
        </div>
    );
}

export default withStyles({}, { withTheme: true })(Transportation);
