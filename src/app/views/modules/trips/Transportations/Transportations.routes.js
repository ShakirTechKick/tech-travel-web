import { EgretLoadable } from "egret";
import { authRoles } from "../../../auth/authRoles";

const Transportations = EgretLoadable({
    loader: () => import("./Transportations.component")
});

const transportationsRoutes = [
    {
        name: "Transportations",
        path: "/tour/transportations",
        exact: true,
        component: Transportations,
        auth: authRoles.admin,
        access: 'AUTH'
    }
];

export default transportationsRoutes;
