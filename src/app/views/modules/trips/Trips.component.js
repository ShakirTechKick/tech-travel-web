import React, { useState, useEffect } from "react";
import { Button, MenuItem, Grid, Icon, Switch, Dialog, DialogActions, DialogTitle, DialogContent, useTheme, IconButton } from "@material-ui/core";
import { Breadcrumb, EgretMenu } from "egret";
import { withStyles } from "@material-ui/styles";
import { useDispatch, useSelector } from "react-redux";

import * as TripActions from "app/redux/actions/modules/Trips.action";
import * as BuilderActions from "app/redux/actions/modules/Builder.actions";
import * as ClientActions from "app/redux/actions/modules/Clients.action";
import * as UserActions from "app/redux/actions/modules/Users.actions";
import { fetchCompanies } from "app/redux/actions/modules/Company.actions";

import TableMD from "mui-datatables";
import ActionList from "app/helpers/ActionList";
import { Dropdown } from "semantic-ui-react";
import moment from "moment";
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
import { ReceiptOutlined, CommuteOutlined, InsertDriveFileOutlined, DescriptionOutlined } from "@material-ui/icons";
import QuotationsComponent from "./Quotations/Quotations.component";
import VouchersComponent from "./Vouchers/Vouchers.component";
import LogsComponent from "./Logs/Logs.component";
import TransportationsComponent from "./Transportations/Transportations.component";

const getMuiStyles = () => createMuiTheme({
    overrides: {
        MUIDataTableHeadCell: {
            root: {
                '&:nth-child(1)': {
                    width: 100
                }
            }
        }
    }
})

const Trips = (props) => {

    const dispatch = useDispatch();


    const { user: auth_user } = useSelector(state => state.user);

    const [trip, setTrip] = useState({
        company_id: auth_user.data.company_id
    });

    const [status, setStatus] = useState({
        id: null, text: null
    });

    const [UpdateStatus, setUpdateStatus] = useState({
        onCancel: () => setUpdateStatus({ ...UpdateStatus, open: 'closed' }),
        onSuccess: null, open: 'closed'
    });

    const [ResultComponent, setResultComponent] = useState({
        closeView: () => setResultComponent({ ...ResultComponent, open: 'closed' }),
        onSuccess: null, open: 'closed', component: null
    });


    const { list: trips } = useSelector(state => state.trips);
    const { list: itineraries } = useSelector(state => state.builder);
    const { list: clients } = useSelector(state => state.clients);
    const { list: users } = useSelector(state => state.users);
    const { companies } = useSelector(state => state.company);

    const [view, setView] = useState('ManageView');

    useEffect(() => {

        dispatch(TripActions.fetchTrips(auth_user.data.company_id));

        if (['SuperAdmin', 'Admin'].includes(auth_user.data.role)) {
            dispatch(BuilderActions.fetchItineraries({ company_id: auth_user.data.company_id }))
        } else {
            dispatch(BuilderActions.fetchItineraries({ company_id: auth_user.data.company_id, user_id: auth_user.data.user_id }))
        }

        dispatch(ClientActions.fetchClients(auth_user.data.company_id));
        dispatch(UserActions.fetchUsers(auth_user.data.company_id));
        dispatch(fetchCompanies());

    }, [])

    const CheckPermission = (type) => {

        var module_exists = false;
        var action_permission = false;

        auth_user.permissions.forEach(module => {
            if (Object.keys(module)[0] === "Trips") {
                module_exists = true;
                if (module['Trips'][type] === "ALLOWED") {
                    action_permission = true;
                }
            }
        });

        if (module_exists && action_permission) {
            return true;
        }

        return false;

    }


    const assignUpdatedStatus = () => {
        dispatch(TripActions.statusTrip({ ...trips.find(t => t.trip_id === status.id), status: status.text }, () => {
            setUpdateStatus({ ...UpdateStatus, open: 'closed' });
            setStatus({ id: null, text: null });
        }))
    }


    const updateStatusTrip = (id) => {
        setUpdateStatus({ ...UpdateStatus, open: 'open' });
        setStatus({ ...status, id });
    }

    const deleteEvent = (id) => {
        dispatch(TripActions.removeTrip({ trip_id: id }))
    };

    const editEvent = (id) => {
        updateStatusTrip(id);
    };

    const changeView = (v) => {
        if (v === 'AddView') {
            setTrip({
                company_id: auth_user.data.company_id
            });
        }
        setView(v)
    }

    const updateTrip = () => {

        if (view === 'AddView') {
            dispatch(TripActions.addTrip({ ...trip }, () => {
                changeView('ManageView')
            }));
        } else {
            dispatch(TripActions.updateTrip({ ...trip }, () => {
                changeView('ManageView')
            }));
        }
    }


    const OpenResultView = (component, header) => {
        setResultComponent({ ...ResultComponent, open: 'open', component, header });
    }


    const list = trips.sort((a, b) => b.trip_id - a.trip_id).map(t => (
        [t.trip_id, `${t.name} - (Itinerary ${t.itinerary_id})`,
        clients.find(c => c.client_id === t.client_id) ? clients.find(c => c.client_id === t.client_id).name : 'Not Set',
        t.status, users.find(u => u.user_id === t.created_by) ? users.find(u => u.user_id === t.created_by).name : 'Not Set',
        moment(t.createdAt).format('YYYY MMM DD'), <ActionList del={CheckPermission("delete")} edit={CheckPermission("edit")}
            id={t.trip_id} deleteEvent={deleteEvent} editEvent={editEvent} status={t.status}>

            <EgretMenu menuButton={
                <IconButton>
                    <Icon>more_vert</Icon>
                </IconButton>
            }>
                <MenuItem style={{ minWidth: 185 }} onClick={OpenResultView.bind(null, <QuotationsComponent {...{ trip: t, itineraries, clients, companies, users, closeView: ResultComponent.closeView }} />, 'Quotation')}>
                    <ReceiptOutlined />
                    <span className="pl-16">Quotation</span>
                </MenuItem>
                <MenuItem style={{ minWidth: 185 }} onClick={OpenResultView.bind(null, <VouchersComponent {...{ trip: t, itineraries, clients, companies, users, closeView: ResultComponent.closeView }} />, 'Vouchers')}>
                    <DescriptionOutlined />
                    <span className="pl-16">Hotel Vouchers</span>
                </MenuItem>
                <MenuItem style={{ minWidth: 185 }} onClick={OpenResultView.bind(null, <LogsComponent {...{ trip: t, itineraries, clients, companies, users, closeView: ResultComponent.closeView }} />, 'Driver Logs')}>
                    <InsertDriveFileOutlined />
                    <span className="pl-16">Driver Logs</span>
                </MenuItem>
                <MenuItem style={{ minWidth: 185 }} onClick={OpenResultView.bind(null, <TransportationsComponent {...{ trip: t, itineraries, clients, companies, users, closeView: ResultComponent.closeView }} />, 'Transportations')}>
                    <CommuteOutlined />
                    <span className="pl-16">Manage Transportation</span>
                </MenuItem>
            </EgretMenu>

        </ActionList>
        ]
    ));



    return (
        <div className="analytics m-sm-30">
            <div className="mb-sm-30">
                <Breadcrumb
                    routeSegments={[
                        { name: "Dashboard", path: "/dashboard" },
                        { name: "Trips" }
                    ]}
                />
            </div>


            <div className="mb-sm-30">
                {/* {
                    view === 'ManageView' &&
                    <Button
                        onClick={changeView.bind(null, 'AddView')}
                        variant="outlined"
                        color="primary">Add Trip</Button>
                } */}
                {/* {
                    (view === 'AddView' || view === 'EditView') &&
                    <Button
                        variant="outlined"
                        onClick={changeView.bind(null, 'ManageView')}
                        color="primary">All Trips</Button>
                } */}
            </div>

            <MuiThemeProvider theme={getMuiStyles()}>
                <TableMD
                    columns={['Trip ID', 'Name', 'Client', 'Status', 'Created By', 'Created On', 'Actions']}
                    title="Trips List" data={list} options={{
                        responsive: 'scrollMaxHeight'
                    }}
                />
            </MuiThemeProvider>


            <Dialog
                open={UpdateStatus.open === 'open'} disableBackdropClick fullWidth>
                <DialogTitle>Update Status</DialogTitle>
                <DialogContent style={{ height: 200 }}>
                    <Dropdown
                        placeholder='Select Status'
                        fluid
                        selection
                        value={status.text ? status.text : null}
                        onChange={(event, { value }) => setStatus({ ...status, text: value })}
                        options={['STARTED', 'ONGOING', 'COMPLETED', 'CANCELLED'].map((m, index) => {
                            return { key: index, text: m, value: m }
                        })}
                    />
                </DialogContent>
                <DialogActions>
                    <Button variant='outlined' onClick={UpdateStatus.onCancel}>Cancel</Button>
                    <Button variant='contained' onClick={assignUpdatedStatus}>Ok</Button>
                </DialogActions>
            </Dialog>


            <Dialog
                open={ResultComponent.open === 'open'} disableBackdropClick fullScreen>
                <DialogTitle>{ResultComponent.header}</DialogTitle>
                <DialogContent style={{ padding: 0 }}>
                    {ResultComponent.component}
                </DialogContent>
            </Dialog>

        </div >
    );
}

export default withStyles({}, { withTheme: true })(Trips);
