import { EgretLoadable } from "egret";
import { authRoles } from "../../../auth/authRoles";

const Trips = EgretLoadable({
    loader: () => import("./Trips.component")
});

const tripsRoutes = [
    {
        name: "Trips",
        path: "/trips",
        exact: true,
        component: Trips,
        auth: authRoles.admin,
        access: 'AUTH'
    }
];

export default tripsRoutes;
