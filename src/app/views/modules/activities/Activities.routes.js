import { EgretLoadable } from "egret";
import { authRoles } from "../../../auth/authRoles";

const Activities = EgretLoadable({
  loader: () => import("./Activities.component")
});

const activitiesRoutes = [
  {
    name: 'Hotels',
    path: "/library/activities",
    exact: true,
    component: Activities,
    auth: authRoles.admin,
    access: 'AUTH'
  }
];

export default activitiesRoutes;
