import React, { useState, useEffect } from "react";
import { ButtonGroup, Button, MenuItem, Grid, Icon } from "@material-ui/core";
import { Breadcrumb } from "egret";
import { withStyles } from "@material-ui/styles";
import { useDispatch, useSelector } from "react-redux";
import ActionList from "app/helpers/ActionList";
import { Dropdown, Label, Divider } from "semantic-ui-react";
import { ToastsStore } from "react-toasts";
import history from '../../../../history';
import * as ActivityActions from '../../../redux/actions/modules/Activities.action';
import FlexRow from "app/helpers/FlexRow";
import { ValidatorForm, TextValidator } from "react-material-ui-form-validator";


const Activities = (props) => {

    const dispatch = useDispatch();

    const { user: auth_user } = useSelector(state => state.user);

    const [activity, setActivity] = useState({
        company_id: auth_user.data.company_id
    });

    const [view, setView] = useState('CategoryView');

    const [current, setCurrentActivities] = useState({
        company_id: auth_user.data.company_id,
        activity_id: null,
        category: null,
        activities: []
    });

    const [activityItem, setActivityItem] = useState('');
    const [editActivityItem, setEditActivityItem] = useState('');


    const onClickActivities = (activity_id, category, activities) => {
        setCurrentActivities({ ...current, activity_id, category, activities });
        setView('ActivityView');
    }

    const onUpdateActivitiesList = (activitylist) => {
        dispatch(ActivityActions.updateActivitiesList(activitylist));
    }

    const [edit, setEdit] = useState([]);

    const { list } = useSelector(state => state.activities);

    useEffect(() => {
        dispatch(ActivityActions.fetchActivities(auth_user.data.company_id));
    }, [])

    useEffect(() => {
        if (!edit.length) {
            if (list.length) {
                setEdit([...list.map(item => {
                    return {
                        company_id: auth_user.data.company_id,
                        activity_id: item.activity_id,
                        name: item.name
                    }
                })])
            }
        }
    }, [list, edit])


    const blockEvent = (id) => {
        dispatch(ActivityActions.statusActivity({ activity_id: id }))
    };

    const CheckPermission = (type) => {

        var module_exists = false;
        var action_permission = false;

        auth_user.permissions.forEach(module => {
            if (Object.keys(module)[0] === "Activities") {
                module_exists = true;
                if (module['Activities'][type] === "ALLOWED") {
                    action_permission = true;
                }
            }
        });

        if (module_exists && action_permission) {
            return true;
        }

        return false;

    }


    const onChangeField = (e) => {
        setActivity({ ...activity, [e.target.name]: e.target.value });
    }

    const onChangeFieldEdit = (key, e) => {

        const array = Object.assign([], edit);
        const item = edit[key];
        const updated = { ...item, [e.target.name]: e.target.value };

        array.splice(key, 1, updated);

        setEdit([...array]);
    }

    const saveForm = () => {
        dispatch(ActivityActions.addActivity({ ...activity }));
    }

    const saveFormActivityList = () => {
        const updated = {...current, activities: [...current.activities, activityItem]};
        setCurrentActivities({...updated});
        onUpdateActivitiesList(updated);
    }

    const updateForm = (key) => {
        if (edit[key].name.length) {
            dispatch(ActivityActions.updateActivity({ ...edit[key] }));
        } else {
            ToastsStore.error('Name cannot be empty !', 2000);
        }
    }

    const updateFormActivityList = (key) => {
        const activities_list = Object.assign([], current.activities);
        activities_list.splice(key, 1, editActivityItem);
        setCurrentActivities({...current, activities: activities_list});
        onUpdateActivitiesList({...current, activities: activities_list});
    }

    const deleteForm = (id) => {
        dispatch(ActivityActions.removeActivity({ activity_id: id }))
    };

    const deleteFormActivityList = (id) => {

        const activities_list = Object.assign([], current.activities);
        activities_list.splice(id, 1);
        setCurrentActivities({...current, activities: activities_list});
        onUpdateActivitiesList({...current, activities: activities_list});
        setView('CategoryView');
    };

    const viewHotels = () => {
        history.push({ pathname: '/library/hotels' });
    }

    return (

        <React.Fragment>
            {view === 'CategoryView' &&

                <div className="analytics m-sm-30">
                    <div className="mb-sm-30">
                        <Breadcrumb
                            routeSegments={[
                                { name: "Dashboard", path: "/dashboard" },
                                { name: "Activity Categories List" }
                            ]}
                        />
                    </div>

                    {CheckPermission('write') &&
                        <ValidatorForm onSubmit={saveForm}>

                            <Grid container style={{ marginTop: '20px' }} spacing={2}>
                                <Grid md={10} sm={12} xs={12} item>
                                    <TextValidator
                                        className="mb-16 w-100"
                                        variant="outlined"
                                        label="Category"
                                        type="text"
                                        value={activity.category ? activity.category : ``}
                                        onChange={onChangeField}
                                        name="category"
                                        validators={["required"]}
                                        errorMessages={[
                                            "Category Name is required"
                                        ]}
                                    />
                                </Grid>
                                <Grid md={2} sm={12} xs={12} item>
                                    <Button variant='outlined' className='row-button' type='submit'>
                                        Add
                        </Button>
                                </Grid>
                            </Grid>
                        </ValidatorForm>
                    }

                    <Divider horizontal section>
                        <Icon>menu</Icon>
                        <span className='divider-header'>Category List</span>
                    </Divider>

                    {
                        !!list.length &&

                        list.map((act, index) => (
                            <ValidatorForm key={act.activity_id} onSubmit={updateForm.bind(null, index)}>

                                <Grid container style={{ marginTop: '20px' }} spacing={2}>
                                    <Grid md={6} sm={12} xs={12} item>
                                        <TextValidator
                                            className="mb-16 w-100"
                                            variant="outlined"
                                            label="Category"
                                            type="text"
                                            defaultValue={act.category}
                                            onChange={onChangeFieldEdit.bind(null, index)}
                                            name="category"
                                        />
                                    </Grid>
                                    <Grid md={2} sm={12} xs={12} item>
                                        <Button onClick={onClickActivities.bind(null, act.activity_id, act.category, act.activities ? act.activities.split(',') : [])} variant='outlined' color='secondary' className='row-button' type='button'>
                                            Activities
                                </Button>
                                    </Grid>
                                    {CheckPermission('edit') &&
                                        <Grid md={2} sm={12} xs={12} item>
                                            <Button variant='outlined' color='primary' className='row-button' type='submit'>
                                                Update
                                </Button>
                                        </Grid>
                                    }
                                    {CheckPermission('delete') &&
                                        <Grid md={2} sm={12} xs={12} item>
                                            <Button onClick={deleteForm.bind(null, act.activity_id)} variant='outlined' color='secondary' className='row-button' type='button'>
                                                Del
                                </Button>
                                        </Grid>
                                    }
                                </Grid>
                            </ValidatorForm>

                        ))
                    }

                </div>
            }


            {view === 'ActivityView' &&

                <div className="analytics m-sm-30">
                    <div className="mb-sm-30">
                        <Breadcrumb
                            routeSegments={[
                                { name: "Dashboard", path: "/dashboard" },
                                { name: `Activities List (${current.category})` }
                            ]}
                        />
                    </div>

                    <Button variant="contained" onClick={() => setView('CategoryView')} color="secondary">Go Back</Button>

                    {CheckPermission('write') &&
                        <ValidatorForm onSubmit={saveFormActivityList}>

                            <Grid container style={{ marginTop: '20px' }} spacing={2}>
                                <Grid md={10} sm={12} item>
                                    <TextValidator
                                        className="mb-16 w-100"
                                        variant="outlined"
                                        label="Activity"
                                        type="text"
                                        value={activityItem}
                                        onChange={(e) => setActivityItem(e.target.value)}
                                        name="activity"
                                        validators={["required"]}
                                        errorMessages={[
                                            "Activity Name is required"
                                        ]}
                                    />
                                </Grid>
                                <Grid md={2} sm={12} item>
                                    <Button variant='outlined' className='row-button' type='submit'>
                                        Add Activity
                                    </Button>
                                </Grid>
                            </Grid>
                        </ValidatorForm>
                    }

                    <Divider horizontal section>
                        <Icon>menu</Icon>
                        <span className='divider-header'>Activities List For {current.category}</span>
                    </Divider>

                    {
                        !!current.activities.length &&

                        current.activities.map((act, index) => (
                            <ValidatorForm key={index} onSubmit={updateFormActivityList.bind(null, index)}>

                                <Grid container style={{ marginTop: '20px' }} spacing={2}>
                                    <Grid md={6} sm={12} item>
                                        <TextValidator
                                            className="mb-16 w-100"
                                            variant="outlined"
                                            label="Activity"
                                            type="text"
                                            defaultValue={act}
                                            onChange={(e) => setEditActivityItem(e.target.value)}
                                            name="activity"
                                        />
                                    </Grid>
                                    {CheckPermission('edit') &&
                                        <Grid md={2} sm={12} item>
                                            <Button variant='outlined' color='primary' className='row-button' type='submit'>
                                                Update
                                            </Button>
                                        </Grid>
                                    }
                                    {CheckPermission('delete') &&
                                        <Grid md={2} sm={12} item>
                                            <Button onClick={deleteFormActivityList.bind(null, index)} variant='outlined' color='secondary' className='row-button' type='button'>
                                                Del
                                            </Button>
                                        </Grid>
                                    }
                                </Grid>
                            </ValidatorForm>

                        ))
                    }

                </div>
            }
        </React.Fragment>
    );
}

export default withStyles({}, { withTheme: true })(Activities);
