import { EgretLoadable } from "egret";
import { authRoles } from "../../../auth/authRoles";

const Excursions = EgretLoadable({
  loader: () => import("./Excursions.component")
});

const excursionsRoutes = [
  {
    name: 'Hotels',
    path: "/library/excursions",
    exact: true,
    component: Excursions,
    auth: authRoles.admin,
    access: 'AUTH'
  },
  {
    name: 'Hotels',
    path: "/library/excursions/view/:id",
    exact: true,
    component: Excursions,
    auth: authRoles.admin,
    access: 'AUTH'
  }
];

export default excursionsRoutes;
