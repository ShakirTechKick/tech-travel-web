import React, { useCallback, useState, useEffect } from "react";
import { ValidatorForm, TextValidator, SelectValidator } from "react-material-ui-form-validator";
import { ButtonGroup, Button, MenuItem, Grid, Switch, Table, TableRow, TableCell } from "@material-ui/core";
import MapRounded from '@material-ui/icons/MapRounded';
import { Breadcrumb } from "egret";
import { withStyles } from "@material-ui/styles";
import { useDispatch, useSelector } from "react-redux";
import TableMD from "mui-datatables";
import ActionList from "app/helpers/ActionList";
import { Dropdown, Label, Divider } from "semantic-ui-react";
import { ToastsStore } from "react-toasts";
import Widget from 'app/helpers/TravUploadWidget';
import * as ExcursionsActions from 'app/redux/actions/modules/Excursions.action';
import * as PlacesActions from 'app/redux/actions/modules/Places.action';
import FlexRow from "app/helpers/FlexRow";
import history from '../../../../history';
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
import GooglePlacesAutocompleteInput from 'react-google-places-autocomplete';
import 'react-google-places-autocomplete/dist/assets/index.css';
import GoogleMapComponent from "app/helpers/GoogleMapComponent";
import GooglePlacesSuggest from "react-geosuggest";
import moment from "moment";

const getMuiThemeCustom = () => createMuiTheme({
    overrides: {
        MUIDataTableHeadCell: {
            root: {
                '&:nth-child(5)': {
                    width: '100px'
                }
            }
        }
    }
})

const Excursions = (props) => {

    const dispatch = useDispatch();

    const { user: auth_user } = useSelector(state => state.user);

    const [current, setCurrent] = useState({
        city: null, country: null
    });

    const [isMapShown, setToggleMap] = useState(false);

    const static_excursion = {
        company_id: auth_user.data.company_id
    };

    const [excursion, setExcursion] = useState(static_excursion);

    const { countries, cities } = useSelector(state => state.places);
    const { list } = useSelector(state => state.excursions);

    const [view, setView] = useState('ManageView');

    useEffect(() => {
        dispatch(ExcursionsActions.fetchExcursions(auth_user.data.company_id));
        dispatch(PlacesActions.fetchCountries(auth_user.data.company_id));
        dispatch(PlacesActions.fetchCities(auth_user.data.company_id));
    }, [])

    const deleteEvent = (id) => {
        dispatch(ExcursionsActions.removeExcursion({ excursion_id: id }))
    };

    const editEvent = (id) => {
        setExcursion(list.find(a => a.excursion_id === id));
        setView('EditView');
    };

    const blockEvent = (id) => {
        dispatch(ExcursionsActions.statusExcursion({ excursion_id: id }))
    };

    const CheckPermission = (type) => {

        var module_exists = false;
        var action_permission = false;

        auth_user.permissions.forEach(module => {
            if (Object.keys(module)[0] === "Excursions") {
                module_exists = true;
                if (module['Excursions'][type] === "ALLOWED") {
                    action_permission = true;
                }
            }
        });

        if (module_exists && action_permission) {
            return true;
        }

        return false;

    }


    const onChangeField = (e) => {
        setExcursion({ ...excursion, [e.target.name]: e.target.value });
    }

    const saveExcursionForm = () => {

        if (excursion.country_id && excursion.city_id) {
            if (view === 'AddView') {
                dispatch(ExcursionsActions.addExcursion({ ...excursion }));
            } else if (view == 'EditView') {
                dispatch(ExcursionsActions.updateExcursion({ ...excursion }));
            }

            setExcursion({ ...static_excursion });
            setView('ManageView')
        } else {
            ToastsStore.error('Country and city are required !', 2000);
        }
    }



    const excursions_list = list.sort((a, b) => b.excursion_id - a.excursion_id).map(e => ([
        e.name, `${countries.find(c => c.country_id === e.country_id) ? countries.find(c => c.country_id === e.country_id).name : ''} : ${cities.find(c => c.city_id === e.city_id) ? cities.find(c => c.city_id === e.city_id).name : ''}`,
        e.from, e.to, <ActionList del={CheckPermission("delete")} edit={CheckPermission("edit")} block={CheckPermission("edit")}
            id={e.excursion_id} deleteEvent={deleteEvent} blockEvent={blockEvent}
            editEvent={editEvent} status={e.status} view={`/library/excursions/view/${e.excursion_id}`} />
    ]))

    const toggleView = () => {

        setExcursion({ ...static_excursion });

        view === 'ManageView' ? setView('AddView') : setView('ManageView');
    }

    const makeToggleMap = () => {
        setToggleMap(!isMapShown);
    }

    const { path, params } = props.match ? props.match : { path: null, params: null };

    return (
        <div className="analytics m-sm-30">

            {!props.modal &&
                <div className="mb-sm-30">
                    <Breadcrumb
                        routeSegments={[
                            { name: "Dashboard", path: "/dashboard" },
                            { name: "Excursions" }
                        ]}
                    />
                </div>
            }

            {(path === '/library/excursions/view/:id') ?

                list.find(c => c.excursion_id == params.id) ?
                <Table style={{ border: '.5px solid' }}>
                        <TableRow>
                            <TableCell colspan="2" style={{ padding: '15px' }}>
                                <FlexRow>
                                    <Button variant='outlined' onClick={() => history.push('/transport/drivers')}>Back</Button>
                                    <ActionList del={CheckPermission("delete")} block={CheckPermission("edit")}
                                        id={list.find(c => c.excursion_id == params.id).excursion_id} deleteEvent={deleteEvent} blockEvent={blockEvent}
                                        status={list.find(c => c.excursion_id == params.id).status} />
                                </FlexRow>
                            </TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell style={{ padding: '15px' }}>Name</TableCell>
                            <TableCell>{list.find(c => c.excursion_id == params.id).name}</TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell style={{ padding: '15px' }}>Country</TableCell>
                            <TableCell>{countries.find(cn => cn.country_id === list.find(c => c.excursion_id == params.id).country_id) ?
                                countries.find(cn => cn.country_id === list.find(c => c.excursion_id == params.id).country_id).name : null}</TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell style={{ padding: '15px' }}>City</TableCell>
                            <TableCell>{cities.find(cn => cn.city_id === list.find(c => c.excursion_id == params.id).city_id) ?
                                cities.find(cn => cn.city_id === list.find(c => c.excursion_id == params.id).city_id).name : null}</TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell style={{ padding: '15px' }}>From</TableCell>
                            <TableCell>{list.find(c => c.excursion_id == params.id).from}</TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell style={{ padding: '15px' }}>To</TableCell>
                            <TableCell>{list.find(c => c.excursion_id == params.id).to}</TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell style={{ padding: '15px' }}>Distance</TableCell>
                            <TableCell>{list.find(c => c.excursion_id == params.id).distance}</TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell style={{ padding: '15px' }}>Cost</TableCell>
                            <TableCell>{list.find(c => c.excursion_id == params.id).cost}</TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell style={{ padding: '15px' }}>Status</TableCell>
                            <TableCell>{list.find(c => c.excursion_id == params.id).status === 'A' ? 'Active' : 'Blocked'}</TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell style={{ padding: '15px' }}>Created On</TableCell>
                            <TableCell>{moment(list.find(c => c.excursion_id == params.id).createdAt).format('YYYY MMM DD, hh:mm:ss a ddd')}</TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell style={{ padding: '15px' }}>Last Updated On</TableCell>
                            <TableCell>{moment(list.find(c => c.excursion_id == params.id).updatedAt).format('YYYY MMM DD, hh:mm:ss a ddd')}</TableCell>
                        </TableRow>
                    </Table> : <div className="no-matches">No matched item found !</div> :
                <React.Fragment>
                    <FlexRow>
                        <ButtonGroup>
                            <Button variant='outlined' onClick={makeToggleMap}>
                                <MapRounded />
                            </Button>
                        </ButtonGroup>
                        <Button variant='outlined' onClick={toggleView}>
                            {(view === 'AddView' || view === 'EditView') ? 'All' : 'Add'} Excursions
                </Button>
                    </FlexRow>

                    {
                        isMapShown &&

                        <GoogleMapComponent isMarkerShown defaultZoom={8} defaultCenter={{ lat: -34.397, lng: 150.644 }} />

                    }

                    <div style={{ marginTop: `20px`, marginBottom: `20px` }}>
                        <React.Fragment>

                            {(view === 'AddView' || view === 'EditView') &&
                                <ValidatorForm onSubmit={saveExcursionForm}>

                                    <Grid container spacing={6}>
                                        <Grid md={4} sm={12} xs={12} item>
                                            <TextValidator
                                                className="mb-16 w-100"
                                                variant="outlined"
                                                label="Name"
                                                type="text"
                                                value={excursion.name ? excursion.name : ``}
                                                onChange={onChangeField}
                                                name="name"
                                                validators={["required"]}
                                                errorMessages={[
                                                    "Name is required"
                                                ]}
                                            />
                                        </Grid>
                                        <Grid md={4} sm={12} xs={12} item>
                                            <Dropdown
                                                placeholder='Country'
                                                fluid
                                                selection
                                                search
                                                value={excursion.country_id ? excursion.country_id : ``}
                                                onChange={(event, { value }) => setExcursion({ ...excursion, country_id: value })}
                                                options={countries.map((m, index) => {
                                                    return { key: index, text: m.name, value: m.country_id }
                                                })}
                                            />
                                        </Grid>
                                        <Grid md={4} sm={12} xs={12} item>
                                            <Dropdown
                                                placeholder='City'
                                                fluid
                                                search
                                                selection
                                                value={excursion.city_id ? excursion.city_id : ``}
                                                onChange={(event, { value }) => setExcursion({ ...excursion, city_id: value })}
                                                options={excursion.country_id ? cities.filter(c => c.country_id === excursion.country_id).map((m, index) => {
                                                    return { key: index, text: m.name, value: m.city_id }
                                                }) : []}
                                            />
                                        </Grid>
                                    </Grid>


                                    <Grid container spacing={6}>

                                        <Grid md={6} sm={12} xs={12} item>
                                            <TextValidator
                                                className="mb-16 w-100"
                                                variant="outlined"
                                                label="Place From"
                                                type="text"
                                                value={excursion.from ? excursion.from : ``}
                                                onChange={onChangeField}
                                                name="from"
                                            />
                                        </Grid>

                                        <Grid md={6} sm={12} xs={12} item>
                                            <TextValidator
                                                className="mb-16 w-100"
                                                variant="outlined"
                                                label="Place To"
                                                type="text"
                                                value={excursion.to ? excursion.to : ``}
                                                onChange={onChangeField}
                                                name="to"
                                            />
                                        </Grid>
                                    </Grid>

                                    <Grid container spacing={6}>

                                        <Grid md={6} sm={12} xs={12} item>
                                            <TextValidator
                                                className="mb-16 w-100"
                                                variant="outlined"
                                                label="Distance (KM)"
                                                type="text"
                                                value={excursion.distance ? excursion.distance : ``}
                                                onChange={onChangeField}
                                                name="distance"
                                            />
                                        </Grid>

                                        <Grid md={6} sm={12} xs={12} item>
                                            <TextValidator
                                                className="mb-16 w-100"
                                                variant="outlined"
                                                label="Cost"
                                                type="text"
                                                value={excursion.cost ? excursion.cost : ``}
                                                onChange={onChangeField}
                                                name="cost"
                                            />
                                        </Grid>
                                    </Grid>

                                    <Divider horizontal clearing />

                                    <Button
                                        variant="contained"
                                        color="primary"
                                        type="submit">{view === 'AddView' ? 'Save' : 'Update'} Excursion</Button>

                                </ValidatorForm>
                            }
                            {view === 'ManageView' &&
                                <TableMD
                                    columns={['Name', 'Country | City', 'From', 'To', {
                                        label: 'Action', options: {
                                            sort: false, filter: false
                                        }
                                    }]}
                                    options={{
                                        responsive: 'scrollMaxHeight',
                                        selectableRows: !props.modal
                                    }}
                                    title='Excursions'
                                    data={excursions_list}
                                />
                            }
                        </React.Fragment>
                    </div>
                </React.Fragment>}
        </div >
    );
}

export default withStyles({}, { withTheme: true })(Excursions);
