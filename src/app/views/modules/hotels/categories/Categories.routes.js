import { EgretLoadable } from "egret";
import { authRoles } from "../../../../auth/authRoles";

const HotelCategories = EgretLoadable({
  loader: () => import("./Categories.component")
});

const hotelsRoutes = [
  {
    name: 'Hotels',
    path: "/library/hotels/room-categories",
    component: HotelCategories,
    exact: true,
    auth: authRoles.admin,
    access: 'AUTH'
  }
];

export default hotelsRoutes;
