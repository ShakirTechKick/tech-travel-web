import { EgretLoadable } from "egret";
import { authRoles } from "../../../../auth/authRoles";

const HotelMeals = EgretLoadable({
  loader: () => import("./Meals.component")
});

const hotelMealsRoutes = [
  {
    name: 'Hotels',
    path: "/library/hotels/meal-types",
    component: HotelMeals,
    exact: true,
    auth: authRoles.admin,
    access: 'AUTH'
  }
];

export default hotelMealsRoutes;
