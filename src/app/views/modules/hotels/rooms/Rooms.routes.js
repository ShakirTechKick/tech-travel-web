import { EgretLoadable } from "egret";
import { authRoles } from "../../../../auth/authRoles";

const HotelRooms = EgretLoadable({
  loader: () => import("./Rooms.component")
});

const hotelRoomsRoutes = [
  {
    name: 'Hotels',
    path: "/library/hotels/room-types",
    component: HotelRooms,
    exact: true,
    auth: authRoles.admin,
    access: 'AUTH'
  }
];

export default hotelRoomsRoutes;
