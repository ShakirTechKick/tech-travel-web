import React, { useState, useEffect } from "react";
import { ButtonGroup, Button, MenuItem, Grid, Icon } from "@material-ui/core";
import { Breadcrumb } from "egret";
import { withStyles } from "@material-ui/styles";
import { useDispatch, useSelector } from "react-redux";
import ActionList from "app/helpers/ActionList";
import { Dropdown, Label, Divider } from "semantic-ui-react";
import { ToastsStore } from "react-toasts";
import history from '../../../../../history';
import * as HotelActions from '../../../../redux/actions/modules/Hotels.action';
import FlexRow from "app/helpers/FlexRow";
import { ValidatorForm, TextValidator } from "react-material-ui-form-validator";


const Rooms = (props) => {

    const dispatch = useDispatch();

    const { user: auth_user } = useSelector(state => state.user);

    const [room, setRoom] = useState({
        company_id: auth_user.data.company_id
    });

    const [edit, setEdit] = useState([]);


    const { rooms } = useSelector(state => state.hotels);

    useEffect(() => {
        dispatch(HotelActions.fetchHotelRooms(auth_user.data.company_id));
    }, [])

    useEffect(() => {
        if (!edit.length) {
            if (rooms.length) {
                setEdit([...rooms.map(item => {
                    return {
                        company_id: auth_user.data.company_id,
                        room_id: item.room_id,
                        name: item.name,
                        description: item.description
                    }
                })])
            }
        }
    }, [rooms, edit])

    const blockEvent = (id) => {
        dispatch(HotelActions.statusHotelRoom({ room_id: id }))
    };

    const CheckPermission = (type) => {

        var module_exists = false;
        var action_permission = false;

        auth_user.permissions.forEach(module => {
            if (Object.keys(module)[0] === "Hotels") {
                module_exists = true;
                if (module['Hotels'][type] === "ALLOWED") {
                    action_permission = true;
                }
            }
        });

        if (module_exists && action_permission) {
            return true;
        }

        return false;

    }


    const onChangeField = (e) => {
        setRoom({ ...room, [e.target.name]: e.target.value });
    }

    const onChangeFieldEdit = (key, e) => {

        const array = Object.assign([], edit);
        const item = edit[key];
        const updated = { ...item, [e.target.name]: e.target.value };

        array.splice(key, 1, updated);

        setEdit([...array]);
    }

    const saveForm = () => {
        dispatch(HotelActions.addHotelRoom({ ...room }));
    }

    const updateForm = (key) => {
        if (edit[key].name.length) {
            dispatch(HotelActions.updateHotelRoom({ ...edit[key] }));
        } else {
            ToastsStore.error('Name cannot be empty !', 2000);
        }
    }

    const deleteForm = (id) => {
        dispatch(HotelActions.removeHotelRoom({ room_id: id }))
    };

    const viewHotels = () => {
        history.push({ pathname: '/library/hotels' });
    }

    return (
        <div className="analytics m-sm-30">
            <div className="mb-sm-30">
                <Breadcrumb
                    routeSegments={[
                        { name: "Dashboard", path: "/dashboard" },
                        { name: "Hotel Rooms" }
                    ]}
                />
            </div>

            <FlexRow>
                <Button variant='outlined' onClick={viewHotels}>
                    View Hotels
                </Button>
            </FlexRow>
            {CheckPermission('write') &&
                <ValidatorForm onSubmit={saveForm}>

                    <Grid container style={{ marginTop: '20px' }} spacing={2}>
                        <Grid md={4} sm={12} xs={12} item>
                            <TextValidator
                                className="mb-16 w-100"
                                variant="outlined"
                                label="Name"
                                type="text"
                                value={room.name ? room.name : ``}
                                onChange={onChangeField}
                                name="name"
                                validators={["required"]}
                                errorMessages={[
                                    "Name is required"
                                ]}
                            />
                        </Grid>
                        <Grid md={6} sm={12} xs={12} item>
                            <TextValidator
                                className="mb-16 w-100"
                                variant="outlined"
                                label="Description"
                                type="text"
                                value={room.description ? room.description : ``}
                                onChange={onChangeField}
                                name="description"
                            />
                        </Grid>
                        <Grid md={2} sm={12} xs={12} item>
                            <Button variant='outlined' className='row-button' type='submit'>
                                Add
                        </Button>
                        </Grid>
                    </Grid>
                </ValidatorForm>
            }

            <Divider horizontal section>
                <Icon>menu</Icon>
                <span className='divider-header'>Hotel Room List</span>
            </Divider>

            {
                !!rooms.length &&

                rooms.map((rm, index) => (
                    <ValidatorForm key={rm.room_id} onSubmit={updateForm.bind(null, index)}>

                        <Grid container style={{ marginTop: '20px' }} spacing={2}>
                            <Grid md={4} sm={12} xs={12} item>
                                <TextValidator
                                    className="mb-16 w-100"
                                    variant="outlined"
                                    label="Name"
                                    type="text"
                                    defaultValue={rm.name}
                                    onChange={onChangeFieldEdit.bind(null, index)}
                                    name="name"
                                />
                            </Grid>
                            <Grid md={6} sm={12} xs={12} item>
                                <TextValidator
                                    className="mb-16 w-100"
                                    variant="outlined"
                                    label="Description"
                                    type="text"
                                    defaultValue={rm.description}
                                    onChange={onChangeFieldEdit.bind(null, index)}
                                    name="description"
                                />
                            </Grid>
                            {CheckPermission('edit') &&
                                <Grid md={1} sm={12} xs={12} item>
                                    <Button variant='outlined' color='primary' className='row-button' type='submit'>
                                        Update
                                </Button>
                                </Grid>
                            }
                            {CheckPermission('delete') &&
                                <Grid md={1} sm={12} xs={12} item>
                                    <Button onClick={deleteForm.bind(null, rm.room_id)} variant='outlined' color='secondary' className='row-button' type='button'>
                                        Del
                                </Button>
                                </Grid>
                            }
                        </Grid>
                    </ValidatorForm>

                ))
            }

        </div >
    );
}

export default withStyles({}, { withTheme: true })(Rooms);
