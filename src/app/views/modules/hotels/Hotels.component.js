import React, { useCallback, useState, useEffect } from "react";
import { ValidatorForm, TextValidator, SelectValidator } from "react-material-ui-form-validator";
import { ButtonGroup, Button, MenuItem, Grid, Icon, Switch, Table, TableRow, TableCell, Dialog, DialogTitle, DialogContent, DialogActions, FormGroup, FormControlLabel, Checkbox, TextField } from "@material-ui/core";
import MapRounded from '@material-ui/icons/MapRounded';
import { Breadcrumb } from "egret";
import { withStyles } from "@material-ui/styles";
import { useDispatch, useSelector } from "react-redux";
import TableMD from "mui-datatables";
import ActionList from "app/helpers/ActionList";
import { Dropdown, Label, Divider } from "semantic-ui-react";
import { ToastsStore } from "react-toasts";
import Widget from 'app/helpers/TravUploadWidget';
import * as HotelActions from 'app/redux/actions/modules/Hotels.action';
import * as PlacesActions from 'app/redux/actions/modules/Places.action';
import FlexRow from "app/helpers/FlexRow";
import history from '../../../../history';
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
import 'react-google-places-autocomplete/dist/assets/index.css';
import GoogleMapComponent from "app/helpers/GoogleMapComponent";
import GooglePlacesSuggest from "react-geosuggest";
import { geocodeByPlaceId } from "react-google-places-autocomplete";
import moment from "moment";
import { CSVDownload, CSVLink } from "react-csv";
import { Request } from "app/config/Request";


const getMuiThemeCustom = () => createMuiTheme({
    overrides: {
        MUIDataTableHeadCell: {
            root: {
                '&:nth-child(5)': {
                    width: '100px'
                }
            }
        }
    }
})

const Hotels = (props) => {

    const dispatch = useDispatch();

    const { user: auth_user } = useSelector(state => state.user);

    const [current, setCurrent] = useState({
        city: null, country: null
    });

    const [isMapShown, setToggleMap] = useState({
        visibility: false, location: { lat: 7.873053999999999, lng: 80.77179699999999 }, zoom: 10
    });

    const [hotel, setHotel] = useState({
        company_id: auth_user.data.company_id, images: []
    });


    const [fetchRemoteImages, setFetchRemoteImages] = useState({
        allowed: false, photos: []
    });

    const [bulk, setBulk] = useState({ allowed: false, file: null });

    const [BulkDialog, setBulkDialog] = useState({
        closeView: () => setBulkDialog({ ...BulkDialog, open: 'closed' }),
        open: 'closed', country_id: null, city_id: null, fields: []
    })

    const [isUploadOpen, setUploadOpen] = useState(false);
    const onCompleteHandler = (id, name, response) => {
        if (response.success) {
            if (hotel.images) {
                setHotel({ ...hotel, images: [...hotel.images, `${response.path}/${name}`] });
            } else {
                setHotel({ ...hotel, images: [`${response.path}/${name}`] });
            }
        }
    }
    const onOpenGallery = () => {
        setUploadOpen(true);
    }

    const onRemoveGallery = (img) => {

        const images = hotel.images.filter(i => i !== img);

        setHotel({ ...hotel, images: [...images] });

        Request.post(`utilities/fileuploader/destroy_resources`, { path: img });
    }

    const { countries, cities } = useSelector(state => state.places);
    const { list, standards, categories, rooms, meals } = useSelector(state => state.hotels);

    const [view, setView] = useState('ManageView');

    useEffect(() => {
        dispatch(HotelActions.fetchHotels(auth_user.data.company_id));
        dispatch(PlacesActions.fetchCountries(auth_user.data.company_id));
        dispatch(PlacesActions.fetchCities(auth_user.data.company_id));
        dispatch(HotelActions.fetchHotelStandards(auth_user.data.company_id));
        dispatch(HotelActions.fetchHotelCategories(auth_user.data.company_id));
        dispatch(HotelActions.fetchHotelRooms(auth_user.data.company_id));
        dispatch(HotelActions.fetchHotelMeals(auth_user.data.company_id));
    }, [])

    const deleteEvent = (id) => {
        dispatch(HotelActions.removeHotel({ hotel_id: id }))
    };

    const editEvent = (id) => {
        const h = list.find(h => h.hotel_id === id);
        setHotel({ ...h, images: h.images ? h.images.split(',').filter(i => !!i) : [] });

        geocodeByPlaceId(h.place_id).then(result => {

            const location = { lat: result[0].geometry.location.lat(), lng: result[0].geometry.location.lng() };

            setToggleMap({ ...isMapShown, location, zoom: 10 });

        }).catch(error => null);

        setView('EditView');
    };

    const blockEvent = (id) => {
        dispatch(HotelActions.statusHotel({ hotel_id: id }))
    };

    const CheckPermission = (type) => {

        var module_exists = false;
        var action_permission = false;

        auth_user.permissions.forEach(module => {
            if (Object.keys(module)[0] === "Hotels") {
                module_exists = true;
                if (module['Hotels'][type] === "ALLOWED") {
                    action_permission = true;
                }
            }
        });

        if (module_exists && action_permission) {
            return true;
        }

        return false;

    }


    const onChangeField = (e) => {
        setHotel({ ...hotel, [e.target.name]: e.target.value });
    }

    const saveHotelForm = () => {

        setFetchRemoteImages({ allowed: false, name: null, photos: [] });

        if (view === 'AddView') {
            dispatch(HotelActions.addHotel({ ...hotel }, () => {
                setView('ManageView');
            }));
        } else if (view == 'EditView') {
            dispatch(HotelActions.updateHotel({ ...hotel }, () => {
                setView('ManageView');
            }));
        }
    }



    const hotels_list = list.sort((a, b) => b.hotel_id - a.hotel_id).map(h => ([
        h.name, `${!!countries.find(c => c.country_id === h.country_id) ? countries.find(c => c.country_id === h.country_id).name : ''} : ${!!cities.find(c => c.city_id === h.city_id) ? cities.find(c => c.city_id === h.city_id).name : ''}`,
        h.standard, h.contact_person, <ActionList del={CheckPermission("delete")} edit={CheckPermission("edit")} block={CheckPermission("edit")}
            id={h.hotel_id} deleteEvent={deleteEvent} blockEvent={blockEvent}
            editEvent={editEvent} status={h.status} view={`/library/hotels/view/${h.hotel_id}`} />
    ]))

    const toggleView = () => {
        setHotel({ company_id: auth_user.data.company_id, images: [] });

        view === 'ManageView' ? setView('AddView') : setView('ManageView');
    }

    const viewHotelPath = path => {
        history.push({ pathname: path })
    }

    const makeToggleMap = () => {
        setToggleMap({ ...isMapShown, visibility: !isMapShown.visibility });
    }


    const onSelectEstablishment = (establishment) => {
        if (establishment) {
            const { location, gmaps: { name, place_id, formatted_address,
                international_phone_number, photos, website, url } } = establishment;


            setToggleMap({ ...isMapShown, location, zoom: 10 });

            setHotel({
                ...hotel, name, place_id, url, website, location: JSON.stringify(location),
                address: formatted_address, phone_number: international_phone_number
            });


            setFetchRemoteImages({ ...fetchRemoteImages, photos });

            if (fetchRemoteImages.allowed) {
                ToastsStore.info('Fetching images, please wait...!', 2000);
                const images = [];

                var counter = 0;
                const caption = `c${auth_user.data.company_id}-${name.replace(/\s/g, String.apply())}`;

                const pictures = photos.filter((p, index) => index < 5);

                pictures.forEach(photo => {
                    Request.post(`utilities/fileuploader/download`, { url: photo.getUrl(), filepath: `google-${caption}-${counter++}-hotel-photo.jpg` })
                        .then(result => {
                            if (result.data.status === 'success') {
                                images.push(`/${result.data.path.replace('public/uploads/', '')}`);

                                if (counter == pictures.length) {
                                    setHotel({
                                        ...hotel, name, place_id, url, website, images, location: JSON.stringify(location),
                                        address: formatted_address, phone_number: international_phone_number
                                    });
                                    ToastsStore.info('Successfully images are fetched !', 2000);
                                }
                            }
                        }).catch(error => null);

                })
            }
        }



    }


    const onChangeImageFetch = e => {

        setFetchRemoteImages({ ...fetchRemoteImages, allowed: e.target.checked });

        if (e.target.checked) {
            if (fetchRemoteImages.photos && fetchRemoteImages.photos.length) {

                const photos = fetchRemoteImages.photos;

                ToastsStore.info('Fetching images, please wait...!', 2500);
                
                const images = [];

                var counter = 0;
                const caption = `c${auth_user.data.company_id}-${hotel.name.replace(/\s/g, String.apply())}`;

                const pictures = photos.filter((p, index) => index < 5);

                pictures.forEach(photo => {
                    Request.post(`utilities/fileuploader/download`, { url: photo.getUrl(), filepath: `google-${caption}-${counter++}-hotel-photo.jpg` })
                        .then(result => {
                            if (result.data.status === 'success') {
                                images.push(`/${result.data.path.replace('public/uploads/', '')}`);

                                if (counter == pictures.length) {
                                    setHotel({
                                        ...hotel, images
                                    });
                                    ToastsStore.info('Successfully images are fetched !', 2000);
                                }
                            }
                        }).catch(error => null);

                });

            }
        } else {

            if (hotel.images.length) {
                const googleimages = hotel.images.filter(img => img.indexOf('google') !== -1);

                if (googleimages.length) {
                    ToastsStore.info('Removing google images fetched', 2500);

                    setHotel({ ...hotel, images: [...hotel.images.filter(img => img.indexOf('google') === -1)] });

                    googleimages.forEach(img => {
                        Request.post(`utilities/fileuploader/destroy_resources`, { path: img });
                    })
                }
            }

        }
    }

    const formatField = field => {
        return (field[0].toUpperCase() + field.slice(1, field.length)).replace(/_/g, ' ');
    }

    const selectFileBulk = (e) => {

        if (e.target.files && e.target.files.length) {
            if ((
                e.target.files[0].type === "application/vnd.ms-excel" || e.target.files[0].type === "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")) {
                setBulk({
                    file: e.target.files[0], allowed: true,
                    format: e.target.files[0].type === "application/vnd.ms-excel" ? 'csv' : 'xlsx'
                });
            } else {
                ToastsStore.error("Please select .csv /.xlsx file format only");
                setBulk({ allowed: false, file: null });
            }
        } else {
            ToastsStore.error("Error selecting files");
            setBulk({ allowed: false, file: e.target.files[0] });
        }
    }

    const uploadBulk = () => {
        const fd = new FormData();
        fd.append('hotels', bulk.file);
        fd.append('company_id', auth_user.data.company_id);
        fd.append('format', bulk.format);

        Request.post(`/utilities/bulkuploader/hotel`, fd)
            .then(result => {
                if (result.data.status === 'success') {
                    ToastsStore.success("Hotel uploaded successfully");
                    setBulkDialog({ ...BulkDialog, hotels: result.data.hotels });

                    dispatch(HotelActions.fetchHotels(auth_user.data.company_id));
                } else {
                    ToastsStore.error("Something error occured!");
                }

            }).catch(error => {
                ToastsStore.error("Something error occured!");
            })
    }

    const { path, params } = props.match ? props.match : { path: null, params: null };

    return (
        <div className="analytics m-sm-30">

            {!props.modal &&
                <div className="mb-sm-30">
                    <Breadcrumb
                        routeSegments={[
                            { name: "Dashboard", path: "/dashboard" },
                            { name: "Hotels" }
                        ]}
                    />
                </div>
            }

            {(path === '/library/hotels/view/:id') ?

                list.find(c => c.hotel_id == params.id) ?

                    <React.Fragment>
                        {!!list.find(c => c.hotel_id == params.id).location &&
                            <GoogleMapComponent isMarkerShown defaultZoom={10} defaultCenter={JSON.parse(list.find(c => c.hotel_id == params.id).location)} />}
                        <Table style={{ border: '.5px solid' }}>
                            <TableRow>
                                <TableCell colspan="2" style={{ padding: '15px' }}>
                                    <FlexRow>
                                        <Button variant='outlined' onClick={() => history.push('/library/hotels')}>Back</Button>
                                        <ActionList del={CheckPermission("delete")} block={CheckPermission("edit")}
                                            id={list.find(c => c.hotel_id == params.id).hotel_id} deleteEvent={deleteEvent} blockEvent={blockEvent}
                                            status={list.find(c => c.hotel_id == params.id).status} />
                                    </FlexRow>
                                </TableCell>
                            </TableRow>
                            <TableRow>
                                <TableCell style={{ padding: '15px' }}>Name</TableCell>
                                <TableCell>{list.find(c => c.hotel_id == params.id).name}</TableCell>
                            </TableRow>
                            <TableRow>
                                <TableCell style={{ padding: '15px' }}>Country</TableCell>
                                <TableCell>
                                    {
                                        countries.find(cn => cn.country_id === list.find(c => c.hotel_id == params.id).country_id) ?
                                            countries.find(cn => cn.country_id === list.find(c => c.hotel_id == params.id).country_id).name : null
                                    }
                                </TableCell>
                            </TableRow>
                            <TableRow>
                                <TableCell style={{ padding: '15px' }}>City</TableCell>
                                <TableCell>
                                    {
                                        cities.find(cn => cn.city_id === list.find(c => c.hotel_id == params.id).city_id) ?
                                            cities.find(cn => cn.city_id === list.find(c => c.hotel_id == params.id).city_id).name : null
                                    }
                                </TableCell>
                            </TableRow>
                            <TableRow>
                                <TableCell style={{ padding: '15px' }}>Standard</TableCell>
                                <TableCell>{list.find(c => c.hotel_id == params.id).standard}</TableCell>
                            </TableRow>
                            <TableRow>
                                <TableCell style={{ padding: '15px' }}>Categories</TableCell>
                                <TableCell>{
                                    list.find(c => c.hotel_id == params.id).categories ?
                                        JSON.parse(list.find(c => c.hotel_id == params.id).categories).join() : null
                                }</TableCell>
                            </TableRow>
                            <TableRow>
                                <TableCell style={{ padding: '15px' }}>fetchHotelRooms</TableCell>
                                <TableCell>{
                                    list.find(c => c.hotel_id == params.id).rooms ?
                                        JSON.parse(list.find(c => c.hotel_id == params.id).rooms).join() : null
                                }</TableCell>
                            </TableRow>
                            <TableRow>
                                <TableCell style={{ padding: '15px' }}>Meals</TableCell>
                                <TableCell>{
                                    list.find(c => c.hotel_id == params.id).meals ?
                                        JSON.parse(list.find(c => c.hotel_id == params.id).meals).join() : null
                                }</TableCell>
                            </TableRow>
                            <TableRow>
                                <TableCell style={{ padding: '15px' }}>Address</TableCell>
                                <TableCell>{list.find(c => c.hotel_id == params.id).address}</TableCell>
                            </TableRow>
                            <TableRow>
                                <TableCell style={{ padding: '15px' }}>Checkin</TableCell>
                                <TableCell>{list.find(c => c.hotel_id == params.id).checkin}</TableCell>
                            </TableRow>
                            <TableRow>
                                <TableCell style={{ padding: '15px' }}>Checkout</TableCell>
                                <TableCell>{list.find(c => c.hotel_id == params.id).checkout}</TableCell>
                            </TableRow>
                            <TableRow>
                                <TableCell style={{ padding: '15px' }}>Phone Number</TableCell>
                                <TableCell>{list.find(c => c.hotel_id == params.id).phone_number}</TableCell>
                            </TableRow>
                            <TableRow>
                                <TableCell style={{ padding: '15px' }}>Fax Number</TableCell>
                                <TableCell>{list.find(c => c.hotel_id == params.id).fax_number}</TableCell>
                            </TableRow>
                            <TableRow>
                                <TableCell style={{ padding: '15px' }}>Contact Person</TableCell>
                                <TableCell>{list.find(c => c.hotel_id == params.id).contact_person}</TableCell>
                            </TableRow>
                            <TableRow>
                                <TableCell style={{ padding: '15px' }}>Contact Person Email</TableCell>
                                <TableCell>{list.find(c => c.hotel_id == params.id).contact_person_email}</TableCell>
                            </TableRow>
                            <TableRow>
                                <TableCell style={{ padding: '15px' }}>Contact Person Number</TableCell>
                                <TableCell>{list.find(c => c.hotel_id == params.id).contact_person_number}</TableCell>
                            </TableRow>
                            <TableRow>
                                <TableCell style={{ padding: '15px' }}>Website</TableCell>
                                <TableCell><a href={list.find(c => c.hotel_id == params.id).website}>{list.find(c => c.hotel_id == params.id).website}</a></TableCell>
                            </TableRow>
                            <TableRow>
                                <TableCell style={{ padding: '15px' }}>Google Map Link</TableCell>
                                <TableCell><a href={list.find(c => c.hotel_id == params.id).url}>Link</a></TableCell>
                            </TableRow>
                            <TableRow>
                                <TableCell style={{ padding: '15px' }}>Description</TableCell>
                                <TableCell>{list.find(c => c.hotel_id == params.id).description}</TableCell>
                            </TableRow>
                            <TableRow>
                                <TableCell style={{ padding: '15px' }}>Status</TableCell>
                                <TableCell>{list.find(c => c.hotel_id == params.id).status === 'A' ? 'Active' : 'Blocked'}</TableCell>
                            </TableRow>
                            <TableRow>
                                <TableCell style={{ padding: '15px' }}>Created On</TableCell>
                                <TableCell>{moment(list.find(c => c.hotel_id == params.id).createdAt).format('YYYY MMM DD, hh:mm:ss a ddd')}</TableCell>
                            </TableRow>
                            <TableRow>
                                <TableCell style={{ padding: '15px' }}>Last Updated On</TableCell>
                                <TableCell>{moment(list.find(c => c.hotel_id == params.id).updatedAt).format('YYYY MMM DD, hh:mm:ss a ddd')}</TableCell>
                            </TableRow>
                        </Table>
                    </React.Fragment> : <div className="no-matches">No matched item found !</div> :
                <React.Fragment>
                    <FlexRow>
                        <ButtonGroup>
                            <Button variant='outlined' onClick={makeToggleMap}>
                                <MapRounded />
                            </Button>
                        </ButtonGroup>

                        <Button variant='outlined' onClick={viewHotelPath.bind(null, '/library/hotels/standards')}>
                            Hotel Standards
                </Button>
                        <Button variant='outlined' onClick={viewHotelPath.bind(null, '/library/hotels/room-categories')}>
                            Room Categories
                </Button>
                        <Button variant='outlined' onClick={viewHotelPath.bind(null, '/library/hotels/room-types')}>
                            Room Types
                </Button>
                        <Button variant='outlined' onClick={viewHotelPath.bind(null, '/library/hotels/meal-types')}>
                            Meal Types
                </Button>
                        <ButtonGroup>
                            <Button variant='contained' color='secondary' onClick={() => {
                                setBulkDialog({ ...BulkDialog, open: 'open', country_id: null, city_id: null, fields: [] })
                            }}>Bulk Upload</Button>
                            <Button variant='outlined' onClick={toggleView}>
                                {(view === 'AddView' || view === 'EditView') ? 'All' : 'Add'} Hotels
                </Button>
                        </ButtonGroup>

                    </FlexRow>

                    {
                        isMapShown.visibility &&

                        <GoogleMapComponent isMarkerShown defaultZoom={isMapShown.zoom}
                            defaultCenter={isMapShown.location} />

                    }

                    <div style={{ marginTop: `20px`, marginBottom: `20px` }}>
                        <React.Fragment>

                            {(view === 'AddView' || view === 'EditView') &&
                                <ValidatorForm onSubmit={saveHotelForm}>

                                    <Grid container spacing={6}>
                                        <Grid md={3} sm={12} xs={12} item>
                                            <div className='add-image-button' onClick={onOpenGallery}>
                                                <Icon>camera</Icon> Add Gallery
                                    </div>
                                            <Switch value={fetchRemoteImages.allowed} onChange={onChangeImageFetch} />
                                            <span>Google images?</span>
                                        </Grid>
                                        <Grid md={9} sm={12} xs={12} item>

                                            <div className='gallery-images-list'>
                                                {
                                                    !!hotel.images &&
                                                    hotel.images.map(img => (
                                                        <React.Fragment>
                                                            <div className='gallery-item' style={{ background: `url(${img.indexOf('http') !== -1 ? img : process.env.REACT_APP_STATIC + img})`, backgroundSize: 'cover' }}>
                                                                <div className='gallery-remove-button' onClick={onRemoveGallery.bind(null, img)}>
                                                                    <Icon>close</Icon>
                                                                </div>
                                                            </div>
                                                        </React.Fragment>
                                                    ))
                                                }
                                            </div>

                                        </Grid>
                                    </Grid>


                                    <Grid container spacing={6}>
                                        <Grid md={4} sm={12} xs={12} item>
                                            <Dropdown
                                                placeholder='Country'
                                                fluid clearable search selection
                                                value={hotel.country_id ? hotel.country_id : ``}
                                                onChange={(event, { value }) => setHotel({ ...hotel, country_id: value })}
                                                options={countries.map((m, index) => {
                                                    return { key: index, text: m.name, value: m.country_id }
                                                })}
                                            />
                                        </Grid>
                                        <Grid md={4} sm={12} xs={12} item>
                                            <Dropdown
                                                placeholder='City'
                                                fluid clearable selection search
                                                value={hotel.city_id ? hotel.city_id : ``}
                                                onChange={(event, { value }) => setHotel({ ...hotel, city_id: value })}
                                                options={hotel.country_id ? cities.filter(c => c.country_id === hotel.country_id).map((m, index) => {
                                                    return { key: index, text: m.name, value: m.city_id }
                                                }) : []}
                                            />
                                        </Grid>
                                        <Grid md={4} sm={12} xs={12} item>
                                            <GooglePlacesSuggest
                                                country={countries.find(c => c.country_id === hotel.country_id) ?
                                                    countries.find(c => c.country_id === hotel.country_id).code : 'LK'}
                                                types={['establishment']}
                                                placeholder={hotel.name ? hotel.name : 'Search Hotel'}
                                                disabled={!(hotel.country_id || hotel.city_id)}
                                                onClick={() => (
                                                    (hotel.country_id || hotel.city_id) ? null :
                                                        window.alert("Please select country and city first !")
                                                )}
                                                onSuggestSelect={onSelectEstablishment} />
                                        </Grid>
                                    </Grid>

                                    <Grid container spacing={6}>
                                        <Grid md={3} sm={12} xs={12} item>
                                            <TextValidator
                                                className="mb-16 w-100"
                                                variant="outlined"
                                                label="Address"
                                                type="text"
                                                value={hotel.address ? hotel.address : ``}
                                                onChange={onChangeField}
                                                name="address"
                                            />
                                        </Grid>
                                        <Grid md={3} sm={12} xs={12} item>
                                            <TextValidator
                                                className="mb-16 w-100"
                                                variant="outlined"
                                                label="Phone Number"
                                                type="text"
                                                value={hotel.phone_number ? hotel.phone_number : ``}
                                                onChange={onChangeField}
                                                name="phone_number"
                                            />
                                        </Grid>
                                        <Grid md={3} sm={12} xs={12} item>
                                            <TextValidator
                                                className="mb-16 w-100"
                                                variant="outlined"
                                                label="Fax Number"
                                                type="text"
                                                value={hotel.fax_number ? hotel.fax_number : ``}
                                                onChange={onChangeField}
                                                name="fax_number"
                                            />
                                        </Grid>
                                        <Grid md={3} sm={12} xs={12} item>
                                            <TextValidator
                                                className="mb-16 w-100"
                                                variant="outlined"
                                                label="Website"
                                                type="text"
                                                value={hotel.website ? hotel.website : ``}
                                                onChange={onChangeField}
                                                name="website"
                                            />
                                        </Grid>
                                    </Grid>
                                    <Grid container spacing={6}>
                                        <Grid md={4} sm={12} xs={12} item>
                                            <TextValidator
                                                className="mb-16 w-100"
                                                variant="outlined"
                                                label="Contact Person"
                                                type="text"
                                                value={hotel.contact_person ? hotel.contact_person : ``}
                                                onChange={onChangeField}
                                                name="contact_person"
                                            />
                                        </Grid>
                                        <Grid md={4} sm={12} xs={12} item>
                                            <TextValidator
                                                className="mb-16 w-100"
                                                variant="outlined"
                                                label="Contact Person Number"
                                                type="text"
                                                value={hotel.contact_person_number ? hotel.contact_person_number : ``}
                                                onChange={onChangeField}
                                                name="contact_person_number"
                                            />
                                        </Grid>
                                        <Grid md={4} sm={12} xs={12} item>
                                            <TextValidator
                                                className="mb-16 w-100"
                                                variant="outlined"
                                                label="Contact Person Email"
                                                type="text"
                                                value={hotel.contact_person_email ? hotel.contact_person_email : ``}
                                                onChange={onChangeField}
                                                name="contact_person_email"
                                            />
                                        </Grid>
                                    </Grid>
                                    <Grid container spacing={6}>
                                        <Grid md={6} sm={12} xs={12} item>
                                            <TextValidator
                                                className="mb-16 w-100"
                                                variant="outlined"
                                                label="Check In"
                                                type="text"
                                                value={hotel.checkin ? hotel.checkin : ``}
                                                onChange={onChangeField}
                                                name="checkin"
                                            />
                                        </Grid>
                                        <Grid md={6} sm={12} xs={12} item>
                                            <TextValidator
                                                className="mb-16 w-100"
                                                variant="outlined"
                                                label="Check Out"
                                                type="text"
                                                value={hotel.checkout ? hotel.checkout : ``}
                                                onChange={onChangeField}
                                                name="checkout"
                                            />
                                        </Grid>
                                    </Grid>
                                    <Divider horizontal section>
                                        <Icon>menu</Icon>
                                        <span className='divider-header'>Hotel Status Informations</span>
                                    </Divider>

                                    <Grid container spacing={6}>

                                        <Grid md={3} sm={12} xs={12} item>
                                            <Dropdown
                                                placeholder='Hotel Standard'
                                                fluid search clearable selection
                                                value={hotel.standard ? hotel.standard : ``}
                                                onChange={(event, { value }) => setHotel({ ...hotel, standard: value })}
                                                options={standards.map((m, index) => {
                                                    return { key: index, text: m.name, value: m.name }
                                                })}
                                            />
                                        </Grid>

                                        <Grid md={3} sm={12} xs={12} item>
                                            <Dropdown
                                                placeholder='Room Category'
                                                fluid selection
                                                multiple clearable basic search
                                                value={hotel.categories ? JSON.parse(hotel.categories) : []}
                                                onChange={(event, { value }) => setHotel({ ...hotel, categories: JSON.stringify(value) })}
                                                options={categories.map((m, index) => {
                                                    return { key: index, text: m.name, value: m.name }
                                                })}
                                            />
                                        </Grid>

                                        <Grid md={3} sm={12} xs={12} item>
                                            <Dropdown
                                                placeholder='Types Of Room'
                                                fluid selection
                                                multiple clearable basic search
                                                value={hotel.rooms ? JSON.parse(hotel.rooms) : []}
                                                onChange={(event, { value }) => setHotel({ ...hotel, rooms: JSON.stringify(value) })}
                                                options={rooms.map((m, index) => {
                                                    return { key: index, text: m.name, value: m.name }
                                                })}
                                            />
                                        </Grid>


                                        <Grid md={3} sm={12} xs={12} item>
                                            <Dropdown
                                                placeholder='Types Of Meals'
                                                fluid selection
                                                multiple clearable basic search
                                                value={hotel.meals ? JSON.parse(hotel.meals) : []}
                                                onChange={(event, { value }) => setHotel({ ...hotel, meals: JSON.stringify(value) })}
                                                options={meals.map((m, index) => {
                                                    return { key: index, text: m.name, value: m.name }
                                                })}
                                            />
                                        </Grid>
                                    </Grid>

                                    <Grid container spacing={6}>
                                        <Grid md={12} sm={12} xs={12} item>
                                            <TextField variant='outlined' placeholder="Hotel Description"
                                                style={{ width: '100%', ['margin-bottom']: '15px' }} inputProps={{ maxLength: 275 }}
                                                multiline rowsMax={5} onChange={(e) => setHotel({ ...hotel, description: e.target.value })}
                                                defaultValue={hotel.description ? hotel.description : ``} />
                                            <span>* 275 characters ({hotel.description ? hotel.description.length : 0})</span>
                                        </Grid>
                                    </Grid>

                                    <Divider horizontal clearing />

                                    <Button
                                        variant="contained"
                                        color="primary"
                                        type="submit">{view === 'AddView' ? 'Save' : 'Update'} Hotel</Button>

                                </ValidatorForm>
                            }

                            {view === 'ManageView' &&
                                <TableMD
                                    columns={['Name', 'Country | City', 'Standard', 'Contact Person', {
                                        label: 'Action', options: {
                                            sort: false, filter: false
                                        }
                                    }]}
                                    options={{
                                        responsive: 'scrollMaxHeight',
                                        selectableRows: !props.modal
                                    }}
                                    title='Hotels'
                                    data={hotels_list}
                                />
                            }
                        </React.Fragment>
                    </div>
                </React.Fragment>
            }

            {isUploadOpen &&
                <Widget onCloseHandler={() => setUploadOpen(!isUploadOpen)} onCompleteHandler={onCompleteHandler} />
            }

            <Dialog
                open={BulkDialog.open === 'open'} disableBackdropClick fullWidth>
                <DialogTitle>Select Places & Fields</DialogTitle>
                <DialogContent>

                    {BulkDialog.hotels ?
                        <div className='upload-success'>{BulkDialog.hotels.length} Hotel/s uploaded !</div> :
                        <React.Fragment>
                            <Grid spacing="5" container>
                                <Grid md="6" sm={12} xs={12} item>
                                    <Dropdown
                                        placeholder='Select Country'
                                        fluid clearable selection search
                                        value={BulkDialog.country_id}
                                        onChange={(event, { value }) => setBulkDialog({ ...BulkDialog, country_id: value })}
                                        options={countries.map((country, index) => {
                                            return { key: index, text: country.name, value: country.country_id }
                                        })}
                                    />
                                </Grid>
                                <Grid md="6" sm={12} xs={12} item>
                                    <Dropdown
                                        placeholder='Select City'
                                        fluid clearable search selection
                                        value={BulkDialog.city_id} disabled={BulkDialog.country_id === null}
                                        onChange={(event, { value }) => setBulkDialog({ ...BulkDialog, city_id: value })}
                                        options={cities.filter(c => c.country_id === BulkDialog.country_id).map((city, index) => {
                                            return { key: index, text: city.name, value: city.city_id }
                                        })}
                                    />
                                </Grid>
                            </Grid>

                            <Divider clearing />

                            <FormGroup row>
                                {
                                    ["name", "standard", "categories", "rooms", "meals", "address", "checkin", "checkout",
                                        "phone_number", "fax_number", "contact_person", "contact_person_number", "contact_person_email",
                                        "website"].map((field, key) => {
                                            return (
                                                <FormControlLabel key={key}
                                                    control={<Checkbox checked={!!BulkDialog.fields.find(f => f === field)} onChange={({ target: { checked } }) => {
                                                        const fields = BulkDialog.fields;

                                                        if (checked) {
                                                            fields.splice(key, 1, field);
                                                        } else {
                                                            fields.splice(fields.findIndex(f => f === field), 1);
                                                        }
                                                        setBulkDialog({ ...BulkDialog, fields })
                                                    }} name={field} />}
                                                    label={formatField(field)}
                                                />
                                            )
                                        })
                                }
                            </FormGroup>

                            <Divider clearing section />

                            <FlexRow>
                                {
                                    (BulkDialog.country_id && BulkDialog.city_id && !!BulkDialog.fields.length) &&
                                    <CSVLink filename={"hotel-bulk-upload.csv"} className="download-button" data={[
                                        [...['id (Optional ?)', 'country_id', 'city_id'], ...BulkDialog.fields],
                                        [1, BulkDialog.country_id, BulkDialog.city_id, ...BulkDialog.fields.map(field => {
                                            switch (field) {
                                                case 'name':
                                                    return "Hilton Hotel (Sample)";
                                                case 'standard':
                                                    return `One Of ${standards.map(s => s.name).join('|')}`;
                                                case 'categories':
                                                    return `[${categories.map(c => c.name).join(',')}]`;
                                                case 'rooms':
                                                    return `[${rooms.map(r => r.name).join(',')}]`;
                                                case 'meals':
                                                    return `[${meals.map(m => m.name).join(',')}]`;
                                                case 'address':
                                                    return "Sea street, Colombo 1";
                                                case 'checkin':
                                                    return "10:20 AM";
                                                case 'checkout':
                                                    return "12:30 PM";
                                                case 'address':
                                                    return "Sea street, Colombo 1";
                                                case 'phone_number':
                                                    return "0094 11272635";
                                                case 'fax_number':
                                                    return "0094117235735";
                                                case 'contact_person':
                                                    return "Ravi Kanth";
                                                case 'contact_person_number':
                                                    return "0770000000";
                                                case 'contact_person_email':
                                                    return "testcontact@gmail.com";
                                                case 'website':
                                                    return "www.hilton.com";
                                                default: return "Sample Field Text";
                                            }
                                        })]
                                    ]} target="_blank">Download CSV</CSVLink>
                                }
                                <FormGroup row>
                                    <ButtonGroup>
                                        <TextField type='file' name='bulk' variant='outlined' onChange={selectFileBulk} />
                                        {bulk.allowed && <Button variant='contained' color='primary' onClick={uploadBulk}>Upload</Button>}
                                    </ButtonGroup>
                                </FormGroup>

                            </FlexRow>
                        </React.Fragment>}
                </DialogContent>
                <DialogActions>
                    <Button variant='outlined' onClick={BulkDialog.closeView}>Cancel</Button>
                </DialogActions>
            </Dialog>


        </div >
    );
}

export default withStyles({}, { withTheme: true })(Hotels);
