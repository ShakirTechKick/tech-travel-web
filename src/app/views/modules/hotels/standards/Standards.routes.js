import { EgretLoadable } from "egret";
import { authRoles } from "../../../../auth/authRoles";

const HotelStandards = EgretLoadable({
  loader: () => import("./Standards.component")
});

const hotelsRoutes = [
  {
    name: 'Hotels',
    path: "/library/hotels/standards",
    component: HotelStandards,
    exact: true,
    auth: authRoles.admin,
    access: 'AUTH'
  }
];

export default hotelsRoutes;
