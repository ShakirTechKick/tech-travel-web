import React, { useState, useEffect } from "react";
import { ButtonGroup, Button, MenuItem, Grid, Icon } from "@material-ui/core";
import { Breadcrumb } from "egret";
import { withStyles } from "@material-ui/styles";
import { useDispatch, useSelector } from "react-redux";
import ActionList from "app/helpers/ActionList";
import { Dropdown, Label, Divider } from "semantic-ui-react";
import { ToastsStore } from "react-toasts";
import history from '../../../../../history';
import * as HotelActions from '../../../../redux/actions/modules/Hotels.action';
import FlexRow from "app/helpers/FlexRow";
import { ValidatorForm, TextValidator } from "react-material-ui-form-validator";


const Standards = (props) => {

    const dispatch = useDispatch();

    const { user: auth_user } = useSelector(state => state.user);

    const [standard, setStandard] = useState({
        company_id: auth_user.data.company_id
    });


    const { standards } = useSelector(state => state.hotels);

    const [edit, setEdit] = useState([]);

    useEffect(() => {
        dispatch(HotelActions.fetchHotelStandards(auth_user.data.company_id));
    }, [])

    useEffect(() => {
        if (!edit.length) {
            if (standards.length) {
                setEdit([...standards.map(item => {
                    return {
                        company_id: auth_user.data.company_id,
                        standard_id: item.standard_id,
                        name: item.name,
                        description: item.description
                    }
                })])
            }
        }
    }, [standards, edit])


    const blockEvent = (id) => {
        dispatch(HotelActions.statusHotelStandard({ standard_id: id }))
    };

    const CheckPermission = (type) => {

        var module_exists = false;
        var action_permission = false;

        auth_user.permissions.forEach(module => {
            if (Object.keys(module)[0] === "Hotels") {
                module_exists = true;
                if (module['Hotels'][type] === "ALLOWED") {
                    action_permission = true;
                }
            }
        });

        if (module_exists && action_permission) {
            return true;
        }

        return false;

    }


    const onChangeField = (e) => {
        setStandard({ ...standard, [e.target.name]: e.target.value });
    }

    const onChangeFieldEdit = (key, e) => {

        const array = Object.assign([], edit);
        const item = edit[key];
        const updated = { ...item, [e.target.name]: e.target.value };

        array.splice(key, 1, updated);

        setEdit([...array]);
    }

    const saveForm = () => {
        dispatch(HotelActions.addHotelStandard({ ...standard }));
    }

    const updateForm = (key) => {

        if (edit[key].name.length) {
            dispatch(HotelActions.updateHotelStandard({ ...edit[key] }));
        } else {
            ToastsStore.error('Name cannot be empty !', 2000);
        }
    }

    const deleteForm = (id) => {
        dispatch(HotelActions.removeHotelStandard({ standard_id: id }))
    };

    const viewHotels = () => {
        history.push({ pathname: '/library/hotels' });
    }

    return (
        <div className="analytics m-sm-30">
            <div className="mb-sm-30">
                <Breadcrumb
                    routeSegments={[
                        { name: "Dashboard", path: "/dashboard" },
                        { name: "Hotel Standards" }
                    ]}
                />
            </div>

            <FlexRow>
                <Button variant='outlined' onClick={viewHotels}>
                    View Hotels
                </Button>
            </FlexRow>

            {CheckPermission('write') &&
                <ValidatorForm onSubmit={saveForm}>

                    <Grid container style={{ marginTop: '20px' }} spacing={2}>
                        <Grid md={4} sm={12} xs={12} item>
                            <TextValidator
                                className="mb-16 w-100"
                                variant="outlined"
                                label="Name"
                                type="text"
                                value={standard.name ? standard.name : ``}
                                onChange={onChangeField}
                                name="name"
                                validators={["required"]}
                                errorMessages={[
                                    "Name is required"
                                ]}
                            />
                        </Grid>
                        <Grid md={6} sm={12} xs={12} item>
                            <TextValidator
                                className="mb-16 w-100"
                                variant="outlined"
                                label="Description"
                                type="text"
                                value={standard.description ? standard.description : ``}
                                onChange={onChangeField}
                                name="description"
                            />
                        </Grid>
                        <Grid md={2} sm={12} xs={12} item>
                            <Button variant='outlined' className='row-button' type='submit'>
                                Add
                        </Button>
                        </Grid>
                    </Grid>
                </ValidatorForm>
            }

            <Divider horizontal section>
                <Icon>menu</Icon>
                <span className='divider-header'>Hotel Standard List</span>
            </Divider>

            {
                !!standards.length &&

                standards.map((std, index) => (
                    <ValidatorForm key={std.standard_id} onSubmit={updateForm.bind(null, index)}>

                        <Grid container style={{ marginTop: '20px' }} spacing={2}>
                            <Grid md={4} sm={12} xs={12} item>
                                <TextValidator
                                    className="mb-16 w-100"
                                    variant="outlined"
                                    label="Name"
                                    type="text"
                                    defaultValue={std.name}
                                    onChange={onChangeFieldEdit.bind(null, index)}
                                    name="name"
                                />
                            </Grid>
                            <Grid md={6} sm={12} xs={12} item>
                                <TextValidator
                                    className="mb-16 w-100"
                                    variant="outlined"
                                    label="Description"
                                    type="text"
                                    defaultValue={std.description}
                                    onChange={onChangeFieldEdit.bind(null, index)}
                                    name="description"
                                />
                            </Grid>
                            {CheckPermission('edit') &&
                                <Grid md={1} sm={12} xs={12} item>
                                    <Button variant='outlined' color='primary' className='row-button' type='submit'>
                                        Update
                                </Button>
                                </Grid>
                            }
                            {CheckPermission('delete') &&
                                <Grid md={1} sm={12} xs={12} item>
                                    <Button onClick={deleteForm.bind(null, std.standard_id)} variant='outlined' color='secondary' className='row-button' type='button'>
                                        Del
                                </Button>
                                </Grid>
                            }
                        </Grid>
                    </ValidatorForm>

                ))
            }

        </div >
    );
}

export default withStyles({}, { withTheme: true })(Standards);
