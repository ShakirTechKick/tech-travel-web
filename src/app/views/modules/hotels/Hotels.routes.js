import { EgretLoadable } from "egret";
import { authRoles } from "../../../auth/authRoles";

const Hotels = EgretLoadable({
  loader: () => import("./Hotels.component")
});

const hotelsRoutes = [
  {
    name: 'Hotels',
    path: "/library/hotels",
    exact: true,
    component: Hotels,
    auth: authRoles.admin,
    access: 'AUTH'
  },
  {
    name: 'Hotels',
    path: "/library/hotels/view/:id",
    exact: true,
    component: Hotels,
    auth: authRoles.admin,
    access: 'AUTH'
  }
];

export default hotelsRoutes;
