import { EgretLoadable } from "egret";
import { authRoles } from "../../../auth/authRoles";

const Company = EgretLoadable({
  loader: () => import("./Company.component")
});

const companyRoutes = [
  {
    name: 'Settings',
    path: "/settings",
    exact: true,
    component: Company,
    auth: authRoles.admin,
    access: 'AUTH'
  }
];

export default companyRoutes;
