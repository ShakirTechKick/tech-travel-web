import React, { Component, Fragment, useRef, useState, useEffect } from "react";
import { ValidatorForm, TextValidator, SelectValidator } from "react-material-ui-form-validator";
import { Button, MenuItem, Grid, makeStyles, Table, TableBody, TableRow, TableCell, Dialog, DialogTitle, DialogContent, DialogActions, ButtonGroup, IconButton, TextField, Switch } from "@material-ui/core";
import CameraIcon from '@material-ui/icons/CameraAltTwoTone';
import { Breadcrumb } from "egret";
import { withStyles } from "@material-ui/styles";
import { useDispatch, useSelector } from "react-redux";
import { fetchCompanies, updateCompany } from "app/redux/actions/modules/Company.actions";
import TableMD from "mui-datatables";
import ActionList from "app/helpers/ActionList";
import { Dropdown, Label, Divider } from "semantic-ui-react";
import { ToastsStore } from "react-toasts";
import Widget from 'app/helpers/TravUploadWidget';
import PropTypes from 'prop-types';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Box from '@material-ui/core/Box';
import JoditEditor from "jodit-react";
import FlexRow from "app/helpers/FlexRow";
import { CancelOutlined } from "@material-ui/icons";
import AceEditor from "react-ace";//Ace Editor
import "ace-builds/src-noconflict/mode-handlebars";
import "ace-builds/src-noconflict/theme-github";

import { fetchEmailSettings, updateEmailSettings } from 'app/redux/actions/modules/Settings.action';

function TabPanel(props) {
    const { children, value, index, ...other } = props;

    return (
        <div
            role="settings-panel"
            hidden={value !== index}
            id={`scrollable-auto-tabpanel-${index}`}
            aria-labelledby={`scrollable-auto-tab-${index}`}
            {...other}
        >
            {value === index && (
                <Box p={3}>{children}</Box>
            )}
        </div>
    );
}

TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.any.isRequired,
    value: PropTypes.any.isRequired,
};

function PropsSetting(index) {
    return {
        id: `scrollable-auto-tab-${index}`,
        'aria-controls': `scrollable-auto-tabpanel-${index}`
    };
}

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
        width: '100%',
        backgroundColor: theme.palette.background.paper
    }
}));

const Company = (props) => {

    const dispatch = useDispatch();


    const classes = useStyles();
    const [tab, setTab] = React.useState(0);

    const handleChangeTab = (event, tab) => {
        setTab(tab);
    };

    const { user: auth_user } = useSelector(state => state.user);
    const { companies } = useSelector(state => state.company);

    const [isUploadOpen, setUploadOpen] = useState(false);

    const [company, setCompany] = useState(companies.length ?
        companies.find(c => c.company_id === auth_user.data.company_id) : {});

    const [content, setContent] = useState(company ? company.content : '');

    const [view, setView] = useState('ManageView');


    const { email_settings } = useSelector(state => state.settings);

    useEffect(() => {
        dispatch(fetchCompanies((companies) => {
            if (companies.find(c => c.company_id === auth_user.data.company_id)) {
                setCompany(companies.find(c => c.company_id === auth_user.data.company_id));
            }
        }));

        dispatch(fetchEmailSettings(auth_user.data.company_id));

    }, [])



    const CheckPermission = (type) => {

        var module_exists = false;
        var action_permission = false;

        auth_user.permissions.forEach(module => {
            if (Object.keys(module)[0] === "Users") {
                module_exists = true;
                if (module['Users'][type] === "ALLOWED") {
                    action_permission = true;
                }
            }
        });

        if (module_exists && action_permission) {
            return true;
        }

        return false;

    }


    const updateCompanyDetails = () => {
        if (CheckPermission('edit')) {
            dispatch(updateCompany({ ...company }));
        } else {
            ToastsStore.error("Your're not given permission to update company settings", 2000);
        }

    }

    const onChangeField = (e) => {
        setCompany({ ...company, [e.target.name]: e.target.value });
    }


    const onCompleteHandler = (id, name, response) => {
        if (response.success) {
            setCompany({ ...company, logo: `${response.path}/${name}` });
        }
    }

    const updateAboutContent = () => {
        dispatch(updateCompany({ ...company, content }, () => {
            setCompany({ ...company, content });
        }));
    }

    useEffect(() => {
        if (company) {
            setContent(company.content);
        }
    }, [company])


    const [settings, setSettings] = useState({
        send_itinerary: null
    })

    const [updateDialog, setUpdateDialog] = useState({
        open: 'closed', closeView: () => setUpdateDialog({ ...updateDialog, open: 'closed' }),
        view: 'Code', heading: 'Edit Email Setting', from_address: null, from_name: null, subject: null, body: `<html></html`
    })

    const onChangeCodeBlock = (body) => {
        setUpdateDialog({ ...updateDialog, body })
    }

    useEffect(() => {

        if (email_settings && email_settings.find(s => s.setting === 'SEND ITINERARY')) {
            setSettings({ ...settings, send_itinerary: email_settings.find(s => s.setting === 'SEND ITINERARY') })
        }

    }, [email_settings])


    const saveSettings = () => {
        dispatch(updateEmailSettings({
            email_setting_id: updateDialog.email_setting_id ? updateDialog.email_setting_id : null,
            company_id: auth_user.data.company_id,
            from_address: updateDialog.from_address,
            from_name: updateDialog.from_name,
            subject: updateDialog.subject,
            body: updateDialog.body
        }))
    }

    return (
        <div className="analytics m-sm-30">
            <div className="mb-sm-30">
                <Breadcrumb
                    routeSegments={[
                        { name: "Dashboard", path: "/dashboard" },
                        { name: "Company" }
                    ]}
                />
            </div>
            <div className={classes.root}>
                <AppBar position="static" color="default">
                    <Tabs
                        value={tab}
                        onChange={handleChangeTab}
                        indicatorColor="secondary"
                        textColor="primary"
                        variant="scrollable"
                        scrollButtons="auto"
                        aria-label="Setting Scrollable Tabs"
                    >
                        <Tab label="Profile Settings" {...PropsSetting(0)} />
                        <Tab label="About Content" {...PropsSetting(1)} />
                        <Tab label="Email Settings" {...PropsSetting(2)} />
                    </Tabs>
                </AppBar>
                <TabPanel value={tab} index={0}>

                    <ValidatorForm onSubmit={updateCompanyDetails}>
                        <Grid container spacing={6} style={{textAlign: 'center'}}>
                            <div onClick={() => setUploadOpen(true)}>
                                {
                                    !!company.logo ?
                                        <div className="logo-container">
                                            <img style={{ width: 200 }} src={process.env.REACT_APP_STATIC + company.logo} />
                                        </div> : <div className="logo-container"><CameraIcon /></div>
                                }
                            </div>
                        </Grid>

                        <Grid container spacing={6}>
                            <Grid md={6} sm={12} xs={12} item>
                                <TextValidator
                                    className="mb-16 w-100"
                                    variant="outlined"
                                    label="Name"
                                    type="text"
                                    value={company.name ? company.name : ``}
                                    onChange={onChangeField}
                                    name="name"
                                    validators={["required"]}
                                    errorMessages={[
                                        "Name is required"
                                    ]}
                                />
                            </Grid>
                            <Grid md={6} sm={12} xs={12} item>
                                <TextValidator
                                    className="mb-16 w-100"
                                    variant="outlined"
                                    label="Email ID"
                                    type="text"
                                    value={company.email ? company.email : ``}
                                    onChange={onChangeField}
                                    name="email"
                                    validators={["required", "isEmail"]}
                                    errorMessages={[
                                        "Email is required",
                                        "Email is not valid"
                                    ]}
                                />
                            </Grid>
                        </Grid>
                        <Grid container spacing={6}>
                            <Grid md={4} sm={12} xs={12} item>
                                <TextValidator
                                    className="mb-16 w-100"
                                    variant="outlined"
                                    label="Phone Number"
                                    type="text"
                                    value={company.phone_number ? company.phone_number : ``}
                                    onChange={onChangeField}
                                    name="phone_number"
                                />
                            </Grid>
                            <Grid md={4} sm={12} xs={12} item>
                                <TextValidator
                                    className="mb-16 w-100"
                                    variant="outlined"
                                    label="Fax Number"
                                    type="text"
                                    value={company.fax_number ? company.fax_number : ``}
                                    onChange={onChangeField}
                                    name="fax_number"
                                />
                            </Grid>
                            <Grid md={4} sm={12} xs={12} item>
                                <TextValidator
                                    className="mb-16 w-100"
                                    variant="outlined"
                                    label="Address"
                                    type="text"
                                    value={company.address ? company.address : ``}
                                    onChange={onChangeField}
                                    name="address"
                                />
                            </Grid>
                        </Grid>
                        <Divider clearing horizontal>
                            <div>Social Media Links</div>
                        </Divider>
                        <Grid container spacing={6}>
                            <Grid md={6} sm={12} xs={12} item>
                                <TextValidator
                                    className="mb-16 w-100"
                                    variant="outlined"
                                    label="Website URL"
                                    type="text"
                                    value={company.web ? company.web : ``}
                                    onChange={onChangeField}
                                    name="web"
                                />
                            </Grid>
                            <Grid md={6} sm={12} xs={12} item>
                                <TextValidator
                                    className="mb-16 w-100"
                                    variant="outlined"
                                    label="Facebook"
                                    type="text"
                                    value={company.facebook ? company.facebook : ``}
                                    onChange={onChangeField}
                                    name="facebook"
                                />
                            </Grid>
                            <Grid md={6} sm={12} xs={12} item>
                                <TextValidator
                                    className="mb-16 w-100"
                                    variant="outlined"
                                    label="LinkedIn"
                                    type="text"
                                    value={company.linkedin ? company.linkedin : ``}
                                    onChange={onChangeField}
                                    name="linkedin"
                                />
                            </Grid>
                            <Grid md={6} sm={12} xs={12} item>
                                <TextValidator
                                    className="mb-16 w-100"
                                    variant="outlined"
                                    label="Instagram"
                                    type="text"
                                    value={company.instagram ? company.instagram : ``}
                                    onChange={onChangeField}
                                    name="instagram"
                                />
                            </Grid>
                        </Grid>
                        <Button
                            variant="contained"
                            color="primary"
                            type="submit">{view === 'AddView' ? 'Save' : 'Update'} Company</Button>

                    </ValidatorForm>

                </TabPanel>
                <TabPanel value={tab} index={1}>

                    <ValidatorForm onSubmit={updateAboutContent}>

                        <Grid container spacing={6}>
                            <JoditEditor
                                value={content}
                                onChange={html => setContent(html)}
                            />
                        </Grid>

                        <Divider clearing />

                        <Button
                            disabled={company.content === content}
                            variant="outlined"
                            type="submit">Update About Content</Button>

                    </ValidatorForm>

                </TabPanel>
                <TabPanel value={tab} index={2}>

                    <Table style={{ width: '100%' }} cellPadding="15">
                        <TableBody>
                            <TableRow>
                                <TableCell><Button style={{ color: 'blue', fontWeight: 'bold' }} onClick={() => setUpdateDialog({
                                    ...updateDialog, open: 'open', heading: 'Send Itinerary', setting: 'SEND ITINERARY',
                                    from_address: settings.send_itinerary ? settings.send_itinerary.from_address : null,
                                    from_name: settings.send_itinerary ? settings.send_itinerary.from_name : null,
                                    subject: settings.send_itinerary ? settings.send_itinerary.subject : null,
                                    body: settings.send_itinerary ? settings.send_itinerary.body : ``,
                                    email_setting_id: settings.send_itinerary ? settings.send_itinerary.email_setting_id : null,
                                })}>Send Itinerary</Button></TableCell>
                                <TableCell align='center' style={{ fontSize: '15px' }}>Sent to the customer when sharing an itinerary with them</TableCell>
                                <TableCell align='right'><span style={{ fontWeight: 'bold', color: 'green' }}>Enabled</span></TableCell>
                            </TableRow>
                        </TableBody>
                    </Table>

                    <Dialog open={updateDialog.open === 'open'} fullScreen>
                        <DialogTitle className='dialog-title-company'>
                            <FlexRow>
                                <div>{updateDialog.heading}</div>
                                <div>
                                    <IconButton onClick={updateDialog.closeView}>
                                        <CancelOutlined />
                                    </IconButton>
                                </div>
                            </FlexRow>
                        </DialogTitle>
                        <DialogContent>
                            <Grid spacing='5' container>
                                <Grid md='4' sm='12' xs={12} item>
                                    <Grid md='12' sm='12' xs={12} item>
                                        <TextField
                                            className="mb-16 w-100"
                                            variant="outlined"
                                            label="From Address"
                                            type="text"
                                            value={updateDialog.from_address}
                                            onChange={({ target: { value: from_address } }) => setUpdateDialog({ ...updateDialog, from_address })}
                                        />
                                    </Grid>
                                    <Grid md='12' sm='12' xs={12} item>
                                        <TextField
                                            className="mb-16 w-100"
                                            variant="outlined"
                                            label="From Name"
                                            type="text"
                                            value={updateDialog.from_name}
                                            onChange={({ target: { value: from_name } }) => setUpdateDialog({ ...updateDialog, from_name })}
                                        />
                                    </Grid>
                                    <Grid md='12' sm='12' xs={12} item>
                                        <TextField
                                            className="mb-16 w-100"
                                            variant="outlined"
                                            label="Subject"
                                            type="text"
                                            value={updateDialog.subject}
                                            onChange={({ target: { value: subject } }) => setUpdateDialog({ ...updateDialog, subject })}
                                        />
                                    </Grid>
                                </Grid>
                                <Grid md='8' sm='12' xs={12} item>
                                    Code <Switch value={updateDialog.view === 'Preview'} onChange={(e) => setUpdateDialog({ ...updateDialog, view: !e.target.checked ? 'Code' : 'Preview' })} /> Preview

                                    {updateDialog.view === 'Preview' ?
                                        <div className="code-preview" dangerouslySetInnerHTML={{ __html: updateDialog.body }}></div> :
                                        <AceEditor
                                            mode="handlebars"
                                            theme="github"
                                            onChange={onChangeCodeBlock} wrapEnabled
                                            placeholder="Handlebar with HTML"
                                            width="100%" name="Body" fontSize="20"
                                            value={updateDialog.body}
                                            editorProps={{ $blockScrolling: true }}
                                        />
                                    }
                                </Grid>
                            </Grid>
                        </DialogContent>
                        <DialogActions>
                            <ButtonGroup>
                                <Button variant='outlined' style={{ color: 'green' }} onClick={saveSettings}>Update Setting</Button>
                                <Button variant='outlined' style={{ color: 'red' }} onClick={updateDialog.closeView}>Cancel</Button>
                            </ButtonGroup>
                        </DialogActions>
                    </Dialog>

                </TabPanel>
            </div>


            {
                isUploadOpen &&
                <Widget onCloseHandler={() => setUploadOpen(!isUploadOpen)} onCompleteHandler={onCompleteHandler} />
            }

        </div >
    );
}

export default withStyles({}, { withTheme: true })(Company);
