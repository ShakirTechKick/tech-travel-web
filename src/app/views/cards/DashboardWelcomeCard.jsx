import React from "react";
import { Card, Icon, Fab, withStyles } from "@material-ui/core";
import { useEffect } from "react";
import moment from "moment";
import FlexRow from "app/helpers/FlexRow";

const styles = theme => ({
  root: {
    background: `url("/assets/images/dots.png"),
    linear-gradient(90deg, ${theme.palette.primary.main} -19.83%, ${theme.palette.primary.light} 189.85%)`,
    backgroundRepeat: "no-repeat",
    backgroundSize: "100%"
  }
});

const DashboardWelcomeCard = ({ classes, user }) => {

  var interval = null;

  const [stamp, setStamp] = React.useState(new Date());

  useEffect(() => {

    interval = setInterval(() => {
      setStamp(new Date());
    }, 1000);

    return () => {
      if (interval) clearInterval(interval);
    }
  }, [])


  const greeting = m => {
    var g = null;

    if (!m || !moment(m).isValid()) return;


    const split_afternoon = 12;
    const split_evening = 16;

    const currentHour = parseFloat(moment(m).format('HH'));

    if (currentHour >= split_afternoon && currentHour <= split_evening) {
      g = 'Afternoon';
    } else if (currentHour >= split_evening) {
      g = 'Evening';
    } else {
      g = 'Morning';
    }

    return g;
  }



  return (
    <Card
      elevation={3}
      className={`p-16 py-28 text-center w-100 ${classes.root}`}
    >
      <Fab color="primary" className="mb-28">
        <Icon>trending_up</Icon>
      </Fab>
      <FlexRow>
        <div className="uppercase text-white mb-20 font-size-18">
          Hey! Good {greeting(stamp)} {user}
        </div>
        <div className="font-weight-300 uppercase text-white mb-10 font-size-15">
          Today {moment(stamp).format('MMM DD, YYYY ddd  hh:mm:ss a')}
        </div>
      </FlexRow>
    </Card>
  );
};

export default withStyles(styles, { withTheme: true })(DashboardWelcomeCard);
