import React, { memo } from 'react';
import FlexRow from 'app/helpers/FlexRow';
import { Button, useMediaQuery, Card, CardHeader, CardContent, Dialog, DialogContent, DialogActions, IconButton, TextField, Table, TableHead, TableRow, TableCell, TableBody, Fab, TableFooter } from '@material-ui/core';
import { ArrowBack, MapRounded, ErrorOutlineOutlined, HotelRounded, PictureInPicture, ArrowRightAltRounded, CheckCircle, PartyModeOutlined, CheckCircleOutlineOutlined, FileCopy, Web, CameraAltOutlined, Timer10Outlined, CardTravelOutlined, TimeToLeaveRounded, TimelapseOutlined, MapTwoTone, Hotel, CheckBoxOutlineBlankSharp, CameraAltRounded, RouterOutlined, SettingsPowerSharp, Facebook, Instagram, Twitter, YouTube, PhoneEnabledOutlined, EmailOutlined, RoomOutlined, PhoneInTalkOutlined, AlternateEmailOutlined, LanguageOutlined, PictureAsPdf, Description, FlightLandOutlined, DriveEtaRounded, DirectionsBusRounded, TrainRounded, Flight } from '@material-ui/icons';
import { Divider } from 'semantic-ui-react';
import {
    InfoOutlined, AssignmentOutlined, MapOutlined, HotelOutlined,
    LocalOfferOutlined, ListAltOutlined, RadioButtonUncheckedOutlined
} from "@material-ui/icons";

import ViewSlider from "react-slick";

import { withStyles } from '@material-ui/core/styles';
import moment from 'moment';
import GoogleMapDirectionComponent from 'app/helpers/GoogleMapDirectionComponent';
import Gallery from "react-image-gallery";
import 'react-image-gallery/styles/css/image-gallery.css';

import './document.styles.css';


const settings = { dots: false, autoPlay: false, slidesToShow: 1, slidesToScroll: 1 };


const PDFView = memo((props) => {


    const [MapView, setMapView] = React.useState({
        closeView: () => setMapView({ ...MapView, open: 'closed' }),
        open: 'closed'
    });

    const loadTransportIcon = (mode) => {
        switch (mode) {
            case 'Car':
                return <DriveEtaRounded />;
            case 'Bus':
                return <DirectionsBusRounded />;
            case 'Train':
                return <TrainRounded />;
            case 'Flight':
                return <Flight />;
            default:
                return <DriveEtaRounded />;
        }
    }


    const { document, content, agent, client, stays, profilecompany, gallery, countries } = props;


    if (document && content) {

        var counter = 0;

        return (
            <React.Fragment>

                <div className="document-container">

                    <div className="main-header">
                        <div className='document-top-header'>
                            <div className="doc-logo-container">
                                <img src={profilecompany ? `${process.env.REACT_APP_STATIC}${profilecompany.logo}` : require('../../../../assets/primary-logo.png')} />
                            </div>
                            {!!agent &&
                                <div class="divider medium">
                                    <div class="representor-photo" style={{ backgroundImage: `url(${process.env.REACT_APP_STATIC + agent.avatar})`, backgroundSize: 'cover' }}></div>
                                    <div>
                                        <div class="sc-LzMEJ bUmsVe">{agent.name}</div>
                                        <div>Customer Agent</div>
                                        <div><a href={`tel: ${agent.phone_number}`} class="sc-fzXfNf la-dEmr">{agent.phone_number}</a></div>
                                        <div><a href={`mailto: ${agent.email}`} class="sc-fzXfNf la-dEmr">{agent.email}</a></div>
                                    </div>
                                </div>
                            }
                        </div>
                        {!!client &&
                            <div className="customer">
                                <div class="sc-LzMEJ bUmsVe">{client.salutation} {client.name}</div>
                                <div>{client.country}</div>
                                <div><a href={`tel: ${client.phone_number}`} class="sc-fzXfNf la-dEmr">{client.phone_number}</a></div>
                                <div><a href={`mailto: ${client.email}`} class="sc-fzXfNf la-dEmr">{client.email}</a></div>
                            </div>
                        }
                    </div>

                    <div className="view-tabs-container">

                        <div>
                            {(gallery.length && gallery[0]) &&
                                <div className='view-container'>
                                    <div style={{ backgroundImage: `url(${process.env.REACT_APP_STATIC + gallery[0]})`, backgroundSize: 'cover' }} className="slide-image-view">
                                        <div className="overview-gallery-info">
                                            <div>
                                                <div className="name-itinerary">{document.name}</div>
                                                <div className="overview-details">
                                                    <div>Trip Duration: {document.days} days</div>
                                                    <div>Itineray Ref: AUX-7265242</div>
                                                </div>
                                            </div>
                                            {(!!content.costing && (content.costing.status === 'CONFIRMED')) &&
                                                <div className="package-cost">Total Package Cost : {document.currency} {content.costing.grand_package_cost}</div>
                                            }
                                        </div>
                                    </div>
                                </div>
                            }


                            {
                                !!JSON.parse(document.countries).length &&

                                JSON.parse(document.countries).map(country => {

                                    const description = countries.find(c => c.country_id === country) ?
                                        countries.find(c => c.country_id === country).description : '';

                                    if (description) {
                                        return (
                                            <div className="region-description">{description}</div>
                                        )
                                    }

                                })

                            }

                        </div>
                        <div>

                            <div className='doc-itinerary-container'>

                                {
                                    content.days.map((day, key) => {

                                        return (
                                            <div className="day-wise-container">
                                                <div className="day-heading-text">Day {key + 1} {!!day.datestamp && moment(day.datestamp).format('MMM DD YYYY, ddd')}</div>
                                                <div className="day-content">

                                                    {
                                                        day.destinations.map((destination, index) => (

                                                            <div className="content-item">
                                                                <div className="content-top">
                                                                    <table className="table-flex" style={{ width: '100%' }}>
                                                                        <tr>
                                                                            <td style={{ width: '35%' }}>
                                                                                <div className="content-icon" title="Destination Location">
                                                                                    <MapTwoTone color="error" />
                                                                                </div>
                                                                                {
                                                                                    !!(destination.images.split(',').length && destination.images.split(',')[0]) &&
                                                                                    <div style={{ backgroundImage: `url(${process.env.REACT_APP_STATIC + destination.images.split(',')[0]})`, backgroundSize: 'cover', width: '100%', height: 200 }}>
                                                                                    </div>
                                                                                }
                                                                            </td>
                                                                            <td>
                                                                                <div className="right-content">
                                                                                    <div className="heading">{destination.name}</div>
                                                                                    <div styles={{ ['justify-content']: 'flex-start' }}>
                                                                                        <span style={{ ['margin-right']: '10px' }}>Transportation by</span>
                                                                                        {loadTransportIcon(destination.transport ? destination.transport.mode : 'Car')}
                                                                                        <span>{destination.transport ? destination.transport.mode : 'Car'}</span>
                                                                                    </div>
                                                                                    <div className="location">
                                                                                        <MapRounded /> <span>{destination.name}, <a href={destination.url} target="_blank">View on google map</a></span>
                                                                                    </div>
                                                                                    {
                                                                                        !!content.distance_duration_matrix.locations.length &&
                                                                                            (content.distance_duration_matrix.locations[counter].name === destination.name) &&
                                                                                            !!content.distance_duration_matrix.locations[counter++].distance ?
                                                                                            <div className="distance-duration-matrix">
                                                                                                <div><TimelapseOutlined /> <span>{content.distance_duration_matrix.locations[counter - 1].duration.text}</span></div>
                                                                                                <div><TimeToLeaveRounded /> <span>{content.distance_duration_matrix.locations[counter - 1].distance.text}</span></div>
                                                                                                <div><span>From {content.distance_duration_matrix.locations[counter - 2].name}</span></div>
                                                                                            </div> : <div style={{ ['font-weight']: 'bold' }}><span>Tour Started From Here ... </span><FlightLandOutlined /></div>
                                                                                    }
                                                                                    <div className="description">
                                                                                        <p>{destination.description}</p>
                                                                                    </div>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </div>
                                                            </div>

                                                        ))
                                                    }

                                                    {day.hotel.selection &&
                                                        <div className="content-item">
                                                            <div className="content-top">
                                                                <table className="table-flex" style={{ width: '100%' }}>
                                                                    <tr>
                                                                        <td style={{ width: '35%' }}>
                                                                            <div className="content-icon" title="Hotel Stay">
                                                                                <Hotel color="error" />
                                                                            </div>
                                                                            {
                                                                                !!(day.hotel.selection.images.split(',').length && day.hotel.selection.images.split(',')[0]) &&
                                                                                <div style={{ backgroundImage: `url(${process.env.REACT_APP_STATIC + day.hotel.selection.images.split(',')[0]})`, backgroundSize: 'cover', width: '100%', height: 200 }}>
                                                                                </div>
                                                                            }
                                                                        </td>
                                                                        <td>
                                                                            <div className="right-content">
                                                                                <div className="heading">{day.hotel.selection.name}</div>
                                                                                <div className="location">
                                                                                    <MapRounded /> <span>{day.hotel.selection.name}, <a href={day.hotel.selection.url} target="_blank">View on google map</a></span>
                                                                                </div>
                                                                                <div className="distance-duration-matrix">
                                                                                    {
                                                                                        day.hotel.selection.checkin &&
                                                                                        <div><Hotel /> <span>Checkin {day.hotel.selection.checkin}</span></div>
                                                                                    }

                                                                                    {
                                                                                        day.hotel.selection.checkout &&
                                                                                        <div><Hotel /> <span>Checkout {day.hotel.selection.checkout}</span></div>
                                                                                    }

                                                                                </div>
                                                                                <div className="description">
                                                                                    <p>Property Location Located in Phnom Penh City Centre, the hotel is within a 10-minute drive of Royal Palace and Independence Monument. This 4-star hotel is 1.5 miles from Tuol Sleng Genocide Museum and 1 mile from the Temple of the Golden Ginger.</p>
                                                                                </div>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    }



                                                    {
                                                        day.attractions.map((attraction, index) => (
                                                            <div className="content-item" key={index}>
                                                                <div className="content-top">
                                                                    <table className="table-flex" style={{ width: '100%' }}>
                                                                        <tr>
                                                                            <td style={{ width: '35%' }}>
                                                                                <div className="content-icon" title="Attractions">
                                                                                    <CameraAltRounded color="error" />
                                                                                </div>
                                                                                {
                                                                                    !!(attraction.images.split(',').length && attraction.images.split(',')[0]) &&
                                                                                    <div style={{ backgroundImage: `url(${process.env.REACT_APP_STATIC + attraction.images.split(',')[0]})`, backgroundSize: 'cover', width: '100%', height: 200 }}>
                                                                                    </div>
                                                                                }
                                                                            </td>
                                                                            <td>
                                                                                <div className="right-content">
                                                                                    <div className="heading">{attraction.name}</div>
                                                                                    <div className="location">
                                                                                        <MapRounded /> <span>{attraction.name}, <a href={attraction.url} target="_blank">View on google map</a></span>
                                                                                    </div>
                                                                                    <div className="description">
                                                                                        <p>Property Location Located in Phnom Penh City Centre, the hotel is within a 10-minute drive of Royal Palace and Independence Monument. This 4-star hotel is 1.5 miles from Tuol Sleng Genocide Museum and 1 mile from the Temple of the Golden Ginger.</p>
                                                                                    </div>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        ))
                                                    }


                                                    {
                                                        day.excursions.map((excursion, index) => (
                                                            <div className="content-item" key={index}>
                                                                <div className="content-top">
                                                                    <div className="right-content">
                                                                        <div className="heading">{excursion.name}</div>
                                                                        <div className="excursion-details">
                                                                            <div><span>{excursion.from}</span></div>
                                                                            <div><ArrowRightAltRounded /></div>
                                                                            <div><span>{excursion.to}</span></div>
                                                                        </div>
                                                                        <div className="excursion-details">
                                                                            <div>Distance approximately {excursion.distance}</div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>))
                                                    }


                                                    <div className="content-item">
                                                        <div className="activities">
                                                            {
                                                                day.activities.map((activity, index) => (
                                                                    <div key={index}>
                                                                        <div className="category">{activity.category}</div>
                                                                        <table className="list">
                                                                            {
                                                                                activity.activities.split(',').map(item => (
                                                                                    <tr>
                                                                                        <td>
                                                                                            <div><CheckCircleOutlineOutlined /> <span>Tour Union Gatherings</span></div>
                                                                                        </td>
                                                                                    </tr>
                                                                                ))
                                                                            }

                                                                        </table>
                                                                    </div>
                                                                ))
                                                            }
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        )

                                    })
                                }

                            </div>

                        </div>

                        <div>
                            <div className="pricing-container">
                                <div className="heading">Pricing Summary</div>

                                <Table cellSpacing={15} className="table-content">
                                    <TableHead>
                                        <TableRow>
                                            <TableCell>Tour</TableCell>
                                            <TableCell align='right'>Total Package Cost</TableCell>
                                        </TableRow>
                                    </TableHead>
                                    <TableBody>
                                        <TableRow>
                                            <TableCell>{document.name}</TableCell>
                                            {
                                                (!!content.costing && (content.costing.status === 'CONFIRMED')) ?
                                                    <TableCell align='right'>{document.currency} {content.costing.grand_package_cost}</TableCell> :
                                                    <TableCell align='right'>Contact us for price details</TableCell>
                                            }
                                        </TableRow>
                                    </TableBody>
                                </Table>

                                {!!content.inclusions.length &&
                                    <React.Fragment>
                                        <div className="heading">Inclusions</div>
                                        <div className="content">
                                            <table className="list">
                                                {
                                                    content.inclusions.map((note, key) => (
                                                        <tr>
                                                            <td><div key={key}><RadioButtonUncheckedOutlined style={{ ['font-size']: 10, width: 10 }} /> {note.text}</div></td>
                                                        </tr>
                                                    ))
                                                }
                                            </table>
                                        </div>
                                    </React.Fragment>
                                }

                                {!!content.exclusions.length &&
                                    <React.Fragment>
                                        <div className="heading">Exclusions</div>
                                        <div className="content">
                                            {
                                                content.exclusions.map((note, key) => (
                                                    <div key={key}><RadioButtonUncheckedOutlined style={{ ['font-size']: 10 }} /> {note.text}</div>
                                                ))
                                            }
                                        </div>
                                    </React.Fragment>
                                }

                                {!!content.notes.length &&
                                    <React.Fragment>
                                        <div className="heading">Notes</div>
                                        <div className="content">
                                            {
                                                content.notes.map((note, key) => (
                                                    <div key={key}><RadioButtonUncheckedOutlined style={{ ['font-size']: 10 }} /> {note.text}</div>
                                                ))
                                            }
                                        </div>
                                    </React.Fragment>
                                }


                            </div>
                        </div>
                        <div>
                            <div className="about-html-content" dangerouslySetInnerHTML={{
                                __html: profilecompany ? profilecompany.content : `About Content`
                            }} />
                        </div>
                    </div>

                    <Divider clearing section />


                    <div className="document-footer" style={{ height: 25 }}>
                        <div className="company-footer-heading">
                            {profilecompany ? profilecompany.name : 'MAB Technologies Pvt Ltd'}
                        </div>
                        <div className="address" style={{ width: '75%' }}>
                            <div><RoomOutlined /> <span>{profilecompany ? profilecompany.address : 'NewYork, USA'}</span></div>
                            <div><PhoneInTalkOutlined /> <span>{profilecompany ? profilecompany.phone_number : '+1 76321353982'}</span></div>
                            <div><AlternateEmailOutlined /> <span>{profilecompany ? profilecompany.email : 'support@travdynamics.com'}</span></div>
                        </div>
                        <div className="social-media-icon">
                            <a href={profilecompany ? profilecompany.web : 'travdynamics.com'} target="_blank"><LanguageOutlined style={{ color: 'rgba(59,89,152,1)' }} /></a>
                            <a href={profilecompany ? profilecompany.facebook : 'facebook.com'} target="_blank"><Facebook style={{ color: 'rgba(59,89,152,1)' }} /></a>
                            <a href={profilecompany ? profilecompany.linkedin : 'linkedin.com'} target="_blank"><Instagram style={{ color: 'rgba(193,53,132, 1)' }} /></a>
                            <a href={profilecompany ? profilecompany.Instagram : 'instagram'} target="_blank"><Twitter style={{ color: 'rgba(29,161,242, 1)' }} /></a>
                        </div>
                    </div>



                </div>


            </React.Fragment >
        );
    }

});

export default withStyles({}, { withTheme: true })(PDFView);