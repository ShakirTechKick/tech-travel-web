export default () => {
    return (
        `
         <head>
                <title>Itinerary Document</title>
                <style>
                 * {
                    -webkit-box-sizing: border-box;
                }
                div.document-top-header {
                      display: -webkit-box;
                  display: -webkit-flex;
                  display: flex;
                    flex-direction: row;
                    -webkit-box-pack: justify; 
                    -webkit-justify-content: flex-start;
                            justify-content: flex-start;
                }

                *{
                    font-size: 15px !important;
                }

                div > table span{
                    margin-top: 15px;
                }

                div.doc-logo-container{
                  alignItems: center;
                  padding-top: 15px;
                }

                div.doc-logo-container img{
                    width: 125px;
                }
                
                svg{
                    width: 25px !important;
                }
                
                div.main-header {
                      display: -webkit-box;
                  display: -webkit-flex;
                  display: flex;
                    -webkit-box-pack: justify; 
                    -webkit-justify-content: space-between;
                            justify-content: space-between;
                    align-items: center;
                    flex-wrap: wrap;
                    padding: 0 10px;
                    border-bottom: 0.5px solid rgba(49, 49, 49, 0.5);
                }
                
                div.main-header .customer {
                    text-align: right;
                    padding: 10px;
                }
                
                div.main-header-tabs {
                    background-color: rgb(238, 238, 238);
                    z-index: 10000;
                }
                
                div.main-header-tabs .PrivateTabIndicator-colorPrimary-49 {
                    background-color: transparent !important;
                }
                
                
                div.divider {
                      display: -webkit-box;
                  display: -webkit-flex;
                  display: flex;
                    border-left: 1px solid rgb(238, 238, 238);
                    align-items: center;
                    padding: 5px;
                }
                
                div.representor-photo {
                    width: 75px;
                    height: 75px;
                    border-radius: 50%;
                    margin-right: 10px;
                }
                
                div.representor-photo img {
                    border-radius: 50%;
                }
                
                div.slide-image-view {
                    height: 500px;
                    /* background-size: cover !important; */
                    /* background-image: url('https://e.tcdnweb.com/3a275927-3a6d-4c57-a305-793b77223bc9/57e0d4444856ad14f6da8c7dda79367f123dd9ec53576c48702878d1904bc25dbf_1280.jpg'); */
                }
                
                div.overview-gallery-info {
                      display: -webkit-box;
                  display: -webkit-flex;
                  display: flex;
                    padding: 25px;
                    background-color: rgba(49, 49, 49, 0.5);
                    -webkit-box-pack: justify; 
                    -webkit-justify-content: space-between;
                    justify-content: space-between;
                    align-items: center;
                }
                
                div.name-itinerary {
                    font-size: 25px;
                    font-weight: bold;
                    color: rgb(238, 238, 238);
                }
                
                div.overview-details {
                      display: -webkit-box;
                  display: -webkit-flex;
                  display: flex;
                    align-items: center;
                    color: rgb(238, 238, 238);
                }
                
                div.package-cost {
                    font-weight: bold;
                    font-size: 25px;
                    color: rgb(238, 238, 238);
                }
                
                div.days-overall-view {
                    margin-top: 25px;
                      display: -webkit-box;
                  display: -webkit-flex;
                  display: flex;
                    flex-wrap: wrap;
                    -webkit-box-pack: justify; 
                    -webkit-justify-content: space-between;
                    justify-content: space-evenly;
                    margin: auto;
                }
                
                div.day-item-view {
                    margin-top: 50px;
                }
                
                div.day-header {
                    margin-bottom: 20px;
                    font-weight: bold;
                    font-size: 20px;
                    border-bottom: 0.5px solid;
                    padding-bottom: 10px;
                }
                
                div.days-overall-view>div {
                    width: 45%;
                }
                
                div.day-item-list div {
                      display: -webkit-box;
                  display: -webkit-flex;
                  display: flex;
                    align-items: center;
                    margin-top: 10px;
                }
                
                div.day-item-list div>svg {
                    margin-right: 15px;
                    background: rgb(174, 180, 226);
                    border-radius: 50%;
                    padding: 4.5px;
                    color: rgb(255, 252, 245);
                }
                
                div.region-description {
                    padding: 15px;
                    background-color: rgba(49, 49, 49, 0.5);
                    color: rgb(238, 238, 238);
                    font-style: normal;
                    font-size: 18px;
                    text-align: justify;
                }
                
                div.preview-dialog .MuiBox-root {
                    padding: 0 !important;
                }
                
                div.preview-dialog .MuiDialogContent-root {
                    padding: 0 !important;
                    overflow-x: hidden !important;
                }
                
                div.preview-dialog .MuiPaper-root {
                    overflow: hidden !important;
                }
                
                div.day-wise-container {
                    margin-top: 50px;
                    page-break-inside: avoid !important;
                }
                
                div.day-heading-text {
                    padding: 15px;
                    background: linear-gradient(45deg, black, rgb(255, 255, 255));
                    color: rgb(238, 238, 238);
                    font-weight: bold;
                }
                div.content > div{
                    height: 15px !important;
                }

                div.content > table td{
                    text-align: left !important;
                }

                div.content > div svg{
                    width: 0 !important;
                }
                
                div.content-item { 
                    margin: 25px auto;
                    page-break-inside: avoid !important;
                }
                
                div.content-item p {
                    text-align: justify;
                }
                
                div.content-item .content-top {
                      display: -webkit-box;
                  display: -webkit-flex;
                  display: flex;
                  -webkit-box-pack: start; 
                  -webkit-justify-content: flex-start;
                          justify-content: flex-start;
                    align-items: flex-start;
                }
                
                div.content-item a {
                    font-weight: 400;
                    color: rgba(49, 49, 150, 0.5);
                }
                
                div.content-icon {
                   width: 50px !important;
                    height: 50px;
                      display: -webkit-box;
                  display: -webkit-flex;
                  display: flex;
                  -webkit-box-pack: center; 
                  -webkit-justify-content: center;
                          justify-content: center;
                    align-items: center;
                    border-radius: 50%;
                    position: absolute;
                    background: rgb(255, 255, 255);
                    margin: 5px;
                }
                
                div.content-item .right-content {
                    padding-left: 25px;
                }
                
                div.content-item .right-content .excursion-details {
                      display: -webkit-box;
                  display: -webkit-flex;
                  display: flex;
                    align-items: center;
                    -webkit-box-pack: justify; 
                    -webkit-justify-content: space-between;
                            justify-content: space-between;
                    margin-bottom: 25px;
                }
                
                div.content-item .right-content .heading {
                    font-size: 25px;
                    color: rgba(49, 49, 150, 0.5);
                }
                
                div.content-item .right-content>div {
                    margin-bottom: 15px;
                }
                
                div.content-item .location {
                      display: -webkit-box;
                  display: -webkit-flex;
                  display: flex;
                    align-items: center;
                }
                
                div.content-item .location svg {
                    margin-right: 15px;
                }
                
                div.content-item .distance-duration-matrix {
                      display: -webkit-box;
                  display: -webkit-flex;
                  display: flex;
                    align-items: center;
                    -webkit-box-pack: start; 
                    -webkit-justify-content: flex-start;
                            justify-content: flex-start;
                }
                
                div.content-item .distance-duration-matrix>div {
                      display: -webkit-box;
                  display: -webkit-flex;
                  display: flex;
                    align-items: center;
                }
                
                div.content-item .distance-duration-matrix>div svg {
                    margin-right: 15px;
                }
                
                div.content-item .distance-duration-matrix>div:last-child {
                    margin-left: 25px;
                }
                
                div.content-item .activities .category {
                    font-size: 16px;
                    color: rgba(49, 49, 150, 0.5);
                }
                
                div.content-item .activities {
                      display: -webkit-box;
                  display: -webkit-flex;
                  display: flex;
                    flex-wrap: wrap;
                }
                
                div.content-item .activities>div {
                    width: 25%;
                }
                
                div.content-item .activities .list div {
                      display: -webkit-box;
                  display: -webkit-flex;
                  display: flex;
                    align-items: center;
                    margin-bottom: 10px;
                }
                
                div.content-item .activities .list div svg {
                    margin-right: 10px;
                }
                
                div.map-container {
                    width: 100%;
                    height: 500px;
                }
                
                div.hotel-stay-item {
                    margin: 0 auto 50px auto;
                }
                
                div.hotel-stay-item .day-heading {
                    padding: 10px;
                    background: linear-gradient(45deg, black, rgb(255, 255, 255));
                    color: rgb(238, 238, 238);
                }
                div .footer-logos{
                    display: -webkit-box;
                  display: -webkit-flex;
                  display: flex;
                  -webkit-box-pack: center; 
                  -webkit-justify-content: center;
                          justify-content: center;
                }
                
                div.pricing-container {
                    margin: 50px 0;
                    background-color: rgb(250, 250, 250);
                    padding: 10px !important;
                }
                div.pricing-container table{
                    width: 100%;
                }
                
                div.pricing-container tr td:first-child, div.pricing-container tr th:first-child{
                    text-align: left;
                }
                
                div.pricing-container tr td:last-child, div.pricing-container tr th:last-child{
                    text-align: right;
                }
                
                div.pricing-container .heading {
                    font-size: 20px;
                }
                
                div.pricing-container .table-content {
                    margin-bottom: 25px;
                }
                
                div.pricing-container .content {
                    margin: 25px auto;
                }
                
                div.pricing-container .content>div {
                      display: -webkit-box;
                  display: -webkit-flex;
                  display: flex;
                    align-items: center;
                }
                
                div.pricing-container .content>div svg {
                    margin-right: 15px;
                }
                
                div.about-html-content {
                    margin: 25px auto;
                }
                
                div.document-footer {
                    margin: 25px auto;
                    text-align: center;
                }

                div.document-footer svg{
                    width: 15px !important;
                }
                
                div.document-footer .address {
                      display: -webkit-box;
                  display: -webkit-flex;
                  display: flex;
                    flex-wrap: wrap;
                    -webkit-box-pack: justify; 
                    -webkit-justify-content: space-between;
                            justify-content: space-evenly;
                    margin: 15px auto;
                }
                
                div.document-footer .address>div {
                      display: -webkit-box;
                  display: -webkit-flex;
                  display: flex;
                    align-items: center;
                }
                
                div.document-footer .address>div svg {
                    margin-right: 5px;
                }
                
                div.document-footer .social-media-icon a {
                    margin-left: 10px;
                }
                
                div.document-footer .company-footer-heading {
                    font-weight: 600;
                }
                
                div.export-fab-list {
                    position: fixed;
                    bottom: 15px;
                    right: 15px;
                }
                
                div.export-fab-list button {
                    margin-left: 5px;
                }
                
                div.header-control {
                    padding: 10px;
                      display: -webkit-box;
                  display: -webkit-flex;
                  display: flex;
                  -webkit-box-pack: justify; 
                  -webkit-justify-content: space-between;
                          justify-content: space-between;
                }
                div > table.table-flex{
                    width: 100%;
                }
                
                div.divider-top-header {
                    margin: 0 !important;
                    padding: 0 !important;
                }
                
                *.image-gallery-fullscreen-button .image-gallery-svg, .image-gallery-play-button .image-gallery-svg {
                    width: 20px !important;
                    height: 20px !important;
                }
                
                *.image-gallery-fullscreen-button .image-gallery-svg, .image-gallery-play-button .image-gallery-svg {
                    width: 20px !important;
                    height: 20px !important;
                }
                
                *.MuiTab-textColorPrimary.Mui-selected {
                    border-bottom: 0 !important;
                }
                 </style>
        </head>`
    )
}