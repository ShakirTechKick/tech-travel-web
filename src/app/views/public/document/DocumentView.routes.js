import { EgretLoadable } from "egret";

const DocumentView = EgretLoadable({
  loader: () => import("./DocumentView.component")
});

const documentViewRoutes = [
  {
    name: 'DocumentView',
    path: "/public/document/view",
    exact: true,
    component: DocumentView
  }
];

export default documentViewRoutes;
