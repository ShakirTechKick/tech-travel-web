import React, { memo, useEffect, useState } from 'react';
import { renderToString } from "react-dom/server";
import PropTypes from 'prop-types';
import { useDispatch, useSelector } from 'react-redux';
import FlexRow from 'app/helpers/FlexRow';
import { Button, useMediaQuery, Card, CardHeader, CardContent, Dialog, DialogContent, DialogActions, IconButton, TextField, Table, TableHead, TableRow, TableCell, TableBody, Fab, LinearProgress, ButtonGroup, Grid } from '@material-ui/core';
import { ArrowBack, MapRounded, ErrorOutlineOutlined, HotelRounded, ArrowRightAltRounded, CheckCircle, PartyModeOutlined, CheckCircleOutlineOutlined, FileCopy, Web, CameraAltOutlined, Timer10Outlined, CardTravelOutlined, TimeToLeaveRounded, TimelapseOutlined, MapTwoTone, Hotel, CheckBoxOutlineBlankSharp, CameraAltRounded, RouterOutlined, SettingsPowerSharp, Facebook, Instagram, Twitter, YouTube, PhoneEnabledOutlined, EmailOutlined, RoomOutlined, PhoneInTalkOutlined, AlternateEmailOutlined, LanguageOutlined, PictureAsPdf, Description, FlightLandOutlined, ShareOutlined, DriveEtaRounded, DirectionsBusRounded, TrainRounded, Flight } from '@material-ui/icons';
import { Divider } from 'semantic-ui-react';

import * as UsersAction from "../../../redux/actions/modules/Users.actions";
import * as ClientAction from "../../../redux/actions/modules/Clients.action";
import * as CompanyAction from "../../../redux/actions/modules/Company.actions";
import * as BuilderAction from "../../../redux/actions/modules/Builder.actions";
import * as PlacesAction from "../../../redux/actions/modules/Places.action";
import {
    InfoOutlined, AssignmentOutlined, MapOutlined, HotelOutlined,
    LocalOfferOutlined, ListAltOutlined, RadioButtonUncheckedOutlined
} from "@material-ui/icons";

import ViewSlider from "react-slick";

import { makeStyles, withStyles } from '@material-ui/core/styles';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import moment from 'moment';
import GoogleMapDirectionComponent from 'app/helpers/GoogleMapDirectionComponent';
import { ToastsStore } from 'react-toasts';
import Gallery from "react-image-gallery";
import 'react-image-gallery/styles/css/image-gallery.css';
import * as ShareOptions from 'react-share';

import './document.styles.css';
import HTMLViewComponent from './HTMLView.component';
import { saveAs } from 'file-saver';
import Helmet from './Helmet';
import { fetchEmailSettings } from 'app/redux/actions/modules/Settings.action';
import { Request } from 'app/config/Request';


const settings = { dots: false, autoPlay: false, slidesToShow: 1, slidesToScroll: 1 };

function DayTabPanel(props) {
    const { children, day, index, ...other } = props;

    return (
        <div
            role="Day Panel"
            hidden={day !== index}
            id={`scrollable-auto-DayTabPanel-${index}`}
            aria-labelledby={`scrollable-auto-tab-${index}`}
            {...other}
        >
            {day === index && (
                <Box p={3}>
                    <Typography>{children}</Typography>
                </Box>
            )}
        </div>
    );
}

function dayPanelProps(index) {
    return {
        id: `scrollable-auto-tab-${index}`,
        'aria-controls': `scrollable-auto-DayTabPanel-${index}`,
    };
}


function SharePanel(props) {
    const { children, value, index, ...other } = props;

    return (
        <div
            role="Day Panel"
            hidden={value !== index}
            id={`scrollable-auto-DayTabPanel-${index}`}
            aria-labelledby={`scrollable-auto-tab-${index}`}
            {...other}
        >
            {value === index && (
                <Box p={3}>
                    <Typography>{children}</Typography>
                </Box>
            )}
        </div>
    );
}

function sharePanelProps(index) {
    return {
        id: `scrollable-auto-tab-${index}`,
        'aria-controls': `scrollable-auto-DayTabPanel-${index}`,
    };
}

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
        width: '100%',
        backgroundColor: theme.palette.background.paper
    },
    progress: {
        width: '100%',
        '& > * + *': {
            marginTop: theme.spacing(2),
        },
    }
}));




function DetailPanel(props) {
    const { children, value, index, ...other } = props;

    return (
        <div
            role="tabpanel"
            hidden={value !== index}
            id={`scrollable-force-tabpanel-${index}`}
            aria-labelledby={`scrollable-force-tab-${index}`}
            {...other}
        >
            {value === index && (
                <Box p={3}>
                    <Typography>{children}</Typography>
                </Box>
            )}
        </div>
    );
}


function detailPanelProps(index) {
    return {
        id: `scrollable-force-tab-${index}`,
        'aria-controls': `scrollable-force-tabpanel-${index}`,
    };
}



const DocumentView = memo((props) => {

    const dispatch = useDispatch();

    const [MapView, setMapView] = React.useState({
        closeView: () => setMapView({ ...MapView, open: 'closed' }),
        open: 'closed'
    });

    const copyRef = React.useRef();

    const [URLView, setURLView] = React.useState({
        text: null, status: 'NOTCOPIED',
        closeView: () => setURLView({ ...URLView, open: 'closed' }),
        open: 'closed'
    });

    const [link, setLink] = React.useState({
        // original: 'https://facebook.com',
        original: props.document ?
            `${process.env.REACT_APP_URL}/public/document/view?cid=${props.document.company_id}&token=${props.document.token}` : null,
        state: 'LONG'
    });

    useEffect(() => {

        if (link.original && link.state === 'LONG') {
            Request.post(`/utilities/bitly/shorten`, { url: link.original })
                .then(result => {
                    if (result.data.status === 'success') {
                        const { result: data } = result.data;
                        setLink({ ...link, original: data.link, state: 'SHORT' });
                    }
                }).catch(error => null)
        }

    }, [link])


    const [ShareView, setShareView] = React.useState({
        closeView: () => setShareView({ ...ShareView, open: 'closed' }),
        open: 'closed'
    });

    const [loaded, setLoaded] = React.useState(null);

    const [company, setCompany] = React.useState(null);
    const [token, setToken] = React.useState(null);

    const { list: documents } = useSelector(state => state.builder);

    const { companies } = useSelector(state => state.company);
    const { list: users } = useSelector(state => state.users);
    const { list: clients } = useSelector(state => state.clients);
    const { countries, cities } = useSelector(state => state.places);

    const [progress, setProgress] = React.useState(false);


    const classes = useStyles();
    const [day, setDay] = React.useState(0);

    const changeDayPanel = (event, val) => {
        setDetail(0); setDay(val);
    };

    const [detail, setDetail] = React.useState(0);

    const changeDetailPanel = (event, val) => {
        setDetail(val);
    };

    const [shareTab, setShareTab] = React.useState(0);

    const changeShareTab = (event, val) => {
        setShareTab(val);
    };

    const isSmall = useMediaQuery('(max-width:600px)');
    const isMedium = useMediaQuery('(min-width:600px)');

    const { email_settings } = useSelector(state => state.settings);

    useEffect(() => {

        if (company) {

            [
                BuilderAction.fetchItineraries({ company_id: company }, () => {
                    setLoaded(true);
                }),
                UsersAction.fetchUsers(),
                ClientAction.fetchClients(company),
                CompanyAction.fetchCompanies(),
                PlacesAction.fetchCountries(company),
                PlacesAction.fetchCities(company),
                fetchEmailSettings(company)
            ].forEach(action => dispatch(action))

        }
    }, [company])


    const [settings, setSettings] = useState({
        send_itinerary: {
            from_address: null,
            from_name: null, subject: null,
            body: null, message: ``,
            to_address: props.document ? clients.find(c => c.client_id === props.document.client_id) ?
                clients.find(c => c.client_id === props.document.client_id).email : null : null
        }
    })

    const sendEmail = () => {

        const brand = companies.find(c => c.company_id === props.document.company_id);
        const data = {
            ...settings.send_itinerary, link: link.original,
            branding: brand.name, brandingFullAddress: brand.address,
            brandingPhoneNumber: brand.phone_number, brandingEmail: brand.email,
            logo: process.env.REACT_APP_STATIC + brand.logo
        }

        setAction({ ...action, type: 'Mail', progress: true })
        Request.post(`/utility/email/send`, { ...data })
            .then(result => {
                if (result.data.status === 'success') {
                    setAction({ ...action, type: 'None', progress: false });
                    setShareView({ ...ShareView, open: 'closed' });
                    ToastsStore.success('Successfully email sent', 2000);
                } else {
                    setAction({ ...action, type: 'None', progress: false });
                    ToastsStore.error('Unable to send email', 2000);
                }
            }).catch(error => {
                setAction({ ...action, type: 'None', progress: false });
                ToastsStore.error('Unable to send email', 2000);
            })
    }

    useEffect(() => {

        if (email_settings && email_settings.find(s => s.setting === 'SEND ITINERARY')) {
            setSettings({ ...settings, send_itinerary: { ...settings.send_itinerary, ...email_settings.find(s => s.setting === 'SEND ITINERARY') } })
        }

    }, [email_settings])

    const [action, setAction] = useState({
        progress: false, type: 'None', text: 'Please wait'
    })


    useEffect(() => {

        if (props.control) {
            setCompany(props.document.company_id);
            setToken(props.document.token);
        } else {

            const { location } = props;

            const token = location.search.split('&').length ?
                location.search.split('&').find(item => item.indexOf('token') !== -1) ?
                    location.search.split('&').find(item => item.indexOf('token') !== -1).split('=')[1] : null : null;

            const cid = location.search.split('&').length ?
                location.search.split('&').find(item => item.indexOf('cid') !== -1) ?
                    location.search.split('&').find(item => item.indexOf('cid') !== -1).split('=')[1] : null : null;

            if (token && cid) {
                setCompany(parseInt(cid));
                setToken(token);
            }

        }

    }, [])


    const copyToClipBoard = () => {

        if (copyRef) {
            copyRef.current.select();
            window.document.execCommand('copy');
            setURLView({ ...URLView, status: 'COPIED' });
        }

    }

    const generateShortURL = () => {
        setURLView({ ...URLView, text: link.original, open: 'open', status: 'NOTCOPIED' });
    }


    const getRequiredHtmlVariables = () => {
        const document = documents.find(doc => doc.token === token);

        const content = document ? document.content ? JSON.parse(document.content) : null : null;

        const profilecompany = companies.find(c => c.company_id === company) || null;
        const client = clients.find(c => c.client_id === document.client_id) || null;
        const agent = users.find(u => u.user_id === document.created_by) || null;


        const stays = [];

        content.days.forEach((day, index) => {

            if (day.hotel.selection) {
                if (stays.length) {
                    if (stays[stays.length - 1].selection.place_id === day.hotel.selection.place_id) {
                        stays[stays.length - 1].days.push(index + 1);
                    } else {
                        stays.push({ ...day.hotel, days: [index + 1] });
                    }
                } else {
                    stays.push({ ...day.hotel, days: [index + 1] });
                }
            }

        });


        const gallery = [];

        JSON.parse(document.countries).map(country => {

            const images = countries.find(c => c.country_id === country) ?
                countries.find(c => c.country_id === country).images : null;

            if (images) {
                images.split(',').forEach(img => {
                    if (img) gallery.push(img);
                })
            }
        });

        return { document, content, stays, countries, profilecompany, client, agent, gallery };
    }



    const onGeneratePDF = () => {

        setProgress(true);

        const { document, content, stays, countries, profilecompany, client, agent, gallery } = getRequiredHtmlVariables();

        Request.post(`utilities/document/generate/pdf`, { itinerary: document, content, stays, countries, profilecompany, client, agent, gallery, document: `Document-(${document.itinerary_id})` })
            .then(result => Request.get(`utilities/document/download/Document-(${document.itinerary_id}).pdf`, { responseType: 'blob' }))
            .then(result => {
                setProgress(false);

                const pdf = new Blob([result.data], { type: 'application/pdf' });

                saveAs(pdf, 'document-itinerary.pdf');
            })

    }

    const onGenerateWord = () => {

        const { document, content, stays, countries, profilecompany, client, agent, gallery } = getRequiredHtmlVariables();

        const html = renderToString(<HTMLViewComponent document={document} content={content} stays={stays} countries={countries}
            profilecompany={profilecompany} client={client} agent={agent} gallery={gallery} />);

        Request.post(`utilities/document/generate/word`, { html })
            .then(result => Request.get(`utilities/document/download/docx`, { responseType: 'blob' }))
            .then(result => {
                const pdf = new Blob([result.data], { type: 'application/vnd.openxmlformats-officedocument.wordprocessingml.document' });

                saveAs(pdf, 'document-itinerary.docx');
            })

    }

    const setTransportIcon = (mode) => {
        switch (mode) {
            case 'Car':
                return <DriveEtaRounded />;
            case 'Bus':
                return <DirectionsBusRounded />;
            case 'Train':
                return <TrainRounded />;
            case 'Flight':
                return <Flight />;
            default:
                return <DriveEtaRounded />;
        }
    }




    const document = documents.find(doc => doc.token === token);

    const content = document ? document.content ? JSON.parse(document.content) : null : null;

    if (loaded) {

        if (document && content) {

            const profilecompany = companies.find(c => c.company_id === company) || null;
            const client = clients.find(c => c.client_id === document.client_id) || null;
            const agent = users.find(u => u.user_id === document.created_by) || null;


            const stays = [];

            content.days.forEach((day, index) => {

                if (day.hotel.selection) {
                    if (stays.length) {
                        if (stays[stays.length - 1].selection.place_id === day.hotel.selection.place_id) {
                            stays[stays.length - 1].days.push(index + 1);
                        } else {
                            stays.push({ ...day.hotel, days: [index + 1] });
                        }
                    } else {
                        stays.push({ ...day.hotel, days: [index + 1] });
                    }
                }

            });


            const gallery = [];

            JSON.parse(document.countries).map(country => {

                const images = countries.find(c => c.country_id === country) ?
                    countries.find(c => c.country_id === country).images : null;

                if (images) {
                    images.split(',').forEach(img => {
                        if (img) gallery.push(img);
                    })
                }
            });

            var counter = 0;

            return (
                <React.Fragment>

                    {
                        props.control &&

                        <React.Fragment>
                            <div className="header-control">
                                <Button variant='outlined' onClick={() => props.setBackView()}>
                                    <ArrowBack /> Back
                                </Button>
                                <ButtonGroup>
                                    <Button variant='outlined' onClick={() => {
                                        setShareView({ ...ShareView, open: 'open' })
                                    }}><ShareOutlined /> Share Option
                                    </Button>
                                    <Button variant='outlined' onClick={generateShortURL}>
                                        <Web /> Generate Link
                                    </Button>
                                </ButtonGroup>
                            </div>
                            <Divider horizontal className="divider-top-header" clearing>
                                <div>Preview</div>
                            </Divider>
                        </React.Fragment>

                    }



                    <div className="document-container">

                        <div className="main-header">
                            <div className='document-top-header'>
                                <div className="doc-logo-container" style={{ display: 'flex' }}>
                                    <img src={profilecompany ? `${process.env.REACT_APP_STATIC}${profilecompany.logo}` : require('../../../../assets/primary-logo.png')} />
                                </div>
                                {!!agent &&
                                    <div class="divider medium">
                                        <div class="representor-photo" style={{ backgroundImage: `url(${process.env.REACT_APP_STATIC + agent.avatar})`, backgroundSize: 'cover' }}></div>
                                        <div>
                                            <div class="sc-LzMEJ bUmsVe">{agent.name}</div>
                                            <div>Customer Agent</div>
                                            <div><a href={`tel: ${agent.phone_number}`} class="sc-fzXfNf la-dEmr">{agent.phone_number}</a></div>
                                            <div><a href={`mailto: ${agent.email}`} class="sc-fzXfNf la-dEmr">{agent.email}</a></div>
                                        </div>
                                    </div>
                                }
                            </div>
                            {!!client &&
                                <div className="customer">
                                    <div class="sc-LzMEJ bUmsVe">{client.salutation} {client.name}</div>
                                    <div>{client.country}</div>
                                    <div><a href={`tel: ${client.phone_number}`} class="sc-fzXfNf la-dEmr">{client.phone_number}</a></div>
                                    <div><a href={`mailto: ${client.email}`} class="sc-fzXfNf la-dEmr">{client.email}</a></div>
                                </div>
                            }
                        </div>

                        <div className="view-tabs-container">
                            <Tabs
                                value={detail}
                                onChange={changeDetailPanel}
                                variant={isSmall ? 'scrollable' : 'fullWidth'} scrollButtons="on" indicatorColor="primary"
                                textColor="primary" aria-label="Scrollable detail panel"
                                className="main-header-tabs"
                            >
                                <Tab label={isSmall ? null : "OVERVIEW"} icon={<ListAltOutlined />} {...detailPanelProps(0)} />
                                <Tab label={isSmall ? null : "ITINERARY"} icon={<AssignmentOutlined />} {...detailPanelProps(1)} />
                                <Tab label={isSmall ? null : "MAP"} icon={<MapOutlined />} {...detailPanelProps(1)} />
                                <Tab label={isSmall ? null : "STAYS"} icon={<HotelOutlined />} {...detailPanelProps(1)} />
                                <Tab label={isSmall ? null : "PRICING"} icon={<LocalOfferOutlined />} {...detailPanelProps(1)} />
                                <Tab label={isSmall ? null : "ABOUT US"} icon={<InfoOutlined />} {...detailPanelProps(1)} />
                            </Tabs>
                            <DetailPanel value={detail} index={0}>
                                {
                                    isSmall && <div className="main-header-text">OVERVIEW</div>
                                }

                                <ViewSlider {...{
                                    arrows: true, dots: true, slidesPerRow: 1, autoplaySpeed: 1500,
                                    slidesToScroll: 1, adaptiveHeight: true, autoplay: true
                                }}>

                                    {
                                        !!gallery.length ?
                                            gallery.map((img, key) => (
                                                <div key={key} className='view-container'>
                                                    <div style={{ backgroundImage: `url(${process.env.REACT_APP_STATIC + img})`, backgroundSize: 'cover' }} className="slide-image-view">
                                                        <div className="overview-gallery-info">
                                                            <div>
                                                                <div className="name-itinerary">{document.name}</div>
                                                                <div className="overview-details">
                                                                    <div style={{ ['margin-right']: '15px' }}>Trip Duration: {document.days} days</div>
                                                                    <div>Itineray Ref: {document.itinerary_id}</div>
                                                                </div>
                                                            </div>
                                                            {(content.costing && content.costing.status === 'CONFIRMED') &&
                                                                <div className="package-cost">Total Package Cost : {document.currency} {content.costing.grand_package_cost}</div>
                                                            }
                                                        </div>
                                                    </div>
                                                </div>
                                            )) :

                                            <div className="slide-image-view">
                                                <div className="overview-gallery-info">
                                                    <div>
                                                        <div className="name-itinerary">{document.name}</div>
                                                        <div className="overview-details">
                                                            <div>Trip Duration: {document.days} days</div>
                                                            <div>Itineray Ref: {document.itinerary_id}</div>
                                                        </div>
                                                    </div>
                                                    {(content.costing && content.costing.status === 'CONFIRMED') &&
                                                        <div className="package-cost">Total Package Cost : {document.currency} {content.costing.grand_package_cost}</div>
                                                    }
                                                </div>
                                            </div>
                                    }



                                </ViewSlider>

                                <div className='days-overall-view'>


                                    {
                                        content.days.map((day, index) => {

                                            return (
                                                <div className='day-item-view' key={day.id}>
                                                    <div className='day-header'>Day {index + 1}</div>

                                                    <div className='day-item-list'>

                                                        {
                                                            day.destinations.map((destination, index) => (
                                                                <React.Fragment>
                                                                    <div key={index}><RoomOutlined color='primary' /> {destination.name}</div>
                                                                    <div key={index}>
                                                                        {setTransportIcon(destination.transport ? destination.transport.mode : 'Car')}
                                                                        <span style={{ ['margin-right']: '10px' }}>Reached by</span>
                                                                        <span>{destination.transport ? destination.transport.mode : 'Car'}</span>
                                                                    </div>
                                                                </React.Fragment>
                                                            ))
                                                        }

                                                        {
                                                            day.attractions.map((attraction, index) => (
                                                                <div key={index}><CameraAltOutlined color='primary' /> {attraction.name}</div>
                                                            ))
                                                        }

                                                        {
                                                            day.hotel.selection &&
                                                            <div><HotelRounded color='primary' /> {day.hotel.selection.name}</div>
                                                        }


                                                    </div>
                                                </div>
                                            )
                                        })
                                    }

                                </div>



                            </DetailPanel>
                            <DetailPanel value={detail} index={1}>
                                {
                                    isSmall && <div className="main-header-text">ITINERARY</div>
                                }

                                {
                                    !!JSON.parse(document.countries).length &&

                                    JSON.parse(document.countries).map(country => {

                                        const description = countries.find(c => c.country_id === country) ?
                                            countries.find(c => c.country_id === country).description : '';

                                        if (description) {
                                            return (
                                                <div className="region-description">{description}</div>
                                            )
                                        }

                                    })

                                }

                                <div className='doc-itinerary-container'>

                                    {
                                        content.days.map((day, key) => {

                                            return (
                                                <div className="day-wise-container">
                                                    <div className="day-heading-text">
                                                        <FlexRow>
                                                            <div>Day {key + 1} {day.datestamp && moment(day.datestamp).format('MMM DD YYYY, ddd')}</div>
                                                            <div>
                                                                {!!day.destinations.length &&
                                                                    <div className="country-caption">
                                                                        {countries.find(c => c.country_id === day.destinations[0].country_id) ?
                                                                            countries.find(c => c.country_id === day.destinations[0].country_id).name : null}
                                                                    </div>
                                                                }
                                                            </div>
                                                        </FlexRow>
                                                    </div>
                                                    <div className="day-content">

                                                        {
                                                            day.destinations.map((destination, index) => (

                                                                <div className="content-item">
                                                                    <div className="content-top">
                                                                        <div className="content-icon" title="Destination Location">
                                                                            <MapTwoTone color="error" />
                                                                        </div>
                                                                        {!!destination.images &&
                                                                            <div>
                                                                                <Gallery renderItem={(props) => (
                                                                                    <div style={{ width: '100%', height: '250px', background: `url(${props.original})`, backgroundSize: 'cover' }}></div>
                                                                                )} renderThumbInner={(props) => <div style={{ width: '100%', height: '75px', background: `url(${props.original})`, backgroundSize: 'cover' }}></div>} items={[
                                                                                    ...destination.images.split(',').map(img => {
                                                                                        if (img) {
                                                                                            return {
                                                                                                original: process.env.REACT_APP_STATIC + img,
                                                                                                thumbnail: process.env.REACT_APP_STATIC + img,
                                                                                            }
                                                                                        }
                                                                                    })
                                                                                ]} showNav={false} thumbnailPosition='right' />
                                                                            </div>
                                                                        }
                                                                        <div className="right-content">
                                                                            <div className="heading">{destination.name}</div>
                                                                            <FlexRow styles={{ ['justify-content']: 'flex-start' }}>
                                                                                <span style={{ ['margin-right']: '10px' }}>Transportation by</span>
                                                                                {setTransportIcon(destination.transport ? destination.transport.mode : 'Car')}
                                                                                <span>{destination.transport ? destination.transport.mode : 'Car'}</span>
                                                                            </FlexRow>
                                                                            <div className="location">
                                                                                <MapRounded /> {destination.name}, <a href={destination.url} target="_blank">View on google map</a>
                                                                            </div>
                                                                            {
                                                                                !!content.distance_duration_matrix.locations.length &&
                                                                                    (content.distance_duration_matrix.locations[counter].name === destination.name) &&
                                                                                    !!content.distance_duration_matrix.locations[counter++].distance ?
                                                                                    <div className="distance-duration-matrix">
                                                                                        <div><TimelapseOutlined /> {content.distance_duration_matrix.locations[counter - 1].duration.text}</div>
                                                                                        <div><TimeToLeaveRounded /> {content.distance_duration_matrix.locations[counter - 1].distance.text}</div>
                                                                                        <div> From {content.distance_duration_matrix.locations[counter - 2].name}</div>
                                                                                    </div> : <div style={{ ['font-weight']: 'bold' }}>Tour Started From Here ... <FlightLandOutlined /></div>
                                                                            }
                                                                            <div className="description">
                                                                                <p>{destination.description}</p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                            ))
                                                        }


                                                        {day.hotel.selection &&
                                                            <Divider horizontal clearing section>
                                                                <div>Hotels</div>
                                                            </Divider>
                                                        }

                                                        {day.hotel.selection &&
                                                            <div className="content-item">
                                                                <div className="content-top">
                                                                    <div className="content-icon" title="Hotel Stay">
                                                                        <Hotel color="error" />
                                                                    </div>
                                                                    {!!day.hotel.selection.images &&
                                                                        <div>
                                                                            <Gallery renderItem={(props) => (
                                                                                <div style={{ width: '100%', height: '250px', background: `url(${props.original})`, backgroundSize: 'cover' }}></div>
                                                                            )} renderThumbInner={(props) => <div style={{ width: '100%', height: '75px', background: `url(${props.original})`, backgroundSize: 'cover' }}></div>} items={[
                                                                                ...day.hotel.selection.images.split(',').map(img => {
                                                                                    if (img) {
                                                                                        return {
                                                                                            original: process.env.REACT_APP_STATIC + img,
                                                                                            thumbnail: process.env.REACT_APP_STATIC + img,
                                                                                        }
                                                                                    }
                                                                                })
                                                                            ]} showNav={false} thumbnailPosition='right' />
                                                                        </div>
                                                                    }
                                                                    <div className="right-content">
                                                                        <div className="heading">{day.hotel.selection.name}</div>
                                                                        <div className="location">
                                                                            <MapRounded /> {day.hotel.selection.name}, <a href={day.hotel.selection.url} target="_blank">View on google map</a>
                                                                        </div>
                                                                        <div className="distance-duration-matrix">
                                                                            {
                                                                                day.hotel.selection.checkin &&
                                                                                <div><Hotel /> Checkin {day.hotel.selection.checkin}</div>
                                                                            }

                                                                            {
                                                                                day.hotel.selection.checkout &&
                                                                                <div><Hotel /> Checkout {day.hotel.selection.checkout}</div>
                                                                            }

                                                                        </div>
                                                                        <div className="description">{day.hotel.selection.description ? day.hotel.selection.description : 'Description'}</div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        }

                                                        {!!day.attractions.length &&
                                                            <Divider horizontal clearing section>
                                                                <div>Attractions</div>
                                                            </Divider>
                                                        }

                                                        {
                                                            day.attractions.map((attraction, index) => (
                                                                <div className="content-item" key={index}>
                                                                    <div className="content-top">
                                                                        <div className="content-icon" title="Attractions">
                                                                            <CameraAltRounded color="error" />
                                                                        </div>
                                                                        {!!attraction.images &&
                                                                            <div>
                                                                                <Gallery renderItem={(props) => (
                                                                                    <div style={{ width: '100%', height: '250px', background: `url(${props.original})`, backgroundSize: 'cover' }}></div>
                                                                                )} renderThumbInner={(props) => <div style={{ width: '100%', height: '75px', background: `url(${props.original})`, backgroundSize: 'cover' }}></div>} items={[
                                                                                    ...attraction.images.split(',').map(img => {
                                                                                        if (img) {
                                                                                            return {
                                                                                                original: process.env.REACT_APP_STATIC + img,
                                                                                                thumbnail: process.env.REACT_APP_STATIC + img,
                                                                                            }
                                                                                        }
                                                                                    })
                                                                                ]} showNav={false} thumbnailPosition='right' />
                                                                            </div>
                                                                        }
                                                                        <div className="right-content">
                                                                            <div className="heading">{attraction.name}</div>
                                                                            <div className="location">
                                                                                <MapRounded /> {attraction.name}, <a href={attraction.url} target="_blank">View on google map</a>
                                                                            </div>
                                                                            <div className="description">{attraction.description ? attraction.description : 'Description'}</div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            ))
                                                        }


                                                        {!!day.excursions.length &&

                                                            <Divider horizontal clearing section>
                                                                <div>Excursions</div>
                                                            </Divider>

                                                        }

                                                        {
                                                            day.excursions.map((excursion, index) => (
                                                                <div className="content-item" key={index}>
                                                                    <div className="content-top">
                                                                        <div className="right-content">
                                                                            <div className="heading">{excursion.name}</div>
                                                                            <div className="excursion-details">
                                                                                <div>{excursion.from}</div>
                                                                                <div><ArrowRightAltRounded /></div>
                                                                                <div>{excursion.to}</div>
                                                                            </div>
                                                                            <div className="excursion-details">
                                                                                <div>Distance approximately {excursion.distance}</div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>))
                                                        }


                                                        {!!day.activities.length &&

                                                            <Divider horizontal clearing section>
                                                                <div>Activities</div>
                                                            </Divider>
                                                        }



                                                        <div className="content-item">
                                                            <div className="activities">
                                                                {
                                                                    day.activities.map((activity, index) => (
                                                                        <div key={index}>
                                                                            <div className="category">{activity.category}</div>
                                                                            <div className="list">
                                                                                {!!activity.activities &&
                                                                                    activity.activities.split(',').map(item => (
                                                                                        <div><CheckCircleOutlineOutlined /> Tour Union Gatherings</div>
                                                                                    ))
                                                                                }

                                                                            </div>
                                                                        </div>
                                                                    ))
                                                                }
                                                            </div>
                                                        </div>




                                                    </div>
                                                </div>

                                            )

                                        })
                                    }

                                </div>

                            </DetailPanel>
                            <DetailPanel value={detail} index={2}>
                                {
                                    isSmall && <div className="main-header-text">MAP</div>
                                }
                                <div className="map-container">
                                    <GoogleMapDirectionComponent
                                        position={
                                            countries.find(c => c.country_id === document.countries[0]) ?
                                                JSON.parse(countries.find(c => c.country_id === document.countries[0]).location) : null}
                                        destinations={content.days.map(day => {
                                            return day.destinations.map(d => {
                                                return { ...d, day };
                                            });
                                        })} />
                                </div>
                            </DetailPanel>
                            <DetailPanel value={detail} index={3}>
                                {
                                    isSmall && <div className="main-header-text">STAYS</div>
                                }

                                {
                                    stays.map((stay, key) => (
                                        <div key={key} className="hotel-stay-item">
                                            <div className="day-heading">Day {stay.days[0]} - {stay.days[stay.days.length - 1] + 1}</div>

                                            <div className="content-item">
                                                <div className="content-top">
                                                    {!!stay.selection.images &&
                                                        <div>
                                                            <Gallery renderItem={(props) => (
                                                                <div style={{ width: '100%', height: '250px', background: `url(${props.original})`, backgroundSize: 'cover' }}></div>
                                                            )} renderThumbInner={(props) => <div style={{ width: '100%', height: '75px', background: `url(${props.original})`, backgroundSize: 'cover' }}></div>} items={[
                                                                ...stay.selection.images.split(',').map(img => {
                                                                    if (img) {
                                                                        return {
                                                                            original: process.env.REACT_APP_STATIC + img,
                                                                            thumbnail: process.env.REACT_APP_STATIC + img,
                                                                        }
                                                                    }
                                                                })
                                                            ]} showNav={false} thumbnailPosition='right' />
                                                        </div>
                                                    }
                                                    <div className="right-content">
                                                        <div className="heading">{stay.selection.name}</div>
                                                        <div className="location">
                                                            <MapRounded /> {stay.selection.name}, <a href={stay.selection.url} target="_blank"> View on google map</a>
                                                        </div>
                                                        <div className="distance-duration-matrix">
                                                            {
                                                                stay.selection.checkin &&
                                                                <div><Hotel /> Checkin {stay.selection.checkin}</div>
                                                            }

                                                            {
                                                                stay.selection.checkout &&
                                                                <div><Hotel /> Checkout {stay.selection.checkout}</div>
                                                            }
                                                        </div>

                                                        <Divider clearing />

                                                        {
                                                            stay.rooms.map((room, key) => (
                                                                <FlexRow styles={{ padding: 15 }}>
                                                                    <div>{room.number_of_rooms} Room</div>
                                                                    <div>{room.category}</div>
                                                                    <div>{room.type_room}</div>
                                                                    <div>{room.type_meal}</div>
                                                                </FlexRow>
                                                            ))
                                                        }
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    ))
                                }


                            </DetailPanel>
                            <DetailPanel value={detail} index={4}>
                                {
                                    isSmall && <div className="main-header-text">PRICING</div>
                                }

                                <div className="pricing-container">
                                    <div className="heading">Pricing Summary</div>

                                    <Table cellSpacing={15} className="table-content">
                                        <TableHead>
                                            <TableRow>
                                                <TableCell>Tour</TableCell>
                                                <TableCell align='right'>Total Package Cost</TableCell>
                                            </TableRow>
                                        </TableHead>
                                        <TableBody>
                                            <TableRow>
                                                <TableCell>{document.name}</TableCell>
                                                {
                                                    (content.costing && content.costing.status === 'CONFIRMED') ?
                                                        <TableCell align='right'>{document.currency} {content.costing.grand_package_cost}</TableCell> :
                                                        <TableCell align='right'>Contact us for price details</TableCell>
                                                }
                                            </TableRow>
                                        </TableBody>
                                    </Table>

                                    {!!content.inclusions.length &&
                                        <React.Fragment>
                                            <div className="heading">Inclusions</div>
                                            <div className="content">
                                                {
                                                    content.inclusions.map((note, key) => (
                                                        <div key={key}><RadioButtonUncheckedOutlined style={{ ['font-size']: 10 }} /> {note.text}</div>
                                                    ))
                                                }
                                            </div>
                                        </React.Fragment>
                                    }

                                    {!!content.exclusions.length &&
                                        <React.Fragment>
                                            <div className="heading">Exclusions</div>
                                            <div className="content">
                                                {
                                                    content.exclusions.map((note, key) => (
                                                        <div key={key}><RadioButtonUncheckedOutlined style={{ ['font-size']: 10 }} /> {note.text}</div>
                                                    ))
                                                }
                                            </div>
                                        </React.Fragment>
                                    }

                                    {!!content.notes.length &&
                                        <React.Fragment>
                                            <div className="heading">Notes</div>
                                            <div className="content">
                                                {
                                                    content.notes.map((note, key) => (
                                                        <div key={key}><RadioButtonUncheckedOutlined style={{ ['font-size']: 10 }} /> {note.text}</div>
                                                    ))
                                                }
                                            </div>
                                        </React.Fragment>
                                    }


                                </div>
                            </DetailPanel>
                            <DetailPanel value={detail} index={5}>
                                {
                                    isSmall && <div className="main-header-text">ABOUT</div>
                                }

                                {!!agent &&
                                    <div class="small agent">
                                        <div class="representor-photo">
                                            <img src={process.env.REACT_APP_STATIC + agent.avatar} />
                                        </div>
                                        <div>
                                            <div class="sc-LzMEJ bUmsVe">{agent.name}</div>
                                            <div>Customer Agent</div>
                                            <div><a href={`tel: ${agent.phone_number}`} class="sc-fzXfNf la-dEmr">{agent.phone_number}</a></div>
                                            <div><a href={`mailto: ${agent.email}`} class="sc-fzXfNf la-dEmr">{agent.email}</a></div>
                                        </div>
                                    </div>
                                }

                                <div className="about-html-content" dangerouslySetInnerHTML={{
                                    __html: profilecompany ? profilecompany.content : `About Content`
                                }} />
                            </DetailPanel>
                        </div>

                        <Divider clearing section />

                        <div className="document-footer">
                            <div className="company-footer-heading">
                                {profilecompany ? profilecompany.name : 'MAB Technologies Pvt Ltd'}
                            </div>
                            <div className="address">
                                <div><RoomOutlined /> {profilecompany ? profilecompany.address : 'NewYork, USA'}</div>
                                <div><PhoneInTalkOutlined /> {profilecompany ? profilecompany.phone_number : '+1 76321353982'}</div>
                                <div><AlternateEmailOutlined /> {profilecompany ? profilecompany.email : 'support@travdynamics.com'}</div>
                            </div>
                            <div className="social-media-icon">
                                <a href={profilecompany ? profilecompany.web : 'travdynamics.com'} target="_blank"><LanguageOutlined style={{ color: 'rgba(59,89,152,1)' }} /></a>
                                <a href={profilecompany ? profilecompany.facebook : 'facebook.com'} target="_blank"><Facebook style={{ color: 'rgba(59,89,152,1)' }} /></a>
                                <a href={profilecompany ? profilecompany.linkedin : 'linkedin.com'} target="_blank"><Instagram style={{ color: 'rgba(193,53,132, 1)' }} /></a>
                                <a href={profilecompany ? profilecompany.Instagram : 'instagram'} target="_blank"><Twitter style={{ color: 'rgba(29,161,242, 1)' }} /></a>
                            </div>
                        </div>


                        <div className='export-fab-list'>
                            <Fab disabled={progress} variant="extended" onClick={onGeneratePDF}>
                                <PictureAsPdf /> {progress ? 'Wait ...' : 'PDF Doc'}
                            </Fab>
                        </div>

                    </div>


                    <Dialog
                        open={URLView.open === 'open'} disableBackdropClick fullWidth>
                        <DialogContent>
                            <FlexRow>
                                <div>
                                    <TextField
                                        fullWidth defaultValue={URLView.text} inputRef={copyRef} />
                                </div>
                                <div>
                                    {
                                        URLView.status === 'COPIED' ?
                                            <div>Copied !</div> : <IconButton onClick={copyToClipBoard}>
                                                <FileCopy />
                                            </IconButton>
                                    }
                                </div>

                            </FlexRow>
                        </DialogContent>
                        <DialogActions>
                            <Button variant='contained' onClick={URLView.closeView}>Cancel</Button>
                        </DialogActions>
                    </Dialog>
                    {progress &&
                        <div style={{ position: 'fixed', bottom: 0, left: 0 }} className={classes.progress}>
                            <LinearProgress color='secondary' />
                        </div>
                    }

                    <Dialog
                        open={ShareView.open === 'open'} disableBackdropClick fullWidth>
                        <DialogContent style={{ padding: 0 }}>

                            <Tabs
                                value={shareTab}
                                onChange={changeShareTab}
                                variant='scrollable' scrollButtons="on" indicatorColor="primary"
                                textColor="primary" aria-label="Scrollable Share panel"
                            >
                                <Tab label="Email" {...sharePanelProps(0)} />
                                <Tab label="WhatsApp" {...sharePanelProps(1)} />
                                <Tab label="Others" {...sharePanelProps(2)} />
                            </Tabs>
                            <SharePanel value={shareTab} index={0}>
                                <Grid spacing='2' container>
                                    <Grid md='12' item>
                                        <TextField
                                            className="mb-16 w-100"
                                            variant="outlined"
                                            label="To Address"
                                            type="text"
                                            value={settings.send_itinerary.to_address}
                                            onChange={({ target: { value: to_address } }) => {
                                                setSettings({ ...settings, send_itinerary: { ...settings.send_itinerary, to_address } })
                                            }} />
                                    </Grid>
                                    <Grid md='12' item>
                                        <TextField
                                            className="mb-16 w-100"
                                            variant="outlined"
                                            label="From Address"
                                            type="text" disabled
                                            value={settings.send_itinerary.from_address}
                                            onChange={({ target: { value: from_address } }) => {
                                                setSettings({ ...settings, send_itinerary: { ...settings.send_itinerary, from_address } })
                                            }} />
                                    </Grid>
                                    <Grid md='12' item>
                                        <TextField
                                            className="mb-16 w-100"
                                            variant="outlined"
                                            label="From Name"
                                            type="text" disabled
                                            value={settings.send_itinerary.from_name}
                                            onChange={({ target: { value: from_name } }) => {
                                                setSettings({ ...settings, send_itinerary: { ...settings.send_itinerary, from_name } })
                                            }} />
                                    </Grid>
                                    <Grid md='12' item>
                                        <TextField
                                            className="mb-16 w-100"
                                            variant="outlined"
                                            label="Subject"
                                            type="text" disabled
                                            value={settings.send_itinerary.subject}
                                            onChange={({ target: { value: subject } }) => {
                                                setSettings({ ...settings, send_itinerary: { ...settings.send_itinerary, subject } })
                                            }} />
                                    </Grid>

                                    <Grid md='12' item>
                                        <TextField
                                            className="mb-16 w-100"
                                            variant="outlined"
                                            label="Message"
                                            type="text"
                                            value={settings.send_itinerary.message}
                                            onChange={({ target: { value: message } }) => {
                                                setSettings({ ...settings, send_itinerary: { ...settings.send_itinerary, message } })
                                            }} />
                                    </Grid>

                                    <Grid md='12' item>
                                        <Button variant='outlined' onClick={sendEmail}
                                            disabled={action.type === 'Mail' && action.progress}>{action.progress ? action.text : 'Send Now'}</Button>
                                    </Grid>
                                </Grid>
                            </SharePanel>
                            <SharePanel value={shareTab} index={1}>
                                <ShareOptions.WhatsappShareButton title="Itinerary shared with you." url={link.original}>
                                    <FlexRow styles={{ ['justify-content']: 'flex-start' }}>
                                        <div style={{ ['margin-right']: '10px' }}>Share via </div>
                                        <ShareOptions.WhatsappIcon size="20" />
                                    </FlexRow>
                                </ShareOptions.WhatsappShareButton>
                            </SharePanel>
                            <SharePanel value={shareTab} index={2}>
                                <FlexRow>
                                    <ShareOptions.ViberShareButton title="Itinerary shared with you." url={link.original}>
                                        <FlexRow styles={{ ['justify-content']: 'flex-start' }}>
                                            <div style={{ ['margin-right']: '10px' }}>Share via Viber</div>
                                            <ShareOptions.ViberIcon size='20px' />
                                        </FlexRow>
                                    </ShareOptions.ViberShareButton>

                                    <ShareOptions.TelegramShareButton title="Itinerary shared with you." url={link.original}>
                                        <FlexRow styles={{ ['justify-content']: 'flex-start' }}>
                                            <div style={{ ['margin-right']: '10px' }}>Share via Telegram</div>
                                            <ShareOptions.TelegramIcon size='20px' />
                                        </FlexRow>
                                    </ShareOptions.TelegramShareButton>

                                    <ShareOptions.LineShareButton title="Itinerary shared with you." url={link.original}>
                                        <FlexRow styles={{ ['justify-content']: 'flex-start' }}>
                                            <div style={{ ['margin-right']: '10px' }}>Share via Line App</div>
                                            <ShareOptions.LineIcon size='20px' />
                                        </FlexRow>
                                    </ShareOptions.LineShareButton>
                                </FlexRow>
                            </SharePanel>

                        </DialogContent>
                        <DialogActions>
                            <Button variant='contained' onClick={ShareView.closeView}>Cancel</Button>
                        </DialogActions>
                    </Dialog>

                </React.Fragment >
            );
        }

        return (
            <React.Fragment>
                <Card>
                    <CardHeader
                        avatar={<ErrorOutlineOutlined />} title="Not Allowed !" />
                    <CardContent>
                        <div style={{ color: 'red' }}>Document Expired or Not Allowed to view !</div>
                    </CardContent>
                </Card>
            </React.Fragment>
        )

    }

    return (
        <React.Fragment>
            <div className="loading-text">Please wait ... Loading !</div>
        </React.Fragment>
    )

});

DocumentView.propTypes = {

};

export default withStyles({}, { withTheme: true })(DocumentView);