import React, { useState, useEffect, Fragment } from 'react';
import './Login.css';

import { Button, Card, Checkbox, Container, Divider, Form } from 'semantic-ui-react';
import { Form as ValidatorForm, Input } from 'semantic-ui-react-form-validator';

import LoaderComponent from '../../components/LoaderComponent';

import { limitMaxLength } from '../../helpers/functions/inputs';
import { useDispatch } from 'react-redux';
import { loginWithEmailAndPassword, updateUserPassword, verifyToken, verifyUserExists } from 'app/redux/actions/LoginActions';
import { Grid } from '@material-ui/core/index';


const Login = () => {

  const dispatch = useDispatch();

  const [view, setView] = useState('Login');

  const getCookieDataForCredentials = (key) => {
    var name = key + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for (var i = 0; i < ca.length; i++) {
      var c = ca[i];
      while (c.charAt(0) == ' ') {
        c = c.substring(1);
      }
      if (c.indexOf(name) == 0) {
        return c.substring(name.length, c.length);
      }
    }
    return "";
  }

  const [auth, setAuth] = useState({
    loading: null,
    rememberMe: !!getCookieDataForCredentials('email'),
    email: getCookieDataForCredentials('email'),
    password: getCookieDataForCredentials('password')
  })

  const [pin, setPin] = useState({
    loading: false, secret: ''
  })

  const [pass, setPass] = useState({
    loading: false, email: '', token: '', password: '', repeat: '', step: '1-STEP'
  })

  const saveRememberMe = () => {

    const d = (new Date());

    d.setMonth(d.getMonth() + 2);
    document.cookie = `email=${auth.email}; expires=${d.toUTCString()}`;
    document.cookie = `password=${auth.password}; expires=${d.toUTCString()}`;

  }

  const clearRememberMe = () => {

    const d = (new Date());

    d.setDate(d.getDate() - 1);
    document.cookie = `email=${auth.email}; expires=${d.toUTCString()}`;
    document.cookie = `password=${auth.password}; expires=${d.toUTCString()}`;

  }


  const onSubmitLogin = () => {

    setAuth({ ...auth, loading: 'loading' });

    dispatch(loginWithEmailAndPassword({ ...auth }, () => {

      if (auth.rememberMe) {
        saveRememberMe();
      } else {
        clearRememberMe();
      }

      setAuth({ email: '', password: '', loading: null })
    }))

  }


  useEffect(() => {

    ValidatorForm.addValidationRule('passmatch', val => {
      return val === pass.password;
    })

  }, [pass])


  const onSubmitForgotPassword = (step) => {

    setPass(pass => ({...pass, loading: 'loading' }))

    switch (step) {
      case '1-STEP':

        dispatch(verifyUserExists({ email: pass.email }, () => {
          setPass(pass => ({ ...pass, step: '2-STEP', loading: null }))
        }))

        return;
      case '2-STEP':

        dispatch(verifyToken({ email: pass.email, token: pass.token }, () => {
          setPass(pass => ({ ...pass, step: '3-STEP', loading: null  }))
        }))

        return;
      case '3-STEP':

        dispatch(updateUserPassword({ email: pass.email, token: pass.token, password: pass.password }, () => {
          setView('Login')
        }))

        return;

      default: return

    }


  }


  const onChangeView = v => {
    setView(v);
  }


  const onChangeField = (field, type, { target: { value: input } }) => {

    switch (type) {
      case 'auth':
        setAuth({ ...auth, [field]: input });
        break;
      case 'pass':
        setPass({ ...pass, [field]: input });
        break;
      default: break
    }

  }


  const onViewForgotPassword = () => {
    setPass({ loading: null, step: '1-STEP', email: '', pin: '', password: '', confirmPassword: '' });
    setView('ForgotPassword')
  }

  useEffect(() => {

    ValidatorForm.addValidationRule('passmatch', val => {
      return val === pass.password;
    })

  }, [pass])

  return (
    <div className="c-app c-default-layout login-container">

      <div className='wall' />

      <div className='login-center-section'>


        <Grid container spacing={6}>
          <Grid md={6} sm={12} xs={12} item className='slogan-parent-container'>
            <div className='slogan-container'>
              <div>Tech Travel System</div>
              <div>A fine-tuned system for Travel Industries and Agents.</div>
            </div>
          </Grid>
          <Grid md={6} sm={12} xs={12} item>
            <div className="login-container-forms">

              <Container className="login-container-formbox">
                <Card className="p-4">

                  <Card.Content>


                    {view === 'Login' &&

                      <Fragment>

                        <h3>{"Login"}</h3>

                        <ValidatorForm onSubmit={onSubmitLogin}>
                          <Form.Field>
                            <label>{"email"}</label>
                            <Input icon='user' placeholder={"email"}
                              validators={['required', 'isEmail']} value={auth.email}
                              onChange={onChangeField.bind(null, 'email', 'auth')}
                              errorMessages={['email is required', 'email format is wrong']} />
                          </Form.Field>
                          <Form.Field>
                            <label>{"Password"}</label>
                            <Input icon='lock' placeholder={"Password"}
                              validators={['required']} type='password' value={auth.password}
                              onChange={onChangeField.bind(null, 'password', 'auth')}
                              errorMessages={['Password is required']} />

                            <a onClick={onViewForgotPassword} className='link forgot-password'>{'Forgot password?'}</a>
                          </Form.Field>
                          <Form.Field>
                            <Checkbox className='bottom-text' label={"Remember me"} checked={auth.rememberMe}
                              onChange={(e, { checked }) => setAuth({ ...auth, rememberMe: checked })} />
                          </Form.Field>
                          <Button type='submit' className='button-primary-login' fluid
                            disabled={auth.loading}>
                            {auth.loading ? <LoaderComponent /> : 'Login'}
                          </Button>
                        </ValidatorForm>

                      </Fragment>

                    }



                    {view === 'ForgotPassword' &&

                      <Fragment>
                        <h3>{"Forgot Password"}</h3>

                        {pass.step === '1-STEP' &&

                          <ValidatorForm onSubmit={onSubmitForgotPassword.bind(null, '1-STEP')}>
                            <Form.Field className='fluid' >
                              <Input icon='user' placeholder={'Enter your email'}
                                validators={['required']} type='text'
                                value={pass.email}
                                onChange={onChangeField.bind(null, 'email', 'pass')}
                                errorMessages={['email is required']}>
                                <input type="text" value={pass.email} onInput={limitMaxLength.bind(null, 50)} />
                              </Input>
                            </Form.Field>

                            <Button type='submit' fluid
                              disabled={pass.loading} className='button-primary-login' >
                              {pass.loading ? <LoaderComponent /> : "Next"}
                            </Button>
                          </ValidatorForm>

                        }

                        {pass.step === '2-STEP' &&

                          <ValidatorForm onSubmit={onSubmitForgotPassword.bind(null, '2-STEP')}>
                            <Form.Field className='fluid' >
                              <Input icon='lock' placeholder={'Enter Token Verification'}
                                validators={['required']} type='text'
                                value={pass.token}
                                onChange={onChangeField.bind(null, 'token', 'pass')}
                                errorMessages={['Token is required']}>
                                <input type="number" value={pass.token} onInput={limitMaxLength.bind(null, 5)} />
                              </Input>
                            </Form.Field>

                            <Button type='submit' fluid
                              disabled={pass.loading} className='button-primary-login' >
                              {pass.loading ? <LoaderComponent /> : "Next"}
                            </Button>
                          </ValidatorForm>

                        }

                        {pass.step === '3-STEP' &&

                          <ValidatorForm onSubmit={onSubmitForgotPassword.bind(null, '3-STEP')}>
                            <Form.Field className='fluid' >
                              <Input icon='lock' placeholder={'Enter Password'}
                                validators={['required']} type='text'
                                value={pass.password}
                                onChange={onChangeField.bind(null, 'password', 'pass')}
                                errorMessages={['Password is required']}>
                                <input type="password" value={pass.password} onInput={limitMaxLength.bind(null, 15)} />
                              </Input>
                            </Form.Field>

                            <Form.Field className='fluid' >
                              <Input icon='lock' placeholder={'Enter Repeat Password'}
                                validators={['required', 'passmatch']} type='text'
                                value={pass.repeat}
                                onChange={onChangeField.bind(null, 'repeat', 'pass')}
                                errorMessages={['Re-Password is required', 'Password not matched']}>
                                <input type="password" value={pass.repeat} onInput={limitMaxLength.bind(null, 15)} />
                              </Input>
                            </Form.Field>

                            <Button type='submit' fluid
                              disabled={pass.loading} className='button-primary-login' >
                              {pass.loading ? <LoaderComponent /> : "Submit"}
                            </Button>
                          </ValidatorForm>

                        }


                        <Divider />

                        <p className='bottom-text'>{"Back to"} ?</p>

                        <Button type='button' className='button-secondary-login' onClick={onChangeView.bind(null, 'Login')} fluid>{"Login"}</Button>
                      </Fragment>

                    }

                  </Card.Content>

                </Card>



              </Container>
            </div>
          </Grid>
        </Grid>
      </div>

      <div className='login-bottom-section'>
        <div className='flex-container'>
          <div className='made-by-link'>
            <span>{'Made By'}</span> <img src={require('../../../assets/icons/Parent.png')} />
          </div>
          <div>
            &copy; {(new Date()).getUTCFullYear()}  {'All rights reserved to TechKick Pvt Ltd.'}
          </div>
        </div>
      </div>


    </div>
  )
}

export default Login;