import Login from "./Login";

const settings = {
  activeLayout: "layout1",
  layout1Settings: {
    topbar: {
      show: false
    },
    leftSidebar: {
      show: false,
      mode: "close"
    }
  },
  layout2Settings: {
    mode: "full",
    topbar: {
      show: false
    },
    navbar: { show: false }
  },
  secondarySidebar: { show: false },
  footer: { show: false }
};

const sessionRoutes = [
  { 
    name: "Login",
    path: "/session/Login",
    component: Login,
    exact: true,
    settings,
    access: 'COMMON'
  }
];

export default sessionRoutes;
