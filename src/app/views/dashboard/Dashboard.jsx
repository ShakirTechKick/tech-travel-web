import React, { Component, Fragment } from "react";
import {
  Grid,
  Card,
  Icon,
  IconButton,
  Button,
  Checkbox,
  Fab,
  Avatar,
  Hidden
} from "@material-ui/core";

import { Breadcrumb, SimpleCard, EgretProgressBar } from "egret";
import DashboardWelcomeCard from "../cards/DashboardWelcomeCard";
import AreaChart from "../charts/echarts/AreaChart";

import { format } from "date-fns";
import { withStyles } from "@material-ui/styles";
import { useSelector, useDispatch } from "react-redux";
import SuperView from "./SuperView";
import CommonView from "./CommonView";
import AdminView from "./AdminView";

import { fetchUsers } from "app/redux/actions/modules/Users.actions";
import { fetchItineraries } from "app/redux/actions/modules/Builder.actions";
import { useEffect } from "react";
import { fetchClients } from "app/redux/actions/modules/Clients.action";
import { fetchDrivers } from "app/redux/actions/modules/Drivers.action";
import { fetchVehicles } from "app/redux/actions/modules/Vehicles.action";
import { fetchHotels } from "app/redux/actions/modules/Hotels.action";
import { fetchAttractions } from "app/redux/actions/modules/Attractions.action";
import { fetchExcursions } from "app/redux/actions/modules/Excursions.action";
import { fetchGuides } from "app/redux/actions/modules/Guides.action";
import { fetchCountries, fetchCities } from "app/redux/actions/modules/Places.action";

const Dashboard = (props) => {

  const dispatch = useDispatch();

  const { user: { data: account } } = useSelector(state => state.user);

  const { list: users } = useSelector(state => state.users);
  const { list: itineraries } = useSelector(state => state.builder);

  const { theme } = props;

  useEffect(() => {

    [
      fetchUsers(account.company_id),
      fetchItineraries({ company_id: account.company_id }),
      fetchClients(account.company_id),
      fetchDrivers(account.company_id),
      fetchGuides(account.company_id),
      fetchVehicles(account.company_id),
      fetchCountries(account.company_id),
      fetchCities(account.company_id),
      fetchHotels(account.company_id),
      fetchAttractions(account.company_id),
      fetchExcursions(account.company_id)
    ].forEach(action => {
      dispatch(action);
    })

  }, [])


  return (
    <div className="analytics m-sm-30">
      <div className="mb-sm-30">
        <Breadcrumb
          routeSegments={[
            { name: "Dashboard", path: "/dashboard" },
            { name: "Overview" }
          ]}
        />
      </div>

      <Grid container spacing={3}>
        <Grid sm={12} xs={12} item>
          <DashboardWelcomeCard user={account.name} />
        </Grid>
      </Grid>


      {

        (account.role && account.role === 'SuperAdmin') &&
        <SuperView {...{ users }} />

      }

      {

        (account.role && account.role === 'Admin') &&
        <AdminView  {...{ users, itineraries }} />

      }

      {

        (account.role && (account.role !== 'SuperAdmin' && account.role !== 'Admin')) &&
        <CommonView   {...{ itineraries: itineraries.filter(b => b.created_by === account.user_id) }} />

      }

    </div>
  );
}

export default withStyles({}, { withTheme: true })(Dashboard);
