import React from 'react';
import { SimpleCard, EgretProgressBar } from 'egret';
import { Grid, Table, TableRow, TableCell } from '@material-ui/core';
import ReactEcharts from "echarts-for-react";
import FlexRow from 'app/helpers/FlexRow';
import { Divider } from 'semantic-ui-react';
import { Link } from 'react-router-dom';

const CommonView = ({ itineraries }) => {

    const options_itineraries_piechart = {
        backgroundColor: 'rgba(150, 56, 175, 1)',

        title: {
            text: 'Itineraries Based On Status',
            left: 'center',
            top: 20,
            textStyle: {
                color: 'rgb(250, 250, 250)'
            }
        },

        tooltip: {
            trigger: 'item',
            formatter: '{a} <br/>{b} : {c} ({d}%)'
        },

        visualMap: {
            show: true,
            min: 80,
            max: 600,
            inRange: {
                colorLightness: [0, 1]
            }
        },
        series: [
            {
                name: 'Percentage %',
                type: 'pie',
                radius: '55%',
                center: ['50%', '50%'],
                data: [
                    { value: itineraries.filter(b => b.status === 'CREATED').length, name: 'Created' },
                    { value: itineraries.filter(b => b.status === 'UPDATED (NEW)').length, name: 'Updated (New)' },
                    { value: itineraries.filter(b => b.status === 'APPROVED').length, name: 'Approved' },
                    { value: itineraries.filter(b => b.status === 'PENDING').length, name: 'Pending' },
                    { value: itineraries.filter(b => b.status === 'SALES').length, name: 'Sales' },
                    { value: itineraries.filter(b => b.status === 'CANCELLED').length, name: 'Cancelled' }
                ].sort(function (a, b) { return a.value - b.value; }),
                roseType: 'radius',
                label: {
                    color: 'rgba(255, 255, 255, 0.3)'
                },
                labelLine: {
                    lineStyle: {
                        color: 'rgba(255, 255, 255, 0.3)'
                    },
                    smooth: 0.2,
                    length: 10,
                    length2: 20
                },
                itemStyle: {
                    color: 'rgba(10, 30, 45, 0.95)',
                    shadowBlur: 200,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                },

                animationType: 'scale',
                animationEasing: 'elasticOut',
                animationDelay: function (idx) {
                    return Math.random() * 200;
                }
            }
        ]
    };

    return (
        <div style={{ margin: '50px auto' }}>
            <Grid container spacing={5}>

                <Grid lg={4} md={4} sm={12} xs={12} item>

                    <SimpleCard title="Itineraries Based on Status">
                        <ReactEcharts
                            style={{ ['margin-top']: '-55px' }}
                            option={options_itineraries_piechart}
                        />
                    </SimpleCard>
                </Grid>

                <Grid lg={8} md={8} sm={12} xs={12} item>

                    <SimpleCard title="Itineraries List">
                        <small className="text-muted">Itineraries Based On Status</small>

                        <div className="mb-10 divider" />

                        <FlexRow>
                            <div className="counter-block-dashboard">
                                <div className="count" style={{ background: 'rgb(180, 50, 50)' }}>
                                    {itineraries.filter(b => b.status === 'CREATED').length}
                                </div>
                                <div className="item">Created</div>
                            </div>
                            <div className="counter-block-dashboard">
                                <div className="count" style={{ background: 'rgb(50, 50, 230)' }}>
                                    {itineraries.filter(b => b.status === 'UPDATED (NEW)').length}
                                </div>
                                <div className="item">Updated (New)</div>
                            </div>
                            <div className="counter-block-dashboard">
                                <div className="count" style={{ background: 'rgb(50, 200, 50)' }}>
                                    {itineraries.filter(b => b.status === 'APPROVED').length}
                                </div>
                                <div className="item">Approved</div>
                            </div>
                            <div className="counter-block-dashboard">
                                <div className="count" style={{ background: 'rgb(20, 50, 20)' }}>
                                    {itineraries.filter(b => b.status === 'PENDING').length}
                                </div>
                                <div className="item">Pending</div>
                            </div>
                            <div className="counter-block-dashboard">
                                <div className="count" style={{ background: 'rgb(180, 100, 10)' }}>
                                    {itineraries.filter(b => b.status === 'SALES').length}
                                </div>
                                <div className="item">Sales</div>
                            </div>
                            <div className="counter-block-dashboard">
                                <div className="count" style={{ background: 'rgb(180, 10, 10)' }}>
                                    {itineraries.filter(b => b.status === 'CANCELLED').length}
                                </div>
                                <div className="item">Cancelled</div>
                            </div>
                        </FlexRow>
                        <Divider clearing />

                        <Link to='/itinerary/builder' color='secondary'>view Itineraries</Link>
                    </SimpleCard>

                </Grid>
            </Grid>
        </div>
    );
};

export default CommonView;