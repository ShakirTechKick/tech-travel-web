import { EgretLoadable } from "egret";
import { authRoles } from "../../auth/authRoles";

const Dashboard = EgretLoadable({
  loader: () => import("./Dashboard")
});

const dashboardRoutes = [
  {
    name: "Dashboard",
    path: "/dashboard",
    exact: true,
    component: Dashboard,
    auth: authRoles.admin,
    access: 'COMMON'
  }
];

export default dashboardRoutes;
