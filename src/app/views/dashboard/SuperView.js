import React, { useState } from 'react';
import { SimpleCard, EgretProgressBar } from 'egret';
import { Grid } from '@material-ui/core';
import ReactEcharts from "echarts-for-react";
import FlexRow from 'app/helpers/FlexRow';
import { Link } from 'react-router-dom';
import { Divider, Dropdown } from 'semantic-ui-react';
import moment from 'moment';

const SuperView = ({ users }) => {


    const A_companies = users.filter(u => u.role === 'Admin' && u.status === 'A').length;
    const B_companies = users.filter(u => u.role === 'Admin' && u.status === 'B').length;


    const [year, setYear] = useState(moment(new Date()).format('YYYY'));


    const option = {
        xAxis: {
            type: 'category',
            name: 'Month',
            data: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
        },
        yAxis: {
            type: 'value',
            name: 'Sales',

        },
        series: [{
            data: [...['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'].map(m => {
                
                return users.filter(u => u.role === 'Admin' &&
                    moment(u.created_at).format('MMM') === m &&
                    moment(u.created_at).format('YYYY') == year).length

            })],
            type: 'bar',
            showBackground: true,
            backgroundStyle: {
                color: 'rgba(220, 220, 220, 0.8)'
            }
        }]
    };

    return (
        <div style={{ margin: '50px auto' }}>
            <Grid container spacing={5}>
                <Grid md={4} sm={12} xs={12} item>
                    <div>
                        <SimpleCard title="Companies List">
                            <div className="pt-8" />
                            <FlexRow>
                                <div>Active Companies</div>
                                <div>{A_companies}</div>
                            </FlexRow>
                            <div className="pt-8" />
                            <FlexRow>
                                <div>Non-Active Companies</div>
                                <div>{B_companies}</div>
                            </FlexRow>

                            <Divider clearing />

                            <Link to='/users' color='secondary'>view Companies</Link>
                        </SimpleCard>
                    </div>
                </Grid>


                <Grid md={8} sm={12} xs={12} item>
                    <SimpleCard title="Sales / Logins By Month">
                        <ReactEcharts
                            style={{ ['margin-top']: '-55px' }}
                            option={option}
                        />
                        <Divider clearing />
                        Year   <Dropdown
                            placeholder="Year"
                            value={parseInt(year)}
                            options={[-5, -4, -3, -2, -1, 0, 1, 2, 3, 4, 5].map(i => ({ value: parseInt(year) + i, text: parseInt(year) + i }))}
                            onChange={(e, { value }) => {
                                setYear(value)
                            }}
                        />
                    </SimpleCard>
                </Grid>
            </Grid>
        </div>
    );
};

export default SuperView;