import React from "react";
// import "../styles/_app.scss";

import { Provider } from "react-redux";
import { Router, Route } from "react-router-dom";
import EgretTheme from "./EgretLayout/EgretTheme/EgretTheme.jsx";
import AppContext from "./appContext";
import history from "history.js";
import 'styles/custom/custom.css';
import RootRoutes from "./RootRoutes";
import { Store } from "./redux/Store";
import Auth from "./auth/Auth";
import EgretLayout from "./EgretLayout/EgretLayout";
import AuthGuard from "./auth/AuthGuard";
import { ToastsStore, ToastsContainer } from "react-toasts";

const App = () => {
  return (
    <AppContext.Provider value={{ ...RootRoutes }}>
      <Provider store={Store}>
        <EgretTheme>
          <Auth>
            <Router history={history}>
              <AuthGuard>
                <EgretLayout />
              </AuthGuard>
            </Router>
          </Auth>
          <ToastsContainer store={ToastsStore} />
        </EgretTheme>
      </Provider>
    </AppContext.Provider>
  );
};

export default App;
